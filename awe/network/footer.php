<?php
function network_footer() {
	$sites = am_var('network-site-configs');
	$networkSite = am_var('network-configs');
	?>
<div id="network-in-footer" class="network-links container" style="padding: 20px;">
	<div class="row network-info">
		<div class="p-3 col-md-4 col-sm-12 align-bottom">
			<section class="text-center <?php echo 'site-' . $networkSite['safeName'] . '-network-bgd';?>"><?php echo '<h2 class="network-text">' . $networkSite['network-name'] . '</h2>'; ?></section>
		</div>
		<div class="p-3 col-md-8 col-sm-12">
			<?php if(!(am_var('is-network-site') && am_var('node') == 'index')) echo '<h4>' . $networkSite['network-byline'] . '</h4>'; ?>
			<?php echo '<p class="h5 mb-0">' . renderSingleLineMarkdown($networkSite['network-message'], ['echo' => false]) . '</p>'; ?>
		</div>
	</div>
	<div class="row"><?php
		foreach ($sites as $site) {
			$h3Class = ' site-' . $site['safeName'] . '-bgd';
			renderNetworkPanel($site, 'footer', ['h3-class' => $h3Class]);
		} ?>
	</div>
</div>
<?php } ?>
