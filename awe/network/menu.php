<?php
function network_after_menu() {
	$sites = am_var('network-site-configs');

	$thisSlug = basename(SITEPATH);
	$thisSite = $sites[$thisSlug];

	runFwk('theme');
	setMenuSettings();
	$settings = array_merge(am_var('site-menu-settings'), []);
	$class_li = arrayIfSetAndNotEmpty($settings, 'li-class');
	$class_active = arrayIfSetAndNotEmpty($settings, 'li-active-class', 'selected');
	$class_link = arrayIfSetAndNotEmpty($settings, 'a-class');
	$class_ul = arrayIfSetAndNotEmpty($settings, 'ul-class');

	$thisIcon = am_var('nl') . '		<img src="' . $thisSite['url'] . $thisSite['safeName'] . '-icon.png" class="menu-network-icon" style="height: 30px;" />' . am_var('nl') . '			';
	echo '<li' . cssClass(array_merge($class_li, ['drop-down', 'network-links'])) . '>' . am_var('nl') . '	<a class="site-button site-' . $thisSite['safeName'] . '-bgd">' . $thisIcon . $thisSite['name'] . '</a>' . am_var('nl')
		. '<ul' . cssClass($class_ul) . '>' . am_var('2nl');
	
	if (function_exists('awesome_network_menu')) awesome_network_menu();

	foreach ($sites as $site) {
		if ($thisSite == $site) continue;

		echo '	<li' . cssClass(array_merge($class_li, [' menu-item site-button site-' . $site['safeName'] . '-bgd'])) . '>';
		$icon = am_var('nl') . '		<img src="' . $site['url'] . $site['safeName'] . '-icon.png" class="menu-network-icon" style="height: 30px;" />' . am_var('nl') . '			';
		echo getLink($icon . $site['name'], $site['url']);

		echo '	<ul' . cssClass(array_merge($class_ul, ['site-' . $site['safeName'] . '-bgd'])) . '>' . am_var('nl');
		echo '		<li'. cssClass($class_li) . ' style="margin-left: 30px;">' . getLink('Home', $site['url'], cssClass($class_link)) . '</li>' . am_var('nl');
		foreach($site['vars']['sections'] as $section) {
			echo '		<li' . cssClass($class_li) . ' style="margin-left: 30px;">' . getLink(humanize($section), $site['url'] . $section . '/') . '</li>' . am_var('nl');
		}

		echo '	</ul>' . am_var('2nl');

		echo '</li>' . am_var('nl');
	}

	if (!am_var('no-engage')) {
		$engage = '		<img src="' . am_var('main') . 'amadeus-icon.png" class="menu-network-icon" style="height: 30px;" />' . am_var('nl') . '			';
		echo '<li><a href="javascript: void(0);" class="site-button btn-node toggle-engage engage-scroll" style="border-radius: 0;" data-engage-target="engage-amadeus">'
			. am_var('nl') . $engage . 'Engage With Us</a>';
	}

	echo '</ul></li>';
}
