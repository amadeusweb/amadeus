<?php
if (!isset($config)) { parameterError('Feature Config Missing', [$featureName], false); return; }

//only expects a list of file names
foreach ($config as $file) {
	disk_include_once($featurePath . $file . '.php');
}
