<?php
am_var('network-panels', [
	'divFormat' => '<div class="p-3 col-md-%s col-sm-12 align">' . am_var('nl'),
	'sectionFormat' => '	<section class="network-site%s text-center%s">' . am_var('nl'),
]);

function renderSides($group) {
	$sites = am_var('network-site-configs');
	foreach ($sites as $item) {
		if ($item['group'] != $group) continue;

		$class = ' site-' . $item['safeName'] . '-bgd';
		echo sprintf(am_sub_var('network-panels', 'sectionFormat'), $class . ' site-button', '');
		renderNetworkPanel($item, 'content', ['h3-class' => $class, 'side' => true]);
		echo '	</section>' . am_var('2nl');
	}
}

function renderCenter() {
	$sites = am_var('network-site-configs');
	$item = $sites[am_var('network')];
	
	renderNetworkPanel($item, 'content', ['in-center' => true, 'logo' =>  'assets/' . am_var('network-splash-image')]);
	//echo '<img src="' . $item['url'] . 'assets/' . am_var('network-splash-image') . '" class="img-fluid img-max-400" />' . am_var('2nl');
	echo '<div class="network-splash-name text-center">' . $item['network-byline'] . '</div>';
}

function renderNetworkPanel($site, $location, $settings) {
	$return =  valueIfSetAndNotEmpty($settings, 'return');
	$result = [];

	$link = $site['link'];
	if (valueIfSet($settings, 'in-center', false)) {
		$logo = '<img src="' . $site['url'] . $settings['logo'] . '" height="100" class="img-fluid img-max-400 content-network-logo" />';
		$result[] = '<div class="text-center">' . $logo . '</div>';
		return;
	}

	$side = valueIfSet($settings, 'side', false);
	$h3Class = valueIfSet($settings, 'h3-class', '');
	$bgdClass = valueIfSet($settings, 'bgd-class', '');

	if ($location == 'footer') {
		$result[] = sprintf(am_sub_var('network-panels', 'divFormat'), false ? '4' : '2');
		$result[] = sprintf(am_sub_var('network-panels', 'sectionFormat'), $bgdClass, ' h-100');
	}

	if ($location != 'content') {
		$icon = '<img src="' . $site['url'] . $site['safeName'] . '-icon.png" class="footer-network-icon" /><br />';
		$link = str_replace('">', '">' . $icon, $link);
	}

	$link = '<h3 class="site-button text-center' . $h3Class . '">' . $link . '</h3>';

	$result[] = $link . am_var('nl');
	if ($location == 'content') $result[] = '<small>' . $site['byline'] . '</small>';

	if ($location == 'footer') {
		$result[] = '<div class="site-sections">';
		$sections = $site['vars']['sections']; //doing nothing with groups for now!
		foreach ($sections as $item)
		$result[] = sprintf('<a href="%s%s">%s</a><br />' . am_var('nl'), $site['url'], $item . '/', humanize($item));
		$result[] = '</div>';

		if (isset($_GET['info'])) {
			$item = $site['item'];
			$valueIndex = $site['valueIndex'];
			ksort($item);
			foreach ($item as $key => $rows) {
				$val = $rows[0][$valueIndex];
				$result[] = '<hr />' . $key . ':<br />'  . $val;
			}
		}

		$result[] = '	</section>' . am_var('2nl');
		$result[] = '</div>' . am_var('2nl');
	}

	$result = implode(am_var('nl'), $result);
	if ($return) return $result;
	echo $result;
}
