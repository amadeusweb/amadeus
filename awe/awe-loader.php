<?php
//NOTE: $features has to be a key wtih an array!
function awesomeEngine($features) {
	am_var('awe-config', $features);
	foreach ($features as $name => $config) {
		_includeAweFeature($name, [
			'featureName' => humanize($name),
			'featurePath' => _awePath($name . '/'),
			'config' => $config
		]);
	}
}

function _includeAweFeature($name, $variables) {
	disk_include_once(_awePath($name . '/awe-' . $name . '.php'), $variables);
}

function _awePath($name) {
	return AMADEUSEXTENDED . $name;
}

function access_needed() {
	$usesAurra = am_sub_var('awe-config', 'aurra') != false;
	if (!$usesAurra) return false;

	return true;
}

//assumes from page too (the other types will only be called a few times by the framework)
function cannot_access_page($name) {
	return cannot_access($name, 'page', false, 'page');
}

function cannot_access($name, $what = 'section', $raise = false, $callingFrom = 'menu') {
	if (!access_needed()) return false;
	return _cannot_access($name, $what, $raise, $callingFrom);
}

function awesome_network_menu() {
	foreach (am_var('awe-config') as $feature => $config) {
		$fn = 'awe_' . $feature . '_menu';
		if (!function_exists($fn)) continue;
		$fn($config, $feature);
	}
}

function awesome_before_render() {
	foreach (am_var('awe-config') as $feature => $config) {
		$fn = 'awe_' . $feature . '_before_render';
		if (!function_exists($fn)) continue;
		$fn($config, $feature);
	}
}

function isAwesomeNode() {
	return !!am_var('awesome-node');
}

function renderedAwesome() {
	if (!isAwesomeNode()) return false;

	disk_include_once(am_var('file'));
	return true;
}
