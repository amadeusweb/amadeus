<?php
DEFINE('WORKSPACERELATIVEFOLDER', $config['folderRelative']);
DEFINE('WORKSPACEFOLDER', $config['folderAbsolute']);

function workspacePath($path, $useRelative = false) {
	return ($useRelative ? WORKSPACERELATIVEFOLDER : WORKSPACEFOLDER) . $path;
}
