/*
  Part of https://builder.amadeusweb.com/workspaces/

  View Code / History:
    https://bitbucket.org/amadeusweb/amadeus/src/master/awe/3p/google/folder-indexer.gs

  What this does:
    Lists all files and sub-folders from a folder in Google Drive.
    Looks for a "Drives to Scan" sheet to scan in multiple places, else scans activeSheet >> parent.

----------
  version 1.0:

  Adapted from Code written by @hubgit https://gist.github.com/hubgit/3755293
  Updated since DocsList is deprecated  https://ctrlq.org/code/19854-list-files-in-google-drive-folder
  Bluexm: added recursion on subfolders - SO: https://webapps.stackexchange.com/a/142584

  Imran <imran@amadeusweb.com> in Dec 2024 has
   * introduced "onlyFolders" for own use
   * done a full cleanup of all variables
   * uses ' » ' as folder separator
   * added removeEmptyRows/Columns from Trey (SO)
   * added .getParents()[0].getName() per SO: https://stackoverflow.com/a/17618407
   * Merged Files & Fols into one sheet and added level, indent
   * Decided whether to list subfolders first, not files.
   * Support for Drive Folder / Shared Drive (14th Dec)
   * Uses "Drives to Scan" which tells the name/type, and, if missing, will search SharedDrives to detect the type
   * Set status of row to ... while scanning and update the count (when sheet is present)

  TODO:
   * Move output to a teams.amadeusweb.com and use datatables / an auth database to show it.
*/

var sheet = SpreadsheetApp.getActiveSheet(),
    sheetFile = SpreadsheetApp.getActiveSpreadsheet(),
    namesSheet = sheetFile.getSheetByName('Drives to Scan')

var isSharedDrive = false,
    isTopFolder = false,
    globalSerialNo = 0,
    topFolderName = '',
    topFolderId = ''

function ScanAllDrives() {
  if (sheet == null) {
    Logger.log('No Active Sheet')
    return
  }

  Logger.log('Detected Sheet: ' + sheetFile.getName())
  var parents = []

  if (namesSheet != null) {
    rows = namesSheet.getRange(2, 1, namesSheet.getLastRow() - 1, 2).getValues()
    rows.forEach(function(item) { parents.push({name: item[0], type: item[1]}) })
    sheetFile.setActiveSheet(namesSheet)
  } else {
    var parentType = 'folder', parentName = ''

    try {
      var sharedDrive = Drive.Files.get(sheetFile.getId()).parents.pop()
      topFolderId = sharedDrive.id
      parentType = 'shared'
    } catch {
      var parentFolder = DriveApp.getFileById(sheetFile.getId()).getParents().next();
      topFolderId = parentFolder.getId()
      parentName = parentFolder.getName()
    }

    parents.push({name: parentName, type: parentType})
  }

  globalSerialNo = 0
  parents.forEach(ScanDrive)
}

function ScanDrive(parent, nameIndex) {
  if (parent.name.substring(0, 1) == '|') {
    Logger.log('Skipping: ' + parent.name)
    return
  }

  setCount('...', nameIndex)

  isSharedDrive = parent.type == 'shared'
  topFolderName = parent.name

  if(isSharedDrive)
    topFolderId = getSharedDrive(topFolderName).id

  Logger.log('Scanning ' + (isSharedDrive ? 'Shared' : 'Folder in ') + ' Drive: ' + topFolderName)

  sheet = sheetFile.getSheetByName(parent.name)
  if (sheet == null) {
    sheet = sheetFile.insertSheet(parent.name)
  }

  ScanFolder(topFolderName, '', 0, '')
  removeEmptyColumns()
  removeEmptyRows()
  setCount(sheet.getLastRow() - 1, nameIndex)
}

function ScanFolder(folderName, relativeFolderName, level, indent) {
  isTopFolder = topFolderName == folderName
  if (isTopFolder) {
    sheet.clearContents()

    sheet.appendRow([
      "#sno",
      "_level",
      "_type",
      "_indent",
      "name",
      "parent",
      "_drive-link",
      "_description",
      //ignored
      "date-last-updated",
      "size",
      "full-parent",
    ])

    Logger.log("REBUILDING SHEET In: " + sheetFile.getName())
  }

  Logger.log("SCANNING: " + folderName)

  var relativeFolder = folderName + (isTopFolder ? '' : ' « ' + relativeFolderName)

  var folder = isTopFolder && isSharedDrive
    ? DriveApp.getFolderById(topFolderId)
    : DriveApp.getFoldersByName(folderName).next()

  var subFolders = getFoldersOf(folder)

  var folderIndex = 0
  while (folderIndex < subFolders.length) {
    var item = subFolders[folderIndex]
    folderIndex += 1
    globalSerialNo += 1

    var name = item.getName()
    var skipChildren = name.endsWith('__')
    if (skipChildren) name = name.replace('__', '')

    var data = [
      globalSerialNo,
      level,
      'folder',
      indent + '/ ',
      name,
      folderName,
      item.getUrl(),
      item.getDescription(),
      isTopFolder && isSharedDrive ? item.ModifiedTimeRaw : item.getLastUpdated(),
      item.getSize(),
      relativeFolder,
    ]

    sheet.appendRow(data)

    if (!skipChildren)
      ScanFolder(name, folderName + ' « ' + relativeFolder, level + 1, indent + '  ')
  }

  var files = getFilesOf(folder)

  var fileIndex = 0
  while (fileIndex < files.length) {
    var item = files[fileIndex]
    fileIndex += 1
    globalSerialNo += 1

    var data = [
      globalSerialNo,
      level,
      'file',
      indent + '/ ',
      item.getName(),
      folderName,
      isTopFolder && isSharedDrive ? item.WebViewLink : item.getUrl(),
      item.getDescription(),
      isTopFolder && isSharedDrive ? item.ModifiedTimeRaw : item.getLastUpdated(),
      item.getSize(),
      relativeFolder,
    ]

    sheet.appendRow(data)
  }
}

function sortAscending(item1, item2) {
  var a = item1.getName(), b = item2.getName()
  return a > b ? 1 : (a < b ? -1 : 0)
}

function getFoldersOf(folder) {
  var result = []

  var subFolders = folder.getFolders();
  while (subFolders.hasNext())
    result.push(subFolders.next())
  
  result.sort(sortAscending)
  return result
}

function getFilesOf(folder) {
  if (isTopFolder && isSharedDrive) {
    return Drive.Files.list({driveId: topFolderId, corpora: "drive",
      includeItemsFromAllDrives: true, supportsAllDrives: true}).files
  }
  
  var result = []
  var files = folder.getFiles()

  while (files.hasNext())
    result.push(files.next())

  result.sort(sortAscending)
  return result;
}

function getSharedDrive(name) {
  return Drive.Drives.list({q: 'name = "' + name + '"', supportsAllDrives: true }).drives.pop()
}

function setCount(to, row) {
  if (namesSheet == null) return
  namesSheet.getRange(row + 2, 4).setValue(to)
}

//NOT USED: https://arisazhar.com/remove-empty-rows-in-spreadsheet-instantly/

//FROM: https://stackoverflow.com/a/34781833
//UPDATE: Use Global Sheet Variable
//Remove All Empty Columns in the Current Sheet
function removeEmptyColumns() {
  var maxColumns = sheet.getMaxColumns()
  var lastColumn = sheet.getLastColumn()
  if (maxColumns - lastColumn != 0) {
    sheet.deleteColumns(lastColumn + 1, maxColumns - lastColumn)
  }
}

//Remove All Empty Rows in the Current Sheet
function removeEmptyRows() {
  var maxRows = sheet.getMaxRows()
  var lastRow = sheet.getLastRow()
  if (maxRows - lastRow != 0) {
    sheet.deleteRows(lastRow + 1, maxRows - lastRow)
  }
}

