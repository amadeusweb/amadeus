<?php
$config = am_sub_var('awe-config', 'aurra');

$local = am_var('local');

$userCode = _aurra_getUserCode();
$user = _aurra_getUser($userCode);

if ($local || $user['role'] == 'admin') {
	sectionId('admin');
	h2('Manage Users [Admin]');
		menu('/', [
		'files' => skipExcludedFiles(disk_scandir($config['folderAbsolute'] . '/users/'), [], true),
		'parent-slug' => 'account/manage-user/',
		'innerHtml' => function($userCode, $params) {
			//$params: extension, url, folder, isSpecial

			$user = _aurra_getUser($userCode);

			$output = [getLink(humanize($userCode), $params['url'])];
			$output[] = '<b>ROLES IN:</b>';
			$output[] = _aurra_getRoleIn($userCode, 'network');

			$sites = _aurra_getRestrictedSites();
			foreach ($sites as $site)
				$output[] = _aurra_getRoleIn($userCode, $site);

			$output = implode(am_var('brnl') . '<span style="white-space: pre;">	</span>', $output);
			//if (am_var('local')) $output .= '<hr /><pre>' . print_r($user, 1) . '</pre>';

			return $output;
		},
	]);
	section('end');
}
