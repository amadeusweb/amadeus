<?php
//list where all registration is possible, and use icon where its not.
//if user visits the link, it will ask for signup / additional access request
function awe_aurra_menu($config, $name) {
	extract(_aurra_getVars());
	$sites = am_var('network-site-configs');
	$loggedIn = $loggedIn || isset($_GET['in']);

	echo '<li class="drop-down menu-item site-acccount-bgd site-button">'
		. makeLink(getThemeIcon($loggedIn ? 'user' : 'ui-block') . ' My Account', $networkSite['url'] . 'account/');

	echo '	<ul class="site-acccount-bgd">' . am_var('nl');

	foreach ($allRestricted as $id => $pages) {
		am_var('iterating-site', $sites[$id]); //relying on fact that menu will do it first
		$site = $sites[$id];

		//will automatically ask for a signup!
		foreach ($pages as $page => $when) {
			if (!$loggedIn && !isset($when['show-to-anonymous'])) continue;

			$can = isset($_GET['in']);
			$can = !cannot_access($page, 'page', false, 'network-menu');
			$text = humanize(implode(' &mdash; ', explode('/', $page)));
			
			echo '<li class="menu-item site-' . $site['safeName'] . '-bgd">'
				. getLink(getThemeIcon($can ? 'tick-boxed' : 'tasks') . $text, $site['url'] . $page . '/') . '</li>';
		}

		echo '</li>';
	}
	am_var('iterating-site', false);
	echo '</li></ul></li>';

	$user = _aurra_getUser($userCode);
}
