<?php
if (!isset($config)) { parameterError('Feature Config Missing', [$featureName], false); return; }

function awe_aurra_before_render($config, $name) {
	disk_include_once(_awePath($name . '/menu.php'));
	$networkName = $config['network-name'];
	if (am_var('node') == 'account') {
		am_var('awesome-node', true);
		am_var('file', _awePath('/aurra/home.php'));
	}
}

function _aurra_getVars() {
	$key = 'aurra-working-vars'; //cache it
	if ($result = am_var($key)) return $result;

	$sites = am_var('network-site-configs');

	$userCode = _aurra_getUserCode();
	$loggedIn = _aurra_userIsUnknown($userCode);

	$networkSite = $sites[am_var('network')];
	$thisSlug = basename(SITEPATH);

	$allRestricted = _aurra_getPagesInRestrictedSites();
	$thisRestricted = isset($allRestricted[$thisSlug]) ? $allRestricted[$thisSlug] : [];

	$result = compact('userCode', 'loggedIn', 'thisSlug', 'networkSite', 'allRestricted', 'thisRestricted');
	am_var($key, $result);
	return $result;
}

function _cannot_access($name, $what = 'section', $raise = false, $callingFrom = 'menu') {
	//FOR NOW, assuming sections and nodes will always be visible
		//TODO: BASE ON WS/auth/protection.tsv which has, as columns
		//#name	what	roles	groups	people
	if ($what == 'node' && $name == 'ME') {
		$name = am_var('node');
	} else if ($what == 'page' && $name == 'ME') {
		$name = am_var('node') . '/' . am_var('page_parameter1');
	}

	$cannot = false; //allows all
	//$cannot = true; //blocks all
	//if (contains($name, 'team')) parameterError('ACCESS', [$name], false);
	//$cannot = in_array($name, ['team', 'awe', 'synergy-kares/presentations']); //team -> com, awe -> web

	if ($what == 'page') {
		$vars = _aurra_getVars();
		$loggedIn = $vars['loggedIn'];
		$pages = am_var_or('iterating-site', $vars['thisRestricted']);
		if (!isset($pages[$name])) {
			$cannot = false;
		} else {
			$when = $pages[$name];
			if (!$loggedIn) {
				$anon = isset($when['show-to-anonymous']);
				if ($callingFrom == 'menu')
					$cannot = !$anon;
				else
					$cannot = true;
			}
		}
	}

	if ($cannot && $raise) {
		parameterError('No access to ' . $what . ': '. $name, [], false);
	}

	return $cannot;
}

DEFINE('UNKNOWNUSER', 'unknown');

function _aurra_userIsUnknown($userCode) {
	return $userCode == UNKNOWNUSER;
}

function _aurra_getUserCode() {
	$config = am_sub_var('awe-config', 'aurra');
	$userCode = UNKNOWNUSER; //read from session!
	if (disk_file_exists($userName = $config['folderAbsolute'] . '/_current-user.txt'))
		$userCode = disk_file_get_contents($userName);
	return $userCode;
}

function _aurra_getUser($userCode) {
	$config = am_sub_var('awe-config', 'aurra');
	$userJson = $config['folderAbsolute'] . 'users/' . $userCode . '.json';
	$user = ['name' => 'Blue Oracle', 'role' => 'anonymous'];
	return json_decode(disk_file_get_contents($userJson));
}

function _aurra_getRoleIn($userCode, $where) {
	$config = am_sub_var('awe-config', 'aurra');
	$tsv = $config['folderAbsolute'] . $where . '-users.tsv';

	$where = '<u>' . $where . '</u>';
	if (!has_sheet($tsv))
		return $where . ': missing user list (' . $tsv . ')';

	$usersByCode = get_sheet($tsv, 'usercode');
	if (!isset($usersByCode->sections[$userCode]))
		return $where . ': not defined in (' . $tsv . ')';

	$user = $usersByCode->sections[$userCode][0];
	$cols = $usersByCode->columns;
	return $where . ': <b>' . $user[$cols['role']] . ', groups: ' . $user[$cols['groups']] . '</b>';
}

function _aurra_getPagesInRestrictedSites() {
	$config = am_sub_var('awe-config', 'aurra');
	$sites = $config['folderAbsolute'] . '/sites-config.php';
	$siteConfigs = disk_include($sites);
	return $siteConfigs['restricted-pages'];
}

function _aurra_getRestrictedSites() {
	$config = am_sub_var('awe-config', 'aurra');
	$sites = $config['folderAbsolute'] . '/sites-config.php';
	$siteConfigs = disk_include($sites);
	return $siteConfigs['restricted'];
}
