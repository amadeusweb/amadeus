<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@400;600;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/dark.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/one-page/css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Theme Color Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/colors.php?color=FF8600" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/demos/news/css/fonts.css" type="text/css" />

	<!-- News Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/demos/news/news.css" type="text/css" />
	<!-- / -->

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php echo $theme; ?>assets/include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme; ?>assets/include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $theme; ?>assets/include/rs-plugin/css/navigation.css">

	<!-- Document Title
	============================================= -->
	<title><?php title(); ?></title>
<?php seo_tags(); ?>
<link href="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-icon.png" rel="icon" />
	<style>
		/* Revolution Slider Styles */
		.hesperiden .tp-tab { border-bottom: 0; }
		.hesperiden .tp-tab:hover,
		.hesperiden .tp-tab.selected { background-color: #E5E5E5; }

	</style>
	<?php head_hooks(); ?>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="header-size-sm" data-sticky-shrink="false">
			<div class="container">
				<div class="header-row justify-content-between">

					<!-- Logo
					============================================= -->
					<div id="logo" class="col-auto ms-auto ms-mb-0 me-mb-0 order-md-2">
						<a href="<?php echo am_var('url');?>" class="standard-logo"><img class="mx-auto" src="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo.png" alt="<?php echo am_var('name'); ?>"></a>
						<a href="<?php echo am_var('url');?>" class="retina-logo"><img class="mx-auto" src="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo@2x.png" alt="<?php echo am_var('name'); ?>"></a>
					</div><!-- #logo end -->

					<div class="w-100 d-block d-md-none"></div>

					<div class="col-12 col-sm-6 col-md-4 justify-content-center justify-content-sm-start d-flex order-md-1 mb-4 mb-sm-0">
						<a href="https://facebook.com/semiColonWeb" class="social-icon si-small si-rounded si-dark si-mini si-facebook mb-0">
							<i class="icon-facebook"></i>
							<i class="icon-facebook"></i>
						</a>
						<a href="https://twitter.com/__semicolon" class="social-icon si-small si-rounded si-dark si-mini si-twitter mb-0">
							<i class="icon-twitter"></i>
							<i class="icon-twitter"></i>
						</a>
						<a href="https://instagram.com/semicolonweb" class="social-icon si-small si-rounded si-dark si-mini si-instagram mb-0">
							<i class="icon-instagram"></i>
							<i class="icon-instagram"></i>
						</a>
					</div>

					<div class="col-12 col-sm-6 col-md-4 order-md-3 mb-4 mb-md-0">
						<ul class="nav align-items-center justify-content-center justify-content-sm-end">
							<li class="nav-item">
								<a class="nav-link text-uppercase fw-medium" href="#">Sign In</a>
							</li>
							<li class="nav-item">
								<div class="date-today text-uppercase badge bg-dark rounded-pill py-2 px-3 fw-medium"></div>
							</li>
						</ul>
					</div>

				</div>
			</div>

			<div id="header-wrap" class="border-top border-f5">
				<div class="container">
					<div class="header-row justify-content-between flex-row-reverse flex-lg-row">

						<div class="header-misc">

							<!-- Bookmark
							============================================= -->
							<div class="header-misc-icon">
								<a href="#"><i class="icon-bookmark-empty"></i></a>
							</div>

							<!-- Top Search
							============================================= -->
							<div id="top-search" class="header-misc-icon">
								<a href="#" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
							</div><!-- #top-search end -->

						</div>

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu with-arrows">
							<?php if (am_var('node') == '_demo') include "menu.html"; else if (file_exists(am_var('path') . '/code/menu.php')) include_once am_var('path') . '/code/menu.php'; else menu(); ?>

						</nav><!-- #primary-menu end -->

						<form class="top-search-form" action="search.html" method="get">
							<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
						</form>

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->