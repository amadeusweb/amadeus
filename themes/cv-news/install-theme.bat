@echo off
IF EXIST "cv-news.zip" (
	echo "zip found"
) ELSE (
	powershell -Command "Invoke-WebRequest https://amadeusweb.com/themes/downloads/cv-news.zip -OutFile cv-news.zip"
)
IF EXIST "assets" (
	echo "assets folder found"
) ELSE (
	tar -xzvf ./cv-news.zip
	echo "theme folder extracted"
)
pause