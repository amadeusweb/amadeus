		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark" style="background-color: #1f2024;">

			<div class="container">
				<?php if (!run_site_code('footer-content')) { ?>
				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap row clearfix">

					<div class="col-lg-4 col-sm-6 mb-5 mb-lg-0">
						<!-- Footer Widget 1
						============================================= -->
						<div class="widget clearfix">
							<h4 class="mb-3 mb-sm-4">Recent Posts</h4>
							<div class="posts-sm row col-mb-30" id="post-list-footer">
								<div class="entry col-12">
									<div class="grid-inner row align-items-center g-0">
										<div class="col-auto">
											<div class="entry-image">
												<a href="demo-news-single.html"><img src="<?php echo $theme; ?>assets/demos/news/images/posts/travel/small/1.jpg" alt="Image"></a>
											</div>
										</div>
										<div class="col ps-3">
											<div class="entry-title">
												<h4 class="fw-semibold"><a href="demo-news-single.html" class="text-white">UK government weighs Tesla's Model.</a></h4>
											</div>
											<div class="entry-meta">
												<ul>
													<li><span>by</span> <a href="#">John Doe</a></li>
													<li><i class="icon-time"></i><a href="#">11 Mar 2021</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>

								<div class="entry col-12">
									<div class="grid-inner row align-items-center g-0">
										<div class="col-auto">
											<div class="entry-image">
												<a href="demo-news-single.html"><img src="<?php echo $theme; ?>assets/demos/news/images/posts/sports/small/2.jpg" alt="Image"></a>
											</div>
										</div>
										<div class="col ps-3">
											<div class="entry-title">
												<h4 class="fw-semibold"><a href="demo-news-single.html" class="text-white">UK government weighs Tesla's Model.</a></h4>
											</div>
											<div class="entry-meta">
												<ul>
													<li><span>by</span> <a href="#">John Doe</a></li>
													<li><i class="icon-time"></i><a href="#">11 Mar 2021</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>

								<div class="entry col-12">
									<div class="grid-inner row align-items-center g-0">
										<div class="col-auto">
											<div class="entry-image">
												<a href="demo-news-single.html"><img src="<?php echo $theme; ?>assets/demos/news/images/posts/market/small/3.jpg" alt="Image"></a>
											</div>
										</div>
										<div class="col ps-3">
											<div class="entry-title">
												<h4 class="fw-semibold"><a href="demo-news-single.html" class="text-white">Why market Is No Friend To Small Business</a></h4>
											</div>
											<div class="entry-meta">
												<ul>
													<li><span>by</span> <a href="#">John Doe</a></li>
													<li><i class="icon-time"></i><a href="#">11 Mar 2021</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Footer Widget 2
						============================================= -->
					<div class="col-lg-2 col-sm-6 mb-5 mb-lg-0">
						<h4 class="mb-3 mb-sm-4">Tag Cloud</h4>
						<div class="tagcloud">
							<a href="#">general</a>
							<a href="#">videos</a>
							<a href="#">music</a>
							<a href="#">media</a>
							<a href="#">photography</a>
							<a href="#">parallax</a>
							<a href="#">ecommerce</a>
							<a href="#">terms</a>
							<a href="#">coupons</a>
							<a href="#">modern</a>
							<a href="#">magazine</a>
							<a href="#">bootstrap</a>
							<a href="#">news</a>
							<a href="#">blog</a>
							<a href="#">wordpress</a>
						</div>
					</div>

					<!-- Footer Widget 3
					============================================= -->
					<div class="col-lg-3 col-sm-6 mb-5 mb-sm-0">
						<div class="widget widget_links clearfix">
							<h4 class="mb-3 mb-sm-4">Blogroll</h4>
							<ul>
								<li><a href="https://codex.wordpress.org/">Documentation</a></li>
								<li><a href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a></li>
								<li><a href="https://wordpress.org/extend/plugins/">Plugins</a></li>
								<li><a href="https://wordpress.org/support/">Support Forums</a></li>
								<li><a href="https://wordpress.org/extend/themes/">Themes</a></li>
								<li><a href="https://wordpress.org/news/">Canvas Blog</a></li>
								<li><a href="https://planet.wordpress.org/">Customer Reviews</a></li>
								<li><a href="https://planet.wordpress.org/">Get Licence</a></li>
							</ul>
						</div>
					</div>

					<!-- Footer Widget 4
					============================================= -->
					<div class="col-lg-3 col-sm-6 mb-0">
						<div class="widget widget_links clearfix">
							<h4 class="mb-3 mb-sm-4">Download in Mobile</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus beatae esse iure est, quam libero!</p>
							<a href="#" class="button button-light text-dark w-100 text-center bg-white nott ls0 button-rounded button-xlarge ms-0"><i class="icon-apple"></i>App Store</a>
							<a href="#" class="button button-light text-dark w-100 text-center bg-white nott ls0 button-rounded button-xlarge ms-0"><i class="icon-googleplay"></i>Google Play</a>
						</div>
					</div>

				</div>
				<?php } ?>
			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="row justify-content-center">
						<div class="col-md-6 align-self-center">
							<?php _copyright(); ?><br>
							<?php if (am_var('footer_post_credits')) { echo '<div class="copyright-links">' . replace_vars(am_var('footer_post_credits')) . '</div>'; } ?>
							</div>

						<div class="col-md-6 align-self-center">
							<div class="copyrights-menu float-end copyright-links m-0 clearfix">
								<a href="#">Home</a>/<a href="#">About</a>/<a href="#">Features</a>/<a href="#">Portfolio</a>/<a href="#">FAQs</a>/<a href="#">Contact</a>
							</div>
						</div>
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="<?php echo $theme; ?>assets/js/jquery.js"></script>
	<script src="<?php echo $theme; ?>assets/js/plugins.min.js"></script>
	<script src="<?php echo $theme; ?>assets/js/plugins.infinitescroll.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?php echo $theme; ?>assets/js/functions.js"></script>

	<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.carousel.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script src="<?php echo $theme; ?>assets/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
	<?php styles_and_scripts(); ?>
	<!-- ADD-ONS JS FILES -->
	<script>
		var tpj=jQuery;
		var revapi19;
		var $ = jQuery.noConflict();
		tpj(document).ready(function() {
			if(tpj("#rev_slider_19_1").revolution == undefined){
				revslider_showDoubleJqueryError("#rev_slider_19_1");
			}else{
				revapi19 = tpj("#rev_slider_19_1").show().revolution({
					sliderType:"carousel",
					jsFileLocation: "include/rs-plugin/js/",
					sliderLayout:"fullwidth",
					dottedOverlay:"none",
					delay:7000,
					navigation: {
						keyboardNavigation:"off",
						keyboard_direction: "horizontal",
						mouseScrollNavigation:"off",
						onHoverStop:"on",
						tabs: {
							style:"hesperiden",
							enable:true,
							width:260,
							height:80,
							min_width:260,
							wrapper_padding:25,
							wrapper_color:"#F5F5F5",
							wrapper_opacity:"1",
							tmp:'<div class="tp-tab-content">  <span class="tp-tab-date">{{param1}}</span>  <span class="tp-tab-title font-secondary">{{title}}</span> <span class="tp-tab-date tp-tab-para">{{param2}}</span></div><div class="tp-tab-image"></div>',
							visibleAmount: 9,
							hide_onmobile: false,
							hide_under:480,
							hide_onleave:false,
							hide_delay:200,
							direction:"horizontal",
							span:true,
							position:"outer-bottom",
							space:0,
							h_align:"left",
							v_align:"bottom",
							h_offset:0,
							v_offset:0
						}
					},
					carousel: {
						horizontal_align: "center",
						vertical_align: "center",
						fadeout: "on",
						vary_fade: "on",
						maxVisibleItems: 3,
						infinity: "on",
						space: 0,
						stretch: "off",
						showLayersAllTime: "off",
						easing: "Power3.easeInOut",
						speed: "800"
					},
					responsiveLevels:[1140,992,768,576],
					visibilityLevels:[1140,992,768,576],
					gridwidth:[850,700,400,300],
					gridheight:[580,600,500,400],
					lazyType:"single",
					shadow:0,
					spinner:"off",
					stopLoop:"on",
					stopAfterLoops:-1,
					stopAtSlide:-1,
					shuffle:"off",
					autoHeight:"off",
					disableProgressBar:"off",
					hideThumbsOnMobile:"off",
					hideSliderAtLimit:0,
					hideCaptionAtLimit:0,
					hideAllCaptionAtLilmit:0,
					debugMode:false,
					fallbacks: {
						simplifyAll:"off",
						nextSlideOnWindowFocus:"off",
						disableFocusListener:false,
					}
				});
			}
		});	/* Revolution Slider End */

		// Navbar on hover
		$('.nav.tab-hover a.nav-link').hover(function() {
			$(this).tab('show');
		});

		// Current Date
		var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
			month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			a = new Date();

			jQuery('.date-today').html( weekday[a.getDay()] + ', ' + month[a.getMonth()] + ' ' + a.getDate() );

		// Infinity Scroll
		jQuery(window).on( 'load', function(){

			var $container = $('.infinity-wrapper');

			$container.infiniteScroll({
				path: '.load-next-portfolio',
				button: '.load-next-portfolio',
				scrollThreshold: false,
				history: false,
				status: '.page-load-status'
			});

			$container.on( 'load.infiniteScroll', function( event, response, path ) {
				var $items = $( response ).find('.infinity-loader');
				// append items after images loaded
				$items.imagesLoaded( function() {
					$container.append( $items );
					$container.isotope( 'insert', $items );
					setTimeout( function(){
						SEMICOLON.widget.loadFlexSlider();
					}, 1000 );
				});
			});

		});

		$(window).on( 'pluginCarouselReady', function(){
			$('#oc-news').owlCarousel({
				items: 1,
				margin: 20,
				dots: false,
				nav: true,
				navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
				responsive:{
					0:{ items: 1,dots: true, },
					576:{ items: 1,dots: true },
					768:{ items: 2,dots:true },
					992:{ items: 2 },
					1200:{ items: 3 }
				}
			});
		});

	</script>

</body>
</html>

