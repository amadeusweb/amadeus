<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php title(); ?></title>
	<?php seo_tags(); ?>
	<link href="<?php echo am_var('url'); ?>icon-<?php echo am_var('safeName'); ?>.png" rel="icon">
	<!--NB: Purchased for Amadeus by services@cselian.com from - http://themes.semicolonweb.com/html/canvas/demo-beauty-kit.html -->

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Merriweather:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/dark.css" type="text/css" />
	<!-- / -->

	<link rel="stylesheet" href="<?php echo $theme; ?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>one-page/css/et-line.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/colors.php?color=222E2B" type="text/css" />

	<!-- Beauty Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/beauty-kit/beauty-kit.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/beauty-kit/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/spa/css/fonts/spa-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php echo $theme; ?>include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme; ?>include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $theme; ?>include/rs-plugin/css/navigation.css">

	<!-- Document Title
	============================================= -->
	<?php head_hooks(); ?>

</head>

<body class="stretched side-header">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="<?php echo uses('light-theme') ? '' : 'dark bg-color'; ?>">
			<div id="header-wrap">
				<div class="container">

					<div class="header-row justify-content-lg-between">

						<!-- Logo
						============================================= -->
						<div id="logo">
							<a href="<?php echo am_var('url');?>" class="standard-logo"><img src="<?php echo am_var('url'); ?>logo-<?php echo am_var('safeName') . am_var('safeFolder'); ?>.png" alt="<?php echo am_var('name'); ?>" class="img-fluid"></a>
						</div><!-- #logo end -->

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<div class="header-misc">
							<div class="d-none d-md-flex mb-lg-5">
								<?php if (has_content('Facebook Link')) { ?><a href="<?php content('Facebook Link'); ?>" target="_blank" class="social-icon si-facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a><?php } ?>

								<?php if (has_content('Twitter Link')) { ?><a href="<?php content('Twitter Link'); ?>" target="_blank" class="social-icon si-twitter">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
								</a><?php } ?>

								<?php if (has_content('Instagram Link')) { ?><a href="<?php content('Instagram Link'); ?>" target="_blank" class="social-icon si-instagram">
									<i class="icon-instagram"></i>
									<i class="icon-instagram"></i>
								</a><?php } ?>

								<?php if (has_content('YouTube Link')) { ?><a href="<?php content('YouTube Link'); ?>" target="_blank" class="social-icon si-youtube">
									<i class="icon-youtube"></i>
									<i class="icon-youtube"></i>
								</a><?php } ?>
							</div>

							<!--
							<a href="<?php echo $theme; ?>#" data-scrollto="#price" class="button button-circle font-weight-normal d-lg-none" data-offset="85" data-easing="easeInOutExpo" data-speed="1250"><span class="d-block d-md-none"><i class="icon-line-bag text-larger w-auto m-0"></i></span><span class="d-none d-md-block">Buy Our Pack <i class="icon-caret-right m-0 ls0"></i></span></a>
							-->
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu">

							<ul class="one-page-menu menu-container" data-easing="easeInOutExpo" data-speed="1250" data-offset="0">
								<?php if (!runCode('menu')) menu(); ?>
							</ul>

						</nav><!-- #primary-menu end -->

					</div>

				</div>

			</div>

		</header><!-- #header end -->
