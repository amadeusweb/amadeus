		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element dark swiper_wrapper slider-parallax min-vh-75">
			<div class="slider-inner">

				<div class="swiper-container swiper-parent">
					<div class="swiper-wrapper"><?php foreach (am_var('slider-data') as $item) {?>
						<div class="swiper-slide dark">
							<div class="container">
								<div class="slider-caption">
									<div>
										<h2 class="nott" data-animate="fadeInUp"><?php echo $item['h2']; ?></h2>
										<a href="<?php echo am_var('url') . $item['link']; ?>" data-animate="fadeInUp" data-delay="400" class="button button-rounded button-large button-light shadow nott ls0 ms-0 mt-4"><?php echo $item['linkText']; ?></a>
									</div>
								</div>
							</div>
							<div class="swiper-slide-bg" style="background: linear-gradient(rgba(0,0,0,.3), rgba(0,0,0,.5)), url('<?php echo am_var('url') . 'assets/' . $item['img']; ?>') no-repeat center center; background-size: cover;"></div>
						</div><?php } ?>
					</div>
					<div class="swiper-navs">
						<div class="slider-arrow-left"><i class="icon-line-arrow-left"></i></div>
						<div class="slider-arrow-right"><i class="icon-line-arrow-right"></i></div>
					</div>
					<div class="swiper-scrollbar">
						<div class="swiper-scrollbar-drag">
						<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div></div>
					</div>
				</div>

			</div>
		</section>
