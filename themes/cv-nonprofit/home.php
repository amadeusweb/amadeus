<?php if (!isset($home)) $home = am_var('home'); $theme = am_var('theme_url'); ?>
<div class="content-wrap py-0" style="overflow: visible">

<div class="container">

	<!-- Slider
	============================================= -->
	<section id="slider" class="slider-element dark swiper_wrapper slider-parallax min-vh-75">
		<div class="slider-inner">

			<div class="swiper-container swiper-parent">
				<div class="swiper-wrapper"><?php foreach ($home->sections['slider'] as $item) {?>
					<div class="swiper-slide dark">
						<div class="container">
							<div class="slider-caption">
								<div>
									<h2 class="nott" data-animate="fadeInUp"><?php item_r('title', $item); ?></h2>
									<a href="<?php item_r('link', $item); ?>" data-animate="fadeInUp" data-delay="400" class="button button-rounded button-large button-light shadow nott ls0 ms-0 mt-4"><?php item_r('linkText', $item); ?></a>
								</div>
							</div>
						</div>
						<div class="swiper-slide-bg" style="background: linear-gradient(rgba(0,0,0,.3), rgba(0,0,0,.5)), url('<?php echo am_var('url') . 'assets/slider/'; ?><?php item_r('img', $item); ?>') no-repeat center center; background-size: cover;"></div>
					</div><?php } ?>
				</div>
				<div class="swiper-navs">
					<div class="slider-arrow-left"><i class="icon-line-arrow-left"></i></div>
					<div class="slider-arrow-right"><i class="icon-line-arrow-right"></i></div>
				</div>
				<div class="swiper-scrollbar">
					<div class="swiper-scrollbar-drag">
					<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div></div>
				</div>
			</div>

		</div>
	</section>

	<div class="slider-feature w-100">
		<div class="row justify-content-center"><?php foreach ($home->sections['cards'] as $item) {?>
			<div class="col-md-3 px-1">
				<a href="<?php item_r('link', $item); ?>" class="card center border-start-0 border-end-0 border-top-0 border-bottom border-bottom shadow py-3 rounded-0 fw-semibold text-uppercase ls1">
					<div class="card-body">
						<i class="icon-line-align-center"></i><?php item_r('linkText', $item); ?>
					</div>
				</a>
			</div><?php } ?>
		</div>
	</div>

</div>


<div class="section mt-3" style="background: #FFF url('<?php echo am_var('url'); ?>assets/home/help.jpg') no-repeat 100% 50% / auto 100%;">
	<div class="container"><?php foreach ($home->sections['help'] as $item) {?>
		<div class="row justify-content-center">
			<div class="col-md-7 center">
				<div class="heading-block border-bottom-0 mb-4">
					<h2 class="mb-4 nott"><?php item_r('title', $item); ?></h2>
				</div>
				<div class="svg-line bottommargin-sm">
					<img src="<?php echo $theme; ?>demos/nonprofit/images/divider-1.svg" alt="svg divider" height="20">
				</div>
				<?php if (item_r('content', $item, true)) {?><p><?php item_r('content', $item); ?></p><?php } ?>
			</div>
		</div><?php } ?>

		<div class="row mt-5 col-mb-50 mb-0"><?php foreach ($home->sections['features'] as $item) {?>
			<div class="col-md-3">
				<div class="feature-box flex-column mx-0">
					<div class="fbox-media position-relative">
						<img src="<?php echo am_var('url'); ?>assets/home/<?php item_r('img', $item); ?>" alt="Featured Icon" width="60" class="mb-3">
					</div>
					<div class="fbox-content px-0">
						<h3 class="nott ls0"><a href="<?php item_r('link', $item); ?>" class="text-dark"><?php item_r('title', $item); ?></a></h3>
						<?php if (item_r('content', $item, true)) {?><p><?php item_r('content', $item); ?></p><?php } ?>
						<a href="<?php item_r('link', $item); ?>" class="button button-rounded button-border nott ls0 fw-normal ms-0 mt-4"><?php item_r('linkText', $item); ?></a>
					</div>
				</div>
			</div><?php } ?>
		</div>
	</div>
</div>

<?php if (isset($home->sections['top_donor'])) {?><div class="container">
	<div class="w-100 position-relative">
		<div class="donor-img d-flex align-items-center rounded parallax mx-auto shadow-sm w-100" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -50px;" style="height: 500px; background: url('demos/nonprofit/images/others/3.jpg') no-repeat center center / cover"></div>
		<div class="card bg-white border-0 center py-sm-4 px-sm-5 p-2 shadow-sm" style="position: absolute; top: 50%; right: 80px; transform: translateY(-50%);">
			<div class="card-body">
				<div class="color h1 mb-3"><i class="icon-heart"></i></div>
				<small class="text-uppercase fw-normal ls2 text-muted mb-3 d-block">Our Top Donor</small>
				<h3 class="display-3 fw-bold mb-3 font-secondary">$2.4M</h3>
				<p class="text-uppercase fw-medium text-muted">:Raised</p>
				<a href="demo-nonprofit-causes-single.html" class="button-svg">View More</a>
			</div>
		</div>
	</div>
</div><?php } ?>

<?php if (isset($home->sections['causes'])) {?>
<div class="section bg-transparent mt-0 mb-4">
	<div class="container clearfix">
		<div class="row justify-content-center" style="margin-top: 100px">
			<div class="col-md-7 center">
				<div class="heading-block border-bottom-0 mb-4">
					<h2 class="mb-4 nott">Our Charity Causes</h2>
				</div>
				<div class="svg-line bottommargin-sm clearfix">
					<img src="demos/nonprofit/images/divider-1.svg" alt="svg divider" height="20">
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, quasi, dolorum blanditiis eius laboriosam, quidem temporibus, dolor quod voluptatum perferendis ullam. Odio sequi at dolore consequatur ad, odit expedita tenetur.</p>
			</div>
		</div>
	</div>

<?php if (isset($home->sections['causes'])) {?>
	<div class="owl-carousel owl-carousel-full image-carousel carousel-widget topmargin-sm charity-card" data-stage-padding="20" data-margin="10" data-center="true" data-loop="true" data-nav="true" data-autoplay="500000" data-speed="400" data-pagi="true" data-items-xs="1" data-items-sm="2" data-items-md="2" data-items-lg="3" data-items-xl="4">

		<div class="oc-item text-start">
			<img src="demos/nonprofit/images/cause/1.jpg" alt="Image 1" class="rounded">
			<div class="oc-desc d-flex flex-column justify-content-center shadow-lg">
				<small class="text-uppercase fw-normal ls1 color mb-2 d-block">Homeless</small>
				<h3 class="mb-3"><a href="demo-nonprofit-causes-single.html">Clean Water for All</a></h3>
				<ul class="skills mb-3">
					<li data-percent="57">
						<div class="progress">
							<div class="progress-percent">
								<div class="counter counter-inherit">
									$<span data-from="0" data-to="119700" data-refresh-interval="10" data-speed="1100" data-comma="true"></span> Donated of $210,000
								</div>
							</div>
						</div>
					</li>
				</ul>
				<p class="mb-4 text-black-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut atque quidem consequuntur officiis vitae illo omnis inventore aliquam assumenda iusto mollitia illum similique eum libero rem possimus et, ipsam sapiente..</p>
				<a href="demo-nonprofit-causes-single.html" class="button button-rounded button-border nott ls0 fw-medium m-0 d-flex align-self-start">Donate Now</a>
			</div>
		</div>

	</div><?php } ?>
</div><?php } ?>

<?php if (isset($home->sections['mission_top'])) {?>
<div class="clear"></div>

<div class="container-fluid my-5 clearfix">
	<div class="d-flex flex-column align-items-center justify-content-center center counter-section position-relative py-5" style="background: url('demos/nonprofit/images/world-map.png') no-repeat center center/ contain">
		<div class="mx-auto center" style="max-width: 1000px">
			<h3>Our mission is to help people by distributing Money and Service globally.</h3>
		</div>

		<div class="row align-items-stretch m-0 w-100 clearfix">

			<div class="col-lg-3 col-sm-6 center mt-5">
				<img src="demos/nonprofit/images/icons/breakfast.svg" alt="Counter Icon" width="70" class="mb-4">
				<div class="counter font-secondary"><span data-from="100" data-to="11265" data-refresh-interval="50" data-speed="2100" data-comma="true"></span>+</div>
				<h5 class="nott ls0 mt-0"><u>Meals Surved</u></h5>
			</div>

		</div>
	</div>
</div><?php } ?>

<?php if (isset($home->sections['mission_goals'])) {?>
<div class="clear"></div>

<div class="section" style="background: #fff url('<?php echo am_var('url');?>assets/home/mission.jpg') no-repeat center center / cover; padding: 80px 0;">
	<div class="container clearfix">
		<div class="row">
			<?php foreach ($home->sections['mission_goals'] as $item) {?><div class="col-lg-8">
				<h3 class="mb-2">Our Mission <span>&amp;</span> Goals</h3>
				<div class="svg-line mb-2 clearfix">
					<img src="<?php echo $theme; ?>demos/nonprofit/images/divider-1.svg" alt="svg divider" height="10">
				</div>
				<p class="mb-5"><?php item_r('content', $item); ?></p>
				<?php if (isset($home->sections['mission'])) {?>
				<div class="row mission-goals gutter-30 mb-0">
					<div class="col-md-6">
						<div class="feature-box fbox-plain bg-white mx-0">
							<div class="fbox-media position-relative col-auto p-0 me-4">
								<img src="demos/nonprofit/images/icons/education.svg" alt="Featured Icon 3" width="50">
							</div>
							<div class="fbox-content">
								<h3 class="nott ls0"><a href="#" class="text-dark">Education Charities.</a></h3>
								<p>Quickly benchmark client-centered vortals without cutting.</p>
							</div>
						</div>
					</div>
				</div><?php } ?>
			</div><?php } ?>

			<?php if (isset($home->sections['videos'])) {?><div class="col-lg-4 mt-5 mt-lg-0">
				<h3 class="mb-2">Latest Videos</h3>
				<div class="svg-line mb-2 clearfix">
					<img src="demos/nonprofit/images/divider-1.svg" alt="svg divider" height="10">
				</div>
				<p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit excepturi ipsa!</p>
				<div class="clear"></div>
				<a href="https://www.youtube.com/watch?v=VqmFKnHG5q8" data-lightbox="iframe" class="shadow-sm d-flex align-items-center justify-content-center play-video rounded position-relative bg-color mt-3 clearfix" style="background: linear-gradient(rgba(0,0,0,.05), rgba(0,0,0,.01)), url('demos/nonprofit/images/others/5.jpg') no-repeat center center / cover; height: 300px"><i class="icon-play"></i></a>
				<div class="row mt-4" data-lightbox="gallery">
					<div class="col-sm-6">
						<a href="https://www.youtube.com/watch?v=hc7iuc5KZ8Y" data-lightbox="iframe" class="shadow-sm d-flex align-items-center justify-content-center play-video rounded position-relative bg-color left" style="background: url('demos/nonprofit/images/others/4.jpg') no-repeat center center / cover; height: 140px"><i class="icon-play icon-small"></i></a>
					</div>
				</div>
			</div><?php } ?>
		</div>
	</div>
</div><?php } ?>

<?php if (isset($home->sections['volunteers'])) {?><div class="section bg-transparent">
	<div class="container clearfix">
		<div class="row justify-content-center mb-5">
			<div class="col-md-7 center">
				<div class="heading-block border-bottom-0 mb-4">
					<h2 class="mb-4 nott">Happy Volunteers</h2>
				</div>
				<div class="svg-line bottommargin-sm clearfix">
					<img src="demos/nonprofit/images/divider-1.svg" alt="svg divider" height="20">
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, quasi, dolorum blanditiis eius laboriosam, quidem temporibus, dolor quod voluptatum perferendis ullam. Odio sequi at dolore consequatur ad, odit expedita tenetur.</p>
			</div>
		</div>
		<div class="row justify-content-around">
			<div class="col-lg-2 col-md-4 col-6">
				<div class="team overflow-hidden">
					<div class="team-image">
						<img src="demos/nonprofit/images/volunteers/1.jpg" alt="Penny Tool">
					</div>
					<div class="team-desc">
						<h4 class="team-title pt-3 mb-0 fw-medium nott">Penny Tool<small>Volunteers</small></h4>
					</div>
				</div>
				<div class="team mt-5">
					<div class="team-image">
						<img src="demos/nonprofit/images/volunteers/6.jpg" alt="Piff Jenkins">
					</div>
					<div class="team-desc">
						<h4 class="team-title pt-3 mb-0 fw-medium nott">Piff Jenkins<small>Volunteers</small></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><?php } ?>

<?php if (isset($home->sections['newsletter'])) {?><div class="bg-color subscribe-section position-relative" style="margin-top: 100px;">
	<div class="container" style="z-index: 2;">
		<div class="center collapsed subscribe-section-target" data-bs-toggle="collapse" data-bs-target="#target-1">
			<div class="subscribe-icon"><i class="icon-envelope21"></i></div>
			<h2 class="mb-0 mt-2 position-relative" style="z-index: 1;">Click here to Subscribe our Newsletter <i class="icon-arrow-down position-relative" style="top: 5px"></i>
			</h2>
		</div>
		<div class="collapse" id="target-1">
			<div class="form-widget pb-5" data-alert-type="false">

				<div class="form-result"></div>

				<div class="nonprofit-loader css3-spinner" style="position: absolute;">
					<div class="<?php echo $theme; ?>css3-spinner-bounce1"></div>
					<div class="<?php echo $theme; ?>css3-spinner-bounce2"></div>
					<div class="<?php echo $theme; ?>css3-spinner-bounce3"></div>
				</div>
				<div id="nonprofit-submitted" class="center">
					<h4 class="fw-semibold mb-0">Thank You for Contact Us! Our Team will contact you asap on your email Address.</h4>
				</div>

				<form id="nonprofit" class="row mt-2" action="include/form.php" method="post" enctype="multipart/form-data">
					<div class="col-md-4 mb-4 mb-md-1">
						<label for="nonprofit-name">Name:</label>
						<input type="text" name="nonprofit-name" id="nonprofit-name" class="form-control border-form-control required" value="" placeholder="Enter your Full Name">
					</div>
					<div class="col-md-4 mb-4 mb-md-1">
						<label for="nonprofit-phone">Contact:</label>
						<input type="text" name="nonprofit-phone" id="nonprofit-phone" class="form-control border-form-control" value="" placeholder="Enter your Contact Number">
					</div>
					<div class="col-md-4 mb-4 mb-md-1">
						<label for="nonprofit-email">Email:</label>
						<input type="email" name="nonprofit-email" id="nonprofit-email" class="form-control border-form-control required" value="" placeholder="Enter your Email">
					</div>
					<div class="col-12 d-none">
						<input type="text" id="nonprofit-botcheck" name="nonprofit-botcheck" value="" />
					</div>
					<button type="submit" name="nonprofit-submit" class="btn button button-rounded button-xlarge button-dark bg-dark shadow nott ls0 m-0 subscribe-button">Subscribe Now</button>
					<input type="hidden" name="prefix" value="nonprofit-">
				</form>
			</div>
		</div>
	</div>
	<div style="background-image: url('demos/nonprofit/images/divider-4.svg'); position: absolute; bottom: -20px; left: 0; width: 100%; height: 60px; z-index: 1;"></div>
</div><?php } ?>

<?php if (isset($home->sections['events'])) {?><div class="section m-0 p-0 row align-items-stretch clearfix" style="background-color: rgba(198,192,156, 0.15);">
	<div class="col-lg-4 dark d-flex flex-column align-items-center center justify-content-center" style="background: linear-gradient(rgba(0,0,0,.3), rgba(0,0,0,.5)), url('demos/nonprofit/images/others/event.jpg') center center repeat; background-size: cover; min-height: 300px;">
		<h2 class="display-4 px-4 fw-bold mb-4 d-block">Events Overview</h2>
	</div>
	<div class="col-lg-8 col-padding">
		<div class="events-calendar">
			<div class="events-calendar-header clearfix">
				<div class="calendar-month-year d-flex text-start justify-content-between align-items-center w-100">
					<div>
						<span id="calendar-month" class="calendar-month text-dark"></span>
						<span id="calendar-year" class="calendar-year text-dark"></span>
					</div>
					<nav>
						<span id="calendar-prev" class="calendar-prev bg-color"><i class="icon-chevron-left text-dark"></i></span>
						<span id="calendar-next" class="calendar-next bg-color"><i class="icon-chevron-right text-dark"></i></span>
						<span id="calendar-current" class="calendar-current bg-color" title="Got to current date"><i class="icon-reload text-dark"></i></span>
					</nav>
				</div>
			</div>
			<div id="calendar" class="fc-calendar-container"></div>
		</div>

	</div>
</div><?php } ?>

<?php if (isset($home->sections['works_with_us'])) {?><div class="section bg-transparent" style="padding: 80px 0">
	<div class="container clearfix">
		<div class="row justify-content-center">
			<div class="col-md-7 center">
				<div class="heading-block border-bottom-0 mb-4">
					<h2 class="mb-4 nott">Who Work With Us</h2>
				</div>
				<div class="svg-line bottommargin-sm clearfix">
					<img src="demos/nonprofit/images/divider-1.svg" alt="svg divider" height="15">
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, quasi, dolorum blanditiis eius laboriosam, quidem temporibus, dolor quod voluptatum perferendis ullam. Odio sequi at dolore consequatur ad, odit expedita tenetur.</p>
			</div>
			<div class="clear"></div>
			<div class="col-md-11 my-5">
				<ul class="clients-grid grid-2 grid-sm-3 grid-md-5 mb-0">
					<li class="grid-item"><a href="#"><img src="images/clients/1.png" alt="Clients"></a></li>
				</ul>
			</div>
			<h5 class="fw-normal text-black-50 mb-0">If you want to work with for nonprofit? <a href="demo-nonprofit-contact.html"><u>Send your Details</u></a>.</h5>
		</div>
	</div>
</div><?php } ?>

</div>

