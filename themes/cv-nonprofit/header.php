<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Amadeus CMS, amadeusweb.com/docs/ by Imran Ali Namazi" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css2?family=Caveat+Brush&family=Poppins:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $theme; ?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/swiper.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/custom.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/calendar.css" type="text/css" />

	<!-- NonProfit Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme; ?>css/colors.php?color=<?php echo am_var_or('theme-color', 'C6C09C');?>" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/nonprofit/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/nonprofit/nonprofit.css" type="text/css" />
	<!-- / -->

	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Document Title
	============================================= -->
	<title><?php title(); ?></title>
	<?php seo_tags(); ?>
	<?php head_hooks(); ?>
	<link href="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-icon.png" rel="icon" />

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="header-size-sm border-bottom-0" data-sticky-shrink="false">
			<div id="header-wrap">
				<div class="container">
					<div class="header-row justify-content-lg-between">

						<!-- Logo
						============================================= -->
						<div id="logo" class="me-lg-5">
							<a href="<?php echo am_var('url'); ?>" class="standard-logo"><img src="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo.png" alt="<?php echo am_var('name'); ?>"></a>
							<a href="<?php echo am_var('url'); ?>" class="retina-logo"><img src="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo@2x.png" alt="<?php echo am_var('name'); ?>"></a>
						</div><!-- #logo end -->

						<div class="header-misc">
							<a href="<?php echo am_var('url'); ?>donate/" class="button button-rounded button-light"><div>Donate</div></a>
						</div>

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu with-arrows me-lg-auto">

							<?php
							am_var('site-menu-settings', [
								'diagonal-spacer' => '<li class="menu-item"><span class="menu-bg col-auto align-self-start d-flex"></span></li>',
								'outer-ul-class' => 'menu-container align-self-start',
								'ul-class' => 'sub-menu-container',
								'li-class' => 'menu-item',
								'li-active-class' => 'current',
								'a-class' => 'menu-link',
								'wrap-text-in-a-div' => true,
								'top-level-angle' => '<i class="icon-angle-down"></i>',
							]);
							if (!runCode('menu')) menu();
							am_var('site-menu-settings', false);
							?>

						</nav><!-- #primary-menu end -->

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->

		<?php if (has_var('slider-data')) include_once('slider.php');?>

		<!-- Content
		============================================= -->
		<section id="content">
