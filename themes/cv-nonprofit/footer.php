</section><!-- #content end -->

<!-- Footer
============================================= -->
<footer id="footer" style="background-color: #002D40;">

    <?php runCode('footer-content'); ?>
    <?php if (am_var('footer-help')) { ?>
    <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap dark clearfix" style="background: radial-gradient(rgba(0,45,64,.5), rgba(0,45,64,.1), rgba(0,45,64,.5)), url('<?php echo am_var('url') . 'assets/'; ?><?php echo am_sub_var('footer-help', 'img'); ?>') repeat center center / cover;  padding: 150px 0">
            <div class="mx-auto center" style="max-width: 700px;">
                <h2 class="display-2 fw-bold text-white mb-0 ls1 font-secondary mb-4"><i class="icon-heart d-block mb-3"></i><?php echo am_sub_var('footer-help', 'title'); ?></h2>
                <a href="<?php echo am_var('url') . am_sub_var('footer-help', 'link'); ?>" class="button button-rounded button-xlarge button-white bg-white button-light text-dark shadow nott ls0 ms-0 mt-5">Donate Now</a>
            </div>
        </div>

    </div>
    <?php } ?>

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights" class="bg-color">

        <div class="container clearfix">

            <div class="row justify-content-between align-items-center">
                <div class="col-md-6">
                    <?php _copyright(); ?><br>
                    <?php if (am_var('footer_post_credits')) { echo renderFile(am_var('footer_post_credits')); } ?>
                </div>

                <div class="col-md-6 d-md-flex flex-md-column align-items-md-end mt-4 mt-md-0">
                    <?php _credits(); ?>
                </div>
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->

<?php if (am_var('floating-contact')) { ?>
<!-- Floating Contact
============================================= -->
<div class="floating-contact-wrap">
    <div class="floating-contact-btn shadow">
        <i class="floating-contact-icon btn-unactive icon-envelope21"></i>
        <i class="floating-contact-icon btn-active icon-line-plus"></i>
    </div>
    <div class="floating-contact-box">
        <div id="q-contact" class="widget quick-contact-widget clearfix">
            <div class="floating-contact-heading bg-color p-4 rounded-top">
                <h3 class="mb-0 font-secondary h2 ls0">Quick Contact 👋</h3>
                <p class="mb-0">Get in Touch with Us</p>
            </div>
            <div class="form-widget bg-white" data-alert-type="false">
                <div class="form-result"></div>
                <div class="floating-contact-loader css3-spinner" style="position: absolute;">
                    <div class="<?php echo $theme; ?>css3-spinner-bounce1"></div>
                    <div class="<?php echo $theme; ?>css3-spinner-bounce2"></div>
                    <div class="<?php echo $theme; ?>css3-spinner-bounce3"></div>
                </div>
                <div id="floating-contact-submitted" class="p-5 center">
                    <i class="icon-line-mail h1 color"></i>
                    <h4 class="fw-normal mb-0 font-body">Thank You for Contact Us! Our Team will contact you asap on your email Address.</h4>
                </div>
                <form class="mb-0" id="floating-contact" action="include/form.php" method="post" enctype="multipart/form-data">
                    <div class="input-group mb-3">
                        <span class="input-group-text bg-transparent"><i class="icon-user-alt"></i></span>
                        <input type="text" name="floating-contact-name" id="floating-contact-name" class="form-control required" value="" placeholder="Enter your Full Name">
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text bg-transparent"><i class="icon-at"></i></span>
                        <input type="email" name="floating-contact-email" id="floating-contact-email" class="form-control required" value="" placeholder="Enter your Email Address">
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text bg-transparent"><i class="icon-comment21"></i></span>
                        <textarea name="floating-contact-message" id="floating-contact-message" class="form-control required" cols="30" rows="4"></textarea>
                    </div>
                    <input type="hidden" id="floating-contact-botcheck" name="floating-contact-botcheck" value="" />
                    <button type="submit" name="floating-contact-submit" class="btn btn-dark w-100 py-2">Send Message</button>
                    <input type="hidden" name="prefix" value="floating-contact-">
                    <input type="hidden" name="subject" value="Messgae From Floating Contact">
                    <input type="hidden" name="html_title" value="Floating Contact Message">
                </form>
            </div>
        </div>
    </div>
</div><?php } ?>

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- JavaScripts
============================================= -->
<script src="<?php echo $theme; ?>js/jquery.js"></script>
<script src="<?php echo $theme; ?>js/plugins.min.js"></script>
<script src="<?php echo $theme; ?>js/jquery.calendario.js"></script>
<script src="<?php echo $theme; ?>demos/nonprofit/js/events.js"></script>

<!-- Footer Scripts
============================================= -->
<script src="<?php echo $theme; ?>js/functions.js"></script>
<?php if (uses('calendar')) {?>
<script src="<?php echo $theme; ?>js/calendar-inline.js"></script><?php } ?>

<?php styles_and_scripts(); ?>
<?php foot_hooks(); ?>

</body>
</html>
