<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css2?family=Domine:wght@400;500;700&family=Roboto:wght@400;500&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo $theme; ?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/dark.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Blog Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/blog/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/blog/blog.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/colors.php?color=F39887" type="text/css" />

	<title><?php title(); ?></title>
	<link href="<?php echo am_var('url'); ?>icon-<?php echo am_var('safeName'); ?>.png" rel="icon" />
	<?php seo_tags(); ?>
	<?php head_hooks(); ?>
</head>

<body class="stretched search-overlay">

	<!--
	<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0&appId=915724525182895&autoLogAppEvents=1"></script>
	-->

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper">

		<!-- Header
		============================================= -->
		<header id="header" class="header-size-custom" data-sticky-shrink="false">
			<div id="header-wrap">
				<div class="container">
					<div class="header-row justify-content-lg-between">

						<!-- Logo
						============================================= -->
						<div id="logo" class="mx-lg-auto col-auto flex-column order-lg-2 px-0">
							<a href="<?php echo am_var('url');?>" class="standard-logo" data-dark-logo="<?php echo am_var('url'); ?>logo-<?php echo am_var('safeName') . am_var('safeFolder'); ?>.png"><img src="<?php echo am_var('url'); ?>logo-<?php echo am_var('safeName') . am_var('safeFolder'); ?>.png" alt="<?php echo am_var('name'); ?>"></a>
							<a href="<?php echo am_var('url');?>" class="retina-logo" data-dark-logo="<?php echo am_var('url'); ?>logo-<?php echo am_var('safeName') . am_var('safeFolder'); ?>@2x.png"><img src="<?php echo am_var('url'); ?>logo-<?php echo am_var('safeName') . am_var('safeFolder'); ?>@2x.png" alt="<?php echo am_var('name'); ?>"></a>
							<?php if (uses('date-today')) {?><span class="divider divider-center date-today"><span class="divider-text"></span></span><?php } ?>
						</div><!-- #logo end -->

						<?php if (am_var('social')) { ?><div class="col-auto col-lg-3 order-lg-1 d-none d-md-flex px-0">
							<div class="social-icons"><?php foreach (am_var('social') as $s) { ?>
								<a href="<?php echo $s['url']; ?>" target="_blank" class="social-icon si-rounded si-dark si-mini si-<?php echo $s['type']; ?>">
									<i class="icon-<?php echo $s['type']; ?>"></i>
									<i class="icon-<?php echo $s['type']; ?>"></i>
								</a><?php } ?>
							</div>
						</div><?php } ?>


						<div class="header-misc col-auto col-lg-3 justify-content-lg-end ml-0 ml-sm-3 px-0">

							<?php if (uses('lang')) { ?>
							<!-- Bookmark
							============================================= -->
							<div class="dropdown dropdown-langs">
								<button class="btn dropdown-toggle px-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img src="demos/seo/images/flags/eng.png" alt="French">
								</button>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
									<a href="#" class="dropdown-item"><img src="demos/seo/images/flags/fre.png" alt="Lang"> French</a>
									<a href="#" class="dropdown-item"><img src="demos/seo/images/flags/ara.png" alt="Lang"> Arabic</a>
									<a href="#" class="dropdown-item"><img src="demos/seo/images/flags/tha.png" alt="Lang"> Thailand</a>
									<a href="#" class="dropdown-item disabled" tabindex="-1" aria-disabled="true"><img src="demos/seo/images/flags/eng.png" alt="Lang"> English</a>
								</div>
							</div><?php } ?>

							<?php if (uses('search')) { ?>
							<!-- Top Search
							============================================= -->
							<div id="top-search" class="header-misc-icon">
								<a href="#" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
							</div><!-- #top-search end --><?php } ?>

							<?php if (uses('dark-mode')) { ?>
							<div class="dark-mode header-misc-icon d-none d-md-block">
								<a href="#"><i class="icon-dark"></i></a>
							</div><?php } ?>
						</div>

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

					</div>
				</div>

				<div class="container">
					<div class="header-row justify-content-lg-center header-border">

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu with-arrows">

							<ul class="menu-container justify-content-between">
								<?php if (!run_site_code('menu')) menu(); ?>
							</ul>

						</nav><!-- #primary-menu end -->

						<form class="top-search-form" action="search.html" method="get">
							<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
						</form>

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>

		</header><!-- #header end -->
