		<!-- Footer
		============================================= -->
		<footer id="footer">
			<div class="container">
				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap row col-mb-50">

					<?php if (!runCode('footer-content')) { ?>
					<!-- Footer Widget 3
					============================================= -->
					<div class="col-lg-2 col-sm-6">
						<div class="widget widget_links">
							<h4 class="mb-3 mb-sm-4 nott ls0">Management</h4>
							<ul>
								<li><a href="https://codex.wordpress.org/">About Us</a></li>
								<li><a href="https://wordpress.org/support/forum/requests-and-feedback">Careers</a></li>
								<li><a href="https://wordpress.org/extend/plugins/">Customers</a></li>
								<li><a href="https://wordpress.org/support/">Forums</a></li>
								<li><a href="https://wordpress.org/extend/themes/">Partners</a></li>
								<li><a href="https://wordpress.org/news/">Themes</a></li>
								<li><a href="https://planet.wordpress.org/">Pricing</a></li>
								<li><a href="https://planet.wordpress.org/">Reviews</a></li>
							</ul>
						</div>
					</div>

					<!-- Footer Widget 3
					============================================= -->
					<div class="col-lg-2 col-sm-6">
						<div class="widget widget_links">
							<h4 class="mb-3 mb-sm-4 nott ls0">Our Products</h4>
							<ul>
								<li><a href="https://codex.wordpress.org/">Real Estate</a></li>
								<li><a href="https://wordpress.org/support/forum/requests-and-feedback">Movers</a></li>
								<li><a href="https://wordpress.org/extend/plugins/">Stores</a></li>
								<li><a href="https://wordpress.org/support/">Landing</a></li>
								<li><a href="https://wordpress.org/extend/themes/">Seo</a></li>
								<li><a href="https://wordpress.org/news/">CoWorking</a></li>
							</ul>
						</div>
					</div>

					<!-- Footer Widget 3
					============================================= -->
					<div class="col-lg-2 col-sm-6">
						<div class="widget widget_links">
							<h4 class="mb-3 mb-sm-4 nott ls0">Support</h4>
							<ul>
								<li><a href="https://codex.wordpress.org/">Privacy</a></li>
								<li><a href="https://wordpress.org/support/forum/requests-and-feedback">Help Center</a></li>
								<li><a href="https://wordpress.org/extend/plugins/">Chat</a></li>
								<li><a href="https://wordpress.org/support/">Email Us</a></li>
							</ul>
							<div class="mt-3">
								<a href="https://facebook.com/semicolonweb" class="social-icon si-small si-dark si-facebook" title="Facebook" target="_blank">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>

								<a href="https://instagram.com/semicolonweb" class="social-icon si-small si-dark si-instagram" title="instagram" target="_blank">
									<i class="icon-instagram"></i>
									<i class="icon-instagram"></i>
								</a>

								<a href="https://twitter.com/__semicolon" class="social-icon si-small si-dark si-twitter" title="twitter" target="_blank">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
								</a>

								<a href="#" class="social-icon si-small si-dark si-wikipedia" title="Apple Pay">
									<i class="icon-apple-pay"></i>
									<i class="icon-apple-pay"></i>
								</a>
							</div>
						</div>
					</div>

					<!-- Footer Widget 2
						============================================= -->
					<div class="col-lg-3 col-sm-6">
						<div class="widget widget_links">
							<h4 class="mb-3 mb-sm-4 nott ls0">Tag Cloud</h4>
							<div class="tagcloud">
								<a href="#">general</a>
								<a href="#">videos</a>
								<a href="#">music</a>
								<a href="#">media</a>
								<a href="#">photography</a>
								<a href="#">parallax</a>
								<a href="#">ecommerce</a>
								<a href="#">terms</a>
								<a href="#">coupons</a>
								<a href="#">modern</a>
								<a href="#">magazine</a>
								<a href="#">bootstrap</a>
								<a href="#">news</a>
								<a href="#">blog</a>
								<a href="#">wordpress</a>
							</div>
						</div>
					</div>

					<!-- Footer Widget 4
					============================================= -->
					<div class="col-lg-3 col-sm-6">
						<div class="widget widget_links">
							<h4 class="mb-3 mb-sm-4 nott ls0">Download in Mobile</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus beatae esse iure est, quam libero!</p>
							<a href="#" class="button button-dark  text-light btn-block text-center bg-dark nott ls0 button-rounded button-xlarge noleftmargin"><i class="icon-apple"></i>App Store</a>
							<a href="#" class="button button-dark  text-light btn-block text-center bg-dark nott ls0 button-rounded button-xlarge noleftmargin"><i class="icon-googleplay"></i>Google Play</a>
						</div>
					</div>

					<?php } ?>

				</div>
			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">
				<div class="container">

					<div class="row align-items-center justify-content-between col-mb-30">
						<div class="col-lg-auto text-center text-lg-left">
							<?php copyright_and_credits(); ?>
							<?php echo_if_var('footer_post_credits'); ?>
						</div>

						<?php if (uses('footer-menu')) { ?><div class="col-lg-auto text-center text-lg-left">
							<div class="copyrights-menu copyright-links m-0">
								<?php footer_menu(); ?>
							</div><?php } ?>
						</div>
					</div>

				</div>
			</div><!-- #copyrights end -->
		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up rounded-circle" style="left: 30px; right: auto;"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="<?php echo $theme; ?>js/jquery.js"></script>
	<script src="<?php echo $theme; ?>js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?php echo $theme; ?>js/functions.js"></script>

	<!-- ADD-ONS JS FILES -->
	<script>
		// Current Date
		var weekday = ["Sun","Mon","Tues","Wed","Thurs","Fri","Sat"],
			month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			a = new Date();

		jQuery('.divider-text').html( weekday[a.getDay()] + ', ' + month[a.getMonth()] + ' ' + a.getDate() );

		jQuery('.dark-mode').on( 'click', function() {
		    jQuery("body").toggleClass('dark');
		    SEMICOLON.header.logo();
		    return false;
		});
	</script>
	<?php styles_and_scripts(); ?>
	<?php foot_hooks(); ?>
</body>
</html>
