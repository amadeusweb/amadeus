<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/dark.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>one-page/css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Modern Blog Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/modern-blog/modern-blog.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/modern-blog/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/colors.php?color=dc3545" type="text/css" />
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title><?php title(); ?></title>
	<?php seo_tags(); ?>
	<link href="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-icon.png" rel="icon" />
	<?php head_hooks(); ?>
</head>

<body class="stretched overlay-menu">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header header-size-md" data-sticky-shrink="false">
			<div id="header-wrap">
				<div class="container-fluid">
					<div class="header-row">

						<!-- Logo
						============================================= -->
						<div id="logo">
							<a href="<?php echo am_var('url');?>" class="standard-logo" data-mobile-logo="<?php echo am_var('url'); ?><?php echo am_var('safeName') . am_var('safeFolder'); ?>.png"><img src="<?php echo am_var('url'); ?><?php echo am_var('safeName') . am_var('safeFolder'); ?>-logo.png" alt="Canvas Logo"></a>
							<a href="<?php echo am_var('url');?>" class="retina-logo" data-mobile-logo="<?php echo am_var('url'); ?><?php echo am_var('safeName') . am_var('safeFolder'); ?>@2x.png"><img src="<?php echo am_var('url'); ?><?php echo am_var('safeName') . am_var('safeFolder'); ?>-logo@2x.png" alt="Canvas Logo"></a>
						</div><!-- #logo end -->

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu text-lg-center">
							<ul class="menu-container">
								<?php if (!run_site_code('menu')) menu(); ?>
							</ul>
						</nav><!-- #primary-menu end -->

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->

		<?php runCode('optional-slider'); ?>

		<!-- Content
		============================================= -->
		<section id="content" class="bg-light">

			<div class="content-wrap pt-lg-0 pt-xl-0 pb-0">
