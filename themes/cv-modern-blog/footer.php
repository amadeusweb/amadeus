			</div>

		<?php if (!runCode('footer-content', true)) { ?>
		<footer>
			<!-- Copyrights
			============================================= -->
			<div id="copyrights" class="bg-dark dark">
				<div class="container clearfix">

					<div class="row col-mb-30">
						<div class="col-12 text-center text-muted">
							Copyrights &copy; 2020 All Rights Reserved by Canvas Inc.<br>
						</div>

						<div class="col-12 text-center">
							<a href="#" class="social-icon inline-block si-small si-rounded si-colored si-facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="#" class="social-icon inline-block si-small si-rounded si-colored si-twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="#" class="social-icon inline-block si-small si-rounded si-colored si-gplus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a>

							<a href="#" class="social-icon inline-block si-small si-rounded si-colored si-pinterest">
								<i class="icon-pinterest"></i>
								<i class="icon-pinterest"></i>
							</a>

							<a href="#" class="social-icon inline-block si-small si-rounded si-colored si-vimeo">
								<i class="icon-vimeo"></i>
								<i class="icon-vimeo"></i>
							</a>

							<a href="#" class="social-icon inline-block si-small si-rounded si-colored si-instagram">
								<i class="icon-instagram"></i>
								<i class="icon-instagram"></i>
							</a>
						</div>
					</div>

				</div>
			</div><!-- #copyrights end -->
		</footer>
		<?php } ?>

		</section>><!-- #content end -->
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up rounded-circle"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="<?php echo $theme; ?>js/jquery.js"></script>
	<script src="<?php echo $theme; ?>js/plugins.min.js"></script>
	<script src="<?php echo $theme; ?>js/plugins.infinitescroll.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?php echo $theme; ?>js/functions.js"></script>

	<!-- ADD-ONS JS FILES -->
	<script>

		// Infinity Scroll
		jQuery(window).on( 'load', function(){

			var $container = $('.infinity-wrapper');

			$container.infiniteScroll({
				path: '.load-next-posts',
				history: false,
				status: '.page-load-status',
			});

			$container.on( 'load.infiniteScroll', function( event, response, path ) {
				var $items = $( response ).find('.infinity-loader');
				// append items after images loaded
				$items.imagesLoaded( function() {
					$container.append( $items );
					$container.isotope( 'insert', $items );
					setTimeout( function(){
						SEMICOLON.initialize.resizeVideos();
						SEMICOLON.initialize.lightbox();
						SEMICOLON.widget.loadFlexSlider();
					}, 1000 );
				});
			});

		});

	</script>
	<?php styles_and_scripts(); ?>
</body>
</html>