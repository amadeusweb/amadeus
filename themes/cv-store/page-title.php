		<?php $pageTitle = am_var('page-title'); $bgdPos = isset($pageTitle['background-position']) ? $pageTitle['background-position'] : 'center center'; ?>
		<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-parallax page-title-dark page-title-center" style="background-image: url('<?php echo am_sub_var('node-vars', 'nodeUrl'); ?>images/page-title.jpg'); background-size: cover; padding: 120px 0 60px; background-position: <?php echo $bgdPos;?>">

			<div class="container clearfix">
				<h1><?php echo $pageTitle['heading']; ?></h1>
				<span><?php echo $pageTitle['page']; ?></span>
			</div>

		</section><!-- #page-title end -->

