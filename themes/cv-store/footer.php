		<!-- Footer
		============================================= -->
		<footer id="footer" class="bg-color border-0">

			<div class="container-fluid">

				<?php if (!runCode('footer-content')) { ?>
				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap pb-5">

					<div class="row">

						<div class="col-lg-2 col-md-3 col-6">
							<div class="widget">

								<h4 class="ls0 mb-3 nott">Features</h4>

								<ul class="list-unstyled iconlist ms-0">
									<li><a href="#">Help Center</a></li>
									<li><a href="#">Paid with Moblie</a></li>
									<li><a href="#">Status</a></li>
									<li><a href="#">Changelog</a></li>
									<li><a href="#">Contact Support</a></li>
								</ul>

							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-6">
							<div class="widget">

								<h4 class="ls0 mb-3 nott">Support</h4>

								<ul class="list-unstyled iconlist ms-0">
									<li><a href="#">Home</a></li>
									<li><a href="#">About</a></li>
									<li><a href="#">FAQs</a></li>
									<li><a href="#">Support</a></li>
									<li><a href="#">Contact</a></li>
								</ul>

							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-6">
							<div class="widget">

								<h4 class="ls0 mb-3 nott">Trending</h4>

								<ul class="list-unstyled iconlist ms-0">
									<li><a href="#">Shop</a></li>
									<li><a href="#">Portfolio</a></li>
									<li><a href="#">Blog</a></li>
									<li><a href="#">Events</a></li>
									<li><a href="#">Forums</a></li>
								</ul>

							</div>
						</div>
						<div class="col-lg-2 col-md-3 col-6">
							<div class="widget">

								<h4 class="ls0 mb-3 nott">Get to Know us</h4>

								<ul class="list-unstyled iconlist ms-0">
									<li><a href="#">Corporate</a></li>
									<li><a href="#">Agency</a></li>
									<li><a href="#">eCommerce</a></li>
									<li><a href="#">Personal</a></li>
									<li><a href="#">OnePage</a></li>
								</ul>

							</div>
						</div>
						<div class="col-lg-4">
							<div class="widget">

								<h4 class="ls0 mb-3 nott">Instagram Feed</h4>
								<div id="instagram-photos" class="instagram-photos masonry-thumbs grid-5" data-user="blog.canvastemplate" data-count="10"></div>

							</div>
						</div>

					</div>

				</div><!-- .footer-widgets-wrap end -->
				<?php } ?>

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container-fluid clearfix">

					<div class="row justify-content-between align-items-center">
						<div class="col-md-6">
							<?php copyright_and_credits(); ?>
							<?php if (am_var('footer_post_credits')) { echo replace_vars(am_var('footer_post_credits')); } ?>
						</div>
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="<?php echo $theme; ?>js/jquery.js"></script>
	<script src="<?php echo $theme; ?>js/plugins.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?php echo $theme; ?>js/functions.js"></script>

	<script>
		$(document).ready(changeHeaderColor);
		$(window).on('resize',changeHeaderColor);

		function changeHeaderColor(){
			if (jQuery(window).width() > 991.98) {
				jQuery( "#header" ).hover(
					function() {
						if (!$(this).hasClass("sticky-header")) {
							$( this ).addClass( "hover-light" ).removeClass( "dark" );
							SEMICOLON.header.logo();
						}
						$( "#wrapper" ).addClass( "header-overlay" );
					}, function() {
						if (!$(this).hasClass("sticky-header")) {
							$( this ).removeClass( "hover-light" ).addClass( "dark" );
							SEMICOLON.header.logo();
						}
						$( "#wrapper" ).removeClass( "header-overlay" );
					}
				);
			}
		};

		$(window).scroll(function() {
			if ($(document).scrollTop() > 2000 && $("#modal-subscribe").attr("displayed") === "false") {
				$('#modal-subscribe').modal('show');
				$("#modal-subscribe").attr("displayed", "true");
			}
		});

		jQuery('#modal-subscribe-form').on( 'formSubmitSuccess', function(){
			$("#modal-subscribe").addClass("fadeOutDown");
			setTimeout(function() { $('#modal-subscribe').modal('hide'); }, 400);
			$("#modal-subscribe").attr("displayed", "false");
		});

	</script>
	<?php foot_hooks(); ?>
	<?php styles_and_scripts(); ?>


</body>
</html>
