<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Amadeus CMS, amadeusweb.com/docs/ by Imran Ali Namazi" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/swiper.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/dark.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Store Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme; ?>css/colors.php?color=<?php echo am_var_or('theme-color', '222222'); ?>" type="text/css" /> <!-- Store Theme Color -->
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/store/css/fonts.css" type="text/css" /> <!-- Store Theme Font -->
	<link rel="stylesheet" href="<?php echo $theme; ?>demos/store/store.css" type="text/css" /> <!-- Store Theme Custom CSS -->
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title><?php title(); ?></title>
	<?php seo_tags(); ?>
	<?php
	$homeUrl = homeUrl();
	//NOT YET $logoUrl = logoUrl();
	$logoRel =  logoRel();
	?>
	<link href="<?php echo am_var('url'); ?><?php echo $logoRel; ?>-icon.png" rel="icon" />
	<!--NB: Purchased for Amadeus by services@cselian.com from - http://themes.semicolonweb.com/html/canvas/demo-store.html -->
	<?php head_hooks(); ?>
</head>

<body class="stretched modal-subscribe-bottom has-plugin-html5video">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<?php if(am_var('wants-theme-head')) runCode('head'); ?>

		<!-- Header
		============================================= -->
		<header id="header" class="floating-header header-size-custom" data-sticky-shrink="false" data-sticky-class="not-dark">
			<div id="header-wrap">
				<div class="container-fluid">
					<div class="header-row justify-content-lg-between">

						<!-- Logo
						============================================= -->
						<div id="logo" class="me-lg-0">
							<a class="standard-logo" href="<?php echo $homeUrl;?>"><img src="<?php echo am_var('url'); ?><?php echo $logoRel; ?>-logo.png" alt="<?php echo am_var('name'); ?>" /></a>
							<a class="retina-logo" href="<?php echo $homeUrl;?>"><img src="<?php echo am_var('url'); ?><?php echo $logoRel; ?>-logo@2x.png" alt="<?php echo am_var('name'); ?>" /></a>
						</div><!-- #logo end -->

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu with-arrows">
							<?php
							am_var('site-menu-settings', [
								'no-outer-ul' => true,
								'outer-ul-class' => 'menu-container',
								'ul-class' => 'menu-container',
								'li-class' => 'menu-item',
								'li-active-class' => 'current',
								'a-class' => 'menu-link',
								//'wrap-text-in-a-div' => true,
								'top-level-angle' => '<i class="icon-angle-down"></i>',
							]);
							if (!runCode('menu')) menu();
							am_var('site-menu-settings', false);
							?>

						</nav><!-- #primary-menu end -->

					</div>

				</div>

			</div>
			<div class="header-wrap-clone"></div>

		</header><!-- #header end -->

		<?php if(am_var('wants-theme-slider')) disk_include_once(__DIR__ .'/slider.php'); ?>

		<?php if(am_var('wants-page-title')) disk_include_once(__DIR__ .'/page-title.php'); ?>

