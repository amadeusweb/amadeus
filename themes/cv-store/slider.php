<?php $themeUrl = am_var('theme_url'); ?>
		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element swiper_wrapper min-vh-100" data-loop="true" data-speed="1000" data-autoplay="5000">
			<div class="slider-inner">

				<div class="swiper-container swiper-parent">
					<div class="swiper-wrapper"><?php foreach(am_var('slider-items') as $item) { ?>
						<div class="swiper-slide dark">
							<div class="container">
								<div class="slider-caption">
									<h2 data-animate="fadeInUp"><?php echo $item['heading']; ?></h2>
									<p class="mb-4" data-animate="fadeInUp" data-delay="100"><?php echo $item['tagline']; ?></p>
									<div><?php foreach($item['links'] as $link) { ?>
										<a href="<?php
										$externalLink = startsWith($link['href'], 'http');
										echo ($externalLink ? '' : am_sub_var('node-vars', 'nodeLink')) . $link['href'];
										if ($externalLink) echo '" target="_blank';
										?>" data-animate="fadeInUp" data-delay="200" class="button button-large button-white button-light"><?php echo $link['text']; ?></a><?php } ?>
									</div>
								</div>
							</div>
							<div class="swiper-slide-bg" style="background-image: linear-gradient(to bottom, rgba(0,0,0,.2), rgba(0,0,0,.3)), url('<?php echo am_sub_var('node-vars', 'nodeUrl') . 'images/' . $item['img']; ?>'); background-position: top center; background-size: contain;"></div>
						</div><?php } ?>
					</div>
					<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
					<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
					<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
				</div>

				<div class="social-icons"><?php foreach(am_var('social') as $item) { ?>
					<a target="_blank" href="<?php echo $item['link']; ?>" title="<?php echo isset($item['name']) ? $item['name'] : $item['type']; ?>" class="social-icon si-small si-borderless si-rounded si-<?php echo $item['type']; ?>">
						<i class="icon-<?php echo $item['type']; ?> text-white-50"></i>
						<i class="icon-<?php echo $item['type']; ?>"></i>
					</a><?php } ?>
				</div>

			</div>
		</section><!-- #Slider End -->

