		<!-- Footer
		============================================= -->
		<footer id="footer" class="border-0" style="background-image: linear-gradient(to top, #61BFDB, #E0F2C2, #FFF);">

			<div class="position-absolute top-0 start-0 w-100 h-100" style="<?php if (uses('footer-image')) { ?>background: transparent url('<?php echo am_var('url'); ?>assets/images/footer.png<?php echo version();?>') no-repeat center bottom / contain;<?php } ?>"></div>

			<div class="container" style="<?php if (uses('footer-image')) { ?>padding-bottom: 380px;<?php } ?>">
				<div class="footer-widgets-wrap">
					<div class="row col-mb-50">
						<?php if (!run_site_code('footer-content')) { ?>
						<div class="col-lg-1-5 col-4">
							<div class="widget widget_links">

								<h3 class="ls0 mb-3 fw-bold">Features</h3>

								<ul>
									<li><a href="#" class="h-text-color">Help Center</a></li>
									<li><a href="#" class="h-text-color">Paid with Moblie</a></li>
									<li><a href="#" class="h-text-color">Status</a></li>
									<li><a href="#" class="h-text-color">Changelog</a></li>
								</ul>

							</div>
						</div>
						<div class="col-lg-1-5 col-4">
							<div class="widget widget_links">

								<h3 class="ls0 mb-3 fw-bold">Support</h3>

								<ul>
									<li><a href="#" class="h-text-color">Home</a></li>
									<li><a href="#" class="h-text-color">About</a></li>
									<li><a href="#" class="h-text-color">FAQs</a></li>
									<li><a href="#" class="h-text-color">Contact</a></li>
								</ul>

							</div>
						</div>
						<div class="col-lg-1-5 col-4">
							<div class="widget widget_links">

								<h3 class="ls0 mb-3 fw-bold">Trending</h3>

								<ul>
									<li><a href="#" class="h-text-color">Shop</a></li>
									<li><a href="#" class="h-text-color">Portfolio</a></li>
									<li><a href="#" class="h-text-color">Blog</a></li>
									<li><a href="#" class="h-text-color">Events</a></li>
								</ul>

							</div>
						</div>
						<div class="col-lg-1-5 col-6">
							<div class="widget">
								<h3 class="ls0 mb-3 fw-bold">Open Hours</h3>

								<ul class="list-unstyled iconlist ms-0">
									<li class="mb-2"><a href="#" class="text-dark h-text-color">Monday - Saturday</a></li>
									<li class="mb-2"><a href="#" class="text-dark h-text-color">6:30 - 20:00</a></li>
									<li class="mb-2"><a href="#" class="text-dark h-text-color">Sunday Closed</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-1-5 col-6">
							<div class="widget clearfix">

								<h3 class="ls0 mb-3 fw-bold">Contact</h3>

								<div>
									<address class="font-body text-dark mb-3">
										795 Folsom Ave, Suite 600<br>
										San Francisco, CA 94107<br>
									</address>
									<div class="mb-1"><i class="h6 icon-line-phone-call me-2">:</i> <a class="text-dark h-text-color" href="tel:08547632521">+(0) 8547 632521</a></div>
									<div><i class="h6 icon-whatsapp me-2">:</i> <a class="text-dark h-text-color" href="https://wa.me/01147521433">+(0) 11 4752 1433</a></div>
								</div>

							</div>
						</div><?php } ?>
					</div>

				</div>
			</div>
			<div class="col-auto text-center mt-4 text-smaller pb-3 font-primary">
				&copy; Copyright <strong><span><?php echo am_var('name'); ?></span></strong>. <?php echo (am_var('start_year') ? am_var('start_year') . ' - ' : '') . date('Y'); ?> All Rights Reserved
				<br /><?php _credits(); ?>
			</div>

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-hand-up rounded-circle"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="<?php echo $theme; ?>assets/js/jquery.js"></script>
	<script src="<?php echo $theme; ?>assets/js/plugins.min.js"></script>

	<!-- Include Date Range Picker -->
	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?php echo $theme; ?>assets/js/functions.js"></script>

	<script>
		let carousels = jQuery('#kindergarten-carousel-img, #kindergarten-carousel-text');
		jQuery('.carousel-control-prev').on( 'click', function(){
			carousels.carousel('prev');
		});
		jQuery('.carousel-control-next').on( 'click', function(){
			carousels.carousel('next');
		});

		jQuery(function() {
			jQuery(".component-flatpickr").flatpickr({
				enableTime: true,
				dateFormat: "d/m/yy - H:i",
			});
		});
	</script>

</body>
</html>

