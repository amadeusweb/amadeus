<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="<?php echo am_var('name'); ?>" />

	<title><?php title(); ?></title>
	<?php seo_tags(); ?>
	<link href="<?php echo am_var('url'); ?>icon-<?php echo am_var('safeName'); ?>.png" rel="icon" />
	<!--NB: Purchased for Amadeus by services@cselian.com from - http://themes.semicolonweb.com/html/canvas/demo-kindergarten.html -->

	<!-- Stylesheets
	============================================= -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/swiper.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/magnific-popup.css" type="text/css" />

	<!-- Date & Time Picker CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

	<!-- Kindergarten Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/kindergarten.css" type="text/css" /> <!-- Kindergarten Custom Css -->
	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/fonts.css" type="text/css" /> <!-- Kindergarten Custom Fonts -->
	<!-- / -->

	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="stylesheet" href="<?php echo $theme; ?>assets/css/colors.php?color=6953A8" type="text/css" />
	<?php styles_and_scripts(); ?>
	<?php head_hooks(); ?>
</head>

<body class="stretched">

	<?php if (file_exists(am_var('path') . '/code/head.php')) include_once am_var('path') . '/code/head.php'; ?>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header">
			<div id="header-wrap">
				<div class="container">
					<div class="header-row justify-content-lg-between">

						<!-- Logo
						============================================= -->
						<div id="logo" class="me-lg-0 col-lg-3">
							<a class="standard-logo" href="<?php echo am_var('url');?>"><img src="<?php echo am_var('url'); ?>logo-<?php echo am_var('safeName') . am_var('safeFolder'); ?>.png" alt="<?php echo am_var('name'); ?>"></a>
							<a class="retina-logo" href="<?php echo am_var('url');?>"><img src="<?php echo am_var('url'); ?>logo-<?php echo am_var('safeName') . am_var('safeFolder'); ?>@2x.png" alt="<?php echo am_var('name'); ?>"></a>
						</div><!-- #logo end -->

						<div class="col-lg-3 justify-content-end">
							<!-- WhatsApp Icon
							============================================= -->
							<a href="https://wa.me/<?php echo str_replace('-', '', am_sub_var('contact', 'whatsapp')); ?>" target="_blank" class="Whatsapp-icon d-flex align-items-center me-2 text-dark h-text-color font-primary fw-bold h5 mb-0">
								<i class="icon-whatsapp me-2"></i> <?php echo am_sub_var('contact', 'whatsapp'); ?>
							</a> <?php echo am_sub_var('contact', 'phone-timings'); ?>
						</div>


						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<nav class="primary-menu">

							<ul class="menu-container">
								<?php if (am_var('node') == '_demo') include "menu.html"; else if (file_exists(am_var('path') . '/code/menu.php')) include_once am_var('path') . '/code/menu.php'; else menu(); ?>
							</ul>

						</nav><!-- #primary-menu end -->

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->
