<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<title><?php title();?></title>
	<link rel="icon" href="<?php echo am_var('url'); ?>icon-<?php echo am_var('safeName'); ?>.png" sizes="192x192" />
	<?php seo_tags(); ?>
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" /><!--WEBXR FIX-->
	<?php styles_and_scripts(); ?>
	<link href="<?php echo $theme; ?>site.css" rel="stylesheet">
	<?php head_hooks(); ?>
</head>
<body class="theme-<?php echo_if_var('theme'); chat_class_on_body();?>">

<?php if (!uses('no-header')) { ?>
<header id="header">
	<div class="container">
		<div class="logo float-left">
			<h1 class="text-light"><a href="<?php echo am_var('url');?>"><?php echo sprintf('<img src="%s%s-logo.png" alt="%s" class="img-max-300 img-fluid" /> ', am_var('url'), am_var('safeName'), am_var('safeName')); ?></a></h1>
		</div>
		
		<?php if (!uses('no-menu')) { ?>
		<nav class="main-nav float-right">
			<?php if (!runCode('menu')) menu(); ?>
		</nav><!-- .main-nav -->
		<?php } ?>
	</div>
</header>
<?php } ?>
<?php if (!uses('custom-wrapper-html')) { ?>
<hr />
<main id="main">
  <section>
    <div class="container">
<?php
}

