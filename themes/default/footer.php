<?php if (!uses('custom-wrapper-html')) { ?>
    </div>
  </section>
</main>
<?php } ?>

<?php if (!uses('no-footer')) { ?>
<?php runCode('footer-content'); ?>
<hr />
<?php copyright_and_credits(); ?>
</body>
</html>
<?php
}
