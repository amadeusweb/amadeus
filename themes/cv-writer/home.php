		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element include-header" style="background:#1E232A; height: 800px;">
			<div class="move-bg position-absolute w-100 h-100" style="top: 0; left: 0;background: url('demos/writer/images/hero/hero.jpg') center center no-repeat; background-size: cover;"></div>
			<div class="vertical-middle ignore-header">
				<div class="container dark py-5">
					<div class="row">
						<div class="col-lg-6 offset-lg-1 col-md-8" data-lightbox="gallery">
							<?php foreach ($featured as $item) {?>
								<a href="<?php item_r('image', $item); ?>" data-lightbox="gallery-item" class="slider-book-img" data-animate="fadeInUp" data-delay="300"><img src="<?php item_r('image', $item); ?>" alt="<?php item_r('name', $item); ?>"></a>
							<?php }?>
							<div class="emphasis-title bottommargin-sm">
								<h1 class="400" data-animate="fadeInUp" data-delay="600">Latest released by<br><span><em><?php echo am_var('name'); ?></em></span>.</h1>
							</div>
							<a data-animate="fadeIn" class="button button-large button-border button-white button-light button-rounded text-capitalize mt-0" href="tel:<?php echo am_var('phone'); ?>"><i class="icon-phone"></i>Call and Get it Now</a><br class="d-block d-sm-none">
						</div>
					</div>
				</div>
			</div>

		</section>

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap pb-0" style="background-color: #1E232A;">

				<!-- Latest All Books
				============================================= -->
				<div class="section p-0 m-0" style="background: url('demos/writer/images/section/bg1.jpg') center center no-repeat; background-size: cover;">

					<div class="container clearfix">

						<div class="row clearfix">
							<div class="col-lg-6">
								<div class="heading-block dark text-start border-bottom-0">
									<h3 class="nott fw-normal ls0" style="font-size: 36px;">Latest Books</h3>
								</div>
							</div>
						</div>

						<div id="portfolio" class="portfolio row grid-container gutter-50">

							<?php foreach ($books as $item) {?>
							<div class="portfolio-item col-12 col-sm-6 col-md-4 mb-3 mb-sm-0">
								<div class="book-wrap">
									<div class="book-card">
										<a href="<?php item_r('ajax', $item); ?>" class="item-quick-view book-image" data-lightbox="ajax"><img src="<?php item_r('image', $item); ?>" alt="<?php item_r('name', $item); ?>"></a>
										<div class="book-detail">
											<h2 class="book-title"><a href="<?php item_r('link', $item); ?>"><?php item_r('name', $item); ?></a></h2>
											<small class="book-category"><a href="<?php item_r('link', $item); ?>"><?php item_r('price', $item); ?></a></small>
											<div class="button button-white button-light text-capitalize button-circle"><i class="icon-line-zoom-in"></i><span>Quick View</span></div>
										</div>
									</div>
								</div>
							</div>
							<?php }?>

						</div>

						<div class="center"><a href="<?php echo am_var('url');?>catalogue/" class="button button-large button-white button-light text-capitalize topmargin-lg bottommargin button-rounded">View all Books</a></div>
					</div>

				</div>

			</div>
		</section><!-- #content end -->
