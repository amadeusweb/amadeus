<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Amadeus CMS, amadeusweb.com/docs/ by Imran Ali Namazi" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title><?php title(); ?></title>
<?php seo_tags(); $homeUrl = homeUrl(); $logoRel =  logoRel(); ?>
	<link href="<?php echo am_var('url'); ?><?php echo $logoRel; ?>-icon.png" rel="icon" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i%7CLato:300,400,600,700%7CParisienne&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>one-page/onepage.css" type="text/css" />

	<!-- Writer Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme;?>demos/writer/writer.css" type="text/css" />
	<!-- / -->

	<link rel="stylesheet" href="<?php echo $theme;?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>one-page/css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/magnific-popup.css" type="text/css" />

	<!-- Writer Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo $theme;?>css/colors.php?color=<?php echo am_var_or('theme-font-color', 'FFC013');?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>demos/writer/css/fonts.css" type="text/css" />
	<!-- / -->

	<link rel="stylesheet" href="<?php echo $theme;?>css/custom.css" type="text/css" />
	<?php head_hooks(); ?>

</head>

<body class="<?php if (!am_var('footer-not-sticky')) echo 'sticky-footer ';?>stretched <?php echo 'node-' . am_var('node'); ?>">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix theme-<?php echo_if_var('theme'); ?>">

		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header dark header-size-custom" data-sticky-shrink="false" data-sticky-class="semi-transparent">
			<div id="header-wrap">
				<div class="container">
					<div class="header-row justify-content-lg-between">

						<!-- Logo
						============================================= -->
						<div id="logo" class="col-auto me-lg-0 order-lg-2">
							<a href="<?php echo $homeUrl; ?>" class="standard-logo"><?php if (am_var('image-in-logo')) { ?><img src="<?php echo am_var('url'); ?><?php echo $logoRel; ?>-logo.png" alt="<?php echo am_var('name'); ?>" /><?php } else { echo am_var('name'); } ?></a>
							<a href="<?php echo $homeUrl; ?>" class="retina-logo"><?php if (am_var('image-in-logo')) { ?><img src="<?php echo am_var('url'); ?><?php echo $logoRel; ?>-logo@2x.png" alt="<?php echo am_var('name'); ?>" /><?php } else { echo am_var('name'); } ?></a>
						</div><!-- #logo end -->

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu not-dark col-lg-auto order-lg-1">

							<ul class="menu-container">
								<?php
								am_var('site-menu-settings', [
									'no-outer-ul' => true,
									'outer-ul-class' => 'menu-container',
									'ul-class' => 'menu-container',
									'li-class' => 'menu-item',
									'li-active-class' => 'current',
									'a-class' => 'menu-link',
								]);
								if (!runCode('menu')) menu();
								?>
							</ul><!-- /end .menu-container -->

						</nav>

						<nav class="primary-menu not-dark col-lg-auto order-lg-3">

							<ul class="menu-container justify-content-lg-end">
								<?php runCode('optional-menu-aux');
								am_var('site-menu-settings', false); ?>
							</ul><!-- /end aux .menu-container -->

						</nav><!-- #primary-menu end -->

					</div>
				</div>
				<?php echo_if_var('group-links'); ?>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->

<?php runCode('header-content'); ?>

		<!-- Content
		============================================= -->
		<section id="content" style="<?php echo uses('content-background') ? 'background: url(\'' . $theme . 'demos/writer/images/section/bg2.jpg\') center center no-repeat; background-size: cover; padding: 100px 0;' : ''; ?>">

