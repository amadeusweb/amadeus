<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Amadeus CMS, amadeusweb.com/docs/ by Imran Ali Namazi" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/swiper.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $theme;?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $theme;?>css/custom.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title><?php title(); ?></title>
	<?php seo_tags(); ?>
	<link href="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-icon.png" rel="icon" />
	<?php head_hooks(); ?>
</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header transparent-header" data-sticky-class="not-dark">
			<div id="header-wrap">
				<div class="container">
					<div class="header-row">

						<!-- Logo
						============================================= -->
						<div id="logo">
							<a href="<?php echo am_var('url');?>" class="standard-logo" data-dark-logo="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo-dark.png"><img src="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo.png" alt="<?php echo am_var('name'); ?>"></a>
							<a href="<?php echo am_var('url');?>" class="retina-logo" data-dark-logo="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo-dark@2x.png"><img src="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo@2x.png" alt="<?php echo am_var('name'); ?>"></a>
						</div><!-- #logo end -->

						<?php runCode('optional-head-misc'); ?>

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu">

							<ul class="menu-container">
								<?php if (!runCode('menu')) menu(); ?>
							</ul>

						</nav><!-- #primary-menu end -->

						<form class="top-search-form" action="search.html" method="get">
							<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
						</form>

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->

		<?php runCode('head'); ?>

		<!-- Content
		============================================= -->
		<section id="content">
