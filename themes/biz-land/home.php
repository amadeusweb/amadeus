<?php if (!isset($home)) $home = am_var('home'); ?>
<?php
function get_heading($what) {
	if (am_var('no-home-headings')) return;
	echo '<div class="section-title"><h2 class="assistant-heading">' . humanize($what) . '</h2></div>';
}
?>
<?php foreach ($home->sections['hero'] as $item) {?>
<!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center<?php echo uses('custom-image-background') ? ' custom-image-background' : ''; ?>">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <h1><?php item_r('title', $item); ?></h1>
      <h2><?php item_r('content', $item); ?></h2>
      <div class="d-flex">
        <a href="<?php item_r('link', $item); ?>" target="_blank" class="btn-get-started scrollto"><?php item_r('linkText', $item); ?></a>
        <a href="<?php item_r('link2', $item); ?>" class="venobox btn-watch-video" data-vbtype="video" data-autoplay="true"><?php item_r('link2Text', $item); ?> <i class="icofont-play-alt-2"></i></a>
      </div>
    </div>
  </section><!-- End Hero --><?php } ?>

  <main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <?php get_heading('featured-services');?>
      <div class="container" data-aos="fade-up">
        <?php if (am_var('banner') == 'featured-services') echo '<div class="bannner"><img src="'.am_var('url') . am_var('safeName') . '-banner.jpg" alt="featured services" /></div>'; ?>
        <div class="row"><?php foreach ($home->sections['featured'] as $item) {?>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="<?php item_r_or('css', $item, 'bx bxl-dribbble'); ?>"></i></div>
              <h4 class="title"><a href="<?php item_r('link', $item); ?>"><?php item_r('title', $item); ?></a></h4>
              <p class="description"><?php item_r('content', $item); ?></p>
            </div>
          </div>

        <?php } ?>
        </div><!-- /row -->

      </div>
    </section><!-- End Featured Services Section -->

<?php if (!am_var('community-person')) { ?>

<?php if (isset($home->sections['abouts'])) { ?>
    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2 class="assistant-heading">About</h2>
          <?php foreach ($home->sections['about1'] as $item) {?>
          <h3><?php item_r('title', $item); ?></h3>
          <p><?php item_r('content', $item); ?></p>
          <?php } ?>
        </div>

        <div class="row">
          <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
            <img src="<?php echo am_var('url'); ?>assets/about-background.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
            <?php foreach ($home->sections['about2'] as $item) {?>
            <h3><?php item_r('title', $item); ?></h3>
            <p class="font-italic"><?php item_r('content', $item); ?></p>
            <?php } ?>
            <ul>
              <?php foreach ($home->sections['abouts'] as $item) {?>
              <li>
                <i class="<?php item_r_or('css', $item, 'bx bx-store-alt'); ?>"></i>
                <div>
                  <h5><?php item_r('title', $item); ?></h5>
                  <p><?php item_r('content', $item); ?></p>
                </div>
              </li>
              <?php } ?>
            </ul>
            <?php foreach ($home->sections['about_bottom'] as $item) {?>
            <p><?php item_r('content', $item); ?></p>
            <?php } ?>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->
<?php } ?>

<?php if (isset($home->sections['skills'])) { ?>
    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills">
      <?php get_heading('skills');?>
      <div class="container" data-aos="fade-up">

        <div class="row skills-content">

          <div class="col-lg-6">

            <?php foreach ($home->sections['skills'] as $item) {?>
            <div class="progress">
              <span class="skill"><?php item_r('title', $item); ?> <i class="val"><?php item_r('content', $item); ?>%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="<?php item_r('content', $item); ?>" aria-valuemin="0" aria-valuemax="100" title="<?php item_r('title', $item); ?>"></div>
              </div>
            </div>
            <?php } ?>

          </div>

        </div>

      </div>
    </section><!-- End Skills Section -->
<?php } ?>

<?php if (isset($home->sections['counts'])) { ?>
    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
      <?php get_heading('counts');?>
      <div class="container" data-aos="fade-up" style="padding-top: 30px">

        <div class="row">

          <?php foreach ($home->sections['counts'] as $item) {?>
          <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
            <div class="count-box">
              <i class="<?php item_r('css', $item); ?>"></i>
              <span data-toggle="counter-up"><?php item_r('content', $item); ?></span>
              <p><?php item_r('title', $item); ?></p>
            </div>
          </div>
          <?php } ?>

        </div>

      </div>
    </section><!-- End Counts Section -->
<?php } ?>

<?php if (isset($clients)) {?>
    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients section-bg">
      <div class="container" data-aos="zoom-in">

        <div class="section-title">
          <h2 class="assistant-heading">Clients</h2>
          <h3><a href="<?php echo am_var('url'); ?>showcase/">Showcase</a></h3>
          <p>See how our projects went and what our clients had to say.</p>
        </div>

        <div class="row">

<?php foreach ($clients as $client) { ?>
          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="<?php echo $client['link']; ?>" title="<?php echo $client['title']; ?>" target="_blank"><img src="<?php echo am_var('url'); ?>assets/clients/icon-<?php echo $client['slug']; ?>.png" class="img-fluid" alt="<?php echo $client['name']; ?>" width="150" /></a>
          </div>
<?php } ?>

        </div>

      </div>
    </section><!-- End Clients Section -->
<?php } ?>

<?php if (isset($home->sections['services'])) { ?>
    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <?php get_heading('services');?>
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <?php foreach ($home->sections['services_top'] as $item) {?>
          <h2><?php item_r('content2', $item); ?></h2>
          <h3><?php item_r('title', $item); ?></h3>
          <p><?php item_r('content', $item); ?></p>
          <?php } ?>
        </div>

        <div class="row">
          <?php foreach ($home->sections['services'] as $item) {?>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="<?php item_r('css', $item); ?>"></i></div>
              <h4><a href="<?php item_r('link', $item); ?>"><?php item_r('title', $item); ?></a></h4>
              <p><?php item_r('content', $item); ?></p>
            </div>
          </div><?php } ?>
        </div>

      </div>
    </section><!-- End Services Section -->
<?php } ?>

<?php $testimonials = false && $home->sections['testimonials'];
//TODO: this
if ($testimonials) {?>
    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <?php get_heading('testimonials');?>
      <div class="container" data-aos="zoom-in">

        <div class="owl-carousel testimonials-carousel">

<?php foreach ($testimonials as $person) { ?>
          <div class="testimonial-item">
            <img src="assets/testimonials/<?php echo $person['file']; ?>" class="testimonial-img" alt="<?php echo $person['name']; ?>">
            <h3><?php echo $person['name']; ?></h3>
            <h4><?php echo $person['role']; ?></h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php echo $person['about']; ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>
<?php } ?>
        </div>

      </div>
    </section><!-- End Testimonials Section -->
<?php } ?>

<!-- TODO: Add back portfolio / use floating blurbs from AM2 -->

    <?php if (isset($home->sections['team'])) {?>
    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <?php get_heading('team');?>
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <?php foreach ($home->sections['team_top'] as $item) {?>
          <h2>Services</h2>
          <h3><?php item_r('title', $item); ?></h3>
          <p><?php item_r('content', $item); ?></p>
          <?php } ?>
        </div>


        <div class="row">
<?php foreach ($home->sections['team'] as $item) {?>
          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="member">
              <div class="member-img">
                <img src="assets/team/<?php echo urlize($item[0]); ?>.jpg" class="img-fluid" alt="<?php item_r('title', $item); ?>">
              </div>
              <div class="member-info">
                <h4><?php item_r('title', $item); ?></h4>
                <span><?php item_r('content2', $item); ?></span>
                <p><?php item_r('content', $item); ?></p>
              </div>
            </div>
          </div>
<?php } ?>

        </div>

      </div>
    </section><!-- End Team Section -->
    <?php } ?>

    <?php if (isset($home->sections['faqs'])) {?>
    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <?php foreach ($home->sections['faqs_top'] as $item) {?>
          <h2 class="assistant-heading">F.A.Q</h2>
          <h3><?php item_r('title', $item); ?></h3>
          <p><?php item_r('content', $item); ?></p>
          <?php } ?>
        </div>

        <div class="row justify-content-center">
          <div class="col-xl-10">
            <ul class="faq-list" data-aos="fade-up" data-aos-delay="100">

              <?php foreach ($home->sections['faqs'] as $ix => $item) {
              $id = 'question' . ($ix + 1); ?>
              <li>
                <a data-toggle="collapse" class="" href="#<?php echo $id;?>"><?php item_r('title', $item); ?><i class="icofont-simple-down"></i></a>
                <div id="<?php echo $id;?>" class="collapse" data-parent=".faq-list">
                  <p><?php item_r('content', $item); ?></p>
                </div>
              </li>
              <?php } ?>

            </ul>
          </div>
        </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->
    <?php } ?>

<?php if (!am_var('no-contact')) { ?>
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2 class="assistant-heading">Contact</h2>
          <h3><span>Contact Us</span></h3>
          <p><?php echo am_var('contact_intro') ?></p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-6">
            <div class="info-box mb-4">
              <i class="bx bx-map"></i>
              <h3>Our Address</h3>
                <p><?php echo am_var('address') ?></p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-envelope"></i>
              <h3>Email Us</h3>
                <p><a target="_blank" href="mailto:<?php echo am_var('email') ?>"><?php echo am_var('email') ?></a></p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-phone-call"></i>
              <h3>Call Us</h3>
                <p><a href="tel:<?php echo am_var('phone') ?>"><?php echo am_var('phone') ?></a></p>
            </div>
          </div>

        </div>
<?php } ?>

<?php if (uses('contact-form')) { ?>
        <div class="row" data-aos="fade-up" data-aos-delay="100">

          <div class="col-lg-6 ">
            <iframe class="mb-4 mb-lg-0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>
          </div>

          <div class="col-lg-6">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="col form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="col form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>

        </div><?php } ?>

      </div>
    </section><!-- End Contact Section -->

<?php } //community person ?>

  </main><!-- End #main -->
