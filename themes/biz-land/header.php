<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?php title(); ?></title>
<?php seo_tags(); $logoUrl = logoUrl(); $logoRel = logoRel(); ?>
  <link href="<?php echo $logoUrl; ?><?php echo_if_var('siteImgPrefix'); ?><?php echo $logoRel; ?>-icon.png<?php echo version(); ?>" rel="icon" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CRoboto:300,300i,400,400i,500,500i,600,600i,700,700i%7CPoppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo $theme; ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/icofont/icofont.min.css?fver=2" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/boxicons/css/boxicons.min.css?fver=2" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo $theme; ?>assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo $theme; ?>assets/css/style.css" rel="stylesheet">
  <?php head_hooks(); ?>

  <!-- =======================================================
  * Template Name: BizLand - v2.0.0
  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body class="theme-<?php echo_if_var('theme'); chat_class_on_body();?>">
  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="<?php echo uses('always-topbar') ? 'd-flex position-fixed' : 'd-none d-lg-flex'; ?> align-items-center fixed-top">
    <div class="container d-flex">
      <?php if(!am_var('no-contact-info')) {?><div class="contact-info mr-auto">
        <?php echo_if_var('group-links'); ?>
        <i class="icofont-envelope"></i> <a href="mailto:<?php echo am_var('email'); ?>">email</a>
        <i class="icofont-phone"></i> <a href="tel:<?php echo am_var('phone'); ?>">phone</a><?php if (am_var('phone2')) { ?>
        <i class="icofont-phone"></i> <a href="tel:<?php echo am_var('phone2'); ?>">second phone</a><?php } ?>
      </div><?php } ?>
      <div class="social-links"><?php foreach(am_var_or('social', []) as $item) { ?>
        <a target="_blank" href="<?php echo $item['link']; ?>" title="<?php echo isset($item['name']) ? $item['name'] : $item['type']; ?>" class="<?php echo $item['type']; ?>"><i class="icofont-<?php echo $item['type']; ?>"></i></a><?php } ?>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center network-links">
      <h1 class="logo mr-auto"><?php if (has_var('network-home')) echo '<a class="site-button site-network-home-bgd" href="' . am_var('network-url'). '"><span class="icofont-1x icofont-home"></span></a>';
      ?><a class="site-button site-<?php echo am_var('safeName'); ?>-bgd" href="<?php echo am_var('url'); ?>">
        <?php echo am_var('image-in-logo') ? '<img class="menu-network-icon" height="32" src="' . $logoUrl . (am_var('siteImgPrefix') ? am_var('siteImgPrefix') : '')
          . am_var('safeName') . am_var('image-in-logo') . version() . '" alt="' . am_var('name') . '" />'
          . (am_var('image-in-logo') == '-icon.png' ? ' ' . am_var('name') : '') :  am_var('name'); ?></a></h1>

      <nav class="nav-menu d-none d-lg-block">
        <?php if (!runCode('menu')) menu(); ?>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

<?php if (uses('always-topbar')) { ?>
<style>
.mobile-nav-toggle { top: 65px!important; }
#header { padding: 25px 0!important; top: 40px!important; }
#topbar.topbar-scrolled { top: 0; }
.above-header-content { margin-top: 170px!important; }
</style>
<?php } ?>
