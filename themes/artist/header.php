<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<title><?php title();?></title>
	<link rel="icon" href="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-icon.png<?php echo version(); ?>" sizes="192x192" />
	<?php seo_tags(); ?>
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" /><!--WEBXR FIX-->
	<link href="<?php echo $theme; ?>site.css<?php echo version(); ?>" rel="stylesheet" />
	<link href="<?php echo $theme; ?>styles.css<?php echo version(); ?>" rel="stylesheet" />
	<link href="<?php echo $theme; ?>icons.css<?php echo version(); ?>" rel="stylesheet" />
	<link href="<?php echo $theme; ?>../biz-land/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<?php styles_and_scripts(); ?>
	<?php head_hooks(); ?>
</head>
<body class="<?php body_classes();?>">

<div id="wrapper">
	<div id="main">
		<div class="inner">
<?php if (am_var('logo-above-content')) {
	sectionId('header', 'content-box');
	echo concatSlugs(['<a href="', am_var('url'), '"><img src="', am_var('url'), am_var('safeName'), '-logo@2x.png" class="img-fluid img-max-',
		am_var_or('footer-logo-max-width', '500'), '" alt="', am_var('name'), '" /></a><br />'], '');
	//TODO: add menu too
	section('end');
} ?>
<!--header.php ends-->
