<!--content ends, footer starts-->
<?php runCode('optional-footer-content'); ?>
<div id="container06" class="container default style2">
	<div class="wrapper">
		<div class="inner">
			<u><?php echo am_var('name'); ?></u> &mdash;
			<?php if (am_var('footer-message')) { renderSingleLineMarkdown(am_var('footer-message')); echo '<hr />' . am_var('nl'); } ?>
			<ul id="icons021" class="style3 icons social-links"><?php foreach(am_var('social') as $item) { ?>
				<li><a class="n01" target="_blank" href="<?php echo replaceHtml($item['link']); ?>" title="<?php echo isset($item['name']) ? $item['name'] : $item['type']; ?>"
					class="<?php echo $item['type']; ?>"><i class="bi bi-<?php echo $item['type']; ?>"></i></a></li><?php } ?>
			</ul>
			<hr />
			<?php includeFeature('share'); ?>
			<hr />
			<p id="text09" class="style5"><?php copyright_and_credits(); ?></p>
		</div>
	</div>
</div>
</section>

		</div>
	</div>
</div>

<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 40" display="none" width="0" height="0">
	<symbol id="icon-next-section" viewBox="0 0 40 40"><path d="M19.8,34.2c-0.3,0-0.3-0.1-0.4-0.1L6.6,21.4c-0.1-0.1-0.1-0.1-0.1-0.4c0-0.3,0.1-0.3,0.1-0.4l1.5-1.5 C8.2,19.1,8.2,19,8.5,19s0.3,0.1,0.4,0.1l9.2,9.2V5.9c0-0.1,0-0.2,0-0.2c0,0,0,0,0,0c0.1-0.1,0.2-0.1,0.4-0.1H21 c0.2,0,0.4,0.1,0.5,0.1c0,0,0,0.1,0,0.2v22.4l9.2-9.2c0.1-0.1,0.1-0.1,0.4-0.1c0.3,0,0.3,0.1,0.4,0.1l1.5,1.5 c0.1,0.1,0.1,0.1,0.1,0.4c0,0.3-0.1,0.3-0.1,0.4L20.2,34.1C20.1,34.1,20.1,34.2,19.8,34.2z"></path></symbol>
</svg>

	<!-- Template Main JS File -->
	<?php if (am_var('rich-page')) {?>
	<script src="<?php echo $theme; ?>scrolling-sections.js<?php echo version(); ?>"></script><?php } ?>
	<script src="<?php echo $theme; ?>../biz-land/assets/vendor/jquery/jquery.min.js"></script>
	<script src="<?php echo $theme; ?>../biz-land/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<?php foot_hooks(); ?>
</body>
</html>
