<?php $imageUrl = am_var_or('images-url', ($baseUrl = am_var('url')) . 'assets/home/'); ?>
<div id="container07" class="style3 container default full screen" style="background-image: url('data:image/svg+xml;charset=utf8,%3Csvg%20viewBox%3D%220%200%20512%20512%22%20width%3D%22512%22%20height%3D%22512%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%20%3Cfilter%20id%3D%22noise%22%3E%20%3CfeTurbulence%20type%3D%22fractalNoise%22%20baseFrequency%3D%220.875%22%20result%3D%22noise%22%20%2F%3E%20%3CfeColorMatrix%20type%3D%22matrix%22%20values%3D%220.09765625%200%200%200%200%200%200.09765625%200%200%200%200%200%200.09765625%200%200%200%200%200%200.41015625%200%22%20%2F%3E%20%3C%2Ffilter%3E%20%3Crect%20filter%3D%22url%28%23noise%29%22%20x%3D%220%22%20y%3D%220%22%20width%3D%22512%22%20height%3D%22512%22%20fill%3D%22transparent%22%20opacity%3D%221%22%20%2F%3E%3C%2Fsvg%3E'), linear-gradient(to top, rgba(0,0,0,0.4), rgba(0,0,0,0.4)), url('<?php echo $imageUrl; get_page_value('main', 'image'); ?>');">
	<div class="wrapper">
		<div class="inner">
			<h1 id="text17" class="style6"><?php get_page_value('main', 'initials'); ?></h1>
			<h2 id="text18" class="style3">
				<strong><?php get_page_value('main', 'name'); ?></strong>
			</h2>
			<p id="text13"><?php get_page_value('main', 'service'); ?></p>
			<p id="text21" class="style4"><?php get_page_value('main', 'tagline'); ?></p>
			<ul id="icons03" class="style1 icons">
				<li>
					<a class="n01" href="#about" aria-label="Arrow Down">
						<svg>
							<use xlink:href="#icon-next-section"></use>
						</svg>
						<span class="label">Arrow Down</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div id="container04" data-scroll-id="about" data-scroll-behavior="center" data-scroll-offset="0"
	data-scroll-speed="3" class="style2 container columns">
	<div class="wrapper">
		<div class="inner">
			<div>
				<h3 id="text10" class="style1">About</h3>
				<p id="text11" class="style2">
					<span class="p"><?php get_page_value('about', 'content'); ?></span>
					<span class="p"><?php get_page_value('about', 'content2'); ?></span>
				</p>
				<ul id="icons04" class="style1 icons">
					<li>
						<a class="n01" href="#services" aria-label="Arrow Down">
							<svg>
								<use xlink:href="#icon-next-section"></use>
							</svg>
							<span class="label">Arrow Down</span>
						</a>
					</li>
				</ul>
			</div>
			<div>
				<div id="image04" class="style1 image full">
					<a href="#services" class="frame">
						<img src="<?php echo $imageUrl; get_page_value('about', 'image'); ?>" alt="about" />
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="container03" data-scroll-id="services" data-scroll-behavior="default" data-scroll-offset="0"
	data-scroll-speed="3" class="style2 container default">
	<div class="wrapper">
		<div class="inner">
			<h3 id="text01" class="style1">Services</h3>
			<p id="text02" class="style2"><?php get_page_value('services', 'content'); ?></p>
			<div id="image01" class="style2 image full">
				<a href="#consulting" class="frame deferred">
					<img src="<?php echo $imageUrl; get_page_value('services', 'image'); ?>" alt="services" style="background-color: maroon" />
				</a>
			</div>
			<ul id="icons05" class="style1 icons">
				<li>
					<a class="n01" href="#consulting" aria-label="Arrow Down">
						<svg>
							<use xlink:href="#icon-next-section"></use>
						</svg>
						<span class="label">Arrow Down</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div id="container05" data-scroll-id="consulting" data-scroll-behavior="center" data-scroll-offset="0"
	data-scroll-speed="3" class="style2 container columns">
	<div class="wrapper">
		<div class="inner">
			<div>
				<div id="image05" class="style1 image full">
					<a href="#writing" class="frame deferred">
						<img src="<?php echo $imageUrl; get_page_value('consulting', 'image'); ?>" alt="services" style="background-color: navy" />
					</a>
				</div>
			</div>
			<div>
				<h3 id="text12" class="style1">Consultancy</h3>
				<p id="text14" class="style2"><?php get_page_value('consulting', 'content'); ?></p>
				<ul id="icons05" class="style1 icons">
				<li>
					<a class="n01" href="#writing" aria-label="Arrow Down">
						<svg>
							<use xlink:href="#icon-next-section"></use>
						</svg>
						<span class="label">Arrow Down</span>
					</a>
				</li>
			</ul>
			</div>
		</div>
	</div>
</div>
<div id="container01" data-scroll-id="writing" data-scroll-behavior="center" data-scroll-offset="0"
	data-scroll-speed="3" class="style2 container columns">
	<div class="wrapper">
		<div class="inner">
			<div>
				<div id="image06" class="style1 image full">
					<a href="#one" class="frame deferred">
						<img src="<?php echo $imageUrl; get_page_value('writing', 'image'); ?>" alt="writing" />
					</a>
				</div>
			</div>
			<div>
				<h3 id="text15" class="style1"><?php get_page_value('writing', 'title', 'Writing'); ?></h3>
				<p id="text16" class="style2"><?php get_page_value('writing', 'content'); ?></p>
			</div>
		</div>
	</div>
</div>
