$(document).ready(function() {
	const playChar = '&#9658;', playText = '?', stopChar = '&#9209;', settingsChar = '&#9881;';
	var speakIn = $('textarea.form-control');
	if (speakIn.length) $(window).on("unload", function() { $('#cancel').trigger('click'); }); //stop playing and unload on close / navigate away

	function safeScrollWithOffset(element) {
		if (window.amadeusUtils)
			window.amadeusUtils.scrollWithOffset(element);
		else
			element.scrollIntoView();
	}

	$('.goto-assistant-toc').click(function() { safeScrollWithOffset($('#amadeus-assistant-toc')[0]); });
	var useUL = $('#toc-loader').hasClass('ul-not-ol');
	var noVoice = $('#toc-loader').hasClass('no-voice');
	const toc = $(useUL ? '<ul></ul>' : '<ol></ol>').appendTo($('#amadeus-assistant-toc'));

	const items = $('#content section:not(.amadeus-feature):not(.amadeus-silent-feature):not(#engages section)');
	const sections = [];
	const multipleEngages = $('#content .engage').length > 1;

	$.each(items, (idx, itm) => {
		if (itm.innerHTML.includes('id="engages"')) return; //TODO: RCA: fix nested section via a css filter once we find where it's coming from

		var section = $(itm);

		var heading = $('h2:first, .assistant-heading', section);
		if (!heading.length) return;

		if (multipleEngages && heading.is('h2')) return;

		let html = heading.html(), txt = heading.text();
		if (html.includes('"number"')) {
			txt = $($(html).remove('span.number')[1]).text();
		}

		var menu = $('<li class="level-' + heading[0].tagName.toLowerCase() + '"><a class="jump-to-heading" href="javascript: void()">' + txt + '</a>' + (noVoice ? '' :
			'<a href="javascript: void()" style="float: right;" class="btn btn-primary start-playing">' + playChar + '</a></li>'));

		menu.appendTo(toc);
		menu.data('block', section);

		if (!noVoice) {
			section.data('menu', $('.start-playing', menu));
			var reader = section.prepend('<span class="section-reader"><a class="btn btn-primary section-toggle-play" href="javascript: void()">' + playChar + '</a> <a class="btn btn-primary section-control" href="javascript: void()">' + settingsChar + '</a></span>');

			section.data('toggle', $('.section-toggle-play', section));
		}

		sections.push(section);
	});

	$('#amadeus-assistant-toc .jump-to-heading').click(jumpTo);

	function jumpTo(ev) {
		var menu = $(ev.originalEvent.target);
		var section = menu.closest('li').data('block');
		safeScrollWithOffset(section[0]);
		return section;
	}

	$('.section-control').click(jumpToControls);

	function jumpToControls(ev) {
		var controls = $('#assistant');
		safeScrollWithOffset(controls[0]);
	}

	$('#amadeus-assistant-toc .start-playing').click(playBlock);
	
	function playBlock(ev) {
		var section = jumpTo(ev);
		playSection(section);
	};

	$('.section-toggle-play').click(sectionTogglePlay);

	function sectionTogglePlay(ev) {
		var section = $(ev.originalEvent.target).closest('section');
		playSection(section);
	}

	function playSection(section) {
		var menu = section.data('menu');
		var toggle = section.data('toggle');

		var toPause = menu.data('is-playing') == 'yes';
		var newText = toPause ? playChar : stopChar;

		clearSectionPlayStatus();

		menu.data('is-playing', toPause ? 'no' : 'yes');
		menu.html(newText);
		toggle.html(newText);

		stopSpeaking();

		if (!toPause)
			speak('Reading out the section', section, true);
	}

	function stopSpeaking() {
		$('#cancel').trigger('click');
	}
	
	function speak(prelude, section, autoPlay) {
		var content = section.text();
		content = content.substring(4);

		speakIn.val(prelude + "\r\n\r\n" + content);

		$('#speech').show();

		if (autoPlay) $('#start').trigger('click');
	}

	function clearSectionPlayStatus() {
		sections.forEach((section, idx) => {
			var menu = section.data('menu');
			var toggle = section.data('toggle');
	
			menu.html(playChar);
			toggle.html(playChar);
		});
	}

	if (sections.length) $('#toc-loader').remove(); else $('#toc-loader').closest('section').hide();
	$('#speech').hide();
});
