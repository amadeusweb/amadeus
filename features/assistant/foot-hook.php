

    <!-- Amadeus Assistant's Includes --><?php
$ver = '?assistantVersion=2.0.2.2';
$assistantUrl = getFeatureUrl('assistant/');
cssTag($assistantUrl . 'assistant.css' . $ver);
scriptTag($assistantUrl . 'textToSpeech.js' . $ver);
scriptTag($assistantUrl . 'speakable.js' . $ver);
