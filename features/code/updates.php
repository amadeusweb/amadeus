<?php
$where = am_var('node') == 'index' || am_var('node') == 'updates' ? false : (am_var('section') != am_var('node') ? am_var('node') : am_var('section'));

$sheet = get_sheet('updates', 'where');
$cols = $sheet->columns;

$items = $sheet->rows;

if ($where) {
	if (!isset($sheet->sections[$where])) {
		sectionId('updates');
		echo '****<b>No Updates</b> for this page: ' . humanize($where);
		section('end');
		return;
	}

	$items = $sheet->sections[$where];
}

sectionId('updates');
echo '<h2>Updates from: ' . ($where ? humanize($where) : am_var('name')) . '</h2>'; //TODO: proper when node is updates

includeFeature('social'); //NOTE: load on demand!

$social = social_available($cols);
foreach($items as $item) {
	echo replaceItems('<hr /><a name="%sno%"></a>%nl%%nl%'
			. '<a href="%url%updates/#%sno%" target="%safeName%_updates">%sno%</a> &mdash; '
			. '<strong>%title%</strong> %date% posted in: "%where%" - by %source%%brnl%'
			. '<i>%text%</i>%nl%'
			. '%social%',
	[
		'url' => am_var('url'),
		'safeName' => am_var('safeName'),
		'nl' => am_var('nl'),
		'brnl' => am_var('brnl'),
		'sno' => $item[$cols['sno']],
		'date' => $item[$cols['date']] ? 'on' . $item[$cols['date']] . ' - ' : '',
		'where' => $item[$cols['where']] ? humanize($item[$cols['where']]) : 'all',
		'source' => $item[$cols['source']] ? renderSingleLineMarkdown($item[$cols['source']], ['echo' => false]) : '[none]',
		'title' => $item[$cols['title']],
		'text' => $item[$cols['text']],
		'social' => social_links($item, $cols, $social, '<hr style="width: 120px;" />'),
	], '%'). am_var('2nl');
}
section('end');

