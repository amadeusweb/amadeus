<?php
function assistant($what = 'section') {
	if (am_var('no-assistant')) return;

	if ($what == 'section') {
		$no = am_sub_var('node-vars', 'no-yieldmore-icon');
		$h2 = featureHeading('assistant-toc', 'h2-start');
		$nodeOrHome = am_var('node') == 'index' ? am_var('name') : humanize(am_var('node'));
		if ($no) $h2 = str_replace('yieldmore-icon', 'no-yieldmore-icon', $h2);

		$loaderClasses = [];

		if (am_sub_var('node-vars', 'toc-ul') || am_var('ul-not-ol-in-assistant')) $loaderClasses[] = 'ul-not-ol';

		if (am_var('no-voice-in-assistant')) $loaderClasses[] = 'no-voice';

		$loader = count($loaderClasses) ? ' class="' . implode(' ', $loaderClasses) . '"' : '';

		echo '<section id="amadeus-assistant-toc" class="amadeus-feature">' . am_var('nl') . $h2 .
			'		<abbr title="table of contents">TOC</abbr>: ' . am_var('nl') . '		' .
			(($subPage = am_var('page_parameter1')) ? humanize($subPage) . ' &mdash; ' : '') . '"' . $nodeOrHome . '"' . am_var('nl') .
			'		' . featureHeading('assistant', 'link-only') . am_var('nl') . '	</h2>' . am_var('nl') . 
			'	<span id="toc-loader"' . $loader . '>...scanning for headings</span>' . am_var('nl') . '</section>' . am_var('2nl');
	} else if ($what == 'load') {
		add_foot_hook(featurePath('assistant/foot-hook.php'));
		sectionId('assistant', 'amadeus-feature container');
		echo featureHeading('assistant-voice');
		renderPlainHtml(featurePath('assistant/speech-ui.html'));
		echo '<br /><a class="btn btn-primary img-fluid goto-assistant-toc">Go back to <b>Table of Contents</b></a>';
		section('end');
	} else if ($what == 'quick-links') {
		$menu = get_menu_files('quick-links');
		if (!$menu) return;

		$menu[] = '----'; $menu[] = 'links'; //todo - assume BOTH quicklinks and links are there

		sectionId('quick-links', 'amadeus-feature');
		echo _excludeFromGoogleSearch(featureHeading('links')
				. menu('/', ['files' => $menu, 'ul-class' => 'collapsed', 'return' => true])
			);
		section('end');
	} else {
		error('Amadeus Assistant - unknown request: ' . $what);
	}
}
