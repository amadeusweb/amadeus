<?php
/*****
 * version 2.5 - Jan 2025 - shows node items by default
 */

$openInEvents = '';
if ($openIn = (isset($_GET['open-in']) ? $_GET['open-in'] : false)) {
	//for Checker - code.amadeusweb.com/domain-checker/
	echo replaceItems('<html>%eol%<head>%eol%'
			. '	<style type="text/css">%css%</style>%eol%'
			. '	<script type="text/javascript">%script%</script>%eol%'
			. '</head>%eol%<body>%eol%', [
		'eol' => am_var('nl'),
		'css' => '* { font-size: small; } li { padding-left: 8px; margin-bottom: 8px; } ul, ol { margin: 0; padding: 0; list-style-type: none; }',
		'script' => 'function openIn(event) { window.parent.openInParentPanel(event.target, "content"); event.preventDefault(); }',
	], '%');
	$openInEvents = ' onclick="javascript: openIn(event, \'' . $openIn . '\');"';
}
am_var('openInEvents', $openInEvents);

echo '<section>' . am_var('nl');
echo '<h1>' . am_var('siteMenuName') . '</h2>';
menu('/' . am_var('folder'), ['files' => get_menu_files(am_var('folder')), 'innerHtml' => function($file, $params){
	$plImages = isset($_GET['banners']) ? '<hr />' . makePLImages('/assets/pages/' . $file, false) : '';
	return $plImages . getLink(humanize($file), $params['url'], am_var('openInEvents')) . get_seo_for($file);
}]);
echo '</section>' . am_var('2nl');

echo '<section>' . am_var('nl');
echo '<h1>Social Links</h2><ol>';
foreach(am_var('social') as $item) { ?>
	<li><a target="_blank" href="<?php echo $item['link']; ?>" class="no-icon <?php echo $item['type']; ?>"><i class="icofont-<?php echo $item['type']; ?>"></i> <?php echo $item['name']; ?></a></li>
<?php
}
echo '</ol><hr />';
echo '</section>' . am_var('2nl');

foreach (am_var('sections') as $slug) {
	$name = humanize($slug);
	echo '<section>' . am_var('nl');
	echo '<h1>' . $name . '</h1>';
	am_var('thisSection', $slug);
	am_var('hadMenuSection', false);
	menu('/' . $slug . '/', [
		'list-only-folders' => !am_var('sections-have-files'),
		'files' => get_menu_files($slug),
		'innerHtml' => function($file, $params){
			$plImages = isset($_GET['banners']) ? '<hr />' . makePLImages('/assets/pages/' . $file, false) : '';
			$items = get_node_items(am_var('thisSection'), $file);
			am_var('indented', '');
			$hr = am_var('hadMenuSection') ? '' : '<hr />';
			$internal = am_var('safeName') == 'amadeus' || contains(SITEPATH, 'our-sites');
			$div = !$internal ? '' : '<div style="margin: 8px 0 8px 0; border-top: 2px solid #666;"></div>';
			return $hr . $plImages . getLink(humanize($file), $params['url'], am_var('openInEvents')) . get_seo_for($file) . $items . $div;
		}]);
	echo '</section>' . am_var('2nl');
}

if ($openIn) echo '</body></html>' . am_var('2nl');

function get_node_items($section, $node) {
	$relativeFolder = '/' . $section . '/' . $node . '/';
	if (!disk_is_dir(am_var('path'). $relativeFolder)) return '';
	am_var('indented', '');
	return menu($relativeFolder, [ 'files' => get_menu_files($node), 'parent-slug' => $node . '/', 'ul-class' => 'indented', 'return' => true]);
}
