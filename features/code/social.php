<?php

function social_available($cols) {
	$all = ['site', 'external', 'linkedin', 'facebook', 'instagram', 'youtube', 'image'];
	$r = [];
	foreach ($all as $c) if (isset($cols[$c])) $r[] = $c;
	return $r;
}

function social_links($item, $cols, $available, $prefix) {
	$r = [];
	foreach ($available as $social) {
		$link = $item[$cols[$social]];
		if (!$link || $social == 'image') continue;
		$base = $social == 'site' ? am_var('url') : '';
		$r[] = '<a class="icon-' . $social . '" href="' . $base . $link . '" target="_blank">' . $social . '</a>';
	}
	
	$link = $item[$cols['image']];
	if ($link) $r[] = '<a class="icon-image" href="javascript: void();" onclick="debugger; $(\'img\', $(this)).toggle();">image (toggle)'
		. '<img src="' . $link . '"" style="display: none" class="img-max-400" loading="lazy" /></a>';

	return !count($r) ? '' : $prefix . implode(' / ', $r);
}


//TODO: embedAll (dict by type) and embedMixed (type as array item)

function embed($what, $items, $settings = []) {
	$shuffle = valueIfSet($settings, 'shuffle', true);
	$take = valueIfSet($settings, 'take', 1);
	
	if (!is_array($items)) return parameterError('$items - array expected!', $items);

	if ($shuffle) $items = getShuffledItems($items, $take);

	if ($what == 'youtube') {
		embedYoutube($items);
	} else if ($what = 'spotify') {
		embedSpotify($items);
	}
}

function embedYoutube($items) {
	foreach ($items as $id => $title) {
		section(); div('start', $title);
		echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $id . '" title="' . $title . '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>';
		div('end'); section('ends'); 
	}
}

function embedSpotify($items) {
	foreach ($items as $id => $title) {
		_embedSection('start', $title);
		echo '<iframe style="border-radius:12px" src="https://open.spotify.com/embed/' . $id . '?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>';
		_embedSection('end');
	}
}

function _embedSection($where = 'start', $heading = '') {
	if ($where == 'start') section('start', $heading);
	else section('end');
}
 
//TODO: bring in stuff from legacy cns - facebook etc!

?>
