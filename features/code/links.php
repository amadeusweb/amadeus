<?php
/*****
 * Adapted from 2021's resources.php
 ***/

$to = am_var('page_parameter1') ? am_var('page_parameter1') : false;

$sheet = get_sheet('links', 'slug', am_var('multisite'));
$cols = $sheet->columns;

$go = am_var('node') == 'go';

if ($go && isset($sheet->sections[$to])) {
	$link = prepareLinks($sheet->sections[$to][0][$cols['goto']]);
	header('Location: ' . $link);
	exit;
}

if ($go) { renderMarkdown('No link defined, pls visit [our links page](%url%/links/).'); return; }

$cols = $sheet->columns;
section();

echo '<ol>' . am_var('nl');

foreach ($sheet->rows as $item) {
	$text = $item[$cols['text']];

	if ($text == '----') {
		echo '<li><hr /></li>';
		continue;
	}

	$link = $item[$cols['goto']];
	echo prepareLinks(sprintf('<li><a href="%s">%s</a> &mdash; ', $link, $text) . am_var('nl'));
	echo sprintf('<a href="%s">%s</a></li>', am_var('url') . 'go/' . $item[$cols['slug']] . '/', 'shortlink') . am_var('2nl');
}

echo '</ol>';

section('end');

