<?php
/***
 * promote variable if a csv string will let it show randomly one each in that order
 * if an array, each category key has whitelist csv as it's value and one is again rendered randomly
 * if .html, it is rendered as a slide
 * if -engage.md a form always open so no button
 ***/

if (true || am_var('no-promotions')) return;

echo '<div id="promote">' . am_var('nl');

echo '<section class="amadeus-feature">' . am_var('nl');
echo featureH2Start('promote') . makeLink(am_var('promotionsSiteName'), am_var('promotionsUrl') . '#utm', false) . '</h2>' . am_var('nl');
echo '</section>';

$wantedFol = isset($_GET['fol']) ? $_GET['fol'] : am_var('promoteSection');
$wantedName = isset($_GET['name']) ? $_GET['name'] : am_var('promoteName');

$suffix = am_var('promotionsSite') ? '' : ' &mdash; <span class="yieldmore-icon">Promotion</span>';
$promotionsUrl = am_var('promotionsUrl');

foreach ($promote as $fol => $files) {
	if (is_numeric($fol)) {
		$fol = $files;
		$files = scandir(PROMOTIONSPATH . '/' . $files);
	} else if (is_string($files)) {
		$files = explode(', ', $files);
	}

	if ($wantedFol && $wantedFol != $fol) continue;

	echo featureH2Start('promote-what-is') . makeLink(humanize($fol) . $suffix, $promotionsUrl . $fol . '/#utm', false) . '</h2>' . am_var('nl');

	//. and .. dont appear on mobile
	if ($files[0] == '.') unset($files[0]);
	if (isset($files[1]) && $files[1] == '..') unset($files[1]);
	$files = array_values($files);

	if (($assetsIndex = array_search('assets', $files)) !== false)
		unset($files[$assetsIndex]);

	if (!am_var('show-all-promotions')) $files = [$files[array_rand($files)]];

	$base = PROMOTIONSPATH . '/' . $fol . '/';

	foreach ($files as $file) {
		$file = str_replace('.md', '', $file);

		$appendHome = disk_file_exists($base . $file . '/home.md') ? '/home' : '';

		if ($wantedName && $wantedName != $file) continue;

		echo '<section>' . am_var('nl');
		$link = $promotionsUrl . $file . '/';

		echo '<h1 style="text-align: center;">' . makeLink(humanize($file), $link, false) . '</h1><hr />' . am_var('nl');

		renderEngageOrMd(ENGAGEPROMOTION, $base . $file . $appendHome . '.md', $file, true);

		echo '</section>' . am_var('nl');
	}
}
echo '</section>';
echo '</div>';
?>
