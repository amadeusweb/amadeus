<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title><?php echo am_var('special-filename') . ' [blurbs] - ' . am_var('name');?></title>
		<style type="text/css">
		<!--
		body { cursor: default; background-color: #96e77f; }
		h2 { padding: 8px; background-color: #80A5E1; border-radius:6px; }
		//-->
		</style>
		<link rel="stylesheet" href="<?php echo getFeatureUrl('blurbs/blurbs.css' . version());?>" />
	<script type="text/javascript" src="<?php echo getFeatureUrl('blurbs/ghosts.js');?>"></script>
		<script type="text/javascript">
			var items = Array();
<?php
			$items = explode(am_var('safeNL'), disk_file_get_contents(am_var('blurb-file')));
			foreach ($items as $item) {
				$item = trim($item);
				if ($item == '' || $item[0] == '|') continue;
				$text = strip_paragraph(markdown($item));
				echo "			items.push('<h2>" . trim($text) . "</h2>');" . PHP_EOL;
			}
			?>
		</script>
	</head>
	<body id="<?php echo am_var('page_parameter1'); ?>">
	<script type="text/javascript">
		Halloween.ghostImages = items;
		Halloween.interval = 150;
		Halloween.offset = { X: 150, Y: 150 };
		Halloween.ghostOutput();
		Halloween.ghostLoad = window.onload;
		window.onload = Halloween.ghostStart;
	</script>
	</body>
</html>
