<?php
add_foot_hook(featurePath('panels/foot-hook.php'));

function _renderBSGrid($cells) {
	am_var('panels-grid', true); //for foot hook
	$op = [];
	$op[] = '<div class="panels container">';
	$op[] = '	<div class="row">';

	foreach ($cells as $pane) {
		$id = $pane['id']; unset($pane['id']);
		$at = $pane['startAt']; unset($pane['startAt']);
		$class = '';
		if (isset($pane['class'])) {
			$class =  ' ' . $pane['class'];
			unset($pane['class']);
		}

		$op[] = '		<div id="panel-' . $id . '" class="col-' . $at . ' p-1 col-sm-12 hideable-panel cell' . $class . '"><section class="h-100">'; $op[] = '';

		foreach ($pane as $name => $block) {
			$op[] = '<!--block: .' . $name . '.-->';
			$op[] = '<div class="container-' . $name . '">';
			$op[] = $block;
			$op[] = '</div>';
			$op[] = '<!--end block: .' . $name . '.-->';
		}

		$op[] = '		</section></div>'; $op[] = '';
	}

	$op[] = '	</div>';
	$op[] = '</div>';

	echo implode(am_var('nl'), $op);
}

function _renderPanels($panels, $format) {
	am_var('panels-v1', true);
	$replaces = [];
	foreach ($panels as $id => $panelReplaces) {
		if (is_string($panelReplaces)) {
			$replaces[$id] = $panelReplaces;
			continue;
		}

		foreach ($panelReplaces as $key => $item)
			$replaces[$id . '-' . $key] = $item;
	}

	if ($format == 'default') $format = 'top-bottom-full-right.html';
	$template = disk_file_get_contents($file = featurePath('panels/' . $format));

	echo replaceItems($template, $replaces, '%');
}
