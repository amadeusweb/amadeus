<?php
/*****
 * Based on code developed in June 2015 at: https://bitbucket.org/ianamazi/sixtyplus/src/master/
 */

includeFeature('family-tree/Family', false);
includeFeature('family-tree/foot-hook', false);

function renderFamilyTree($mdFile) {
	echo featureHeading('tree');

	$raw = disk_file_get_contents($mdFile);
	$bits = explode('<!--family-->', $raw);
	$start = true; $index = 0;
	foreach ($bits as $family) {
		$index += 1;
		$last = $index == count($bits);
		if ($start) {
			renderMarkdown($family);
			section('end');
			$start = false;
			continue;
		}

		$bits2 = explode(am_var('nl'), $family, 4);
		$id = 'family-of-' . ($fname = trim(replaceItems($bits2[1], ['FamilyId: ' => '']), am_var('nl')));
		$around = trim(replaceItems($bits2[2], ['Around: ' => '']), am_var('nl'));

		echo '<a name="' . $id . '"></a>';
		echo '<section class="family">';

		$name = renderSingleLineMarkdown(humanize($around), ['echo' => false]);
		echo '<h2>Family Tree of ' . $name . ' &mdash; (<a href="#' . $id . '">#' . $fname . '</a>)</h2>' . am_var('nl');

		$tree = new Family($family);
		echo $tree->renderInfo(false, $name);
		if (!$last) section('end');
	}
}
