<?php
//builtin css classes
DEFINE('ENGAGESITE', 'site');
DEFINE('ENGAGENODE', 'node');
DEFINE('ENGAGENODEITEM', 'node-item');
DEFINE('ENGAGEPROMOTION', 'promotion'); //TODO: grid & buttons only mode so div on popup

function renderEngageItems($class, $email, $path, $items) {
	foreach ($items as $id => $name)
		renderEngage($class, $name, $id, $email, $path . $id . '.md');
}

am_var('_engageButtonFormat', '<a href="javascript: void(0);" class="btn btn-primary btn-%class% toggle-engage" data-engage-target="engage-%id%">%name%</a>');

function engageButton($id, $name, $class, $scroll = false) {
	if ($scroll) $class .= ' engage-scroll';
	$class .= ' btn-fill';
	return replaceItems(am_var('_engageButtonFormat'), ['id' => $id, 'name' => $name, 'class' => $class], '%') . am_var('nl');
}

//TODO: Make a toggle-more when the md contains <!--more-->
function renderEngage($class, $name, $id, $email, $file, $open = false, $echo = true) {
	if (is_array($email)) $email = implode(';', $email);

	if (!$open) echo engageButton($id, $name, $class);

	$result = '	<div id="engage-' . $id . '" class="engage" ' . ($open ? '' : 'style="display: none" ') . 'data-to="' . $email . '" data-cc="' . am_var('rootEmail') . '" data-name="' . $name . '">' . am_var('nl');

	$replaces = [];
	if (disk_file_exists($note = (AMADEUSPATH . '/data/engage-note.md'))) {
		$replaces['engage-note'] = renderMarkdown($note, ['echo' => false]);
		if (disk_file_exists($note2 = (AMADEUSPATH . '/data/engage-note-above.md')))
			$replaces['engage-note-above'] = renderMarkdown($note2, ['echo' => false]);
		$replaces['email'] = $email;
	}

	$result .= renderMarkdown($file, ['replaces' => $replaces, 'echo' => false]);

	$result .= '</div>' . am_var('nl');
	if (!$echo) return $result;
	echo $result;
}

function renderEngageOrMd($class, $raw, $id, $noSectionStart = false, $echo = true) {
	$id = str_replace('/home', '', str_replace('.md', '', $id));
	$link = am_var('url') . $id . '/';

	$result = '';
	if (!$noSectionStart)
		$result .= '<section class="file-wrapper amadeus-feature"><h1 style="text-align: center;">' . makeLink(humanize($id), $link, false) . '</h1><hr />' . am_var('nl');

	$single = $id == am_var('node');
	if (endsWith($raw, '.md'))
		$raw = disk_file_get_contents($raw);

	if (is_engage($raw)) {
		//SITE MAIL ONLY: <!--engage: SITE //engage-->
		$email = replaceItems(explode(' //engage-->', $raw, 2)[0], ['<!--engage: ' => '']);

		$raw = renderMarkdown($raw, ['replaces' => [
				'promotion-email' => $email,
				'promotion-email-textbox' => textBoxWithCopyOnClick('', $email),
			], 'echo' => false]);

		$email = ($email && $email != 'SITE' ? $email . ';' : '') . am_var('email');
		$result .= renderEngage($class, humanize($id), $id, $email, $raw, true, false); //no need button css for promotion as always open
	} else {
		if ($single)
			$result .= renderMarkdown($raw, ['echo' => false]);
		else
			$result .= renderExcerpt($raw, $link, '', false);
	}

	if (!$noSectionStart)
		$result .= '</section>' . am_var('2nl');
	
	if (!$echo) return $result;
	echo $result;
}

function runDefinedEngages() {
	if (am_var('no-engage')) return;
	sectionId('engages', 'amadeus-silent-feature');
	featureHeading('engage');

	//TODO: @<team> - test these out
	if ($engage = am_sub_var('node-vars', 'engage')) {
		$item = am_var('page_parameter1') ? am_var('page_parameter1') : false;
		$email =  am_sub_var('node-vars', 'email'). ';' . am_var('email');
		$path = am_sub_var('node-vars', 'nodeFolder') . 'engage/';
		if (isset($engage[$item])) renderEngageItems(ENGAGENODEITEM, $email, $path, $engage[$item]);
		$item = 'all';
		if (isset($engage[$item])) renderEngageItems(ENGAGENODE, $email, $path, $engage[$item]);
	} else if (disk_file_exists(($path = am_sub_var('node-vars', 'nodeFolder') . '_engage.md'))) {
		$email =  am_sub_var('node-vars', 'email'). ';' . am_var('email');
		renderEngage(ENGAGENODE, 'Engage With ' . humanize(am_var('node')), am_var('node'), $email, $path);
	}

	renderEngage(ENGAGESITE, 'Engage with ' . am_var('name'), am_var('safeName'), am_var('email'), SITEPATH . am_var('folder') . '_engage.md');

	section('end');
}

function runEngageFromSheet($sheetName, $introduction) {
	$sheet = get_sheet($sheetName);
	$contentIndex = $sheet->columns['content'];
	$introIndex = $sheet->columns['section-intro'];
	//$items = []; //trying to make as pills in a later version
	$raw = ['<!--engage: SITE //engage--><!--render-processing-->', $introduction, ''];
	$firstSection = true;
	$raw[] = '%engage-note-above%';
	
	foreach ($sheet->sections as $name => $rows) {
		$raw[] = '## ' . $name;
		$raw[] = '';

		$firstRow = true;
		foreach ($rows as $row) {
			if ($firstRow) {
				$raw[] = $row[$introIndex];
				$raw[] = '';
				$firstRow = false;
			}
	
			$line = $row[$contentIndex];
			$raw[] = '* '  . $line;
			//$content[] = ;
		}
	
		$raw[] = '';
		//$items[$key] = $content;
	}
	$raw[] = '';
	$raw[] = '%engage-note%';
	
	section();
	//$raw = print_r($items, 1); //$raw = renderPills($items); //todo: LATER!
	renderEngageOrMd(ENGAGESITE, implode(am_var('nl'), $raw), 'signup', true, true);
	section('end');
}
