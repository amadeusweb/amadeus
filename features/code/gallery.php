<?php
//v4.1 now supports calling without node (from amadeus' blog)
$vars = am_var('node-vars');
$nodeItem = am_var('node-item');
$settings = getMediaSettings($nodeItem);
$grid = isset($settings['use-grid']);

$skip = $nodeItem != 'home' && isset($settings['no-browse-in-node-item']);
if ($vars && !$skip) {
	echo '<section><h1>' . am_var('nl') . ($grid ? humanize(am_var('page_parameter1') . ' &mdash; ' . am_var('node') . ' &mdash; ' . am_var('name')) :
		humanize(am_var('node-item')) . ' in ' . humanize(am_var('node')) .
		' &mdash; ' . makeLink('browse', $vars['nodeRelativePath'] . am_var('node-item') . '/')) .
		'</h2></section>' . am_var('2nl')
	;
}

echo '<div class="gallery">';

$itemsInSubfolder = isset($settings['node-item-is-subfolder']);
$mediaRelative = am_var('mediaRelativePath');
if (!$mediaRelative) {
	$mediaRelative = $vars['nodeRelativePath'];
	if ($nodeItem && $itemsInSubfolder) $mediaRelative .= $nodeItem . '/';
	else $mediaRelative .= am_var('page_parameter1') . '/';
}

am_var('mediaRelative', $mediaRelative);

if ($nodeItem != 'home' && !$itemsInSubfolder) {
	$params = enhanceParams(['extension' => '.jpg', 'mediaRelative' => $mediaRelative, 'link' => '#', 'wrap-in-section' => true, 'not-in-menu' => true]);
	$params['no-link-in-node-item'] = isset($settings['no-link-in-node-item']);
	echo renderMedia(am_var('node-item'), $params, $settings);
	return;
}

am_var('mediaSettings', $settings);

function enhanceParams($params) {
	$vars = am_var('node-vars'); //expected there else the gallery itself wouldnt load
	$params['nodeUrl'] = $vars['nodeUrl']; //these too are expected
	$params['nodeLink'] = $vars['nodeLink'];
	return $params;
}

am_var('site-menu-settings', false);
menu('/' . $mediaRelative, [
	'parent-slug' => am_var('node') . am_var('node_parameter1'),
	'add-extension' => true,
	'exclude-extensions' => ['.md'],
	'sections-not-list' => !$grid,
	'no-links' => $grid,
	'ul-class' => $grid ? 'row' : '',
	'li-class' => $grid ? 'col-sm-12 col-md-3' : '',
	'innerHtml' => function($file, $params) {
		$params['mediaRelative'] = am_var('mediaRelative');
		$params = enhanceParams($params);
		$settings = am_var('mediaSettings');
		return renderMedia($file, $params, $settings);
	}
]);
?>
</div>
