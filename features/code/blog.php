<?php
$node = am_var('node') == 'index' ? 'blog' : am_var('node');
$pp1 = am_var_or('page_parameter1', '');

$dontBreak = am_var('node') == 'blog';
$fol = SITEPATH . '/blog/';
$files = get_menu_files("$node");
if (!$files) $files = disk_scandir($fol);

foreach ($files as $item) {
	if ($item[0] == '.') continue;

	if ($item == '----') {
		if ($dontBreak)
			continue;
		else
			break;
	}

	if ($node != 'blog' && $node != $item) continue;

	$first = true;

	$posts = disk_scandir($fol . '/' . $item);
	$posts = array_reverse($posts);

	foreach ($posts as $fil) {
		if ($fil[0] == '.' || disk_is_dir($fol . '/' . $item . '/' . $fil)) continue;

		$fileName = remove_extension($fil);
		if ($fileName == 'home') continue;
		if ($pp1 && $pp1 != $fileName) continue;

		section();

		if ($first) { 
			renderMarkdown('# <center style="font-size: large">[' . humanize($item) . '](' . am_var('url') . $item . '/)</center>');
			section('end');
			section();
		}

		$h1 = '<h1><u>' . blog_heading($fileName, $item) . '</u></h1>';
		$relUrl = am_var('node') . '/' . $fileName;

		if ($relUrl == am_var('all_page_parameters')) {
			echo $h1;
			renderMarkdown($fol . $item . '/' . $fil);
		} else {
			renderExcerpt($fol . $item . '/' . $fil, am_var('url') . $item . '/' . remove_extension($fil) . '/', $h1);
		}

		section('end'); 

		$first = false;
	}
}
?>

