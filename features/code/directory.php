<?php
/*****
 * 18 Oct 2024:
 *     Supporting Listing of Sub-directory by introducing $topFolder
 *     directory_of better than hacking the page_parameter1 (needs cleanup)
 *****/

$of = am_var_or('directory_of', am_var('node') != 'index' ? am_var('node') : (am_var('page_parameter1') ? am_var('page_parameter1') : false));
$folders = explode('/', $of);
$subFolder = count($folders) > 1;
$topFolder = $subFolder ? $folders[0] : $of;
$secondFolder = $subFolder ? $folders[1] : false;

if (!am_var('no-directory-header')) {
	sectionId('directory-sections');
	echo '<h1 class="flash flash-green">' . humanize($of) . ' &mdash; Section @ ' . am_var('abbr') . ' World</h1>' . am_var('nl');

	if ($of == false)
		echo '<p class="flash flash-yellow">Click One of the Section Buttons below:</p>'; 

	echo '<div style="text-align: center;">' . am_var('nl');
	foreach (am_var('sections') as $item) {
		if (cannot_access($item)) continue;
		if ($item == $topFolder) am_var('__this-section', $item);
		echo sprintf(am_var('nl') . '<a class="btn btn-%s" href="%s">%s</a> ',
			$item == $topFolder ? 'primary' : 'secondary',
			am_var('url') . $item . '/',
			humanize($item)
		);
	}

	if ($secondFolder) {
		echo sprintf('<hr /><h2><a class="btn btn-primary" href="%s">%s</a></h2>',
			am_var('url') . $secondFolder . '/',
			humanize($secondFolder));
	}

	echo '</div>' . am_var('nl');
	echo '</section>' . am_var('2nl');
}

$folder = SITEPATH . '/' . $of . '/';

if (disk_file_exists($home = $folder . 'home.md')) {
	section();
	renderFile($home);
	section('end');

	section();
	echo '<h2>' . humanize($of) . ' &mdash; Menu</h2>' . am_var('2nl');
	$params = am_var('use-menu-files') ? ['files' => get_menu_files($of), 'could-have-slashes' => true] : [];

	menu('/' . $of . '/', $params);
	section('end');

	return;
}

if ($topFolder == 'blog') {
	includeFeature('blog');
	return;
} else if ($file == am_var('file')) {
	renderMarkdown($file);
	return;
}

if ($of == false) return;
$baseUrl = am_var('url');
if ($secondFolder) $baseUrl .= $secondFolder . '/';

$files = get_menu_files($of);
if (!$files) $files = disk_scandir($folder);

am_var('is-in-directory', true);

//TODO: CLEANUP: - move this to promote?
foreach ($files as $node) {
	if ($node[0] == '.' || $node[0] == '_' || $node == 'assets' || $node == 'images') continue;
	if ($node == '----') { echo '<hr />'; continue; }
	if (startsWith($node, '~')) { section('start', substr($node, 1), true); section('end'); continue; }
	if ($subFolder && disk_is_dir($folder . $node)) continue;

	$file = $node;
	am_var('__this-node', $node);
	if (!endsWith($node, '.md') && !endsWith($node, '.tsv')) {
		$file = $node . '/home.md';
		if (am_var('sections-have-files') && !disk_file_exists($folder . $file)) {
			$fileToCheck = $folder . $node . '.md';
			if (disk_file_exists($fileToCheck))
				$file = $node . '.md';
		}
	} else {
		$node = replaceItems($node, ['.md' => '', '.tsv' => '']);
	}

	echo '<section>' . am_var('nl');
	$h2 = sprintf('<h2><a href="%s">%s</a></h2>' . am_var('nl'), $baseUrl . $node . '/', humanize(handle_slashes($node, true, true)));

	if ($relUrl = am_var('all_page_parameters')) {
		echo $h2;
		renderMarkdown($folder . $file);
	} else {
		if (am_var('auto-render') && needsSpecialRender($folder . $file)) {
			echo $h2;
			autoRenderIfSpecial($folder . $file, true);
		} else {
			renderExcerpt($folder . $file, $baseUrl . $node . '/', $h2);
		}
	}
	echo '</section>' . am_var('2nl');
}
