<?php
/*****
 * AURRA - AUth foRR All
 * v1 - for internal team
 *  1.1 - block site (stubbed but ui complete)
 *  
 */

function blockIfNotAuthenticated($who = 'admins, team') {
	if (am_var('local') && isset($_GET['internal'])) return;
	am_var('site-lock', true);
}

function doSiteLock() {
	includeFeature('engage');
	section();
	renderEngage(ENGAGESITE, am_var('name'), 'site-lock', 'SITE', SITEPATH . '/data/site-lock.md', true);
	section('end');
}
