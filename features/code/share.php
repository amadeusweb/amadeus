<?php
/****
 * Based on archives.yieldmore.org/go/share/
 * REFER: resources.php and _menu.php
 ****/

if (isset($_GET['share'])) {
	read_seo_info();
	am_var('uses', 'no-menu, no-footer');
	renderThemeFile('header', 'default');

	$for = $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_GET['url'];
	$url .= '?utm_source=%source%';
	$url .= isset($_GET['campaign']) && $_GET['campaign'] ? '&utm_campaign=' . $_GET['campaign'] : '';
	$url .= isset($_GET['by']) && $_GET['by'] ? '&utm_content=referred-by-' . strtolower($_GET['by']) : '';

	echo featureHeading('share');
	echo 'Click any label / textbox to copy it\'s links<br />and share on that source (whatsapp / linkedin etc):';
	echo textBoxWithCopyOnClick('tracker for url', $for, 'copied plain link (no tracker)', true);

	$sources = ['whatsapp', 'instagram', 'facebook', 'email', 'linkedin'];
	foreach ($sources as $source) echo textBoxWithCopyOnClick($source, str_replace('%source%', $source, $url), 'copied ' . $source . ' link!', true);

	_credits('<br /><br />');
	renderThemeFile('footer', 'default');
	exit;
} else { ?>
<section id="amadeus-share" class="container amadeus-silent-feature" style="text-align: center; padding-top: 30px;">
	<?php echo featureHeading('share-form');?>
	<form action="<?php echo am_var('url') ?>" target="_blank">
		<input type="hidden" name="share" value="1" />
		<input type="hidden" name="url" value="<?php echo $_SERVER['SERVER_NAME'] . explode('?', $_SERVER['REQUEST_URI'], 2)[0]; ?>" />
		<input style="width: 100%; margin-bottom: 10px;" type="text" name="campaign" placeholder="campaign (if known)" value="<?php echo isset($_GET['utm_campaign']) ? $_GET['utm_campaign'] : ''; ?>" /><br />
		<input style="width: 100%; margin-bottom: 10px;" type="text" name="by" placeholder="your name" value="<?php echo isset($_GET['utm_content']) ? str_replace('referred-by-', '', $_GET['utm_content']) : ''; ?>" /><br />
		<input style="width: 100%;" type="submit" value="Share This Page" />
	</form>
</section>
<?php } ?>
