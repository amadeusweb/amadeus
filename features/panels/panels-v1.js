//this one is a little buggy, and dependant on a table for rendering
	//'right-or-middle-tr-td' => $wantsMobile ? '</td></tr><tr><td class="panel-cell">' : '<td class="panel-cell control-left">',
	//'bottom-td' => $wantsMobile ? '<td class="panel-cell">' : '<td class="panel-cell" colspan="2">',
	//top		 'button-settings' => 'data-name="sites" data-metric="height" data-now="small" data-small="25%" data-large="calc(' . ($wantsMobile ? '100%' : '60%"') . ' - 250px);"',
	//bottom 	 'button-settings' => 'data-name="website loader" data-metric="height" data-now="big" data-small="400px" data-large="600px"',
	//full-right 'button-settings' => 'data-name="sitemap" data-metric="width" data-now="large" data-small="200px" data-large="500px"',

function togglePanel(btn, event = null) {
	var leftArrow = '←', upArrow = '↑', rightArrow = '→', downArrow = '↓';

	const metric = btn.getAttribute('data-metric');
	const changeTo = btn.getAttribute('data-now') == 'small' ? 'large' : 'small';
	const setTo = btn.getAttribute('data-' + changeTo);
	const panelId = btn.getAttribute('id').replaceAll('btn-', '');
	const panelName = btn.getAttribute('data-name');

	var arrow = metric == 'width'
		? (changeTo == 'small' ? leftArrow : rightArrow)
		: (changeTo == 'small' ? downArrow : upArrow);

	btn.setAttribute('value', panelName + ' ' + arrow);
	btn.setAttribute('data-now', changeTo);

	const panel = document.getElementById(panelId + '-panel');
	panel.style[metric] = setTo;

	if (event) { debugger; resizeLeft(panel); }

	if (event) event.preventDefault();
}

function resizeLeft(panel) {
	const resizeLeft = panel.parentElement.classList.contains('control-left');
	if (!resizeLeft) return;

	const top = document.getElementById('top-panel').parentElement;
	const bottom = document.getElementById('bottom-panel').parentElement;

	const sizeTo = document.getElementsByClassName('size-width-to')[0];
	bottom.style.width = top.style.width = (sizeTo.offsetWidth - panel.parentElement.offsetWidth) + 'px';
}

window.onload = function() {
	togglePanel(document.getElementById('btn-top'));
	togglePanel(document.getElementById('btn-full-right'));
	togglePanel(document.getElementById('btn-bottom'));

	const rightPanel = document.getElementById('full-right-panel');
	debugger;
	resizeLeft(rightPanel)
};

function openInBottom(url) {
	//window.parent.
	openInPanel(url, 'bottom');
}

function openInPanel(link, where, event) {
	debugger;
	const panel = document.getElementById(where + '-panel');
	const url = typeof(link) == 'string' ? link : link.getAttribute('href');
	if (!panel.innerHTML.includes('iframe')) {
		panel.innerHTML = '<iframe id="' + where + '-frame" style="height: 100%; width: 100%;" src="' + url + '"></iframe>';
	} else {
		const frame = document.getElementById(where + '-frame');
		frame.setAttribute('src', url);
	}
	event.preventDefault();
}
