$(document).ready(function () {
	$('.overflow-at-starting-height').each((ix, el) => {
		const element = $(el);
		element
			.css('height', element.css('height'))
			.css('overflow', 'scroll')
			.addClass('no-scrollbars');
	})

	$('.open-in').on('click', openInPanel);

	function openInPanel(event) {
		const panelName = $(this).data('panel');
		openInParentPanel(this, panelName);
		event.preventDefault();
	}

	window.openInParentPanel = (link, panelName) => {
		link = $(link);
		const frameId = panelName + '-frame';
		let frame = $('#' + frameId);
		const panelOuter = $('#panel-' + panelName);
		const panelHeading = $('.container-heading h2 .heading-text', panelOuter);
		const panel = $('.container-content', panelOuter);
		const url = link.attr('href');

		if (!panel.hasClass('h-100')) panel.addClass('h-100');

		if (!frame.length) {
			panel.html('<iframe id="' + frameId + '" style="height: 100%; width: 100%;" src="' + url + '"></iframe>');
			frame = $('#' + frameId);
		} else {
			frame.attr('src', url);
		}

		if (panelOuter.hasClass('auto-adjust-height') && !frame.hasClass('registered-auto-adjust')) {
			frame.on('load', function() {
				var time = Math.floor(Date.now() / 1000) + '';
				panelHeading.text(frame.attr('src') + ' @ ' + timeFormat());
				const myFrame = frame[0];
				myFrame.style.height = myFrame.contentWindow.document.body.scrollHeight + 'px';
			});
			frame.addClass('registered-auto-adjust');
		}
	}
});
