

	<!-- AmadeusWeb's Panels Includes (uses bootstrap tabs) --><?php
$url = getFeatureUrl('panels/');
$version = '?panelsVersion=2.2.0.2';
cssTag($url . 'panels.css' . $version);

if (am_var('panels-v1'))
	scriptTag($url . 'panels-v1.js' . $version);

if (am_var('panels-grid'))
	scriptTag($url . 'bs-grid.js' . $version);
