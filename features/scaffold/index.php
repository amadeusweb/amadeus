<?php
sectionId('top-content');
renderMarkdown('# Welcome to %siteName%');

renderAny(SITEPATH . am_var('folder') . '_index.md');
section('end');

//home.tsv!

$sections = am_var('sections');

if (count($sections) == 0) { parameterError('NO SECTIONS DEFINED', 'refer to site.tsv / site-cms.php', false); return; }

$section = $sections[0];

if (access_needed()) {
	$section = false;
	foreach ($sections as $slug) {
		if (!cannot_access($slug)) {
			$section = $slug;
			break;
		}
	}
}

if ($section == false) { parameterError('NO ACCESS TO ANY SECTION', $sections, false); return; }

am_var('directory_of', $section);
clear_var('htmlSitewideReplaces');
am_var('section', $section);
includeFeature('directory');
