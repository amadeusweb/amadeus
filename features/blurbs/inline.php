<?php
if (has_module('blurbs/inline')) return; //single include

function inline_blurbs($items, $categories, $id)
{
    echo '<ul class="blurb-categories" data-for="' . $id . '">' . am_var('nl');
    foreach ($categories as $item)
        echo '  <li class="' . humanize($item) . '"><a href="javascript: void(0);" data-filter="' . humanize($item) . '">' . $item . '</a></li> . am_var('nl')';
    echo '</ul>' . am_var('2nl');
}
?>
<link rel="stylesheet" href="<?php echo featureUrl('blurbs/blurbs.css' . version());?>" />
<script type="text/javascript" src="<?php echo featureUrl('blurbs/ghosts.js');?>"></script>
