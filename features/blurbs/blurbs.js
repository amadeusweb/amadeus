$(document).ready(function(){
    Halloween.interval = 150;
    Halloween.offset = { X: 150, Y: 150 };

    $('.blurbs').each(function() {
        var ghostImages = $('li', $(this)).map(function(){ return $(this).html(); });
        var id = $(this).attr('id');
        $(this).hide();
        $('<div class="blurbs-container" id="' + id + '-div" />').height($(this).data('height')).css({position: 'relative'}).insertAfter('#' + id); 
        Halloween.ghostOutput(ghostImages, id);
    });
    
    $('.blurb-categories a').click(function(){
        var blurbSetName = $(this).closest('.blurb-categories').data('for');
        var cont = $('#' + blurbSetName + '-div');
        var all = $('div', cont);
        var forWhat = $(this).data('filter');

        if (forWhat == 'all') {
            all.show();
            return;
        }

        var blurbList = $('#' + blurbSetName + ' li');
        all.each(function (index, el) { if ($(blurbList[index]).hasClass(forWhat)) $(el).show(); else $(el).hide(); });
    });
});