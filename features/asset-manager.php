

    <!-- AmadeusWeb's Assets by flavour and feature --><?php
am_vars([
	'featuresVersion' => ['id' => '3.1.5.8', 'date' => '4 Mar 2025'],
	'writer-with-works-assetsVersion' => ['id' => '1.0.2', 'date' => '8 Nov 2024'],
	'writer-assetsVersion' => ['id' => '1.1.2.2', 'date' => '16 Dec 2024'],
]);

$flv = am_var('flavour');
$url = getFeatureUrl('assets/');
$ver = version('features');

cssTag($url . 'amadeus.css' . $ver);
if ($flv == 'yieldmore') cssTag($url . 'yieldmore.css' . $ver);

scriptTag($url . 'bells-and-whistles.js' . $ver);

$urlEngage = getFeatureUrl('engage/');
scriptTag($urlEngage . 'engage.js' . $ver);
cssTag($urlEngage . 'engage.css' . $ver);

if (am_var('theme') == 'biz-land' || am_var('theme') == 'artist' || am_var('theme') == 'canvas') cssTag($url . 'biz-land.css' . $ver);

if (am_var('theme') == 'cv-writer' && am_var('flavour') != 'writer'
	&& am_var('flavour') != 'writer-with-works') cssTag($url . 'cv-writer.css' . $ver);

if (am_var('theme') == 'cv-store') cssTag($url . 'cv-store.css' . $ver);

if (am_var('theme') == 'cv-nonprofit') cssTag($url . 'cv-nonprofit.css' . $ver);

if ($flv == 'yieldmore') return;

$url = flavourAssetsUrl($flv);
$ver = version($flv . '-assets');

if ($flv == 'writer-with-works') {
	scriptTag($url . 'posts-popup.js' . $ver);
	scriptTag($url . 'posts-search.js' . $ver);
	cssTag($url . 'styles.css' . $ver);
	cssTag(flavourAssetsUrl('writer') . 'styles.css' . version('writer-assets'));
} else if ($flv == 'writer') {
	cssTag($url . 'styles.css' . $ver);
}
