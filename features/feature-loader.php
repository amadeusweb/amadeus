<?php
function getFeatureUrl($relative) {
	return am_var('app') . 'features/' . $relative;
}

includeFeature('assistant');

function table($id, $htmlFile, $dataFile, $columns, $template) {
	includeFeature('tables');

	if ($htmlFile) {
		sectionId($id . '-filter', 'table-filters');
		renderMarkdown($htmlFile, ['strip-paragraph-tag' => true]);
		section('end');
	}
	
	sectionId($id, 'table-feature');
	add_table($id, $dataFile, $columns, $template);
	section('end');
}

function tableHeadingsOnLeft($id, $data) {
	includeFeature('tables');
	_tableHeadingsOnLeft($id, $data);
}

function renderPanels($panels, $format = 'default') {
	//TODO: make this a draggable using:
	//https://dev.to/sohrabzia/building-a-resizable-and-sortable-html-table-with-ease-2a0e
	includeFeature('panels');
	_renderPanels($panels, $format);
}

function renderGrid($panels) {
	includeFeature('panels');
	_renderBSGrid($panels);
}
