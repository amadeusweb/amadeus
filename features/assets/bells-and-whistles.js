if (typeof($) === 'undefined') $ = jQuery.noConflict();

window.amadeusUtils = {
	scrollWithOffset: function(element) {
		let offsetElement = $('.scroll-to-offset');
		if (!offsetElement.length) { element[0].scrollIntoView(); return; }

		offsetElement = offsetElement[0]; //pure js
		//https://stackoverflow.com/a/49860927
		var elementPosition = element.getBoundingClientRect().top;
		var offsetPosition = elementPosition + window.scrollY - offsetElement.offsetTop;
		window.scrollTo({
				top: offsetPosition,
				behavior: "smooth"
		});
	},
};

$(document).ready(function() {
	function toggleBelow(event) {
		$(this).next('ul, ol, .dataTables_wrapper, .amadeus-plain-table').toggle();
	}

	const arrowDown = 'icofont-arrow-down', arrowUp = 'icofont-arrow-up';

	function toggleParent(event) {
		const button = $(this);
		const parent = button.closest('.hideable-panel');
		const icon = $('.toggle-icon', button);

		parent.css('height', icon.hasClass(arrowUp) ? 'auto' : '70px');

		icon.toggleClass(arrowDown);
		icon.toggleClass(arrowUp);
	}

	var togglers = $('.toggle-list-below').click(toggleBelow);
	togglers.filter(':not(#amadeus-site)').trigger('click');

	$('.toggle-parent-panel').click(toggleParent);
	$('.toggle-deck-fullscreen').click(toggleDeckFullscreen);
})

function toggleDeckFullscreen() {
	const link = $(this);
	const full = link.hasClass('is-full-screen');
	$('.text', link).text(full ? 'maximize' : 'minimize');
	const icon = $('.icon', link);

	debugger;
	if (!full) {
		link.addClass('is-full-screen').addClass('icofont-2x')
		icon.removeClass(icon.data('remove'));
		icon.addClass(icon.data('add'));
		$('.deck-container iframe').addClass('is-full-screen');
	} else {
		link.removeClass('is-full-screen').removeClass('icofont-2x')
		icon.addClass(icon.data('remove'));
		icon.removeClass(icon.data('add'));
		$('.deck-container iframe').removeClass('is-full-screen');
	}
}

$(document).ready(togglePieceContent);

function togglePieceContent() {
	const links = $('.read-piece');
	if (links.length == 0) return;

	$('.piece-content').hide();
	links.click(function () {
		const link = $(this);
		$('.piece-content', link.closest('section')).toggle();
	});
}

var slideIndex = 0;

function bwCarousel() {
	var i;
	var x = document.getElementsByClassName("am-slide");
	for (i = 0; i < x.length; i++) {
		x[i].style.display = "none";
	}
	slideIndex++;
	if (slideIndex > x.length) {slideIndex = 1}
	x[slideIndex-1].style.display = "block";
	setTimeout(bwCarousel, 2000); // Change image every 2 seconds
}

if (document.getElementsByClassName("am-slide").length) bwCarousel();

function timeFormat(when) {
	if (!when) when = new Date();
	return _addZero(when.getHours()) + ':' + _addZero(when.getMinutes()) + ':' + _addZero(when.getSeconds());
}

function _addZero(num) {
	return (num < 10 ? '0' : '') + num;
}
