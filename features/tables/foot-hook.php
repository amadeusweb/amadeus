

    <!-- AmadeusWeb's Tables Includes (uses datatables.net) --><?php
$tablesUrl = getFeatureUrl('tables/');
$tablesVer = '?tablesVersion=2.1.7';
 foreach ([
    'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
	'https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css',
	'https://cdn.datatables.net/buttons/1.2.2/css/buttons.bootstrap.min.css',
    $tablesUrl . 'tables-styles.css' . $tablesVer,
] as $style)
    cssTag($style);

//FROM - view-source:https://cdpn.io/RedJokingInn/fullpage/bGoppqP
foreach ([
    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js',
    'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',
    'https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js',
    'https://cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js',
    'https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js',
    'https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js',
    'https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js',
    'https://cdn.datatables.net/buttons/1.2.2/js/buttons.bootstrap.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js',
    'https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js',
    'https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js',
    $tablesUrl . 'tables-loader.js' . $tablesVer,
] as $script)
    scriptTag($script);
