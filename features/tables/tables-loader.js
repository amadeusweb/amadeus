//FROM: https://codepen.io/RedJokingInn/pen/bGoppqP
//Imran - Modified Dec 2024 to support multiple '.amadeus-table'
$(document).ready(function () {
	if ($('.amadeus-table').length == 0) return;

	function safeScrollWithOffset(element) {
		if (window.amadeusUtils)
			window.amadeusUtils.scrollWithOffset(element);
		else
			element.scrollIntoView();
	}

	function thisTable(el) {
		return el.closest('.amadeus-table');
	}

	$('.amadeus-table .filter').click(function() {
		const el = $(this);
		$('.filter-' + el.data('type')).val(el.data('value')).change();
		safeScrollWithOffset(thisTable(el)[0]);
	});

	$('.table-filters li').click(function() {
		const el = $(this);
		const filterType = el.parent().prev('strong')
			.text().includes('Account')
				? 'account' : 'project';
		$('.filter-' + filterType).val(el.text()).change();
		safeScrollWithOffset(thisTable(el)[0]);
	});

	$('.amadeus-table').DataTable({
		amadeusTable: undefined, id: undefined, cardView: undefined,
		dom: '<"dt-buttons"Bf><"clear">lirtp',
		paging: true,
		pageLength: 50,
		autoWidth: true,
		buttons: [
			'colvis',
			'copyHtml5',
			'csvHtml5',
			'excelHtml5',
			'pdfHtml5',
			'print'
		],

		initComplete : function (settings, json) {
			const amadeusTable = thisTable($(this))
				tableId = amadeusTable.attr('id'),
				cardView = tableId + '-card-view';

			$('.dt-buttons .btn-group').append(
				'<a data-table-id="' + tableId + '" class="' + cardView + ' btn btn-primary" href="#">CARD VIEW</a>'
			);

			//https://codepen.io/RedJokingInn/pen/bGoppqP
			$('.' + cardView).on('click', function (el, ix) {
				el = $(el.target);
				const table = $('#' + el.data('table-id'));
				if (table.hasClass('card')) {
					$('.colHeader', table).remove();
				} else {
					var labels = [];
					$('thead th', table).each(function () {
						labels.push($(this).text());
					});
					$('tbody tr', table).each(function () {
						$(this)
							.find('td')
							.each(function (column) {
								$('<span class="colHeader">' + labels[column] + ':</span>').prependTo(
									$(this)
								);
							});
					});
				}
				table.toggleClass('card');
			});

		    // Setup - add a text input to each header cell with header name
		    $('th', amadeusTable).each( function () {
		        var title = $(this).text();
		        $(this).html(title + '<br><input class="filter filter-' + title.toLowerCase().replaceAll(' ', '-') + '" type="text" placeholder="Search ' + title + '" />');
		    });

		    const table = this.api();
			// Apply the search
		    table.columns().every( function () {
		        var that = this;
		        $( 'input', this.header() ).on( 'keyup change clear', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		},
	});
});
