<?php
$prose = 'prose'; $poems = 'poems';
disk_include_once(__DIR__ . '/image-functions.php');

function add_to_work_menu($slug, $item, $cols) {
	if (!$item[$cols['menu']]) return;

	$workMenu = am_var_or('work_menu', []);

	$text = humanize($slug . ' &mdash; ' . $item[$cols['menu']]);
	$link = ($item[$cols['type']] == 'collection' ? 'all/collection/' : '') . $slug . '/';

	$workMenu[$link] = $text;
	am_var('work_menu', $workMenu);
}

function setup_blog_menu() {
	$list = get_menu_files('blog');
	if (!$list) return;
	
	$op = [];
	foreach ($list as $item) {
		$op[$item . '/'] = humanize($item);
	}

	am_var('blog_menu', $op);
}

function setup_sheets() {
	$sheet = get_sheet('works', 'parent');
	$cols = $sheet->columns;
	$collections = [];

	$works = [];
	foreach($sheet->sections[''] as $work) {
		$slug = $work[$cols['slug']];
		$works[$slug] = [ 'mega' => $work[$cols['mega']], 'title' => $work[$cols['title']], 'intro' => $work[$cols['intro']] ];

		$colls = [];
		if (startsWith($slug, ':')) continue;
		add_to_work_menu($slug, $work, $cols);

		foreach ($sheet->sections[$slug] as $item) {
			$sl = $item[$cols['slug']];
			$collections[$sl] = $colls[$sl] = [ 'title' => $item[$cols['title']] ];
			add_to_work_menu($sl, $item, $cols);
		}

		$works[$slug]['collections'] = $colls;
	}

	am_var('works', $works);
	am_var('all_collections', array_merge($collections));
}

function before_render() {
	setup_blog_menu();
	setup_sheets(); //new in Oct 2024

	if (before_section_or_file('blog')) return;

	if (function_exists('site_before_render') && site_before_render()) return; //returns true only if rest of code below is not needed

	$node = am_var('node');

	if ($node == 'tags') am_var('embed', hasPageParameter('embed'));

	//if (_node_exists(($fol = am_var('path') . '/content/profile/') . $node . '.txt', 'profile', 'Profile of Imran Ali Namazi', $fol)) return; 

	foreach (am_var('works') as $work => $item) {
		if ($work == $node) { am_var('work', $work); return; }
		if (isset($item['collections'])) {
			$items = $item['collections'];
			foreach ($items as $coll => $colItem) {
				$file = am_var('path') . '/works/' . $work . '/' . $coll . '/' . $node . '.txt';
				if (_node_exists($file, $coll, $colItem['title'], $work . '/' . $coll)) { am_var('work', $work); return; }
			}
		} else {
			$file = am_var('path') . '/works/' . $work . '/' . $node . '.txt';
			if (_node_exists($file, $work, $item['title'], $work)) { am_var('work', $work); return; }
		}
	}
}

function _node_exists($file, $folder, $name, $folPath) {
	if (disk_file_exists($file)) {
		am_var('txtFile', $file);
		am_var('folderName', $folder);
		am_var('folderHeading', $name);
		am_var('folder', ($contentFolder = contains($folPath, 'content/')) ? $folPath : 'works/' . $folPath);
		am_var('contentFolder', $contentFolder);
		return true;
	}
	return false;
}

function did_render_page() {
	if (did_render_section_or_file()) return true;
	if (function_exists('site_did_render_page') && site_did_render_page()) return true;

	//global $cscore; print_r($cscore);
	$pseudos = ['all', 'poems', 'prose'];
	$index = array_search(am_var('node'), $pseudos);

	if (am_var('contentFolder')) {
		renderFile(am_var('txtFile'));
		return true;
	}

	if (am_var('work') || $index !== false) {
		if ($index !== false)
			am_var('work', ':' . $pseudos[$index]);
		else
			$fol = am_var('folderName');

		include_once 'work.php';
		return true;
	}

	return false;
}

function get_works($pseudo, $sitemap) {
	$pieces = []; $dupes = []; $missing = []; $matches = [];
	$cols = $sitemap['cols'];
	foreach ($sitemap['items'] as $item) {
		$n = urlize($item[$cols->Name]);
		if (isset($pieces[$n])) $dupes[] = $item;
		$pieces[$n] = $item[$cols->Work] . '	' . $item[$cols->Collection] . '	' . $n;
	}

	foreach (am_var('works') as $wn => $work) {
		if ($pseudo != 'all' && $work['mega'] != $pseudo) continue;
		if ($wn[0] == ':' || !isset($work['collections'])) continue;
		foreach ($work['collections'] as $cn => $c) {
			$rb = $wn . '	' . $cn . '	';
			$files = scandir(SITEPATH . '/works/' . $wn . '/' . $cn);
			foreach ($files as $f) {
				if ($f[0] == '.' || endsWith($f, '.jpg') || $f == '_toc.tsv') continue;
				$f = str_replace('.txt', '', $f);
				if (isset($pieces[$f])) {
					unset($pieces[$f]);
					$matches[] = $rb . $f;
				} else {
					$missing[] = $rb . $f;
				}
			}
		}
	}
	
	return [
		'matches' => $matches,
		'missingInSitemap' => $missing,
		'missingInFolder' => $pieces,
	];
}

function get_sitemap($settings) {
	$cols = 'object';
	$tsv = tsv_to_array(file_get_contents(SITEPATH . '/data/sitemap.tsv'), $cols);

	am_var('sm_cols', $cols);

	if (isset($settings['work'])) {
		am_var('sm_filter', $settings['work']);
		$tsv = array_filter($tsv, function($itm) { $cols = am_var('sm_cols'); return $itm[$cols->Work] == am_var('sm_filter'); });
	} else {
		if (am_var('work') != 'all') {
			$works = [];
			foreach (am_var('works') as $key => $w)
				if ($w['mega'] == am_var('work')) $works[] = $key;
			am_var('sm_works', $works);
			//print_r($works); die();
			$tsv = array_filter($tsv, function($itm) { $cols = am_var('sm_cols'); return array_search($itm[$cols->Work], am_var('sm_works')) !== false; });
		}

		$tsv = array_reverse($tsv, true);
	}

	$coll = array_unique( array_map(function($itm) { $cols = am_var('sm_cols'); return $itm[$cols->Collection]; }, $tsv) );
	$ded = array_unique( array_map(function($itm) { $cols = am_var('sm_cols'); return $itm[$cols->Dedication]; }, $tsv) );
	$cat = array_unique( array_map(function($itm) { $cols = am_var('sm_cols'); return $itm[$cols->Category]; }, $tsv) ); //TODO: support multiple
	$feat = array_filter($tsv, function($itm) { $cols = am_var('sm_cols'); return $itm[$cols->Featured] != ''; });

	natsort($ded);
	natsort($cat);

	$dedCount = []; $dedLe2 = []; $dedGt2 = [];
	foreach ($ded as $d) {
		am_var('ded_loop_item', $d);
		$count = count(array_filter($tsv, function($itm) { $cols = am_var('sm_cols'); return isset($itm[$cols->Dedication]) && $itm[$cols->Dedication] == am_var('ded_loop_item'); }));
		$itm = ['name' => $d, 'count' => $count];
		if ($count > 2) array_push($dedGt2, $itm); else array_push($dedLe2, $itm);
		array_push($dedCount, $itm);
	}

	$catCount = [];
	foreach ($cat as $i) {
		am_var('cat_loop_item', $i);
		$count = count(array_filter($tsv, function($itm) { $cols = am_var('sm_cols'); return isset($itm[$cols->Category]) && $itm[$cols->Category] == am_var('cat_loop_item'); }));
		$itm = ['name' => $i, 'count' => $count];
		array_push($catCount, $itm);
	}

	if (isset($settings['collection'])) {
		am_var('sm_filter', $settings['collection']);
		$tsv = array_filter($tsv, function($itm) { $cols = am_var('sm_cols'); return isset($itm[$cols->Collection]) && urlize($itm[$cols->Collection]) == am_var('sm_filter'); });
	}

	if (isset($settings['dedication'])) {
		am_var('sm_filter', $settings['dedication']);
		$tsv = array_filter($tsv, function($itm) { $cols = am_var('sm_cols'); return isset($itm[$cols->Dedication]) && urlize($itm[$cols->Dedication]) == am_var('sm_filter'); });
	}

	if (isset($settings['category'])) {
		am_var('sm_filter', $settings['category']);
		$tsv = array_filter($tsv, function($itm) { $cols = am_var('sm_cols'); return urlize($itm[$cols->Category]) == am_var('sm_filter'); });
	}

	foreach ($tsv as $poem) {
		$poem['img'] = get_poem_image($poem, $cols);
	}

	return [
		'cols' => $cols,
		'items' => $tsv,
		'collections' => $coll,
		'dedications' => $ded,
		'dedicationsWithCount' => $dedCount,
		'dedicationsLe2' => $dedLe2,
		'dedicationsGt2' => $dedGt2,
		'categories' => $cat,
		'categoriesWithCount' => $catCount,
		'featured' => $feat
	];
}

function get_poem_image($poem, $cols) {
	$name = $poem[$cols->Name];
	$name_websafe = urlize($name);

	$img = 'works/' . urlize($poem[$cols->Work]) . '/' . ($poem[$cols->Collection] ? urlize($poem[$cols->Collection]) .'/' : '') . $name_websafe . '.jpg';

	return file_exists(am_var('path') . '/' . $img) ? am_var('url') . $img : false;
}

function page_about() {
	$seo = seo_about(true);
	if (!$seo) return;
	if (am_var('node') != 'index') echo '<h2>' . $seo['name'] . '</h2>';
	$fmt = '  <p><b>%s</b>: %s</p>' . PHP_EOL;
	if ($seo['description']) echo sprintf($fmt, 'Description', $seo['description']);
	if ($seo['keywords']) echo sprintf($fmt, 'Keywords', $seo['keywords']);
	page_social();
	echo '<hr />';
}

function sub_site_heading()
{
	echo am_var('folderHeading');
}

function after_file() {
	if (am_var('embed')) return;
	echo '</div>';
}

function before_file() {
	if (am_var('embed')) return;

	echo sprintf('<div class="container%s">', am_var('work') || !am_var('txtFile') ? '' : ' single');

	if (am_var('contentFolder')) {
		echo '<h1>' . am_var('folderHeading') . '</h2>' . am_var('nl');
		menu('/content/' . am_var('folderName'));
		echo '<hr><h3>' . humanize(am_var('node')) . '</h3>' . am_var('nl');
	}

	//include_once 'subscribe.php';
}

