<div id="search-results"></div>

<div id="search">
	<form action="./">
		<input class="search-text" type="text" name="s" placeholder="Search Current Page" />
		<input class="search-clear" type="button" name="clear" value="x" />
		<input type="submit" value="Search Server" />
		<label title="this works only in search on server"><input type="checkbox" name="include" value="text" />Include Text</label>
	</form>
	<label><input class="search-in-all" type="radio" name="where" value="all" checked />all</label>
	<label><input class="search-in-titles" type="radio" name="where" value="titles" />titles</label>
	<label><input class="search-in-descriptions" type="radio" name="where" value="descriptions" />descriptions</label>
	<label><input class="search-in-filters" type="radio" name="where" value="filters" />filters</label>
</div>
