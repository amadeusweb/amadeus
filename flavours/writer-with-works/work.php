<?php
$work = am_var('work');
$pseudo = $work && $work[0] == ':';
$meta = am_var('works')[$work];
$scandir = am_var('folderName') == 'prayers';

if ($pseudo) {
	$work = substr($work, 1);
	am_var('work', $work);
	am_var('pseudo', true);
}

section();
echo '<h2>' . $meta['title'] . ' &mdash; [' . humanize($work) . ']</h2>';
echo $meta['intro'] . '<br /><br />';
if (!$scandir) include_once 'search.php';
section('end');

//TODO: pseudo images
if (am_var('node') == am_var('work') && !$pseudo) {
	section();
	echo sprintf('<img class="img-fluid img-max-300" src="%sassets/banners/%s.jpg" alt="%s" />', am_var('url'), $work, $work);
	section('end');
}

$param1 = am_var('page_parameter1') ? am_var('page_parameter1') : false;
$param2 = am_var('page_parameter2') ? am_var('page_parameter2') : false;

$filter = $pseudo ? [] : ['work' => $work];

if ($param1 == 'collection')
	$filter['collection'] = $param2;
else if ($param1 == 'for')
	$filter['dedication'] = $param2;
else if ($param1 == 'category')
	$filter['category'] = $param2;

$poems = get_sitemap($filter);

if ($param1 && $param1 !== 'for' && $param1 !== 'featured' && $param1 !== 'graphics') {
	$header = ucfirst($param1);
	$poem = array_values($poems['items'])[0];

	$cols = am_var('sm_cols');	
	$name = $cap = $poem[$cols->{$header}];

	if ($param1 == 'collection') {
		$fols = am_var('all_collections');
		$name = $fols[$name]['title'];
		$param1 .= '-';
	}

	section();
	echo sprintf('<h3 class="post-head">%s: %s<br/>%s', $header, $name, image_or_text(am_var('work'), $param1, $cap, true));
	section('end');
}

function print_filters($poems) {
	echo '<div id="filters" class="searchable-content">';
	$fmt = '<a class="searchable-filter" data-match="filter" href="' . am_var('url') . am_var('work') . '/%s/%s/">%s</a> ';

	section();
	echo '<b>Featured</b>: ';
		echo '<a href="' . am_var('url') . am_var('work') . '/featured/">Featured</a> ';
		echo '<a href="' . am_var('url') . am_var('work') . '/graphics/">Graphics Only</a> ';
		echo '<a href="' . am_var('url') . am_var('work') . '/">All</a>';
	section('end');

	echo '<div class="filter filter-collections"><h3>Collections</h3>';
	echo '<div class="row">';
	foreach($poems['collections'] as $itm) echo '<div class="col-lg-4 col-sm-6 col-xs-12">' . sprintf($fmt, 'collection', urlize($itm), image_or_text('banners', 'collection-', $itm, false)) . '</div>';
	echo '</div>';

	echo '</div><hr/><div class="filter filter-categories"><h3>Categories</h3>';
	foreach($poems['categoriesWithCount'] as $itm) echo sprintf($fmt, 'category', urlize($itm['name']), $itm['name'] . ' (' . $itm['count'] . ')');//image_or_text('categories', 'category-', $cat['name']));

	echo '</div><hr/><div class="filter filter-dedicationsGt2"><h3>Frequent Dedications</h3>';
	foreach($poems['dedicationsGt2'] as $ded) echo sprintf($fmt, 'for', urlize($ded['name']), $ded['name'] . ' (' . $ded['count'] . ')');

	echo '</div><hr/><div class="filter filter-dedicationsLe2"><h3>Rare Dedications</h3>';
	foreach($poems['dedicationsLe2'] as $ded) echo sprintf($fmt, 'for', urlize($ded['name']), $ded['name']);
	echo '</div></div>';
}

echo '<hr />';
echo '<div class="searchable-content">';

if (am_var('txtFile')) {
	$sm_poem = array_values(array_filter($poems['items'], function($itm) { $cols = am_var('sm_cols'); return urlize($itm[$cols->Name]) == am_var('node'); }));

	if (!count($sm_poem)) {
		if ($scandir)
			echo menu('/' . am_var('folder')) . '<hr /><h2>' . humanize(am_var('node')) . '</h2>';
		else
			echo '<p style="background-color: lightpink; padding: 8px;">Sitemap Entry missing</p>';

		$poem = am_var('txtFile');

		$img = am_var('folder') . '/'. urlize(am_var('node')) . '.jpg';
		$img = disk_file_exists(am_var('path') . '/' . $img) ? am_var('url') . $img : false;
		if ($img) echo '<img class="img-fluid img-max-300" src="'.  $img . '" alt="' . humanize(am_var('node')) . '" />';

		if (disk_file_exists($poem)) renderFile($poem);
		return;
	}

	$sm_poem = $sm_poem[0];
	$cols = am_var('sm_cols');

	$index = array_search($sm_poem, $poems['items']);
	$previous = isset($poems['items'][$index -1]) ? $poems['items'][$index -1] : false;
	$next = isset($poems['items'][$index +1]) ? $poems['items'][$index +1] : false;

	$img = am_var('folder') . '/'. urlize($sm_poem[$cols->Name]) . '.jpg';
	$img = disk_file_exists(am_var('path') . '/' . $img) ? am_var('url') . $img : false;

	echo '<div class="single">';
	if ($img) echo '<img class="piece-image-single img-fluid img-max-300" src="'.  $img . '" alt="' . $sm_poem[$cols->Name] . '" />';
	echo '<div class="post-meta">'
		 . print_poem($sm_poem, $cols, 'return-category')
		 . '</div>' . PHP_EOL;

	echo '<h2 class="piece-title">' . $sm_poem[$cols->SNo] . ' ' . $sm_poem[$cols->Name] . '</h2>' . PHP_EOL;
	$description = $sm_poem[$cols->Description];
	echo '<p class="description">' . ($description ? $description : '[Will be added]') . '</p>' . PHP_EOL;
	echo '<strong>Rhyme Scheme: ' . ($sm_poem[$cols->RhymeScheme]) . '</strong><br /><br />' . PHP_EOL;

	$yt = false; //$sm_poem[$cols->YouTube];
	if ($yt) echo '<div class="video-bgd"><div class="video-container"><iframe title="' . $sm_poem[$cols->Name] . '" src="https://www.youtube.com/embed/' . $yt . '?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div>';

	$poem = am_var('txtFile');
	if (disk_file_exists($poem)) echo renderFile($poem);

	/*
	echo '<div class="post-meta">'
		 . sprintf('<h3>%s: %s<br/>%s', 'Collection', $sm_poem[$cols->Collection], true ? '' : image_or_text(am_var('work'), 'collection', $sm_poem[$cols->Collection], true))
		 . sprintf('<h3>%s: %s<br/>%s', 'Category', $sm_poem[$cols->Category], true ? '' : image_or_text(am_var('work'), 'category', $sm_poem[$cols->Category], true))
		 . '</div>' . PHP_EOL;
	*/
	echo '</div>';

	echo '<hr />';
	echo '<div id="post-area" class="masonry-xc">';
	//print_poem($sm_poem, $cols);
	if ($previous) print_poem($previous, $cols, 'PREVIOUS:<hr/>');
	if ($next) print_poem($next, $cols, 'NEXT:<hr/>');
	echo '</div>';
} else {
	$cols = $poems['cols'];

	$pps = am_var_or('page_parameters', []);

	$featured = in_array('featured', $pps);
	$graphics = in_array('graphics', $pps);

	$items = $featured ? $poems['featured'] : $poems['items'];

	echo '<div id="post-area" class="masonry-xc">';

	$hasGraphics = [];
	foreach($items as $poem) {
		if (!isset($poem['img']))
			$poem['img'] = get_poem_image($poem, $cols);

		if ($poem['img'])
			$hasGraphics[] = $poem;
	}
	if ($graphics) $items = $hasGraphics;

	$total = count($items);
	$index = 1;

	foreach($items as $poem) {

		print_poem($poem, $cols, '', $index++ . '/' . $total);
	}
	echo '</div>';
	//TODO: If page_parameter1 == export
}

echo '</div>'; //end searchable-content
echo '<hr />';

print_filters($poems);

function print_poem($poem, $cols, $what = '', $xofy = false) {
	$fmt = '<a title="%s" href="' . am_var('url') . am_var('work') . '/%s/%s/">%s</a>';
	$workItem = am_var('works')[$poem[$cols->Work]];
	$fols = isset($workItem['collections']) ? $workItem['collections'] : [];

	$cat = sprintf('%s, %s %s<br />%s, %s',
		sprintf($fmt, 'Dedicated To', 'for', urlize($poem[$cols->Dedication]), $poem[$cols->Dedication]),
		$poem[$cols->Date],
		$xofy ? '<span style="float: right">' . $xofy . '</span>' : '',
		sprintf($fmt, 'Category', 'category', urlize($poem[$cols->Category]), $poem[$cols->Category]),
		count($fols) ? sprintf($fmt, 'Collection', 'collection', urlize($poem[$cols->Collection]), $fols[$poem[$cols->Collection]]['title']) : '<i>No Collection</i>'
	);

	if (am_var('pseudo')) $cat .= ' | WORK: <a href="' . am_var('url') . $poem[$cols->Work] . '/">' . $poem[$cols->Work] . '</a>';

	if ($what == 'return-category') return $cat;

	$name = $poem[$cols->Name];
	$name_websafe = urlize($name);

	$url = am_var('url') . urlize($name) . '/';

	$ig = false; //$poem[$cols->Instagram] ? 'https://www.instagram.com/p/' . $poem[$cols->Instagram] . '/' : false;
	$img = isset($poem['img']) ? $poem['img'] : get_poem_image($poem, $cols);

	$coll_if = am_var('work') == urlize($poem[$cols->Collection]) ? '' : urlize($poem[$cols->Collection]) .'/';
	$txt = am_var('url') . /*'serve/?txt=/' .*/ 'works/' . $poem[$cols->Work] . '/' . $coll_if . $name_websafe . '.txt';
?>
       <div data-id="<?php echo $name_websafe; ?>" class="post<?php echo ($img ? '' : ' no-image') . ($what === '' ? '' : ' post-slim'); ?>">
         <?php if ($what) echo '<h3>' . $what . '</h3>'; ?>
         <?php if ($img) { ?><div class="pinbin-image"><a href="<?php echo $url; ?>"><img class="piece-image-single img-fluid img-max-300" src="<?php echo $img; ?>" alt="<?php echo $name; ?>" /></a></div><?php } ?>
          <div class="pinbin-category"><p><?php echo $cat; ?> </p></div>
          <div class="pinbin-copy" data-popup-url="<?php echo $txt; ?>"><h2 class="piece-title"><a class="front-link searchable-title" data-match="title" href="<?php echo $url; ?>"><?php echo $poem[$cols->SNo] . ' ' . $name; ?> &rarr;</a></h2>
            <p class="description"><?php echo $poem[$cols->Description]; ?></p> 
            <p class="pinbin-links">
              <a class="open-post">Load Here</a><?php if ($ig) { ?>
              <a class="instagram" href="<?php echo $ig; ?>" target="_blank">InstaGram</a><?php } ?>
            </p>
          </div>
       </div>
<?php
}
