<?php $slim = am_var('node') != 'index'; ?>
		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element include-header" style="height: <?php echo $slim ? '100' : '600'; ?>px;">
			<?php if (!$slim) {?>
			<div class="move-bg position-absolute w-100 h-100" style="top: 0; left: 0;background: url('assets/banners/writing.jpg') center center no-repeat; background-size: cover;"></div>
			<div class="vertical-middle ignore-header">
				<div class="container dark py-5" style="margin-top: 40px;">
					<div class="row">
						<div class="col-lg-6 offset-lg-1 col-md-8" data-lightbox="gallery">
							<?php foreach (am_var('works') as $key => $item) {
								if ($key[0] == ':' || $key == 'reviews') continue;
								echo sprintf('<a href="%sassets/banners/%s.jpg" data-lightbox="gallery-item" class="slider-book-img" data-animate="fadeInUp"><img src="%sassets/banners/tn_%s.jpg" alt="%s" title="%s"></a>',
									am_var('url'), $key, am_var('url'), $key, $key, humanize($key));
							} ?>
							<div class="emphasis-title bottommargin-sm">
								<h1 class="400" data-animate="fadeInUp" data-delay="600">Latest works of me,<br><span><em><?php echo am_var('authorName'); ?></em></span></h1>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Slider Video Overlay -->
			<div class="video-wrap" style="height: 800px; position: absolute; left: 0; bottom: 0;">
				<div class="video-overlay" style="background: -moz-linear-gradient(top,  rgba(30,35,42,0) 21%, rgba(30,35,42,0) 66%, rgba(30,35,42,1) 100%); background: -webkit-linear-gradient(top,  rgba(30,35,42,0) 21%,rgba(30,35,42,0) 66%,rgba(30,35,42,1) 100%); background: linear-gradient(to bottom,  rgba(30,35,42,0) 21%,rgba(30,35,42,0) 66%,rgba(30,35,42,1) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001e232a', endColorstr='#1e232a',GradientType=0 );">
				</div>
			</div>
			<?php } ?>
		</section>
