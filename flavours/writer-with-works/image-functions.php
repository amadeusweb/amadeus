<?php
function image_or_text($fol, $prefix, $name, $large = false) {
	$img = $jpg = 'assets/banners/'. $prefix . urlize($name) . '.jpg';
	$img = file_exists(am_var('path') . '/' . $img) ? am_var('url') . $img : false;

	$text = $name;
	if ($prefix == 'collection-') {
		$fols = am_var('all_collections');
		$text = $fols[$name]['title'];
	}
	$text = '<span class="image-caption">' . $text . '</span>';

	$dims = $prefix == 'category' ? ($large ? [280, 210] : [140, 105]) : ($large ? [600, 240] : [300, 120]);

	if ($img) return sprintf('<img class="img-fluid img-max-300" src="%s" alt="%s" title="%s" />%s', $img . version(), $name, $name, $text);
	return $text;
}

//TODO: Not in use... move to fwk?
function page_banner($url = false) {
	if (!$url && is_array(am_var('video-banners')) && array_key_exists(am_var('node'), am_var('video-banners'))) {
		$video = am_var('video-banners')[am_var('node')];
		echo '<div class="video-bgd"><div class="container"><div class="video-container"><iframe title="' . $video['title'] . '" src="https://www.youtube.com/embed/' . $video['id'] . '?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div></div>';
		return;
	}

	$imgPath = am_var('path') . '/assets/banners/%s.jpg';
	$img = sprintf($imgPath, am_var('node'));
	$banner = am_var('node');

	if (!disk_file_exists($img) ) {
		if (am_var('folderName')) {
			$parent = am_var('currentParentMenuRow');
			$banner = am_var('folderName');
			$img = sprintf($imgPath, $banner);
			if ($parent) {
				$cols = am_var('currentMenuColumns');
				$parentImg = sprintf($imgPath, $parent[$cols->Page]);
				if (disk_file_exists($parentImg)) {
					$img = $parentImg;
					$banner = $parent[$cols->Page];
				}
			}
			if (!disk_file_exists($img) ) return false;
		}
		else
		{
			return false;
		}
	}

	if ($url) return am_var('url') . 'assets/banners/' . $banner . '.jpg';
	echo '<img class="banner-img" src="' . (am_var('node') == 'index' ? '' : '../') . 'assets/banners/' . $banner . '.jpg" alt="' . $banner . '" />';
}

