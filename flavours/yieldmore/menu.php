<?php
if (am_var('site-lock')) { echo '</ul> <!-- #end site -->' . am_var('2nl'); return; }

$outer = am_sub_var('site-menu-settings', 'outer-ul-class');
if (!$outer) $outer = '';

$nodeTheme = am_var('node-theme');

$noOuterUl = am_sub_var('site-menu-settings', 'no-outer-ul');
if (!$noOuterUl) echo '<ul class="' . $outer . '">';

$spacer = am_sub_var('site-menu-settings', 'diagonal-spacer');
if ($spacer) echo $spacer;

$wrapInDiv = am_sub_var('site-menu-settings', 'wrap-text-in-a-div');

$itemClass = am_sub_var('site-menu-settings', 'li-class');
if (!$itemClass) $itemClass = 'drop-down';

$anchorClass = am_sub_var('site-menu-settings', 'a-class');
if (!$anchorClass) $anchorClass = '';

$angle = am_sub_var('site-menu-settings', 'top-level-angle');
if (!$angle) $angle = '';

$nodeFiles = am_sub_var('node-vars', 'files');
if (!$nodeFiles) $nodeFiles = get_menu_files(am_var('section'), am_var('node'));

$mainMenu = am_var('siteMenuName') . $angle;
if ($wrapInDiv) $mainMenu = '<div>' . $mainMenu . '</div>';

if (am_var('site-home-in-menu'))
	echo '<li class="' . (am_var('theme') == 'biz-land' ? '' : $itemClass) . '"><a class="' . $anchorClass . '" href="' . am_var('url') . '">Home</a></li>';

if(!$nodeTheme) {
	echo '<li class="' . $itemClass . '"><a class="' . $anchorClass . '" href="javascript: void();">' . $mainMenu . '</a>';
	$append = am_var('scaffold') ? array_merge(['----'], am_var('scaffold')) : false;
	menu(am_var('folder'), [
		'home-link-to-section' => am_var('home-link-to-section'),
		'files' => get_menu_files('content'),
		'files-to-append' => $append,
	]);
	echo '</li>' . am_var('nl');
}

$inUseVars = compact('nodeTheme', 'itemClass', 'anchorClass', 'nodeFiles');

if (!$nodeTheme && $groups = am_var('section-groups')) {
	$groupUL = am_sub_var('site-menu-settings', 'group-outer-ul-class');
	if (!$groupUL) $groupUL = $outer;
	foreach ($groups as $group => $items) {
		$isGroup = true;
		if (is_string($items)) {
			$group = $items;
			$items = [$items];
			$isGroup = false;
		}

		$name = humanize($group);
		if ($wrapInDiv) $name = '<div>' . $name . $angle . '</div>';

		if ($isGroup) echo '<li class="' . $itemClass . '"><a class="' . $anchorClass . '">' . $name . '</a>';
		if ($isGroup) echo '<ul class="' . $groupUL . '">';

		foreach ($items as $slug) {
			if (cannot_access($slug)) continue;
			$name = humanize($slug);
			if ($wrapInDiv) $name = '<div>' . $name . $angle . '</div>';

			echo '<li class="' . $itemClass . '"><a class="' . $anchorClass . '">' . $name . '</a>' . am_var('nl');
			$parent = array_search($slug, ['gallery']) !== false ? $slug . '/' : '';
			menu('/' . $slug . '/', [
				'parent-slug' => $parent,
				'files' => get_menu_files($slug),
				'where' => $slug . '/',
				'what' => 'node',
				'a-class' => $anchorClass,
				'list-only-folders' => !am_var('sections-have-files'),
				'home-link-to-section' => am_var('home-link-to-section'),
				'parent-slug-for-home-link' => $slug . '/'
			]);
			echo '</li>' . am_var('nl');
		}

		if ($isGroup) echo '</li>';
		if ($isGroup) echo '</ul>' . am_var('nl');
	}
	render_current_menu($inUseVars);
} else if (!$nodeTheme) {
	foreach (am_var('sections') as $slug) {
		if (cannot_access($slug)) continue;
		$name = humanize($slug);
		if ($wrapInDiv) $name = '<div>' . $name . $angle . '</div>';
		$parent = array_search($slug, ['gallery']) !== false ? $slug . '/' : '';
		echo '<li class="' . $itemClass . '"><a class="' . $anchorClass . '">' . $name . '</a>';
		if ($slug)
		menu('/' . $slug . '/', [
			'parent-slug' => $parent,
			'files' => get_menu_files($slug),
			'where' => $slug . '/',
			'what' => 'node',
			'a-class' => $anchorClass,
			'list-only-folders' => !am_var('sections-have-files'),
			'home-link-to-section' => am_var('home-link-to-section'),
			'parent-slug-for-home-link' => $slug . '/'
		]);
		echo '</li>' . am_var('nl');
	}
	render_current_menu($inUseVars);
}

//TODO: HIGH: cleanup and move to menu.php
function render_current_menu($inUseVars) {
	extract($inUseVars);
	$wanted = !$nodeTheme && am_var('section') && am_var('section') != 'gallery';

	if (!$wanted)
		return;

	if (cannot_access(am_var('section')))
		return;

	$folRelative = '/' . am_var('section') . (am_var('section') != am_var('node') ? '/' . am_var('node') : '') . '/';
	$folAbsolute = am_var('path') . $folRelative;
	//NOTE: still throw an error unless sections are allowed to have files
	$skip = am_var('sections-have-files') && !disk_is_dir($folAbsolute);
	if ($skip || menu_is_empty(false, $folAbsolute))
		return;

	if (!$nodeTheme) echo replaceItems('<li class="%itemClass%"><a class="%anchorClass%" href="javascript: void();" style="background-color: var(--amadeus-current-menu-background, yellow);">%text%</a>' .am_var('2nl'),
		['itemClass' => $itemClass, 'anchorClass' => $anchorClass, 'text' => humanize(am_var('node'), 'no-site')], '%');

	menu($folRelative, [
		'files' => $nodeFiles,
		'where' => am_var('node') . '/',
		'what' => 'node',
		'a-class' => $anchorClass,
		'humanize' => 'no-site',
		'parent-slug' => am_var('node') ? am_var('node') . '/' : '',
			'home-link-to-section' => am_var('home-link-to-section') && !$nodeTheme,
		'back-to-home' => $nodeTheme && !am_sub_var('node-vars', 'aux-files'),
	]);

	if (!$nodeTheme) echo '</li>' .am_var('2nl');
}

if (function_exists('after_menu')) after_menu();
if (function_exists('network_after_menu')) network_after_menu();
if (!$noOuterUl) echo '</ul> <!-- #end site -->' .am_var('2nl');
