<?php
$homeUrl = homeUrl();
$logoRel =  logoRel();
?>
				<div class="footer-widgets-wrap pb-5">

					<div class="row">

						<div class="col-sm-12 col-md-6">
							<div class="widget">
								<a class="standard-logo" href="<?php echo $homeUrl;?>"><img src="<?php echo am_var('url'); ?><?php echo $logoRel; ?>-logo.png" alt="<?php echo am_var('name'); ?>" /></a>
							</div>
						</div>

						<div class="col-sm-12 col-md-6">
							<div id="footer-message" class="widget" style="font-size: 90%; text-align: left;">
								<u><?php echo humanize(am_var('node')); ?></u><br />
								<em style="font-weight: normal;"><?php renderMarkdown(am_var('footer-message'), ['strip-paragraph-tag' => true]); ?></em>
								<h4 class="ls0 mb-3 nott" style="margin-top: 20px">Contact / Social</h4>
									<div class="normal-social-icons contrasting-bg-color"><?php foreach(am_var('social') as $item) { ?>
										<a target="_blank" href="<?php echo $item['link']; ?>" class="me-5 si-borderless si-<?php echo $item['type']; ?>">
											<i class="icon-<?php echo $item['type']; ?>"></i>
											<?php echo contact_r($item['link']); ?>	
										</a><br /><?php } ?>
										<hr style="clear: both; margin: 0;" />
									</div>
							</div>
						</div>
						
					</div>
				</div>

