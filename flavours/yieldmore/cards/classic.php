<div class="classic-card card-wrapper card-of-<?php echo urlize($fullName); ?>">
	<div class="card-design">
		<h1><a href="<?php echo $nodeUrl; ?>"><?php echo $fullName; ?></a></h1>
		<h2><?php echo $title; ?></h2>
		<hr />
		<a href="mailto:<?php echo $email; ?>" target="_blank"><?php echo $email; ?></a><br />
		<a href="tel:<?php echo $phone; ?>" target="_blank"><?php echo $phone; ?></a> (<a href="https://wa.me/<?php echo $phone; ?>/" target="_blank">WA</a>)<br />
		<hr />
		<a href="<?php echo $website; ?>" target="_blank"><?php echo str_replace('http://', '', str_replace('https://', '', $website)); ?></a>
		<?php if (isset($description)) { ?><hr /><p><?php renderAny($description); ?></p><?php } ?>
	</div>
</div>

<style>
.classic-card.card-wrapper.card-of-<?php echo urlize($fullName); ?> { background-color: <?php echo isset($cardBackgroundColor) ? $cardBackgroundColor : '#ccc'; ?>; }
.classic-card.card-wrapper { text-align: center; padding: 25px ; }
.classic-card .card-design { max-width: 360px; background-color: #fff; margin: auto; padding: 30px; border-radius: 20px; }
</style>

