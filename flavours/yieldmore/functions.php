<?php
if (!function_exists('before_render')) { function before_render() {
	if (function_exists('site_before_render')) site_before_render();
	read_seo_info();

	if (isSpecialNode()) return;

	$node = am_var('node');
	if ($node != 'gallery' && in_array($node, am_var('sections'))) {
		am_var('section', am_var('node'));
		setSectionVars(SITEPATH . '/' . $node . '/');
		return;
	}

	$extnsCsv = false;
	if ($list = am_var('supported-extensions')) {
		if (($mdix = array_search('md', $list)) !== -1)
			unset($list[$mdix]); //md index - let that be the fallback below
		$extnsCsv = implode(', ', $list);
	}

	foreach (am_var('sections') as $slug) {
		$path = am_var('path') . '/' . $slug . '/';
		$fol2 = $fol = $path . ($slug != 'gallery' ? $node : '');
		$extn = false;

		//TODO: cleanup (flatten-special) and support all special types based on markers in _renderImplementation (blurbs, ft, decks, rich-pages, articles etc)
		if ($extnsCsv) {
			$page = am_var('page_parameter1') ? '/' . am_var('page_parameter1') : '';
			$fwe = $fol . $page . '.';
			$extn = disk_one_of_files_exist($fwe, $extnsCsv);
		}

		if ($extn == false) {
			$extn = 'md';
			if (am_var('node') != $slug) {
				if (isSpecial($fol)) {
					$extn = am_var('file-extension');
					$fol2 = am_var('special-fwe');
				}
			}
		}

		if (disk_is_dir($fol) && $fol == $fol2) {
			if (!endsWith($fol,'/')) $fol .= '/';

			am_var('section', $slug);
			am_var('nodeFolder', $fol);

			setNodeVars($fol);
			setNodeSeo($fol);
			break;
		} else if (disk_file_exists($fol2 . '.' . $extn)) {
			am_var('file', $fol2 . '.' . $extn);

			if (!endsWith($fol2,'/')) $fol2 .= '/';

			am_var('section', $slug);
			am_var('nodeFolder', am_var_or('special-root-folder', $fol2));

			setNodeVars($fol2);
			setNodeSeo($fol2);
			break;
		}
	}
} }

if (!function_exists('did_render_page')) { function did_render_page() {
	if (cannot_access(am_var('section'), 'section', true)) return true;

	if (renderedSpecial()) return true; //nix the rest!

	if (in_array(am_var('node'), am_var('sections'))) {
		am_var('directory_of', am_var('node'));
		includeFeature('directory');
		return true;
	}

	if ($file = am_var('file')) {
		do_file_wrapper($file);
		echo '<section>';

		if (endsWith($file, '.php')) renderAnyFile($file);
		else renderFile($file);

		echo '</section>';
		do_file_wrapper($file, 'after');
		return true;
	}

	if (!($section = am_var('section')))
		return false;

	$folder = am_var('nodeFolder');
	$node = am_var('node');
	$vars = setNodeVars($folder);

	$isNodeHome = !am_var('page_parameter1');
	$file = am_var('page_parameter1') ? am_var('page_parameter1') : 'home';
	am_var('node-item', $file);
	am_var('is-node-item', $file != 'home');

	$finalFile = am_var_or('file', $folder . $file . '.md');
	$nodeItemAndSkip = $file != 'home' && isset($vars['dont-render-markdown-for-node-item']);

	if (!$nodeItemAndSkip) {
		do_file_wrapper($finalFile);
		echo '<section>' . am_var('nl');
		renderFile($finalFile);
		echo '</section>' . am_var('2nl');
		do_file_wrapper($finalFile, 'after');
	} else {
		includeFeature('engage');
		renderEngageOrMd(ENGAGEPROMOTION, $finalFile, $file);
	}

	return true;
} }

function before_file() {
	if (am_var('theme') == 'biz-land' || am_var('theme') == 'artist') {
		if (am_var('theme') == 'biz-land') echo '<hr class="above-header-content scroll-to-offset" />' . am_var('nl');
		echo '<div id="content" class="content container node-' . am_var('node') . ' site-' . am_var('safeName') . '">' . am_var('2nl');
	} else if (am_var('theme') == 'cv-store') {
		echo '		<section id="content">' . am_var('nl');
		echo '			<div class="content-wrap pb-0">' . am_var('nl');
		if (am_var('page_parameter1') || am_var('has-container')) echo '				<div class="container">' . am_var('nl');
	} else if (am_var('theme') == 'cv-nonprofit') {
		if (am_var('needs-container'))
			echo '<div id="content" class="content container node-' . am_var('node') . ' site-' . am_var('safeName') . '">';
	} else if (am_var('theme') == 'cv-modern-blog') {
		if (am_var('needs-container'))
			echo '<div class="container clearfix node-' . am_var('node') . ' site-' . am_var('safeName') . '">';
	} else if (am_var('theme') == 'cv-writer') {
		if (am_var('needs-container'))
			echo '<div class="container clearfix node-' . am_var('node') . ' site-' . am_var('safeName') . '">';
	}

	if (cannot_access(am_var('section'))) { return; }

	if (function_exists('site_before_file')) site_before_file();
	runFwk('header'); doHeader();
}

function after_file() {
	do_updates();
	if (function_exists('site_after_file')) site_after_file();
	runCode('optional-before-footer');
	if (am_var('local')) seo_info();
	if (am_var('theme') == 'biz-land') {
		//NOTE: cant close div for artist here, but need to open it in before_file
		echo  am_var('2nl') . '</div><!-- end #content-->' . am_var('2nl');
	} else if (am_var('theme') == 'cv-store') {
		if (am_var('page_parameter1') || am_var('has-container')) echo '				</div>' . am_var('nl');
		echo '			</div>' . am_var('nl');
		echo '		</section><!-- #content end -->' . am_var('nl');
	} else if (am_var('theme') == 'cv-nonprofit' || am_var('theme') == 'cv-modern-blog' || am_var('theme') == 'cv-writer') {
		if (am_var('needs-container')) echo '				</div>' . am_var('nl');
	}
}

function site_humanize($txt, $field = 'title', $how = false) {
	if ($how !== 'no-wiki' && function_exists('wiki_topic_humanize') && $wikiTopic = wiki_topic_humanize($txt, $how)) return $wikiTopic;

	$pages = [
		'faqs' => 'Frequently Asked Questions',
	];

	if (array_key_exists($key = strtolower($txt), $pages))
		return $pages[$key];

	$pages = am_var_or('siteHumanizeReplaces', []);
	if (array_key_exists($key = strtolower($txt), $pages))
		return $pages[$key];

	if (has_var('siteHumanizeReplaces2')) {
		$pages = am_var_or('siteHumanizeReplaces2', []);
		if (array_key_exists($key = strtolower($txt), $pages))
			return $pages[$key];
	}

	return $txt;
}

function item_r($col, $item, $return = false) {
	$cols = am_var('sectionColumns');

	$r = $item[$cols[$col]];

	$r = str_replace('|', '<br />', $r);
	$r = simplify_encoding($r);

	if (endsWith($col, '_md') || contains($col, 'content'))
		$r = renderMarkdown($r, ['echo' => false]);
	else
		$r = replace_vars($r);

	$r = trim($r);
	if ($return) return $r;

	echo $r;
}

?>
