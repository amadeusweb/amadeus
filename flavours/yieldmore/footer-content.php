<?php
if (am_var('site-lock') || am_var('skip-footer')) return;
if (am_var('theme') == 'cv-store') { runCode('footer-content-for-store'); return; }

//TODO: use compact?
$homeUrl = homeUrl();
$logoUrl =  logoUrl();
$logoRel =  logoRel();
?>
<div id="footer-content" class="footer-bgd" style="margin-top: 30px;">
<?php if (function_exists('network_footer')) network_footer(); ?>
	<div class="container">
		<?php if (!am_var('no-engage')) { includeFeature('engage'); runDefinedEngages(); }?>
		<section id="footer-message">
			<a href="<?php echo $homeUrl;?>"><img src="<?php echo $logoUrl; ?><?php echo $logoRel; ?>-logo@2x.png" class="img-fluid img-max-<?php echo am_var_or('footer-logo-max-width', '500')?>" alt="<?php echo am_var('name'); ?>" /></a><br />
			<u><?php echo am_var('name'); ?></u> &mdash;
			<?php renderMarkdown(am_var_or('footer-message', ''), ['strip-paragraph-tag' => true]); ?>
			<hr />
			<?php echo_if_var('footer-links'); ?>
			<?php if (has_var('footer-links')) {?><hr /><?php } ?>
			<div class="social-links"><?php foreach(am_var('social') as $item) { ?>
				<a target="_blank" href="<?php echo $item['link']; ?>" title="<?php echo isset($item['name']) ? $item['name'] : $item['type']; ?>" class="<?php echo $item['type']; ?>"><i class="icofont-<?php echo $item['type']; ?>"></i></a><?php } ?>
			</div>
		</section>
		<?php includeFeature('share'); ?>
		<?php assistant('load'); ?>
	</div>
</div>
