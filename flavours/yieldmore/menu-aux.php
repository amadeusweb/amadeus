<?php
$nodeAuxFiles = am_sub_var('node-vars', 'aux-files');
if (!$nodeAuxFiles) return;

$outer = am_sub_var('site-menu-settings', 'outer-ul-class');
if (!$outer) $outer = '';

$nodeTheme = am_var('node-theme');

$noOuterUl = am_sub_var('site-menu-settings', 'no-outer-ul');
if (!$noOuterUl) echo '<ul class="' . $outer . '">';

$spacer = am_sub_var('site-menu-settings', 'diagonal-spacer');
if ($spacer) echo $spacer;

$wrapInDiv = am_sub_var('site-menu-settings', 'wrap-text-in-a-div');

$itemClass = am_sub_var('site-menu-settings', 'li-class');
if (!$itemClass) $itemClass = 'drop-down';

$anchorClass = am_sub_var('site-menu-settings', 'a-class');
if (!$anchorClass) $anchorClass = '';

$angle = am_sub_var('site-menu-settings', 'top-level-angle');
if (!$angle) $angle = '';

$mainMenu = am_var('siteMenuName') . $angle;
if ($wrapInDiv) $mainMenu = '<div>' . $mainMenu . '</div>';

if (!$nodeTheme) { ?><li class="<?php echo $itemClass; ?>"><a class="<?php echo $anchorClass; ?>" href="javascript: void();" style="background-color: yellow;"><?php echo humanize(am_var('node')); ?></a><?php } ?>
<?php menu('/' . am_var('section') . '/' . am_var('node') . '/', [
	'files' => $nodeAuxFiles,
	'back-to-home' => true,
]);

if (!$nodeTheme) echo '</li>';

if (!$noOuterUl) echo '</ul>';
?>

