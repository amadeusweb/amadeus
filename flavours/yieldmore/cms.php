<?php
am_var('local', $local = startsWith($_SERVER['HTTP_HOST'], 'localhost')); //NOTE: declare before including functions as used by sitemap


if ($engine = am_var('engine')) disk_include_once(__DIR__ . '/engine-' . $engine . '.php');

disk_include_once(__DIR__ . '/functions.php');

am_var('rootUrl', $local ? replace_vars('http://localhost%port%/yieldmore/', 'port') : 'https://yieldmore.org/');
am_var('path', SITEPATH); //needed to runCode('site-functions')


if (disk_file_exists(SITEPATH . '/site-cms.php'))
	disk_include_once(SITEPATH . '/site-cms.php');
else
	bootstrap([]);

am_vars([
	'flavourVersion' => [ 'id' => '2.5', 'date' => '22 Jul 2023' ],

	'folder' => '/content/',

	'theme' => has_var('theme') ? am_var('theme') : 'biz-land',
	'uses' => 'custom-image-background, always-topbar',
	'rootEmail' => 'imran@amadeusweb.com',

	'styles' => has_var('styles') ? am_var('styles') : ['site'],
]);

if ($mdFooter = am_var('footer_post_credits_markdown')) am_var('footer_post_credits', renderMarkdown($mdFooter, ['echo' => false]));

if (hasPageParameter('embed')) am_var('embed', true);

render();
