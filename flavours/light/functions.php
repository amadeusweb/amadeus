<?php
function before_render() {
	if (am_var('node') != 'index') am_var('logo-above-content', true); //TODO: do this when all the rich-pages are not in use

	if (function_exists('awesome_before_render')) awesome_before_render();
	read_seo_info();

	if (isAwesomeNode()) return;
	if (isSpecialNode()) return;

	foreach (am_var('sections') as $slug) {
		if ($slug == $node = am_var('node')) {
			am_var('directory_of', $node);
			am_var('section', $slug);
			return _callAndVoid();
		}

		$page = am_var('page_parameter1') ? am_var('page_parameter1') : 'home';
		$fwes = [
			am_var('path') . '/' . $slug . '/' . $node . '/' . $page . '.',
			am_var('path') . '/' . $slug . '/' . $node . '.',
		];

		foreach ($fwes as $fwe) {
			$ext = disk_one_of_files_exist($fwe, 'php, md, tsv');
			if ($ext) {
				am_var('file', $fwe . $ext);
				am_var('section', $slug);
				return _callAndVoid();
			}
		}

		if (disk_is_dir($fol = am_var('path') . '/' . $slug . '/' . $node . '/')) {
			$ext = disk_one_of_files_exist($fwes[0], 'php, md, tsv');
			if ($ext) {
				am_var('file', $fwes[0] . $ext);
				am_var('section', $slug);
				return _callAndVoid();
			}
		}
	}
}

function _callAndVoid() {
	if (function_exists('site_before_render')) site_before_render();
}

function did_render_page() {
	if (renderedAwesome()) return true;
	if (renderedSpecial()) return true; //nix the rest!

	if (am_var('directory_of')) {
		includeFeature('directory');
		return true;
	}

	if ($file = am_var('file')) {
		if (am_var('auto-render'))
			autoRenderIfSpecial($file);
		else
			_renderSingleFile($file);
		return true;
	}
}
