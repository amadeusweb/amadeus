<div id="footer-content" class="footer-bgd" style="margin-top: 30px;">
	<div class="container">
		<?php includeFeature('engage'); runDefinedEngages(); ?>
		<section id="footer-message">
			<u><?php echo am_var('name'); ?></u> &mdash;
			<?php renderMarkdown(am_var_or('footer-message', ''), ['strip-paragraph-tag' => true]); ?>
			<hr />
			<?php echo_if_var('footer-links'); ?>
			<?php if (has_var('footer-links')) {?><hr /><?php } ?>
			<div class="social-links"><?php foreach(am_var('social') as $item) { ?>
				<a target="_blank" href="<?php echo $item['link']; ?>" title="<?php echo isset($item['name']) ? $item['name'] : $item['type']; ?>" class="<?php echo $item['type']; ?>"><i class="icofont-<?php echo $item['type']; ?>"></i></a><?php } ?>
			</div>
		</section>
		<?php includeFeature('share'); ?>
		<?php assistant('load'); ?>
	</div>
</div>
