<?php
if (!am_var('menuLeft')) return;
function menuSide($sections) {
	foreach($sections as $item) {
		echo '<li class="menu-item"><a class="menu-link" href="' . am_var('url') . $item . '/"><div>' . humanize($item) .'</div></a>' . am_var('nl');
		echo '<ul class="sub-menu-container">' . am_var('nl');
		$op = [];
		$items = get_menu_files($item);
		if (!$items) $items = disk_scandir(SITEPATH . '/' . $item);
		foreach($items as $slug) {
			if ($slug[0] == '.') continue;
			if (in_array($slug, ['assets', 'images', 'docs'])) continue;
			$slug = replaceItems($slug, ['.md' => '']);
			$op[$slug . '/'] = humanize($slug);
		}
		render_menu($op);
		echo '</ul></li>' . am_var('2nl');
	}
}
?>

<li class="menu-item"><a class="menu-link" href="<?php echo am_var('url'); ?>"><div>Home</div></a></li>
<?php menuSide(am_var('menuLeft')); ?>

