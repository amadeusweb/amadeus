<?php
am_var('local', $local = startsWith($_SERVER['HTTP_HOST'], 'localhost')); //NOTE: declare before including functions as used by sitemap

include_once 'functions.php';

if (disk_file_exists(SITEPATH . '/site-cms.php'))
	disk_include_once(SITEPATH . '/site-cms.php');
else
	bootstrap([]);

am_vars([
	'flavourVersion' => [ 'id' => '1', 'date' => '18 Oct 2024' ],

	'folder' => '/content/',

	'theme' => am_var_or('theme', 'cv-writer'),
	'theme-font-color' => am_var_or('theme-font-color', '4f6d7a'),
	'styles' => am_var_or('styles', ['site']),
	'footer-not-sticky' => true,

	'path' => SITEPATH,
]);

if ($mdFooter = am_var('footer_post_credits_markdown')) am_var('footer_post_credits', renderMarkdown($mdFooter, ['echo' => false]));

render();
