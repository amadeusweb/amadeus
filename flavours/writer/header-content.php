<?php
if (am_var('node-theme')) return;

//TODO: render_banner(); folder/images/node-PH
render_section_menu();
if (!am_var('no-assistant')) assistant();

function render_section_menu() {
	$folRelative = am_var('section') . '/' . am_var('node') . '/';
	if (!am_var('section') || !disk_is_dir(am_var('path') . '/' . $folRelative)) return;

	echo '<div id="header" class="container">';
	echo '<section id="header-menu" class="amadeus-feature">';
	$collapsible = am_sub_var('node-vars', 'large-menu')
		? ' class="collapsible-ul"' : '';
	echo '<h1' . $collapsible . '><small>SITE:</small> <a href="' . am_var('url') . am_var('node') .'/">"<u>' . humanize(am_var('node')) . '</u>"</a></h1>';

	if ($byline = am_sub_var('node-vars', 'byline'))
		echo '<h3>' . renderMarkdown($byline, ['strip-paragraph-tag' => true]) . '</h3><hr />';

	menu('/' . $folRelative, [
		'blog-heading' => contains(am_var('section'), 'blog'),
		'parent-slug' => am_var('node') . '/',
		'files' => am_sub_var('node-vars', 'files'),
		'breaks' => am_sub_var('node-vars', 'menu-breaks'),
	]);

	echo '</section>';
	echo '</div>';
} //end render_section_menu

?>

