<?php
function before_render() {
	if (function_exists('site_before_render')) site_before_render();
	$node = am_var('node');

	foreach (am_var('sections') as $slug) {
		$section = am_var('path') . '/' . $slug . '/';

		if ($slug == am_var('node')) {
			am_var('section', $slug);
			am_var('nodeFolder', $section);
			break;
		}

		$fol = $section . am_var('node');
		$file = $fol . '/' . (am_var('page_parameter1') ? am_var('page_parameter1') : 'home') . '.md';

		if (disk_is_dir($fol)) {
			am_var('section', $slug);
			am_var('nodeFolder', $fol);
			break;
		/*
		} else if (disk_file_exists($file)) {
			am_var('section', $slug);
			am_var('nodeFolder', $fol);
			am_var('file', $file);
			break;
		*/
		} else {
			$subFils = disk_scandir($section);

			foreach ($subFils as $item2) {
				if ($item2[0] == '.') continue;
				$fol2 = $section . $item2;

				if (disk_file_exists($file = $section . $node . '.md')) {
					am_var('section', $slug);
					am_var('nodeFolder', $section);
					am_var('file', $file);
					return;
				} else if ($item2 == $node && disk_is_dir($fol2)) {
					am_var('section', $slug);
					am_var('nodeFolder', $fol2);
					return;
				}
			}
		}
	}
}

function did_render_page() {
	if ($file = am_var('file')) {
		do_file_wrapper($file);
		sectionId('file');
		renderAny($file, ['plainReplaces' => am_var_or('plainReplaces', [])]);
		section('end');
		do_file_wrapper($file, 'after');
		return true;
	}

	if (!($section = am_var('section')))
		return false;

	$folder = am_var('nodeFolder');
	$relFolder = str_replace(SITEPATH, '', $folder);
	$node = am_var('node');

	am_var('directory_of', $section == $node ? $node : trim($relFolder, '/'));
	includeFeature('directory');

	return true;
}

function before_file() {
	if (am_var('embed')) return;
	echo sprintf('<div class="container%s">', !am_var('is_single') ? '' : ' single');
}

function after_file() {
	if (am_var('embed')) return;
	echo '</div>';
}
