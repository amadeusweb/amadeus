<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

/*
//NB: Uncomment this to setup a standalone version of amadeus
function startsWith2($haystack, $needle)
{
  $length = strlen($needle);
  return (substr($haystack, 0, $length) === $needle);
}

DEFINE('AMADEUSURL', startsWith2($_SERVER['HTTP_HOST'], 'localhost') ? 'http://localhost/amadeus2/' : 'http://v2.amadeusweb.com/');
*/

define('SITEPATH', __DIR__);
include_once 'framework/entry.php';

am_var('flavour', 'yieldmore');
runCode('cms');
?>
