<?php
$usePreview = am_var_or('use-preview', false);
$local = am_var('local'); //this is now in before_bootstrap
if ($usePreview) am_var('preview', $preview = contains($_SERVER['HTTP_HOST'], 'preview'));

//tests preview urls locally
//$local = false; $preview = true;

if ($usePreview) {
	am_var('live', $liveFolder = contains(__DIR__, 'live'));

	if ($liveFolder)
		am_var('site-url-key', ($local ? 'live-on-local' : 'live') . '-url');
	else
		am_var('site-url-key', ($preview ? 'preview' : 'local') . '-url');

	//until green
	if (am_var('live-is-empty') && $liveFolder && !$local) {
		echo '<!--silence-->';
		exit;
	}
} else {
	am_var('site-url-key', ($local ? 'local' : 'live') . '-url');
}

function __testSiteVars($array) {
	return; //comment to test
	print_r($array);
}

$sheet = get_sheet('site', false);
$cols = $sheet->columns;

$siteVars = [];
foreach ($sheet->rows as $row) {
	$key = $row[$cols['key']];
	if (!$key || $key[0] == '|') continue;
	$siteVars[$key] = $row[$cols['value']];
}

am_var('site-vars', $siteVars);

if (isset($siteVars['version'])) {
	am_var('version', $version = textToArray($siteVars['version']));
	__testSiteVars(['version' => $version]);
}

if (contains($url = $siteVars[am_var('site-url-key')], 'localhost')) {
	$url = replaceItems($url, ['localhost' => 'localhost' . am_var('port')]);
	__testSiteVars(['url-for-localhost' => $url]);
}

function parseSectionsAndGroups($siteVars, $return = false, $forNetwork = false) {
	if (am_var('sections') && !$forNetwork) return;
	$sections = isset($siteVars['sections']) ? $siteVars['sections'] : false;
	if (!$sections) {
		$sections = [];
		if (!$forNetwork) am_var('sections', $sections);
		__testSiteVars(['sections' => $sections]);
		return $sections;
	}

	$vars = [];
	//Eg.: research, causes, solutions, us: programs+members+blog
	if (contains($sections, ':')) {
		$swgs = explode(', ', $sections); //sections wtih groups
		$items = []; $groups = [];

		foreach ($swgs as $item) {
			if (contains($item, ':')) {
				$bits = explode(': ', $item, 2);
				$subItems = explode('+', $bits[1]);
				$groups[$bits[0]] = $subItems;
				$items = array_merge($items, $subItems);
			} else {
				$items[] = $item;
				$groups[] = $item;
			}
		}

		$vars['sections'] = $items;
		$vars['section-groups'] = $groups;
	} else {
		$vars['sections'] = explode(', ', $sections);
	}

	if ($return) return $vars;

	__testSiteVars($vars);
	am_vars($vars);
}

parseSectionsAndGroups($siteVars);

//valueIfSetAndNotEmpty
function _visane($siteVars) {
	$possibles = [
		['site-home-in-menu', false, 'bool'],
		['use-menu-files', false, 'bool'],
		['large-menu', false, 'bool'],
		['large-menus-for', [], 'array'],
		['home-link-to-section', false, 'bool'],
		['ChatraID', '--use-amadeusweb'],
		['google-analytics', '--use-amadeusweb'],

		['email', 'imran@amadeusweb.com'],
		['phone', '+91-9841223313'],
		['whatsapp', '919841223313'],
		['address', 'Chennai, India'],

		['description', false],
		['network', false], //string will be returned by default if set
	];

	if (!has_var('theme'))
		$possibles[] = ['theme', 'biz-land'];

	$op = [];
	foreach ($possibles as $cfg)
		$op[$cfg[0]] = valueIfSetAndNotEmpty($siteVars, $cfg[0], $cfg[1], isset($cfg[2]) ? $cfg[2] : 'no-change');

	__testSiteVars($op);
	am_vars($op);
}

function _always($siteVars) {
	$op = [];
	$always = [
		'name',
		'byline',
		'safeName',
		'footer-message',
		'siteMenuName',
	];
	foreach ($always as $item)
		$op[$item] = $siteVars[$item];

	$op['start_year'] = $siteVars['year'];

	__testSiteVars($op);
	am_vars($op);
}

_visane($siteVars);
_always($siteVars);

$safeName = $siteVars['safeName'];
$network = am_var('network');

$css = [];
if (disk_file_exists(SITEPATH . '/assets/site.css')) $css[] = 'site';

am_vars($op = [
	'flavour' => am_var_or('flavour', 'yieldmore'),
	//version done above using textToArray
	'folder' => '/content/',
	//sections also done above in parseSectionsAndGroups
	'image-in-logo' => disk_file_exists(SITEPATH . '/' . $safeName . '-logo.png') ? '-logo.png' : false,
	'siteHumanizeReplaces' => siteHumanize(),

	'home-link-to-section' => true, //directory will show these
	'sections-have-files' => true,

	'scaffold' => isset($siteVars['scaffold']) ? explode(', ', $siteVars['scaffold']) : [],

	'path' => SITEPATH,
	'url' => $url,
]);

__testSiteVars($op);

//TODO: add_foot_hook(AMADEUSTHEMEFOLDER . 'media-kit.php');

if ($network) setupNetworkLinks($network, $css);
am_var('styles', $css); //network file may get added in the function above


function setupNetworkLinks($network, &$css) {
	$data = siteRealPath('/../' . $network . '/data/network.tsv');
	//if (!disk_file_exists($data)) return; //NOTE: Design By Contract - let it throw

	$networkSheet = get_sheet($data, false);

	$op = [];
	$newTab = false ? 'target="_blank" ' : '';
	$configs = [];

	$imgIndex = isset($networkSheet->columns['img']) ? $networkSheet->columns['img'] : '';
	$themeIndex = isset($networkSheet->columns['theme']) ? $networkSheet->columns['theme'] : false;
	$groupIndex = isset($networkSheet->columns['group']) ? $networkSheet->columns['group'] : false;

	foreach ($networkSheet->rows as $row) {
		$site = $row[$networkSheet->columns['slug']];

		$sheetFile = siteRealPath('/../' . $site . '/data/site.tsv');
		if (!has_sheet($sheetFile)) { continue; }

		$sheet = get_sheet($sheetFile, 'key');
		$val = $sheet->columns['value'];

		$img = $imgIndex ? $row[$imgIndex] : '';
		$theme = $themeIndex === false ? false : $row[$themeIndex];
		$group = $groupIndex === false ? false : $row[$networkSheet->columns['group']];

		if ($theme && basename(SITEPATH) == $site)
			am_var('theme', $theme);

		$item = $sheet->sections;

		if (contains($url = $item[am_var('site-url-key')][0][$val], 'localhost'))
			$url = replaceItems($url, ['localhost' => 'localhost' . am_var('port')]);

		$op[] = sprintf('<a href="%s" %stitle="%s &mdash; %s">%s</a>',
			$url, $newTab, $name = $item['name'][0][$val], $byline = $item['byline'][0][$val], $item['name'][0][$val], am_var('nl'));

		$networkName = false; $networkByline = false; $networkMessage = false;
		if ($site == $network) {
			if (!am_var('version') && isset($item['version']))
				am_var('version', textToArray($item['version'][0][$val]));

			if (isset($item['network-name']))
				$networkName = $item['network-name'][0][$val];

			if (isset($item['network-byline']))
				$networkByline = $item['network-byline'][0][$val];

			if (isset($item['network-message']))
				$networkMessage = $item['network-message'][0][$val];

			if (isset($item[$networkKey = 'network-' . am_var('site-url-key')]))
				am_var('network-url', $item[$networkKey][0][$val]);
			
			if (disk_file_exists(siteRealPath('/../' . $site . '/assets/network.css')))
				$css[] = $url . 'assets/network';

			if (disk_file_exists($nfn = siteRealPath('/../' . $site . '/code/network.php')))
				disk_include_once($nfn, ['name' => $name, 'byline' => $byline, 'networkName' => $networkName, 'siteTheme' => $theme]);
		}

		$configs[$site] = [
			'name' => $name, 'byline' => $byline,
			'safeName' => $item['safeName'][0][$val],
			'vars' => parseSectionsAndGroups(['sections' => $item['sections'][0][$val]], true, true),
			'img' => $img, 'url' => $url,
			'link' => end($op),
			'item' => $item, //dont want to recreate the tsv path
			'valueIndex' => $val,
			'network-name' => $networkName,
			'network-byline' => $networkByline,
			'network-message' => $networkMessage,
			'group' => $group,
		];
	}
	
	am_var('network-links', $op);
	__testSiteVars(['network-links' => $op]);

	if ($network) {
		am_var('network-site-configs', $configs);
		am_var('network-configs', $nw = $configs[$network]);
		am_var('is-network-site', am_var('safeName') == $nw['safeName']);
		return; //implies that a network footer will take care
	}

	$html = '<span class="container network-links">NETWORK:<br />';
	$html .= implode(' | ' . am_var('nl'), $op);
	$html .= '</span>';

	am_var('footer-links', $html);
}

runCode('cms');
