<?php
function setSectionVars($folder) {
	$result = [];
	$relative = str_replace(SITEPATH . '/', '', $folder);
	$bits = explode('/', substr($folder, 0, -1));

	$result['nodeFolder'] = $folder;
	$result['sectionUrl'] = am_var('url') . am_var('section') . '/';
	$result['nodeUrl'] = am_var('url') . $relative;
	$result['nodeLink'] = am_var('url') . array_pop($bits) . '/';

	am_var('node-vars', $result);
	return $result;
}

function setNodeVars($folder, $inLoop = false) {
	if (am_var('node-vars-loaded'))
		return am_var('node-vars'); //caching

	$folder = am_var_or('special-root-folder', $folder);
	$relative = str_replace(SITEPATH . '/', '', $folder);
	am_var('nodeUrl', am_var('url') . $relative); //needed to set og:image in _vars.php

	if (disk_file_exists($cfg = $folder . '_vars.php')) {
		disk_include_once($cfg);
		$result = am_var('node-vars');
	} else {
		$result = [];
	}

	$result['nodeFolder'] = $folder;
	$result['nodeRelativePath'] = $relative = str_replace(SITEPATH . '/', '', $folder);
	$result['nodeUrl'] = am_var('url') . $relative;
	$result['sectionUrl'] = am_var('url') . am_var('section') . '/';
	$bits = explode('/', substr($folder, 0, -1));
	$result['nodeLink'] = am_var('url') . array_pop($bits) . '/';

	$result['nodeAndPage'] = (am_var('page_parameter1') ? humanize(am_var('page_parameter1')) . ' &mdash; ' : '') . humanize(am_var('node'));

	$nodePage = $result['nodeRelativePath'] . (!$inLoop && am_var('page_parameter1') ? am_var('page_parameter1') : 'home');
	$result['nodeEdit'] = 'EDIT FILE: ./' . $nodePage . '.md';

	$result['sectionUrl'] = am_var('url') . am_var('section') . '/';

	am_var('node-vars', $result);
	am_var('node-vars-loaded', true);

	return $result;
}

function get_content_items($what) {
	$files = disk_scandir(SITEPATH . '/content/' . $what);
	$items = [];
	foreach ($files as $file) {
		if ($file[0] == '.' || $file[0] == '_' || $file == 'home.md') continue;
		$file = str_replace('.md', '', $file);
		$txt = replaceItems(humanize($file), ['It' => 'IT', 'An' => 'an', 'To' => 'to']);
		$items[$what . $file . '/'] = $txt;
	}
	return $items;
}

function siteHumanize() {
	if (!has_sheet('humanize')) return [];

	$sheet = get_sheet('humanize', false);
	$cols = $sheet->columns;
	$result = [];

	foreach ($sheet->rows as $item)
		$result[$item[$cols['key']]] = $item[$cols['text']];

	return $result;
}

function do_file_wrapper($file, $where = 'before') {
	$cw = am_var_or('cw_' . $file, is_composite_work(disk_file_exists($file) ? disk_file_get_contents($file) : ''));
	if ($cw) echo $where == 'after' ? '</div>' . am_var('2nl') : '<div class="composite-work">' . am_var('nl');
}
