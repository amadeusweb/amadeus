<?php
function am_var($name, $val = null)
{
	global $cscore;
	if (!isset($cscore)) $cscore = array();
	if ($val !== null)
		$cscore[$name] = $val;
	else
		return isset($cscore[$name]) ? $cscore[$name] : false;
}

function am_sub_var($parent, $key)
{
	$a = am_var($parent);
	return is_array($a) && isset($a[$key]) ? $a[$key] : false;
}

function am_vars($a)
{
	foreach ($a as $key=>$value)
		am_var($key, $value);
}

function am_var_or($name, $or, $hasVar = null)
{
	if (!has_var($name) && $hasVar !== null) return $hasVar;
	$val = am_var($name);
	return $val ? $val : $or;
}

function clear_var($name) {
	if (!has_var($name)) return;
	global $cscore;
	unset($cscore[$name]);
}

function has_var($key)
{
	global $cscore;
	return isset($cscore[$key]);
}

function has_sub_var($name, $subName)
{
	$a = am_var_or($name, []);
	return isset($a[$subName]);
}

function echo_if_var($key)
{
	if(!am_var($key)) return;
	echo replace_vars(am_var($key));
}

function is_debug($value = false) {
	$qs = item_or($_GET, 'debug');
	if ($value == 'verbose') return $qs == 'verbose';
	return $qs || am_var('debug');
}

function replace_vars($text, $vars = 'url, app, app-assets')
{
	if (!is_array($vars)) {
		$bits = explode(', ', $vars);
		$vars = [];
		foreach ($bits as $bit) {
			$vars[$bit] = am_var($bit);
		}
	}

	foreach($vars as $key => $value) $text = str_replace('%' . $key . '%', $value, $text);
	return $text;
}

function uses($what) {
	$uses = am_var('uses');
	if (!$uses) return false;
	return in_array($what, explode(', ', $uses));
}

function disabled($what) {
	$uses = am_var('disabled');
	if (!$uses) return false;
	return in_array($what, explode(', ', $uses));
}

