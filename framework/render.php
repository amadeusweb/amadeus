<?php
//DEPRECATED: Cleanup everywhere then remove
//NOTE: 40 occurences in 30 excluded files
function render_txt_or_md($file, $replaces = [], $echo = true) {
	return renderFile($file, $replaces, $echo);
}

am_var('known-extensions', [
	'prefixes' => ['jpg'],
	'core' => ['php', 'md', 'tsv', 'html', 'txt'],
	'suffixes' => ['pdf'],
]);

function remove_extension($file) {
	if (!($core = am_var('replace_extensions'))) {
		$extns = am_sub_var('known-extensions', 'core');
		$core = [];
		foreach ($extns as $extn) {
			$core['.' . $extn] = '';
		}
		am_var('replace_extensions', $core);
	}

	return replaceItems($file, $core);
}

//extensions will render multiple. Can be - explicit / prefixes / core / suffixes / array[]
function renderAnyFile($fwe, $settings = []) {
	$extensions = valueIfSet($settings, 'extensions', 'explicit');
	$fail = valueIfSet($settings, 'fail', true);
	$returnOnFirst = valueIfSet($settings, 'return-on-first', true); //adapt when implementing for archives

	$inUseValues = [ 'extensions' => $extensions, 'fail' => $fail, 'return-on-first' => $returnOnFirst ];
	$failParams = ['$fwe (file with/without extension)' => $fwe, 'in-use-values' => $inUseValues, 'settings' => $settings];
	$known = am_var('known-extensions');

	if ($extensions == 'explicit') {
		$exists = disk_file_exists($fwe);
		if ($exists) {
			_renderSingleFile($fwe);
			return true;
		}

		if ($fail) raiseParameterError('FILE NOT FOUND', $failParams);
	} else if (is_array($extensions)) {
		//fail never applies here
		foreach($extensions as $extension) {
			$fpe = $fwe . $extension; //name PLUS extension
			$exists = disk_file_exists($fpe);
			if ($exists) {
				if (am_var('auto-render'))
					autoRenderIfSpecial($fpe);
				else
					_renderSingleFile($fpe);
				if ($returnOnFirst) return true;
			}
		}
	} else if (array_key_exists($extensions, $known)) {
		$newSettings = array_merge($inUseValues, ['extensions' => $known[$extensions]]);
		return renderAnyFile($fwe, $newSettings);
	} else if ($extensions == 'all') {
		foreach ($known as $key => $item) {
			$newSettings = array_merge($inUseValues, ['extensions' => $key]);
			$result = renderAnyFile($fwe, $newSettings);
			if ($result && $returnOnFirst) return true;
		}
	} else {
		parameterError('CRITICAL - NOT SUPPORTED', $failParams);
		exit;
	}
}

//internal method - expects file to exist 
function _renderSingleFile($file, $extension = 'auto') {
	if ($extension == 'php' || endsWith($file, '.php')) {
		disk_include_once($file);
		return;
	}

	//TODO: Copy media + pdf logic from archives.yieldmore.org

	echo '<section>' . am_var('nl');
	renderFile($file);
	echo '</section>' . am_var('2nl');

}

//The below render methods support text / file path
function renderFile($file, $replacesParams = [], $echo = true, $excerpt = false, $settings = []) {
	//TODO: remove all params from this method
	$nonDefaultParams = $replacesParams != [] || $echo != true || $excerpt != false || $settings != [];

	if (am_var('local') && $nonDefaultParams) parameterError('DEPRECATED FUNCTION:', 'renderFile()');

	$settings['replaces'] = $replacesParams;
	$settings['echo'] = $echo;
	$settings['excerpt'] = $excerpt;

	return renderAny($file, $settings);
}

function renderPlainHtml($file) {
	echo disk_file_get_contents($file);
}

function renderExcerpt($file, $link, $prefix = '', $echo = true) {
	$prefix = $prefix ? renderMarkdown($prefix) : '';
	$raw = renderAny($file, ['excerpt' => true, 'echo' => false, 'markdown' => endsWith($file, '.md')]);

	//NOTE: http vs https bug - if (!contains($link, 'http:')) $link = am_var('url') . $link;

	$result = $prefix . _excludeFromGoogleSearch($raw)
		. '<a class="read-more" href="' . $link . '">Read More&hellip;</a>';
	
	if (!$echo) return $result;
	echo $result;
}

function _excludeFromGoogleSearch($raw) {
	return '<!--googleoff: all-->'
		. am_var('nl') . $raw
		. am_var('nl') . '<!--googleon: all-->'
		. am_var('2nl');
}

function renderOnlyMarkdownOrRaw($raw, $wantsMD, $settings = []) {
	return $wantsMD ? renderSingleLineMarkdown($raw, $settings) : $raw; //so we can use inline in code
}

function renderMarkdown($raw, $settings = []) {
	$settings['markdown'] = true;
	return _renderImplementation($raw, $settings);
}

function renderSingleLineMarkdown($raw, $settings = []) {
	return renderMarkdown($raw, array_merge($settings, ['strip-paragraph-tag' => true]));
}

function renderMarkdownSection($h1, $raw, $settings = []) {
	echo '<section><h1>' . $h1 . '</h1>' . am_var('nl');

	if (isset($settings['excerpt']))
		renderExcerpt($raw, $settings['link'], $settings);
	else
		renderMarkdown($raw, $settings);

	echo am_var('nl') . '</section>' . am_var('2nl');
}

function renderAny($file, $settings = []) {
	return _renderImplementation($file, $settings);
}

//_ denotees its not to be called from outside - see flavours above + remove deprecated
function _renderImplementation($fileOrRaw, $settings) {
	if (endsWith($fileOrRaw, 'family-tree.md')) {
		includeFeature('family-tree');
		renderFamilyTree($fileOrRaw); //only echoes for now
		return;
	}

	if (isset($settings['markdown'])) {
		//$settings['strip-paragraph-tag'] = true;
		$settings['clear-markdown-start'] = true;
		$fileOrRaw = (disk_file_exists($fileOrRaw) ? '' : am_var('markdownStartTag')) . $fileOrRaw;
	}

	//TODO: Consider an explicit-load so file exists can be avoided?
	//debug('render.php - _renderImplementation', ['verbose params!', $fileOrRaw]);

	$endsWithMd = false;
	$raw = $fileOrRaw; $fileName = '[RAW]';
	if ($wasFile = disk_file_exists($fileOrRaw)) {
		$fileName = $fileOrRaw;
		$endsWithMd = endsWith($fileOrRaw, '.md');
		$raw = disk_file_get_contents($fileOrRaw);
	}

	$replaces = valueIfSet($settings, 'replaces', []);
	$echo = valueIfSet($settings, 'echo', true);
	$excerpt = valueIfSet($settings, 'excerpt', false);
	$no_processing = valueIfSet($settings, 'raw', false) || do_md_in_parser($raw);

	if ($excerpt) $raw = explode('<!--more-->', $raw)[0];

	if (function_exists('site_render_content')) $raw = site_render_content($raw);

	$replacesParams = isset($settings['replaces']) ? $settings['replaces'] : [];
	$plainReplaces = isset($settings['plainReplaces']) ? $settings['plainReplaces'] : [];

	$raw = replaceItems($raw, $replacesParams, '%');
	$raw = replaceItems($raw, $plainReplaces, '');

	if ($wasFile && am_var('autofix-encoding')) $raw = simplify_encoding($raw);

	if (am_var('node') && is_string(am_var('section'))) {
		$assetsUrl = am_var('url') . am_var('section') . '/assets/' . am_var('node') . '/';
		$assetsFol = SITEPATH . '/'. am_var('section') . '/assets/' . am_var('node') . '/';
		am_vars($assetsVars = ['assetsUrl' => $assetsUrl, 'assetsFol' => $assetsFol]);
		$raw = replaceItems($raw, $assetsVars, '%');
	}

	if ($vars = am_var('node-vars')) $raw = replaceItems($raw, $vars, '%', true);

	$markdownStart = am_var('markdownStartTag');
	$autopStart = am_var('autopStart');
	$param1IsPageTag = '<!--node-item-is-page-->';
	$param1IsPage = contains($raw, $param1IsPageTag);

	$autop = $raw != '' && startsWith($raw, $autopStart);
	$md = has_module('markdown') && $raw != '' && ($raw[0] == '#' || startsWith($raw, $markdownStart));

	if ($no_processing) {
		$output = $raw;
	} else if ($autop || ($endsWithMd && am_var('autop-for-markdown'))) {
		//TODO: @<team> temp for Sarath site which should use txt (autop) ideally
		$output = wpautop($raw);
	} else {
		$inProgress = '<!--render-processing-->';
		if (is_engage($raw) && !contains($raw, $inProgress)) {
			includeFeature('engage');
			$output = renderEngageOrMd(ENGAGENODE, $raw . $inProgress, 'Engage Us', true, false);
		} else {
			$output = $md || $endsWithMd ? markdown($raw) : wpautop($raw);
		}
	}

	if (contains($output, '%menu-for-this'))
		$output = _renderAllInPageMenus($output);

	$output = runAllMacros($output);

	if (contains($raw, '<!--composite-work-->') && !(am_var('is-in-directory'))) {
		runFwk('parser');
		$prepend = getWorkSettings($fileName);
		$param1IsPage = $param1IsPage || contains($prepend, $param1IsPageTag);
		$output = parseCompositeWork($prepend . $output, $param1IsPage);
	}

	$output = replaceHtml($output);

	if (!isset($settings['dont-prepare-links']))
		$output = prepareLinks($output); //if doing before markdown then this gets messed up

	if (isset($settings['strip-paragraph-tag']))
		$output = strip_paragraph($output);

	if (isset($settings['clear-markdown-start']))
		$output = str_replace(am_var('markdownStart'), '', $output);

	if (contains($output, '%fileName%'))
		$output = str_replace('%fileName%', '<u>EDIT FILE:</u> .' . str_replace(SITEPATH, '', $fileName), $output);

	if (isset($settings['wrap-in-section']))
		$output = '<section>' . am_var('nl') . $output . am_var('nl') . '</section>' . am_var('2nl');

	if (!$echo) return $output;
	echo $output;
}

function _renderAllInPageMenus($output) {
	return $output; //TODO: make it look good...

	$section = am_var('__this-section');
	//TODO: MED: test with 1 section and 2 nodes please!

	//code for node
	if (contains($output, $what = '%menu-for-this-node%')) {
		$where = '/' . $section . '/' . am_var('__this-node') . '/';
		$bit = menu($where, ['return' => true]);
		$output = replaceItems($output, [$what => $bit]);
	}

	return $output;
}


function renderRichPage($sheetFile, $groupBy = 'section', $templateName = 'home') {
	am_var('home', get_sheet($sheetFile, $groupBy));
	$call = am_var('theme_folder') . $templateName . '.php';
	disk_include_once($call);
}

function is_engage($raw) {
	return contains($raw, ' //engage-->');
}

function is_composite_work($raw) {
	return contains($raw, '<!--composite-work-->');
}

function do_md_in_parser($raw) {
	return contains($raw, '<!--markdown-when-processing-->');
}

//TODO: figure this in promo...
function renderDesignOf($node, $fol, $design, $vars) {
	$vars['nodeUrl'] = am_var('url') . $node . '/';
	echo '<section>' . am_var('nl');
	runCode($fol . '/' . $design, ['multiple' => true, 'params' => $vars]);
	echo '</section>' . am_var('2nl');
}
