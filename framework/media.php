<?php
function getMediaSettings($file, $type = 'node') {
	$settings = [];

	if ($type == 'node') {
		$array = am_sub_var('node-vars', 'gallery');
		if (isset($array[$file]) && isset($array[$file]['settings']))
			$settings = $array[$file]['settings'];
		else if (has_sub_var('node-vars', 'gallery-settings'))
			$settings = am_sub_var('node-vars', 'gallery-settings');
	} else {
		parameterError('$type Not Supported', $type);
	}

	return $settings;
}

function renderMedia($file, $params, $settings) {
	$section = valueIfSet($params, 'wrap-in-section');

	$grid = isset($settings['use-grid']);
	if (!$grid)
		$result = ($section ? '<section>' . am_var('nl') : '') . '<h1>' . humanize($file) . '</h1>' . am_var('nl');
	else
		$result = '';

	$mdParams = [
		'item' => $file,
		'searchLinks' => makeLink('pixabay', 'https://pixabay.com/images/search/' . $file . '/', false) . ', '
			. makeLink('unsplash', 'https://unsplash.com/s/photos/' . $file, false) . ', '
			. makeLink('wikimedia commons', 'https://commons.wikimedia.org/w/index.php?search=' . urlencode(humanize($file)) . '&title=Special:MediaSearch&go=Go&type=image', false) . ', '
			. makeLink('google', 'https://www.google.com/search?q=' . urlencode($file) . '+standing+high+resolution+images', false)
			,
	];

	$notInMenu = isset($params['not-in-menu']);
	$extension = $params['extension'];
	$link = $params['url'];
	$base = am_var('url') . $params['mediaRelative'];

	if (!empty($settings)) {
		$fullImageOnNodeItem = am_var('node-inner-item') && valueIfSet($settings, 'image-on-node-item');
		if (!$fullImageOnNodeItem && isset($settings['thumbnails-folder'])) $base = renderAny($settings['thumbnails-folder']);
		if (isset($settings['link-format'])) $link = replaceValues($settings['link-format'], ['nodeUrl' => $params['nodeUrl'], 'nodeLink' => $params['nodeLink'], 'file' => $file, 'extension' => $extension]);
	}

	if (disk_file_exists($mdFile = SITEPATH . '/' . $params['mediaRelative'] . $file . '.md')) {
		if (!$notInMenu) $result .= '</a>';
		if (am_var('node-item') == 'home' && isset($settings['use-excerpt-on-home']))
			$result .= renderExcerpt($mdFile, $link, '', false) . am_var('br');
		else
			$result .= renderMarkdown($mdFile, ['replaces' => $mdParams, 'echo' => false]) . '<hr />' . am_var('nl');
	}

	$noLink = $grid || valueIfSet($params, 'no-link-in-node-item');

	if ($extension == '.jpg' || $extension == '.png') {
		$result .= ($noLink ? '' : '<a href="' . $link .  '">') .
			'<img src="' . $base. $file . $extension . '" class="img-fluid" />' .
			($noLink ? '' : '</a>');
	} else if ($extension == '.mp3') {
		//TODO:
	} else if ($extension == '.mp4') {
		//TODO:
	}

	if ($section) $result .= '</section>' . am_var('2nl');
	return $result;
}
?>
