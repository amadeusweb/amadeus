<?php
/**
 * This php framework is proprietary, Source-available software!
 * It is licensed for distribution at the sole discretion of its owner Imran.
 * Copyright Oct 2019 -> 2025, AmadeusWeb.com, All Rights Reserved!
 * Author: Imran Ali Namazi <imran@amadeusweb.com>
 * Website: https://amadeusweb.com/we/
 * In Action: https://builder.amadeusweb.com/code/
 * Note: Amadeus 6.5 is based on 24 years of Imran's programming experience.
 *       and it's predecessor was MicroViC (https://github.com/yieldmore/MicroVC) from 2013
 */

DEFINE('AMADEUSSAFEFRAMEWORK', __DIR__ . '/'); //safe because ends with /
DEFINE('AMADEUSPATH', str_replace(DIRECTORY_SEPARATOR . 'framework', '', __DIR__));
DEFINE('AMADEUSTHEMEFOLDER', AMADEUSPATH . '/themes/');

include_once AMADEUSSAFEFRAMEWORK . 'stats.php'; //start time, needed to log disk load in files.php
include_once AMADEUSSAFEFRAMEWORK . 'files.php'; //disk_calls, needed first to measure include times

function runFwk($name) {
	disk_include_once(AMADEUSSAFEFRAMEWORK . $name . '.php');
}

runFwk('array');
runFwk('vars');
runFwk('text'); //needs vars
runFwk('html');
runFwk('modules');
runFwk('menu');

//New in 4.1
runFwk('render');
runFwk('seo');
runFwk('assets');
runFwk('media');

//Fresh
runFwk('macros');

//v6.2 - renamed helper to special and added functions
runFwk('functions');
runFwk('special');

//v6.5
runFwk('main');

runFwk('v7-remove'); //TODO: remove menu_recursive from ideas.ym

function before_bootstrap() {
	$port = $_SERVER['SERVER_PORT'];
	am_var('port', $port != 80 ? ':' . $port : '');
	am_var('is_mobile_server', startsWith(__DIR__, '/storage/'));

	//Moved from bootstrap in v4.1 since am_vars is being called with flavours implementation
	am_var('app', startsWith($_SERVER['HTTP_HOST'], 'localhost') && !am_var('is_mobile_server')
		? replace_vars('http://localhost%port%/amadeus/', 'port') : '//builder.amadeusweb.com/');

	if (DEFINED('AMADEUSURL')) am_var('app', AMADEUSURL);

	am_var('local', $local = startsWith($_SERVER['HTTP_HOST'], 'localhost'));
	am_var('main', $local ? replace_vars('http://localhost%port%/amadeusweb/we/', 'port') : '//amadeusweb.com/we/');

	am_var('app-assets', am_var('app') . 'assets/');
	am_var('app_url', am_var('app')); //todo: remove

	$php = contains($_SERVER['DOCUMENT_ROOT'], 'magique') || contains($_SERVER['DOCUMENT_ROOT'], 'Magique');
	am_var('no_url_rewrite', $php);

	//v6.5 - no more allowing disable for these
	runModule('markdown');
	runModule('wordpress');
}

before_bootstrap();

//Now this only sets up the node and page parameters - rest moved to before_bootstrap()
function bootstrap($config) {
	am_vars($config);

	$php = am_var('no_url_rewrite');
	if ($php) $node = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
	else $node = isset($_GET['node']) && $_GET['node'] ? $_GET['node'] : '';

	if (endsWith($node, '/')) $node = substr($node, 0, strlen($node) - 1);
	if (startsWith($node, '/')) $node = substr($node, 1);

	if ($node == '') $node = 'index';

	if (am_var_or('support_page_parameters', true) && strpos($node, '/') !== false) {
		am_var('all_page_parameters', $node);
		$slugs = explode('/', $node);
		$node = array_shift($slugs);
		am_var('page_parameters', $slugs);
		foreach ($slugs as $ix => $slug) am_var('page_parameter' . ($ix + 1), $slugs[$ix]);
	}

	am_var('node', am_var_or('node-alias', $node));
}

function hasPageParameter($param) {
	return in_array($param, am_var_or('page_parameters', [])) || isset($_GET[$param]);
}

function getThemeBaseUrl() {
	$themeName = am_var('theme');
	$themeUrl = am_var('app') . "themes/$themeName/";
	am_var('themeUrl', $themeUrl);
	return $themeUrl;
}

function getThemeFile($file) {
	$themeName = am_var('theme');
	return AMADEUSPATH . "/themes/$themeName/$file";
}

function renderThemeFile($file, $themeName = false) {
	if (!$themeName) $themeName = am_var('theme');
	$variables = [
		'theme' => $theme = am_var('app') . "themes/$themeName/",
		'themeFol' => $themeFol = AMADEUSPATH . "/themes/$themeName/",
	];

	am_var('theme_url', $theme);
	am_var('theme_folder', $themeFol);

	disk_include_once($themeFol . $file . '.php', $variables);
}

function render() {
	add_foot_hook(featurePath('asset-manager.php')); //NOTE: can this be here?
	if (isset($_GET['share'])) includeFeature('share');

	if (function_exists('before_render')) before_render();
	ob_start();

	$theme = am_var('theme') ? am_var('theme') : 'default';
	$embed = am_var('embed');

	if (!$embed) {
	    renderThemeFile('header', $theme);
	    if (function_exists('before_file')) before_file();
	}

	$folder = am_var('path') . '/' . (am_var('folder') ? am_var('folder') : '');
	$fwe =  $folder . am_var('node');
	$rendered = renderAnyFile($fwe . '.', ['extensions' => 'core', 'return-on-first' => true]);

	if (isset($_GET['debug']) || isset($_GET['stats'])) {
		includeFeature('tables');
		am_var('stats', true);
	}

	if (!$rendered) {
		if (function_exists('did_render_page') && did_render_page()) {
			//noop
		} else {
			//NOTE: Uses output buffering magic methods to delay sending of output until 404 header is sent 
			header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
			ob_flush();

			if (isset($_GET['debug'])) {
				echo 'NOTE: Turning on stats so you can additionally see what files are included! This appears below the footer' . am_var('brnl') . am_var('brnl');

				parameterError('FUNCTION EXISTS: did_render_page', function_exists('did_render_page') ? 'YES' : 'NO');

				$verbose = $_GET['debug'] == 'verbose';
				if ($verbose) {
					global $cscore;
					parameterError('ALL AMADEUS VARS - global $cscore;', $cscore);
				}
			}

			error('<h1 class="flash flash-red">Could NOT find file "' . am_var('node') . '"</h1>in ' . $folder);
		}
	}

	ob_end_flush();

	if (!$embed) {
		if (function_exists('after_file')) after_file();
		renderThemeFile('footer', $theme);
		print_stats();
	}

	if (function_exists('after_render')) after_render();
}

DEFINE('BRTAG', '<br />');

function copyright_and_credits($separator = '<br />', $return = false) {
	$copy = _copyright(true);
	$cred = _credits('', true);
	$result = $copy . $separator . $cred;
	if ($return) return $result;
	echo $result;
}

function _copyright($return = false) {
	if (am_var('dont_show_copyright')) return '';

	$year = date('Y');
	$start = am_var('start_year');
	$from = ($start && ($start != $year)) ? $start . ' - ' : '';

	$before = am_var('owned-by') ? '<strong>' . am_var('name') . '</strong>, ' : '';
	$after = am_var('owned-by') ? am_var('owned-by') : am_var('name');

	$result = '&copy; ' . $before . 'Copyright <strong><span>' . $after . '</span></strong>. ' . $from . $year . ' All Rights Reserved';
	if ($return) return $result; else echo $result;
}

function _credits($pre = '', $return = false) {
	if (am_var('dont_show_amadeus_credits')) return '';

	$url = am_var('main') . '?utm_content=site-credits&utm_referrer=' . am_var('safeName');
	$result = $pre . sprintf('Powered by <a href="%s" target="_blank" class="amadeus-credits" style="display: inline-block;"><img src="%s" height="20" alt="%s" style="vertical-align: middle;" /></a><br />', $url, am_var('main') . 'amadeus-logo@2x.png', 'Amadeus Web');

	if ($return) return $result; else echo $result;
}
