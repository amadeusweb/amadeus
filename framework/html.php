<?php
///Tag Helpers

function cssClass($items) {
	if (!count($items)) return '';
	return ' class="' . implode(' ', $items) . '"';
}

//TODO: section tag cleanup!
function section($what = 'start', $h1 = '', $feature = false) {
	if ($h1) $h1 = sprintf('<h1%s>' . $h1 . '</h1>', $feature ? ' class="amadeus-icon"' : ''); 
	echo $what == 'start' ? am_var('nl') . '<section>' . $h1 . am_var('nl') : '</section>' . am_var('2nl');
}

function sectionId($id, $class = '') {
	$attrs = '';
	if ($id) $attrs .= ' id="' . $id . '"';
	if ($class) $attrs .= ' class="' . $class . '"';
	echo am_var('nl') . '<section' . $attrs . '>' . am_var('nl');
}

function iframe($url, $wrapContainer = true) {
	if ($wrapContainer) echo '<div class="video-container">';
	echo '<iframe src="' . $url . '" style="width: 100%; height: 90vh;"></iframe>';
	if ($wrapContainer) echo '</div>';
}

function div($what = 'start', $h1 = '', $class = 'video-container') {
	if ($h1) $h1 = '<h1>' . $h1 . '</h1>';
	echo $what == 'start' ? '<div class="' . $class . '">' . $h1 . am_var('nl') : '</div>' . am_var('2nl');
}

function h2($text, $class = '') {
	if ($class) $class = ' class="' . $class . '"';
	echo '<h2'.$class.'>';
	renderSingleLineMarkdown($text);
	echo '</h2>' . am_var('nl');
}

function listItem($html) {
	return '	<li>' . $html . '</li>' . am_var('nl');
}

///Internal Variables & its replacements

//Needed in site-var for header + footer message! expects url to have already been set (peace/site-cms.php)
function replaceHtmlOnly($html) {
	return replaceItems($html, [
		'[url]' => am_var('url'),
		'<marquee>' => am_var('_marqueeStart'),
	]);
}

function replaceSpecialChars($html) {
	$replaces = [
		'|' => am_var('nl'),
		'–' => ' &mdash; ',
		'’' => '\'',
		'“' => '"',
		'”' => '"',
		'®' => '&reg;',
	];
	return replaceItems($html, $replaces);
}

function replaceHtml($html) {
	//TODO: MEDIUM: Warning if called before bootstrap!
	$key = 'htmlSitewideReplaces';
	$replaces = am_var($key);
	if (!$replaces) {
		$s = am_var('section');
		$s = is_string($s) ? humanize($s) : '';
		am_var($key, $replaces = [
			//TODO: @<team> - all assets links should use this and then %url% should become rewrite safe
			//Also, we should incorporate dev tools like w3c & broken link checkers
			'%assets%' => am_var('url') . 'assets/',
			'%page-url%' => am_page_url('no-rewrite-safe'), //NOTE: for inner pages to work without url rewrite
			'[url]' => am_var('url'),
			'%url%' => am_var('url'),
			'%node%' => am_var('node'),
			'%builder-url%' => am_var('app'),
			'%amadeus-url%' => am_var('main'),
			'%network-url%' => am_var_or('network-url', '#network-url-not-setup--'),
			'[live-url]' => am_var('live-url'),
			'[theme]' => am_var('theme_url'),
			'%phone%' => am_var_or('phone', ''),
			'%email%' => am_var_or('email', ''),
			'%whatsapp%' => 'https://wa.me/'. am_var_or('whatsapp', '') . '?text=',
			'%siteName%' => am_var('name'),
			'%section%' => $s,
			'%sectionAndNode%' => is_string(am_var('section')) ? am_var('section') . '/' . am_var('node') : '', //section is array in archives
			'<marquee>' => am_var('_marqueeStart'),
		]);
	}

	return replaceItems($html, $replaces);
}

am_var('_marqueeStart', '<marquee onmouseover="this.stop();" onmouseout="this.start();">');
am_var('_errorStart', '<div style="padding: 20px; font-size: 130%; font-weight: bold; background-color: #fee; margin-top: 20px;">');

function togglingH2($text, $initialArrow = 'down') {
	return '<h2 class="amadeus-icon toggle-parent-panel mb-3">'
		. '<span class="heading-text">' . $text . '</span><span class="toggle-icon icofont-arrow-' . $initialArrow . '"></h2>';
}

function featureHeading($id, $return = 'full', $text = false) {
	$bits = explode('-', $id, 2);
	$whitelabelled = am_var('whitelabelled-features');
	$link = $whitelabelled ? '' : ' &mdash; ' . makeLink('?', am_var('main') . 'features/' . $bits[0] . '/', false);
	if ($return == 'link-only') return $link;

	if (!$text) $text = '';
	if ($bits[0] == 'site') $what = 'site';
	else if ($bits[0] == 'statistics') $what = 'statistics';
	else $what = $id;

	switch ($what) {
		case 'engage': $text = 'Send a message to ' . am_var('name'); break;
		case 'seo': $text = 'SEO Info for ' . am_var('name'); break;
		case 'share-form': $text = 'Share Link (with Google tracking)'; break;
		case 'assistant': $text = ''; break;
		case 'assistant-voice': $text = 'Voice Controls'; break;
		case 'tree': $text = 'Family Tree of ' . humanize(am_var('node')); break;
		case 'links': $text = 'Quick Links'; break;
		case 'site': if (!$text) $text = $bits[1]; break;
		case 'statistics': $text = 'Statistics ' . humanize($bits[1]); break;
	}

	if ($return == 'text') return $text;
	if ($text) $text .= $link;

	$class = $whitelabelled ? ' whitelabelled' : '';
	$class .= in_array($what, ['statistics', 'site', 'links', 'assistant-toc']) ? ' ' . am_var('toggle-list') : '';
	return '	<h2 id="amadeus-' . $id . '" class="amadeus-icon' . $class . '">'
		 . ($return == 'h2-start' ? '' : $text . '</h2>' . am_var('nl'));
}

///Other Amadeus Stuff
function makePLImages($prefix, $echo = true) {
	$prefix = am_var('url') . $prefix;
	$format = '<img src="%s-%s.jpg" class="img-fluid show-in-%s" />' . am_var('nl');
	$result =
		sprintf($format, $prefix, 'portrait', 'portrait') .
		sprintf($format, $prefix, 'landscape', 'landscape');
	if (!$echo) return $result;
	echo $result;
}

/// Expects the whole link(s) html to be provided so href to target blank and mailto can be substituted.
function prepareLinks($output) {
	$output = str_replace(am_var('url'), '%url%', $output); //so site urls dont open in new tab. not sure when this became a problem. maybe a double call to prepareLinks as the render methods got more complex.
	$output = str_replace('href="http','target="_blank" href="http', $output); //yea, baby! no need a js solution!
	$output = str_replace('href="mailto','target="_blank" href="mailto', $output); //if gmail in chrome is the default, it will hijack current window
	$output = str_replace('%url%', am_var('url'), $output);

	//TODO: " class="analytics-event" data-payload="{clickFrom:'%safeName%' //leave end " as a hack to pile on attributes
	$campaign = isset($_GET['utm_campaign']) ? '&utm_campaign=' . $_GET['utm_campaign'] : '';
	$output = str_replace('#utm','?utm_source=' . am_var('safeName') . $campaign, $output);

	return $output;
}

//When apache is not used - Magique has to be the folder name
function am_page_url($relativeUrl = '') {
	return am_var('url') . ($relativeUrl != '' && am_var('no_url_rewrite') ? 'index.php/' : '') . ($relativeUrl == 'no-rewrite-safe' ? '' : $relativeUrl);
}

function get_back_or_home_href() {
	if (isset($_SERVER['HTTP_REFERER']) && startsWith($_SERVER['HTTP_REFERER'], am_var('url')))
		return 'javascript: history.go(-1);';

	return am_var('url');
}

function makeSpecialLink($what, $typesList, $text = '') {
	$types = explode(', ', $typesList);
	$op = '';

	foreach ($types as $type) {
		if ($type == 'tel')
			$op .= '<a class="icofont-phone" href="tel:' . $what . '">' . $what . '</a> ';
		else if ($type == 'whatsapp')
			$op .= '(<a class="icofont-whatsapp" target="_blank" href="https://wa.me/' . replaceItems($what, ['+' => '', '-' => '', '.' => '']) . '?text=' . $text . '">whatsapp</a>) ';
		else if ($type == 'email')
			$op .= '<a class="icofont-email" target="_blank" href="mailto:' . $what . '?subject=' . replaceItems($text, [' ' => '+']) . '">' . $what . '</a> ';
	}

	return $op;
}

function makeLink($text, $link, $relative = true, $noLink = false) {
	if ($noLink) return $text; //Used when a variable needs to control this, else it will be a ternary condition, complicating things
	if ($relative == 'external') $link .= '" target="_blank'; //hacky - will never 
	else if ($relative) $link = am_var('url') . $link;
	return prepareLinks('<a href="' . $link . '">' . $text . '</a>');
}

function getLink($text, $href, $class = '', $target = false) {
	$target = $target ? ' target="' . (is_bool($target) ? '_blank' : $target) . '"' : '';
	$params = compact('text', 'href', 'class', 'target');
	return replaceItems('<a href="%href%"%class%%target%>%text%</a>', $params, '%');
}

function getIconSpan($what = 'expand', $size = 'large') {
	$theme = am_var('theme');
	if ($theme == 'biz-land') {
		$classes = [
			'expand' => 'icofont-expand',
			'expand-swap' => 'icofont-collapse',
			'toggle' => 'icofont-toggle-on',
			'toggle-swap' => 'icofont-toggle-off',
		];
		$sizes = ['large' => 'icofont-2x', 'normal' => 'icofont'];
		return '<span data-add="' . $classes[$what . '-swap'] . '" data-remove="' . $classes[$what] . '" class="icon ' . $classes[$what] . ' ' . $sizes[$size] . '"></span>';
	}
}

function getThemeIcon($id, $size = 'normal')  {
	return '<span class="icofont-1x icofont-' . $id . '"></span>';
}

function body_classes($return = false) {
	$chatra = has_var('ChatraID') ? ' has-chatra' : '';
	$op = 'theme-' . am_var('theme') . ' site-' . am_var('safeName') . $chatra;
	if ($return) return $op;
	echo $op;
}

//TODO: replace all and remove
function chat_class_on_body() {
	echo has_var('ChatraID') ? ' has-chatra' : '';
}

function isMobile() {
	//CREDITS: https://stackoverflow.com/a/48385715

	//-- Very simple way
	$useragent = $_SERVER['HTTP_USER_AGENT']; 
	$iPod = stripos($useragent, "iPod"); 
	$iPad = stripos($useragent, "iPad"); 
	$iPhone = stripos($useragent, "iPhone");
	$Android = stripos($useragent, "Android"); 
	$iOS = stripos($useragent, "iOS");
	//-- You can add billion devices 

	return ($iPod||$iPad||$iPhone||$Android||$iOS);
}

function error($html, $renderAny = false, $settings = []) {
	$settings['echo'] = false;
	if ($renderAny) $html = renderAny($html, $settings);
	echo am_var('_errorStart') . $html . '</div>';
}

function debug($function, $vars) {
	if (is_debug()) echo am_var('2nl') . '<!--FUNCTION CALLED: ' . $function . ' - ' . print_r($vars, true) . '-->';
}

function raiseParameterError($message, $first, $later = []) {
	foreach ($later as $key => $value) $first[$key] = $value;
	parameterError($message, $first);
}

function parameterError($msg, $param, $trace = true) {
	if (startsWith($msg, '$')) $msg = 'PARAMETER ERROR: ' . $msg;
	echo am_var('_errorStart') . $msg . '<hr /><pre>' . print_r($param, 1);
	if ($trace) { echo '</pre><br />STACK TRACE:<hr/><pre>'; debug_print_backtrace(); }
	echo '</pre></div>';
}
