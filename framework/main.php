<?php

main::initialize();

//NOTE: allows referring to values by name and avoids duplication
class main {

	static function initialize() {
		am_var('systemEmail', 'imran@amadeusweb.com');
	}

	static function chat() {
		$val = am_var('ChatraID');
		$val = $val && $val != 'none' ? ($val != '--use-amadeusweb' ? $val : 'wqzHJQrofB47q5oFj') : false;
		if (!$val) return;
		am_var('ChatraID', $val);
		runModule('chatra');
	}

	static function analytics() {
		$val = am_var('google-analytics');
		$val = $val && $val != 'none' ? ($val != '--use-amadeusweb' ? $val : 'UA-166048963-1') : false;
		if (!$val) return;
		am_var('google-analytics', $val);
		runModule('google-analytics');
	}
}
