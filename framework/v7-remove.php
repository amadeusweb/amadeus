<?php
//runFwk for this in 2020 / 2021 sites - low prio
function recursive_menu($sheet, $section = 'menu', $level = 1, $settings = []) {
	$node = am_var('node'); $subNode = am_var('page_parameter1');
	$menu = $sheet->sections[$section];

	if (!isset($settings['no-ul-al-level1']) || $level != 1)
			echo PHP_EOL . '<ul class="level-' . $level . '-menu">';

	$extract = ['name'];
	if (isset($sheet->columns['about'])) $extract[] = 'about';
	if (isset($sheet->columns['description'])) $extract[] = 'description';
	if ($level == 2 && isset($settings['home-link-to-section']))
			echo sprintf(PHP_EOL . '<li><a href="%s" style="background-color: pink;">%s</a>', am_page_url($section . '/'), 'Home');

	foreach ($menu as $item) {
			if (isset($settings['visible']) && !$settings['visible']($item, $sheet->columns)) continue;
			extract(items($item, $extract));

			$slug = urlize($name);
			$name = humanize($name);
			$prefix = isset($settings['section-prefix']) && !contains($section, 'menu') ? $section . '/' : '';
			if (isset($settings['url-prefix'])) $prefix =  $settings['url-prefix']($item, $sheet->columns);
			$link = am_page_url($prefix . ($name == 'Index' ? '' : $slug . '/'));
			$hasSubMenu = isset($sheet->sections[$slug]);
			$hasManualSubMenu = isset($settings['hasManualSubMenu']) && $settings['hasManualSubMenu']($item, $sheet->columns);
			
			$class = $slug == $node || $slug == $subNode ? 'selected' : '';
			if ($hasSubMenu || $hasManualSubMenu) $class .= ($class ? ' ' : '') . 'drop-down';
			$title = str_replace('"', "'", isset($about) || isset($description) ? isset($about) ? $about : $description : '');

			$prefix = isset($settings['prefix']) ? $settings['prefix']($item, $sheet->columns) : '';
			$suffix = isset($settings['suffix']) ? $settings['suffix']($item, $sheet->columns) : '';
			echo sprintf(PHP_EOL . '<li%s>%s<a href="%s"%s>%s</a>%s', $class ? ' class="' . $class . '"' : '',
					$prefix, $link, $title ? ' title="' . $title . '"' : '', $name, $suffix);

			if ($hasSubMenu) {
					recursive_menu($sheet, $slug, $level + 1, $settings);
			}

			echo '</li>';
	}

	if (!isset($settings['no-ul-al-level1']) || $level != 1)
			echo PHP_EOL . '</ul>';
}
