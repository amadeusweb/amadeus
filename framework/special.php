<?php
am_vars([
	'special-folder-extensions' => $sfe = [
		'articles' => 'md',
		'in-memoriam' => 'md',
		'blurbs' => 'txt',
		'code' => 'php',
		'decks' => 'md',
		'dossiers' => 'tsv',
		'rich-pages' => 'tsv',
		'tables' => 'md',
	],
	'special-folders' => array_keys($sfe),
	'exclude-folders' => ['assets', 'data', 'engage', 'home', 'images', 'thumbnails'],
]);

function isSpecialNode() {
	$node = am_var('node');
	if (!$node) return false;
	if (am_var('site-lock')) return true;

	if (
		    _isLinks($node)
		 || _isScaffold($node)
		) return true;

	return false;
}

//NOTE: this is for special folders inside section->node
function isSpecial($fol) {
	if (am_var('section') == 'gallery') return true;

	$page = am_var('page_parameter1');
	foreach (am_var('special-folders') as $special) {
		$extnsAllowed = am_sub_var('special-folder-extensions', $special);
		$fwe = $fol . '/' . $special . '/' . $page;

		$itemExtn = disk_one_of_files_exist($fwe . '.', $extnsAllowed);
		if (!$itemExtn) continue;

		am_vars([
			'special-fwe' => $fwe,
			'special-folder' => $special,
			'special-root-folder' => $fol . '/',
			'special-filename' => humanize($page),
			'special-filename-websafe' => $page,
			'file-extension' => $itemExtn,
		]);

		if ($special == 'blurbs') _setupBlurbs($fwe, $page);
		else if ($special == 'code') _setupCode($fwe, $page);
		else if ($special == 'decks') _setupDeck($fwe, $page);
		else if ($special == 'dossiers') _setupDossiers($fwe, $page);

		return true;
	}

	return false;
}

function needsSpecialRender($file) {
	$raw = disk_file_exists($file) ? disk_file_get_contents($file) : '[RAW]';
	$tbl = endsWith($file, '.tsv') && startsWith($raw, '|is-table');
	return $tbl;
}

function autoRenderIfSpecial($file) {
	if (endsWith($file, '.php')) {
		renderAnyFile($file);
		return;
	}

	$raw = disk_file_exists($file) ? disk_file_get_contents($file) : '[RAW]';

	if (endsWith($file, '.md') && startsWith($raw, '<!--is-blurbs-->')) {
		_renderedBlurbs($file);
		return;
	}

	if (endsWith($file, '.tsv') && startsWith($raw, '|is-deck')) {
		renderSheetAsDeck($file, am_var('all_page_parameters') . '/');
		return;
	}

	if (endsWith($file, '.md') && startsWith($raw, '<!--is-deck-->')) {
		_renderedDeck($file, am_var('all_page_parameters') . '/');
		return;
	}

	if (endsWith($file, '.tsv') && startsWith($raw, '|is-rich-page')) {
		renderRichPage($file);
		return;
	}

	if (endsWith($file, '.tsv') && startsWith($raw, '|is-table')) {
		$template = dirname($file) . '/.template.html';
		table(pathinfo($file, PATHINFO_FILENAME), false, $file, 'auto', disk_file_get_contents($template));
		return;
	}

	sectionId('file');
	renderMarkdown($file);
	section('end');
}

function renderedSpecial() {
	if (am_var('site-lock')) { doSiteLock(); return true; }
	$node = am_var('node');
	if ($node == 'gallery') { includeFeature('gallery'); return true; }
	if (_renderedLink($node)) return true;
	if (_renderedScaffold($node)) return true;

	$special = am_var('special-folder');
	if (!$special) return false;

	$file = am_var('file');

	if ($special == 'blurbs') {
		_renderedBlurbs($file);
	} else if ($special == 'code') {
		_renderedCode($file);
	} else if ($special == 'decks') {
		_renderedDeck($file);
	} else if ($special == 'dossiers') {
		_renderedDossiers($file);
	} else if ($special == 'rich-pages') {
		am_var('home', get_sheet($file));
		renderThemeFile('home');
	}

	return true;
}

function menuSpecial($folder, $files, $specialFoldersOnly, $sort) {
	if (has_var('engine')) return $files;

	$mergeFromSpecial = [];

	foreach (am_var('special-folders') as $special) {
		if (!disk_is_dir($folder . $special))
			continue;

		unset($files[array_search($special, $files)]);

		if ($specialFoldersOnly) {
			$mergeFromSpecial[] = $special;
		} else {
			$specialFiles = skipExcludedFiles(disk_scandir($folder . $special));
			$mergeFromSpecial = array_merge($mergeFromSpecial, ['~' . humanize($special)], $specialFiles);
		}
	}

	if ($sort) usort($files, 'strnatcmp'); //https://www.php.net/manual/en/function.strnatcmp.php

	if (count($mergeFromSpecial)) {
		$files = array_merge($files, $mergeFromSpecial);
	}

	return $files;
}

// ************************************ Region: Private (Internal) Functions

function _setupBlurbs($fwe, $page) {
	$blurb = $fwe . '.txt';
	am_var('blurb-file', $blurb);
	if (hasPageParameter('embed'))
		am_var('embed', true);
}

function _renderedBlurbs($blurb, $name = false, $actualUrl = false) {
	$url = replaceItems($blurb, ['/' . am_var('section') => '']);
	if (!$name) $name = am_var('special-filename');

	if (hasPageParameter('embed')) {
		includeFeature('blurbs');
		return;
	}

	$url = $actualUrl ? $actualUrl : am_var('url') . am_var('node') . '/' . am_var('page_parameter1');
	$embedUrl = $actualUrl ? $url . '?embed=1' : $url .'/embed/';
	echo '<section class="blurb-container" style="text-align: center;">BLURBS: '
		. makeLink($name, $embedUrl, false) . ' (opens in new tab)<hr />' . am_var('nl');
	echo '<iframe style="height: 80vh; width: 100%; border-radius: 30px;" src="' . $embedUrl . '"></iframe>' . am_var('nl');
	echo '</section>' . am_var('2nl');
}

function _setupCode($fwe, $name) {
	am_var('file', $fwe . '.php');
}

function _renderedCode($code) {
	disk_include_once($code);
}

function _setupDeck($fwe, $name) {
	if (hasPageParameter('expanded')) return false;

	$file = $fwe . '.md';

	am_var('deck-name', $name);
	am_var('file', $file);

	if (!hasPageParameter('embed')) {
		return false;
	}

	am_var('no-permanent-link', true);
	am_var('no-detail-link', true);
	am_var('embed', true);
}

function renderInPageDeck($section, $node, $name) {
	$deck = concatSlugs([am_var('path'), $section, $node, 'decks', $name . '.md']);
	$params = [ 'relativeUrl' => concatSlugs([$node, $name, '']),
		'title' => $node . ' &raquo; ' . $name];
	_renderedDeck($deck, $params);
}

function renderSheetAsDeck($deck, $link) {
	if (!hasPageParameter('embed') && !hasPageParameter('expanded')) {
		_renderedDeck($deck, ['relativeUrl' => $link,
			'title' => humanize(am_var('page_parameter2') . '--' . am_var('page_parameter1')),
		]);
		return;
	}

	$sheet = get_sheet($deck, false);
	$op = [];
	foreach ($sheet->rows as $item) {
		$type = $item[$sheet->columns['type']];
		$text = $item[$sheet->columns['text']];
	
		if ($type == 'slide') {
			if (count($op)) { $op[] = ''; $op[] = '----'; $op[] = ''; }
			$op[] = '<input type="hidden" value="' . $text . '" />';
			$op[] = '';
		} else if ($type == 'heading') {
			$op[] = '## ' . $text;
			$op[] = '';
		} else if ($type == 'sub-heading') {
			$op[] = '### ' . $text;
			$op[] = '';
		} else if ($type == 'paragraph') {
			$op[] = $text;
			$op[] = '';
		} else if ($type == 'item') {
			if (end($op) != '') $op[] = '';
			$op[] = '* ' . $text;
		}
	}

	am_var('nodeLink', $link);
	am_var('deck', implode(am_var('nl'), $op));
	runModule('revealjs');
}

function _renderedDeck($deck, $params = []) {
	$expanded = hasPageParameter('expanded');

	if (!hasPageParameter('embed')) {
		$url = am_var('url') . valueIfSetAndNotEmpty($params, 'relativeUrl',
			am_var('node') . '/' . am_var('page_parameter1') . '/');

		$embedUrl = $url .'embed/';

		echo '<section class="deck-toolbar" style="text-align: center;">';
		h2(humanize(valueIfSetAndNotEmpty($params, 'title', am_var('special-filename'))));
		echo 'DECK: ' . am_var('nl');
		$links = [];

		if (!$expanded) $links[] = '<a class="toggle-deck-fullscreen" href="javascript: $(\'.deck-container\').show();"><span class="text">maximize</span> ' . getIconSpan('expand', 'normal') . '</a>';
		if ($expanded) $links[] = makeLink('open deck page', $url, false);
		$links[] = makeLink('open deck fully', $embedUrl, false);
		$links[] = $expanded ? 'expanded deck below' : makeLink('open deck expanded', $url . 'expanded/', false);
		//TODO: get this working and support multi decks
		//$(this).closest(\'.deck-toolbar\').next(\'.deck-container\').toggle();
		if (!$expanded) $links[] = makeLink('toggle deck below', 'javascript: $(\'.deck-container\').toggle();', false);

		echo implode(' &nbsp;&nbsp;&mdash;&nbsp;&nbsp; ' . am_var('nl'), $links);

		if ($expanded) {
			$op = renderMarkdown($deck, [ 'echo' => false ]);
			$op = replaceItems($op, ['<hr>' => '</section><section>']);
			echo $op;
		} else {
			echo sprintf('<section class="deck-container" style="padding: 10px; background-color: %s;">'
				. '<iframe src="%s?iframe=1"></iframe></section>', '#ccf', $embedUrl);
		}

		echo '</section>' . am_var('2nl');
		return true;
	}

	am_var('deck', $deck);
	runModule('revealjs');
	return true;
}

function _setupDossiers($fwe, $name) {
	$data = $fwe . '.tsv';
	if (!disk_file_exists($data)) return false;

	$data = dirname($fwe) . '/' . $name . '.tsv';

	$folder = SITEPATH . '/data/dossier-templates/';
	$node = am_var('node');

	$templates = [
		'node-item' => $folder . $node . '-' . $name . '.html',
		'node' => $folder . $node . '.html',
		'default' => $folder . 'default.html',
	];

	foreach($templates as $type => $item) {
		if (disk_file_exists($item)) {
			am_vars([
				'file' => $data,
				'template' => $item,
				'template-type' => $type,
			]);
			return true;
		};
	}

	parameterError('Dossier Template Resolver', ['found-data' => $data, 'searched-templates' => $templates], false);
	die(); //this is before render and violating the contract with the isSpecial which calls it

	return false;
}

function _renderedDossiers($data) {
	$page = am_var('special-filename-websafe');
	$html = am_var('template');
	$type = am_var('template-type');

	sectionId($page . '-intro', 'feature-table'); //NOTE: dbc heads up: section nesting will be a problem when using html in dossiers!
	h2('Dossiers or Records');

	//later this can be resolved from multiple filenames as needed
	echo replaceItems(getSnippet('dossier'), [
		'pageName' => humanize($page),
		'nodeName' => humanize(am_var('node')),
		'sectionName' => humanize(am_var('section')),
		'siteName' => am_var('name'),
	], '%');

	section('end');

	table($page, false, $data, 'auto', disk_file_get_contents($html));
}

//TODO: Refactor and move away? or make it a kind of special section
function did_wiki_topic_humanize($txt, $field, $sheetName = 'wiki') {
	$sheetName = trim($sheetName);
	$txt = str_replace(' ', '-', strtolower($txt));
	$sheet = get_sheet($sheetName, 'slug'); //NOTE: Its cached automatically by the framework

	if (isset($sheet->sections[$txt]))
		return $sheet->sections[$txt][0][$sheet->columns['no']] . ' &mdash; ' . humanize($txt, 'no-site');
	else
		return false;
}

function wiki_pages_after_file($sheetName = 'wiki', $wikiSlug = 'wiki') {
	$sheetName = trim($sheetName);
	$sheet = get_sheet($sheetName, 'slug');
	$page = am_var('page_parameter1');
	if (!$page || !isset($sheet->sections[$page])) return;

	$row = $sheet->sections[$page][0];
	$no = $row[$sheet->columns['no']];

	$sheet = get_sheet($sheetName, 'parent');
	$items = $sheet->sections[$no];

	foreach ($items as $item) {
		$content = $item[$sheet->columns['has_content']];
		$itemNo = $item[$sheet->columns['no']];
		$itemSlug = $item[$sheet->columns['slug']];

		$html = renderMarkdown($content == 'N'
			? SITEPATH . '/' . $wikiSlug . '/' . am_var('node') . '/_pages/' . $itemSlug . '.md'
			: $item[$sheet->columns['content']], ['echo' => false, 'strip-paragraph-tag'=> true]);

		$html = str_replace('|',am_var('brnl'), $html);

		section('end');
		section();

		$heading = str_pad('#', substr_count($itemNo, '.') + 1);
		echo renderMarkdown($heading . ' ' . $itemNo . ' &mdash; ' . humanize($itemSlug, 'no-site'));
		echo $html;
	}
}

function _isLinks($node) {
	if ($node == 'go')
	includeFeature('links'); //will just do a redirect

	return $node == 'links';
}

function _renderedLink($node) {
	if ($node != 'links') return false;

	includeFeature('links'); //will list them
	return true;
}

function before_section_or_file($section) {
	$node = am_var('node');

	if ($node == $section) {
		am_var('section', $section);
		return true;
	}

	$fol = SITEPATH . '/' . $section . '/';
	$files = disk_scandir($fol);

	foreach ($files as $fil) {
		if ($fil[0] == '.') continue;
		if ($node == $fil) {
			am_var('fwk-section', $section);
			return true;
		} else if (disk_is_dir($fol . $node . '/')) {
			am_var('fwk-section', $section);
			am_var('fwk-folder', $section . '/' . $node . '/');
			return true;
		} else if ($ext = disk_one_of_files_exist($fwe = $fol . $fil . '.','txt, md')) {
			am_var('fwk-section', $section);
			am_var('fwk-file', $fwe . $ext);
			return true;
		}
	}

	return false;
}

function did_render_section_or_file() {
	$section = am_var('fwk-section');
	$dir = am_var('fwk-folder');
	$file = am_var('fwk-file');

	if ($file) {
		renderAny($file);
		return true;
	} else if ($section || $dir) {
		includeFeature('blog'); //TODO: merge this with directory and use section type if not blog/wiki/sitemap
		return true;
	}

	return false;
}

function _isScaffold() {
	$node = am_var('node');
	$scaffold = am_var_or('scaffold', []);
	//NOTE: sitemap always needed
	$always = am_var('local') && $node == 'sitemap';
	if (!$always && !in_array($node, $scaffold))
		return false;

	if (hasPageParameter('embed')) am_var('embed', true);
	am_var('scaffoldCode', 'scaffold/' . $node);
	return true;
}

function _renderedScaffold() {
	$code = am_var('scaffoldCode');
	if (!$code) return false;

	includeFeature($code, false);
	return true;
}

//scaffolded features
function do_updates() {
	if (!has_sheet('updates') || am_var('no-updates')) return;

	includeFeature('updates');
}
