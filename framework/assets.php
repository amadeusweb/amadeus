<?php
function flavourAssetsUrl($flavour) {
	return am_var('app') . 'flavours/' . $flavour . '/assets/';
}

function scriptTag($url) {
	echo PHP_EOL . '    <script src="' . $url . '" type="text/javascript"></script>';
}

function cssTag($url) {
	echo PHP_EOL . '    <link href="' . $url . '" rel="stylesheet" type="text/css" /> ';
}

function title($return = false) {
	if (am_var('node-alias')) {
		$r = am_var('page-name') . ' | ' . am_var('byline');
		if ($return) return $r;
		echo $r;
		return;
	}

	$page = am_var('page-name') ? am_var('page-name') : am_var('node');
	$siteRoot = $page == 'index' || am_var('under-construction');

	if ($return === 'title-only') return $page;
	$r = (!$siteRoot ? humanize($page) . ' - ' : '') . am_var('name') . ($siteRoot ? ' | ' . am_var('byline') : '');
	$exclude = ['print', 'embed', 'decks', 'articles']; //TODO: SoC: maintain this where special is defined

	foreach(array_reverse(am_var_or('page_parameters', [])) as $slug)
		if (!in_array($slug, $exclude)) $r = humanize($slug) . ' - ' . $r;

	$r = replaceItems($r, ['<strong>' => '', '</strong>' => '', '<br />' => ' &mdash; ']);

	if ($return) return $r;
	echo $r;
}

function homeUrl() {
	return am_var('url') . (am_var('node-theme') ? am_var('node') . '/' : '');
}

function logoUrl() {
	return am_var('logoUrl') ? am_var('logoUrl') : am_var('url');
}

function logoRel() {
	if (am_var('custom-node')) return am_var('safeName') . '-' . am_var('node');
	return am_var('node-theme') ? am_var('section') . '/' . am_var('node') . '/' . am_var('node') : am_var('safeName');
}

function version($what = 'site') {
	$key = 'version_for_' . $what; //cache it to prevent long manipulations below
	if ($result = am_var($key)) return $result;

	$ver = am_var($what == 'site' ? 'version' : $what . 'Version');
	$result = $ver ? '?v=' . $ver['id'] . (false ? '&for=' . $what : '') . '&date=' . urlize($ver['date']) : '';
	am_var($key, $result);
	return $result;
}

function asset_url($slug) {
	return strpos($slug, '%') !== false ? replace_vars($slug) : ((startsWith($slug, 'http') || startsWith($slug, '//') ? '' : am_var('url') . 'assets/') . $slug);
}

function styles_and_scripts() {
	$ver = version();
	if (am_var('styles')) foreach (am_var('styles') as $file)
			cssTag(asset_url($file) . '.css' . $ver);
	if (am_var('scripts')) foreach (am_var('scripts') as $file)
			scriptTag(asset_url($file) . '.js' . $ver);
}

function head_hooks() {
	if (am_var('head_hooks')) foreach (am_var('head_hooks') as $hook) disk_include_once($hook);
	main::analytics();
	main::chat();
}

function foot_hooks() {
	if (am_var('foot_hooks')) foreach (am_var('foot_hooks') as $hook) disk_include_once($hook);
}

function add_foot_hook($file) {
	am_var('foot_hooks', array_merge([$file], am_var('foot_hooks') ? am_var('foot_hooks') : []));
}
