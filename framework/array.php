<?php
///ARRAY FUNCTIONS (from text.php)

function item_or($array, $key, $default = false) {
	return isset($array[$key]) ? $array[$key] : $default;
}

function replaceItems($text, $array, $wrap = '', $arrayCheck = false) {
	foreach($array as $key => $value) {
		if ($arrayCheck && is_array($value)) continue;
		$key = $wrap . $key . $wrap;
		$text = str_replace($key, $value, $text);
	}

	return $text;
}

function replace_dictionary($text, $array) { return replaceVars($text, $array); }


function valueIfSet($array, $key, $default = false) {
	return isset($array[$key]) ? $array[$key] : $default;
}

function valueIfSetAndNotEmpty($array, $key, $default = false, $type = 'no-change') {
	return isset($array[$key]) && $array[$key] ? parseAnyType($array[$key], $type) : $default;
}

function parseAnyType($val, $type) {
	if ($type == 'no-change') return $val;
	if ($type == 'bool') {
		$false = in_array($val, [false, 'false', 'no']);
		return !$false && in_array($val, [true, 'true', 'yes']);
	} else if ($type == 'array') {
		return explode(', ', $val);
	}

	parameterError('unsupported type', $type, false);
}

function arrayIfSetAndNotEmpty($array, $key, $default = false) {
	if (!isset($array[$key]) || !$array[$key])
		return $default ? [$default] : [];

	$value = $array[$key];
	if (!is_array($value)) $value = [$value];
	if ($default) $value[] = $default;
	return $value;
}

function textToArray($line) {
	$r = [];
	$items = explode(', ', $line);
	foreach ($items as $item) {
		$bits = explode(': ', $item);
		$r[$bits[0]] = $bits[1];
	}
	return $r;
}

function replaceValues($text, $array) {
	foreach($array as $key => $value) $text = str_replace('%' . $key . '%', $value, $text);
	return $text;
}

function concatSlugs($params, $sep = '/') {
	return implode($sep, $params);
}

function getShuffledItems($items, $count = 1) {
	$ic = count($items);

	if ($ic == 0) return [];
	else if ($count >= $ic) $count = $ic;

	$keys = array_rand($items, $count);
	if ($count == 1) $keys = [$keys];
	$new = [];
	foreach ($keys as $key) $new[$key] = $items[$key];
	return $new;
}

function getConfigValues($file) {
	if (!disk_file_exists($file)) return false;

	$lines = txt_to_list(file_get_contents($file));
	$config = [];

	foreach ($lines as $kv) {
		$bits = explode(': ', $kv, 2);
		$config[$bits[0]] = $bits[1];
	}

	return $config;
}

function getRange($array, $upto, $exclude = []) {
	if ($upto >= count($array)) return $array;
	$op = [];
	foreach ($array as $item) {
		if (count($op) == $upto) break;
		$op[] = $item;
	}
	return $op;
}

function array_first($gp, $key) {
	if (!isset($gp[$key])) die('unable to find ' . $key . ' in user info');
	$a = $gp[$key];
	if (count($a) > 1) die('duplicates found for ' . $key . ' in user info');
	return $a[0];
}

//Moved from SHEET Section
function array_group_by($array, $index)
{
	$r = array();
	foreach ($array as $i)
	{
		if (!isset($r[$i[$index]])) $r[$i[$index]] = array();
		$r[$i[$index]][] = $i;
	}
	return $r;
}

///JSON Functions
function json_to_array($name) {
	$raw = disk_file_get_contents(contains($name, '/') ? $name : SITEPATH . '/data/' . $name . '.json');
	return json_decode($raw, true);
}

function txt_to_list($data) {
	$r = array();
	$lines = explode(am_var('safeNL'), $data);
	foreach ($lines as $lin)
	{
		$lin = trimCrLf($lin);
		if ($lin == '') continue;
		$r[] = $lin;
	}
	return $r;
}

///SHEET (TSV) FUNCTIONS
function tsv_to_array($data, &$cols = null) {
	$r = array();
	$lines = explode(am_var('safeNL'), $data);
	foreach ($lines as $lin)
	{
		$lin = trimCrLf($lin);
		if ($lin == '' || $lin[0] == '#' || $lin[0] == '|')
		{
			if ($lin != '' && $lin[0] != '|')
				if ($cols === 'object') tsv_set_cols($lin, $cols); else $cols = array_flip(explode("	", substr($lin, 1)));
			continue;
		}
		$r[] = explode("	", $lin);
	}
	return $r;
}

function tsv_set_cols($lin, &$c)
{
	$lin = substr($lin, 1);
	$r = explode("	", $lin);
	$c = new stdClass();
	foreach ($r as $key => $value)
	{
		$value = trim($value);
		$c->$value = trim($key);
	}
}

function _sheetPath($name) {
	return endsWith($name, '.tsv') ? $name
		: SITEPATH . '/data/' . $name . '.tsv';
}

function has_sheet($name) {
	return disk_file_exists(_sheetPath($name));
}

function get_sheet($name, $groupBy = 'section') {
	$varName = 'sheet_' . $name . '_' . $groupBy;
	$existing = am_var($varName);
	if ($existing) {
		am_var('sectionColumns', $existing->columns);
		return $existing;
	}

	$cols = true;
	$file = _sheetPath($name);

	if (!disk_file_exists($file)) $file = $name; //NOTE: some new problem with php8

	$rows = tsv_to_array(disk_file_get_contents($file), $cols);

	$r = new stdClass();
	$r->columns = $cols;
	$r->rows = $rows;

	am_var('sectionColumns', $cols);
	if($groupBy !== false)
		$r->sections = array_group_by($rows, $cols[$groupBy]);

	am_var($varName, $r);
	return $r;
}

function get_page_value($sectionName, $key, $default = false) {
	$values = am_var($cacheKey = 'values_of_' . $sectionName);

	if (!$values) {
		$sheet = am_var('rich-page');
		$section = $sheet->sections[$sectionName];

		$valueIndex = $sheet->columns['value'];
		$values = [];

		$keys = array_group_by($section, $sheet->columns['key']);
		foreach ($keys as $k => $v)
			$values[$k] = $v[0][$valueIndex];

		am_var($cacheKey, $values);
	}

	if ($default && !isset($values[$key])) { echo $default; return; }
	echo !contains($key, 'content') ? $values[$key] : renderSingleLineMarkdown(str_replace('|', am_var('nl'), $values[$key])); //NOTE: be strict!
}

//expects item_r to be defined
function if_not_item_r($col, $item) {
	$cols = am_var('sectionColumns');
	return !$item[$cols[$col]];
}

function if_item_r($col, $item) {
	$cols = am_var('sectionColumns');
	return !!$item[$cols[$col]];
}

function items($item, $cols) {
	$array = [];
	foreach ($cols as $col)
		$array[$col] = item_r($col, $item, true);
	return $array;
}

function item_r_or($col, $item, $or, $return = false) {
	$r = item_r($col, $item, true);

	if (!$r) $r = $or;

	if ($return) return $r;

	echo $r;
}
