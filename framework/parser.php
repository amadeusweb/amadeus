<?php
function getWorkSettings($file) {
	$config = dirname($file) . '/_work-settings.txt';
	return disk_file_exists($config) ? disk_file_get_contents($config) : '';
}

function parseCompositeWork($raw, $param1IsPage) {
	//$called = am_var_or('pcw-called', 0) + 1; am_var('pcw-called', $called); parameterError('parseCompositeWork - call:', $called, false); if ($called > 3) return 'USELESS IMRAN!';

	$pieces = explode('## ', $raw);
	$noBeginning = contains($raw, '<!--no-beginning-->');
	$noCategory = contains($raw, '<!--no-category-->');
	$noAllPieces = contains($raw, '<!--no-all-pieces-->');
	$withMore = contains($raw, '<!--pieces-with-more-->');

	$start = $noBeginning ? false : explode(am_var('safeNL'), $pieces[0], 2)[1]; //cannot start with a level 2 heading, expects all variables on the first line

	$assetsUrl = am_var_or('assetsUrl', '');
	$assetsFol = am_var_or('assetsFol', '');
	$wantsMD = do_md_in_parser($raw);
	$wantsItemMD = contains($raw, '<!--markdown-in-item-when-processing-->');
	$autop = contains($raw, '<!--autop-->');
	$separators = $wantsMD ? [am_var('safeNL')] : [];
	$separators = array_merge($separators, $autop ? ['</p>', '|'] : ['|']);
	$doSkip = //true || 
		!am_var('local') && !isset($_GET['dont-skip']);

	$sorted = [];
	$printSortKeys = isset($_GET['sort-keys']);
	$sortBy = contains($raw, '<!--sort-by-name-->') ? 'name' :
		(contains($raw, '<!--sort-by-category-->') ? 'category' :
			(contains($raw, '<!--sort-by-number-->') ? 'number' : false));

	$result = false;
	$noCount = contains($raw, '<!--no-count-->');
	$noHeader = am_var('no-print-header') || contains($raw, '<!--no-print-header-->');
	$noFooter = am_var('no-print-footer') || contains($raw, '<!--no-print-footer-->');
	$printOnly = hasPageParameter('print');
	$noLinking = $printOnly && am_var('no-linking-in-print') || contains($raw, '<!--no-linking-in-print-->');
	$paramToUse = contains($raw, '<!--node-item-is-param2-->') ? '2' : '1';
	$index = false;

	$pieceOnly = $param1IsPage ? (isset($_GET['item']) ? $_GET['item'] : false) : 
	(am_var('page_parameter' . $paramToUse) && am_var('page_parameter' . $paramToUse) != 'print' ? am_var('page_parameter' . $paramToUse) : false);

	$base = am_var('url') . am_var('node') . '/' . ($paramToUse == 2 ? am_var('page_parameter1') . '/' : '') . ($param1IsPage ? am_var('page_parameter' . $paramToUse) . '/' : '');

	if (!$noCategory) {
		$categoryStart = 'Category: ';
		$categories = [];
		$categoryOnly = false;

		$wantsSingleCategory = $param1IsPage ? isset($_GET['category']) : $pieceOnly == 'category';
		if ($wantsSingleCategory) {
			$categoryOnly = $param1IsPage ? $_GET['category'] : am_var('page_parameter' . ($paramToUse == '2' ? '3' : '2'));
			$pieceOnly = false;
		}
		$categoryBase = $base . ($param1IsPage ? '?category=' : 'category/');
	} else {
		$linksToPieces = [];
	}

	$pieceBase = $base . ($param1IsPage ? '?item=' : '');
	$urlSuffix = $param1IsPage ? '' : '/';
	$contentIndex = $noCategory ? 2 : 3;

	foreach ($pieces as $piece) {
		if ($index === false) {
			$index = 0;
			continue;
		}

		$bits = explodeByArray($separators, $piece, 4);
		//parameterError('BITS', $bits, false);
		if ($doSkip && $bits[1] == 'SKIP') continue;

		$title = trim($bits[0]);

		if (!$noCategory) {
			$category = trim(str_replace($categoryStart, '', $bits[2]));
			if (!$category) $category = 'Uncategorized';

			$count = isset($categories[$category]) ? $categories[$category] + 1 : 1;
			$categories[$category] = $count;
			if ($categoryOnly && urlize($category) != $categoryOnly) continue;
		}

		$index += 1;
		//if ($index <= 170) continue; if ($index == 181) break;

		$slug = urlize($title);
		$isThisTheCurrentPiece = urlize($title) == $pieceOnly;
		$titleLink = $isThisTheCurrentPiece ? $title : (makeLink($title, $pieceBase . $slug . $urlSuffix, false, $noLinking));

		if ($noCategory) $linksToPieces[] = $titleLink;
		if ($noAllPieces && !$pieceOnly) continue;
		if ($pieceOnly && !$isThisTheCurrentPiece) continue;

		$thisResult = '<section>' . am_var('nl') . '	<h2 class="piece">' . $titleLink . ' <span class="number">#' .
			$bits[1] . ($noCount ? '' : ' &mdash; (' . $index . ' / TOTAL)') . '</span></h2>' . am_var('nl');
		
		if (disk_file_exists($assetsFol . $slug . '.jpg'))
			$thisResult .= '<img src="' . $assetsUrl . $slug . '.jpg' . '" class="img-piece img-max-200" />' . am_var('nl');

		$thisResult .= renderOnlyMarkdownOrRaw($bits[$contentIndex], $wantsItemMD, ['echo' => false, 'strip-paragraph-tag' => true]) . am_var('nl') . '</section>' . am_var('2nl');

		if ($sortBy) {
			$key = $sortBy == 'number' ? $bits[1] : ($sortBy == 'category' ? $category . '--' . $slug : $slug); //defaults to title ($slug)
			if ($printSortKeys) $thisResult = $key . $thisResult;
			$sorted[$key] = $thisResult;
		} else {
			$result .= $thisResult;
		}
	}

	if ($sortBy) {
		ksort($sorted, SORT_NATURAL);
		$result .= implode(am_var('2nl'), $sorted);
	}

	$result = str_replace('TOTAL', $index, $result);
	$result = str_replace('<p>- - -</p>', '<div class="break-page' .
		($printOnly ? ' printing' : '') . '"><i>Continued&hellip;</i><hr /></div>'
		. ($printOnly ? '</section><section>&hellip;Continuing<hr />' : ''), $result);

	if ($withMore) $result = str_replace(
		'<!--more--><br />', '<a href="javascript: void(0);" class="read-piece">Read More</a>' . am_var('nl') .
		'<div class="piece-content">', $result) . '</div>';

	if ($param1IsPage) echo '<h2>' . humanize(am_var('node')) . '</h2>' . am_var('2nl');
	$page = $param1IsPage || $paramToUse == '2' ? am_var('page_parameter' . ($paramToUse == '2' ? '1' : '2')) : am_var('node');
	$home = '<h3 class="home"><u>' . makeLink(humanize($page), $base, false, $noLinking) . '</u></h3>' . am_var('2nl');
	$links = '';

	if (!$noCategory) {
		$links = [];
		foreach ($categories as $item => $count) {
			$slug = urlize($item);
			$img = !disk_file_exists($assetsFol . '/categories/' . $slug . '.jpg') ? '' :
				am_var('nl') . '<br /><img src="' . $assetsUrl . 'categories/' . $slug . '.jpg' . '" class="img-category img-max-200" alt="' . $item . '" />' . am_var('nl');
			$links[$slug] = makeLink(humanize($item) . ' (' . $count . ')' . $img, $categoryBase . $slug . $urlSuffix, false, $noLinking);
		}

		ksort($links, SORT_NATURAL);
		$div = '<div class="col-lg-3 col-md-4 col-sm-12">';
		$links = '<section class="navigation">' . $home . '<h3>Categories</h3><div class="row">' . am_var('nl') . $div
			. implode('</div>' . am_var('nl') . $div, $links) . '</div>' . am_var('nl') . '</div></section>' . am_var('2nl');
	} else {
		$links = '<section class="navigation">' . $home . '<h3>Pieces</h3>' . am_var('nl') . '<ol><li>' . am_var('nl')
			. implode('</li><li>' . am_var('nl'), $linksToPieces) . '</li></ol>' . am_var('nl') . '</section>' . am_var('2nl');
	}

	if (am_var('flavour') == 'yieldmore')
		$result .= '<style>#file { background-color: var(--amw-site-background, #A8D4FF)!important; }</style>';

	$start = $start ? renderOnlyMarkdownOrRaw($start, $wantsMD, ['wrap-in-section' => true, 'echo' => false]) : '';
	if (contains($raw, '<!--no-print-->'))
		return $start . am_var('2nl') . $links . $result;

	//NOTE: Print "inner" pdf (w/o covers) combine with: https://pdfjoiner.com/ (no margins for images)
	$front = getSnippet(am_var('node') . '-front');
	$front = $front ? '<section class="first-page">' . $front . '</section>' : '';

	$printBtn = !$printOnly ? '' : '<div class="print-button" style="text-align: center"><a class="btn btn-primary" '
		. 'href="javascript: print();">PRINT THIS WORK</a></div>';

	$back = getSnippet(am_var('node') . '-back');
	if ($back) $back = '<section class="last-page">' . $back . '</section>';

	$printHeader = $noHeader ? '' : replaceItems('<div id="print-header"><h3>"%node%" by <i>%name%</i></h3></div>',
		['node' => humanize(am_var('node')), 'name' => am_var('name')], '%') . am_var('2nl');

	$printFooter = $noFooter ? '' : replaceItems('<div id="print-footer">' . am_var('nl')
		.'<img src="%imgUrl%" style="height: 40px;" alt="footer logo" /> <span>&copy; %year% &mdash; by <u>%name%</u></span></div>',
		['year' => date('Y'), 'node' => humanize(am_var('node')), 'name' => am_var('name'),
			'imgUrl' => am_var('url') . am_var('safeName') . '-logo.png'], '%') . am_var('2nl');

	$result = $printBtn . $start . $front . am_var('2nl') . $links . $result . $back . $printHeader . $printFooter;

	return $result;
}

