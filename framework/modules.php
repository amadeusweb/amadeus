<?php
DEFINE('AMADEUSMODULES', AMADEUSPATH . '/modules/');
DEFINE('AMADEUSFLAVOURS', AMADEUSPATH . '/flavours/');
DEFINE('AMADEUSFEATURES', AMADEUSPATH . '/features/');
DEFINE('AMADEUSEXTENDED', AMADEUSPATH . '/awe/');

disk_include_once(AMADEUSFEATURES . 'feature-loader.php');
disk_include_once(AMADEUSEXTENDED . 'awe-loader.php');

function _load_amadeus_module($mod, $multiple = false) {
	runModule($mod, $multiple);
}

function runModule($mod, $multiple = false) {
	$file = AMADEUSMODULES . $mod . '.php';
	if ($multiple) {
		disk_include($file);
		return;
	}

	disk_include_once($file);
	uses_module($mod);
}

function includeFeature($name, $code = true) {
	if ($code) $name = 'code/' . $name;
	disk_include_once(featurePath($name . '.php'));
}

function featurePath($name) {
	return AMADEUSFEATURES . $name;
}

function uses_module($mod) {
	am_var('module_' . $mod, true);
}

function has_module($mod) {
	return am_var('module_' . $mod);
}

function module_file_path($file) {
	return AMADEUSMODULES . $file;
}

//considering flavours are special modules
function flavour_file_path($file, $flavour) {
	return AMADEUSFLAVOURS . $flavour . '/' . $file;
}

//DEPRECATED: Cleanup everywhere
function run_site_code($code, $checkOnly = false, $multiple = false, $params = [])
{
	return runCode($code, [
		'checkOnly' => $checkOnly,
		'multiple' => $multiple,
		'params' => $params,
	]);
}

function runCode($code, $settings = []) {
	$checkOnly = isset($settings['checkOnly']) ? $settings['checkOnly'] : false;
	$multiple = isset($settings['multiple']) ? $settings['multiple'] : false;
	$params = isset($settings['params']) ? $settings['params'] : [];
	$flavour = isset($settings['flavour']) ? $settings['flavour'] : am_var('flavour');

	$wasOptional = contains($code, 'optional-');
	if ($wasOptional) $code = str_replace('optional-', '', $code);

	$path = am_var('path') . '/code/' . $code . '.php'; //look in site first (override mechanism)

	if (!disk_file_exists($path) && $flavour) {
		$flavourFolder = am_var('flavourPath') ? am_var('flavourPath') : AMADEUSPATH . '/flavours/' . $flavour . '/';
		$path = $flavourFolder . $code . '.php';
	}

	if (!disk_file_exists($path)) {
		$optional = $wasOptional || in_array($code, ['head', 'menu', 'footer-content']);

		if (!$optional)
			parameterError('$path & $code & $flavour', [ 'path' => $path, 'code' => $code, 'flavour' => $flavour, 'flavourPath' => am_var('flavourPath')]);

		return false;
	}

	if ($checkOnly) return true;

	if ($multiple)
		disk_include($path, $params);
	else
		disk_include_once($path, $params);

	return true;
}
?>
