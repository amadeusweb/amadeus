<?php
function get_seo_for($slug, $prefix = ' &mdash;&gt; ', $field = 'about') {
	if (!has_sheet('seo')) return '';
	$sheet = get_sheet('seo', 'slug');
	return isset($sheet->sections[$slug]) ? $prefix . $sheet->sections[$slug][0][$sheet->columns[$field]] : '';
}

function read_seo_info() {
	if (has_var('description')) return; //set from node
	if (!has_sheet('seo')) return;

	$sitemap = get_sheet('seo', 'slug');
	$node = am_var_or('node', 'none');
	$section = am_var('section');
	if (!is_string($section)) $section = 'unknown';

	$item = isset($sitemap->sections[$node]) ? $sitemap->sections[$node][0] : false;
	if (!$item && isset($sitemap->sections[$section])) $item = $sitemap->sections[$section][0];
	if (!$item && isset($sitemap->sections['index'])) $item = $sitemap->sections['index'][0];

	if ($item) am_var('current_page', items($item, array_keys($sitemap->columns)));
	$seo = $item ? items($item, ['description', 'keywords']) : [];
	setup_seo($seo);
}

function setup_seo($seo) {
	extract($seo);
	if (isset($description)) am_var('description', $description);
	if (isset($keywords)) am_var('keywords', $keywords);

	//https://ogp.me/
	am_var('og:description', isset($description) ? $description : am_var('name') . ' - ' . am_var('byline'));
	am_var('og:title', am_var('name'));
	am_var('og:url', $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
	am_var('og:type', 'website');
	am_var('fb:app_id', '966242223397117'); //TODO: how to get this? //https://yoast.com/help/fb-app-id-warnings/

	$fmt = SITEPATH . '/assets/pages/%s.jpg'; //TODO: Add OG images
	$files = [am_var('node'), am_var_or('section', 'unknown'), am_var('safeName')];
	foreach ($files as $file) {
		if (disk_file_exists(sprintf($fmt, $file))) {
			am_var('og:image', am_var('url') . 'assets/pages/' . $file . '.jpg' . version());
			break;
		}
	}
}

function setNodeSeo($folder) {
	$vars = setNodeVars($folder);

	//TODO: HIGH: Resolve Image for Open Graph

	if (isset($vars['description']))
		am_var('description', str_replace('<br /><br />', ' - ', str_replace('**', '', str_replace(am_var('markdownStartTag'), '', $vars['description']))));

	if (isset($vars['keywords']))
		am_var('keywords', $vars['keywords']);
}

function seo_info() {
	$item = am_var('current_page');
	if (!$item) return;

	echo '<section id="seo-info" class="container" style="padding-top: 30px;">' . am_var('nl');
	echo featureHeading('seo');

	$fmt = '<p><h4>%s</h4>%s</p>' . am_var('nl');

	$cols = ['about', 'description', 'keywords'];
	foreach ($cols as $col) {
		$field = isset($item[$col]) ? $item[$col] : false;
		if ($field) echo sprintf($fmt, ($col != 'about' ? 'SEO ' : '') . humanize($col), $field);
	}

	echo am_var('nl') . '</section>' . am_var('nl');
}

function seo_tags($return = false) {
	$fmt = '	<meta name="%s" content="%s" />';
	$ogFmt = '	<meta property="%s" content="%s" />';

	am_var('generator', 'Amadeus Web Builder / CMS at amadeusweb.com');
	$op = [];

	foreach (['generator', 'description', 'keywords', 'og:image', 'og:title', 'og:description', 'og:keywords', 'og:url', 'og:type', 'fb:app_id'] as $key)
		if ($val = am_var($key)) $op[] = sprintf(startsWith($key, 'og:') || startsWith($key, 'fb:') ? $ogFmt : $fmt, $key, replace_vars($val));

	$op = implode(am_var('nl'), $op);
	if ($return) return $op;
	echo $op;
}
