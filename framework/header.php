<?php
function doHeader() {
	if (am_var('site-lock') || am_var('node-theme')) return;
	render_banner();
	render_section_menu();
	assistant('quick-links');
	if (!am_var('no-assistant') && !in_array(am_var('node'), am_var_or('no-assistant-in-header-for', []))) assistant();
}

function render_banner() {
	$pagesSite = am_var('path') . '/assets/pages/';
	$folSite = am_var('path') . '/';
	$folSection = am_var('path') . '/' . am_var('section') . '/';
	$folNode = am_var('path') . '/' . ($folRelative = am_var('section') . '/' . am_var('node') . '/');
	$params = compact('pagesSite', 'folSite', 'folSection', 'folNode');

	function getPrefixes($params) {
		extract($params);
		$node = am_var('node');
		$substitutes = am_var_or('page-image-substitution', []);

		if ($match = array_search($node, $substitutes))
			$node = $substitutes[$match];
		else if (isset($substitutes['*']))
			$node = $substitutes['*'];

		$op = [];
		if (!has_var('node-vars')) {
			if (has_var('is-node-item'))
				$op['page-item'] = $pagesSite . am_var('node') . '-' . am_var('node-item');
			$op['page'] = $pagesSite . $node;
		} else {
			$op['node'] = $folNode . 'images/' . am_var('node');
			$op['section'] = $folSection . '' . am_var('section');
		};
		return $op;
	}

	function renderedBanner($params) {
		extract($params);
		$banners = [
			'node' => $folNode . '_banner.html',
			//'section' => ($sectionPrefix = $folSection . '_banner.html'),
			//'site' => ($sitePrefix = $folSite . am_var('safeName')) . '.html',
		];

		$sectionOrSiteForBanner = am_var('sectionUrl') ? am_var('sectionUrl') : am_var('url') . am_var('safeName');

		foreach($banners as $banner) {
			if (!disk_file_exists($banner))
				continue;

			echo '<!-- RENDERING banner: ' . $banner . ' -->' . am_var('nl');
			echo '<section class="amadeus-feature">' . am_var('nl');
			renderAny($banner, ['replaces' => [
				'sectionOrSiteForBanner' => $sectionOrSiteForBanner,
			]]); //this variable made only to site. section icon expected to be there in every
			echo '</section>' . am_var('2nl');
			return true;
		}

		return false;
	}

	function renderedImages($params) {
		$prefixes = getPrefixes($params);
		foreach($prefixes as $at => $prefix) {
			$portrait = SITEPATH . '/' . ($prefix = str_replace(SITEPATH . '/', '', $prefix)) . '-portrait.jpg';
			if (!disk_file_exists($portrait))
				continue;

			$blur = in_array(basename($prefix), am_var_or('blur_banners', []))
				? 'filter: blur(3px); -webkit-filter: blur(3px); ' : '';
			if ($blur) { ?>
			<div class="show-in-portrait banner banner-<?php echo $at; ?> bg-image"
				style="background-image: url('<?php echo am_var('url') . $prefix;?>-portrait.jpg'); <?php echo $blur;?>background-position: center; height: 400px;">
			</div>
			<div class="show-in-landscape banner banner-<?php echo $at; ?> bg-image"
				style="background-image: url('<?php echo am_var('url') . $prefix;?>-landscape.jpg'); <?php echo $blur;?>background-position: center; height: 400px;">
			</div>
			<?php } else { ?>
			<div class="banner banner-<?php echo $at; ?>">
				<?php
				if(($bannerLink = am_sub_var('node-vars', 'banner-link'))
						|| ($bannerLink = am_sub_var('banner-links', am_var('section')))
						|| ($bannerLink = am_sub_var('banner-links', 'site'))
					) $bannerLink = renderAny('<a href="' . $bannerLink . '">'); //TODO: internal links shouldnt open in new tab, so use smart
				if ($bannerLink) echo $bannerLink;
				makePLImages($prefix);
				if ($bannerLink) echo '</a>';
				?>
			</div>
			<?php }
			echo '<hr />';
			return true;
		}

		return false;
	}

	$matched = renderedImages($params) || renderedBanner($params);

	//TODO: proper resolution with _ for section
	if (false && !$matched && am_var('local')) {
		echo '<pre style="background-color: #fee; padding: 15px;">WARNING - Missing Banner<br />' .
			'Looked for htmls : ' . print_r($banners, 1) .
			'Looked for PH images with prefixes: ' . print_r($prefixes, 1) .
			'</pre>';
	}
}

function render_section_menu() {
	$folRelative = '/' . am_var('section') . '/' . am_var('node') . '/';

	if (!am_var('section') || am_var('no-section-menu') || !disk_is_dir(am_var('path') . $folRelative)) return;

	$files = get_menu_files(am_var('node'));
	if (!$files) $files = am_sub_var('node-vars', 'files');
	if (menu_is_empty($files, am_var('path') . $folRelative)) return;

	echo '<section id="header-menu" class="amadeus-feature' . (menu_is_large() ? ' large-menu' : '') . '">' . am_var('nl');
	echo featureHeading('site', 'full', '<abbr title="site in menu item (section)">SITE:</abbr> <a href="'
		. am_var('url') . am_var('node') .'/">"<u>' . humanize(am_var('node')) . '</u>"</a>');

	if ($byline = am_sub_var('node-vars', 'byline'))
		echo '	<h3>' . renderMarkdown($byline, ['strip-paragraph-tag' => true]) . '</h3><hr />' . am_var('nl');

	menu($folRelative, [
		'blog-heading' => contains(am_var('section'), 'blog'),
		'parent-slug' => am_var('node') . '/',
		'files' => $files,
		'what' => 'header',
		'where' => am_var('node') . '/',
		'in-header' => true,
		'breaks' => am_sub_var('node-vars', 'menu-breaks'),
	]);

	echo '</section>' . am_var('2nl');
} //end render_section_menu
