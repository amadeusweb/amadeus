<?php

$safeNL = "\r"; //platform safe
$nl = "\r\n"; $br = '<br/>';

am_var('safeNL', $safeNL);
am_var('nl', $nl);
am_var('2nl', $nl . $nl);
am_var('3nl', $nl . $nl . $nl);

am_var('br', $br);
am_var('2br', $br . $br);
am_var('brnl', $br . $nl);

am_var('markdownStart', $md = '<!--markdown-->');
am_var('markdownStartTag', $md . $nl); //NOTE: to detect content which doesnt start with a heading
am_var('autopStart', '<!--autop-->');

function trimCrLf($txt) {
	return trim($txt, "\r\n");
}

function urlize($txt) {
	return replaceItems(strtolower($txt), ["'" => '', ' ' => '-', '&hellip;' => '__', '&' => 'and']);
}

function strip_hyphens($txt) {
	return replaceItems($txt, ['-' => ' ']);
}

function strip_paragraph($txt) {
	return replaceItems($txt, ['</p>' => '', '<p>' => '']);
}

function humanize($txt, $how = false) {
	$words = ucwords(replaceItems($txt, ['--' => ' &mdash; ', '-' => ' ', '_' => '']));
	if ($how !== 'no-site' && function_exists('site_humanize')) $words = site_humanize($words, 'title', $how);
	return $words;
}

function blog_heading($name, $fol) {
	if (contains($fol, 'by') || in_array($fol, am_var_or('flat-blogs', []))) return humanize($name);

	$fileName_r = explode('-', $name, 2);
	$month_r = explode('-', $fol, 2);

	$fileName_r = $fileName_r[0] . ' ' . $month_r[0] . ' &mdash; ' . $fileName_r[1];
	return humanize($fileName_r);
}

//https://github.com/yieldmore/MicroVC/blob/master/tlr/app/functions.io.php
function startsWith($haystack, $needle)
{
  $length = strlen($needle);
  return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
  $length = strlen($needle);
  if ($length == 0) {
    return true;
  }

  return (substr($haystack, -$length) === $needle);
}

function contains($haystack, $needle)
{
	return stripos($haystack, $needle) !== false;
}

function simplify_encoding($txt) {
	$replace = [
		'½' => '&frac12',
		'“' => '"', '”' => '"',
		'‘' => "'", '’' => "'",
		'—' => '-', '–' => '-', 'â€"' => '-',
		'…' => '&hellip;'];
	foreach ($replace as $search=>$replace)
 		$txt = str_replace($search, $replace, $txt);
	return $txt;
}

function contact_r($text) {
	$text = replaceItems($text, ['tel:' => '', 'mailto:' => '', 'https://' => '', 'www.' => '']);
	if (contains($text, '?subject')) $text = explode('?subject', $text)[0];
	return $text;
}

function excerpt($fileName, $link, $text = '') {
	$bits = explode('<!--more-->', file_get_contents(am_var('path') . $fileName));
	$contents = markdown($bits[count($bits) > 2 ? 1 : 0]);
	$contents .= '<br />' . markdown("[$text](" . am_var('url') . $link . ')');
	return wpautop($contents);
}

function explodeByArray(array $delimeters, string $input, int $limit = -1): array {
	if($delimeters===[]) return [$input];

	$unidelim = $delimeters[0];
	$step = str_replace($delimeters, $unidelim, $input);
	return explode($unidelim, $step, $limit);
}

