## The Name

Amadeus - named after the movie about "Wolfgang Amadeus Mozart", started in 2019, became a separate website AmadeusWeb.com in 2022.

It's builder is now reaching its 7th verion which will include these AWE (extended features).

Several years ago, we built "[Mozzacart v1](https://showcase.amadeusweb.com/vidzeal/creations/)", the name being a mix of Mozart and Cart.

A digital "cart" is where catalogue items are placed before heading to checkout - just as you would in a supermarket.

So, catalogues can be maintained in excel, copied over into the website and Mozzacart will do the selection, totalling etc.

Mozzamove was a quick way of traversing/filtering by category etc and searching too.

</section><section>

## The database-less way

In v1, order items (sku = quantity) would go as a whatsapp message in a url, for the website owner / business to confirm availability, shipping time, handle custmer needs and send back a final payment link. SKU being the "stock keeping unit".

In the new v2, we will use the "engage" template where a formatted email is created based on the order items and predefined set of choices / fields to fill are given.

This will be available in the following formats.

</section><section>

### Sunlight

For typical businesses with a catalog / service.

</section><section>

### Arya

For collecting donations - predefined set of amounts for various usages with targets and amounts collected for each.

A series of these can be made per center / trainer (with a different UPI / laptop payment link).

</section><section>

### Amelia

For the selling of "**digital material**" made avialable via Google Drive - like books and teaching resources etc.

Again, the request will go by email, but with the drive file link included so, at the backend, the admin can quickly open those files and grant readonly (no download) permission to the user.

They will also be able to put those links into a "starter kit" where the user can have access to the links and track discussions with the author / institution / facilitator.

<small>NB: Amelia (named for Amelia Jane) was the name of the librarian at YM.</small>
