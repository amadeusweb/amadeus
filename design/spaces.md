## Introduction

Amadeus Web Work Spaces - AWWS or just "spaces" in short, is a document management system uniquely ours, where the actual record keeping is only in Google Drive.

The website owner will be able to maintain permissions in "Google Drive", and the end client / user will have a dashboard to access all their records.

In stage 2/3, we will make a mobile app out of this.

## Application

Any team that takes on work for a client - lawyers, chartered accountants, company secretaries and realtors will find this record keeping system easy.

It uses the unique "aurra" A++ library for authentication, recognizing **admins, team members and users**.

Ideally it will be a separate private repository maintained at a subdomain like my.common-planet.org.
