## A Mobile App

All these features we are building will be available as a **Progressive Web App (PWA)** which **should** function offline using localStorage.

A sample PWA was studied and proven in 2020/21 in version 2.

In version 7, it will have a "shell" and can be whitelabelled (for a fee).

The shell (with all the theme files) will be cached, check will check the site/component version and reload only when things are changed.

Sitemap and content (embed only) so the bandwidth usage is kept to a minimum. This will take shape as the system evolves.

More on this later.

</section><section>

## Usage by "end vendors"

Used on the POS (Point of Sale) device.

The end business (micro enterprise) will be able to

* Recording transactions to the device in a tamper-proof manner.
* Work disconnected...
    * Insists to customer in cash / a 2 day credit / a writeoff after 5 days.
    * Anyhow these are small businesses operating on good faith and are encouraged to have a "given freely" quota.
* Prepping report data on "day / week / month / year" closing activities by the **Point of Sale** device.

</section><section>

## Customer can "Claim Bills"

In time, we will develop this. Descoped for now.

End vendor will give him a link to his own space online where he will be able to see collated bills across franchisors.

We will not go into actual mobile payment methode, but enable all to have quick links (see mozzacart too).
