## Welcome

FIRST is the eCommerce library of **Amadeus Web Extended (AWE++)**, unique for its no centralized database feature and option to have reporting in Google Sheets, where sharing can be controlled by the end vendor.

</section><section>

## The Naming

The true transformation is "**spiritual**", not "**digital**" - an old realization of those who can see the farthest because they fly the highest.

**SPRIT** - Flame Imperishable by Recognising Spiritual Trajectories

Imperishable has a special meaning to Imran when he chose it to be the name for a "[wordpress theme](https://bitbucket.org/yieldmore/legacy/src/master/wp-content/themes/imperishable/)" at yieldmore, back in 2013/15. It took many years of evolving file based technology from 2000 to 2019 when "Amadeus" (now AmadeusWeb++) was born.

Flame Imperishable comes from Tolkien lore and is what the "Creator" (Brahman) set to burn at the core of the universe (the backdrop of **consciousness** if you will), something he offers to his **children** at the end of their worldly existence when **[the music shall be played aright](https://legacy.yieldmore.org/books/the-silmarillion/ainulindale/)**.

Since, as is said, we are spiritual beings having a **human existence**, it is only when we recognise that, that our inner work / sadhana can begin.

Hence the imperative, built into the name FIRST iteself - "Recognize Spiritual Trajectories". Trajectories is plural because it begs us to see the people around us and accept and take part in their journeys.

</section><section>

## In Common Parlance

FIRST also has another expansion, this one equally meaningful and close to our hearts as it sets the way for all using the platform to [grow into the still evolving YM growth model](https://ideas.yieldmore.org/growth-model/), whose ultimate aim is to transition the global economy to [debt and tax free **Creditism**](https://common-planet.org/transition/).

**WORLD** - Futuristic Indic Remuneration Strategy and Technology

</section><section>

## License & Usage

* A platform, setup, training and operations fee will be collected, apart from customizations.
* Franchisors may charge their "end vendors" of Small and Micro enterprises a license fee which will be shared with us (AmadeusWebExtended / AWE++) on a 50% / 50% basis.
* An insistence to some degree of the "Growth Model" for team, staff and society made.
* "AdSpace" for Progressive Solutions from dots.ym (a future syndicated promotion network for adherents of the spirit of GameB).
* An active CSR Campaign that integrates with the ARYA vehicle of YieldMore.org will also factor into the terms.
