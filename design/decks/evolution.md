<!--input type="hidden" value="introduction" /-->

## Our Need

* Enable small businesses and micro enterprises
* Good ground-informed operative knowledge
* Futuristic technology solutions

----

## The Transformative Approach

* Currently systems are extremely complex
* They are database centric (point for failure)
* Cannot "work" offline or at heavy cost
* Data ownership is a problem

----

## Our Simplified Way

* A single copy of the frameworks makes maintenance and regression testing a breeze.
* Data does NOT reside on our server - making it decentralized.
* This is even more groundbreaking than "web3" and crypto

