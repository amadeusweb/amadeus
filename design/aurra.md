## An Authentication Library

AURRA - **AU**thentication fo**RR** **A**ll.

Is a simple system to know **who** has access and **where** they have access.

----

### Advantages

* No self signup
* No emails sent by the server
* No need for a captcha

----

### Features

* Sitewide lock unless logged in (this will have a contact us form / mention of how to reset their password via manual admin process)
* Data still will be in **spaces** (drive)
* In the case of **first** (small/micro enterprise segment) only a "anyone with the link" to the vendor transaction/bill protected by a vendor handshake (visible to us as well).
