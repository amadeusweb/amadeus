## Introduction

Bhava means nature, an invitation to us to bring out our best and brightest nature.

Deva Bhava or "divine nature" is a play on db (database) - something Imran's professor SS Ranganathan told him in 2004 was already obsolete. Yet we thrive on creating "warehousing" tools for reporting.

Imran, having build plenty of [windows an web applications](https://imran.yieldmore.org/career-past/) using database systems and reporting tools (pre nosql), AWE++ stands here today, battling with the paradigm of data ownership and decentralization, relying on Version Control technology callet Git which is used throughout the industry - but only for "code".

In amadeus, **everything** is file-based, hence managed byt git - right from the name (config) to code (php) to content (markdown).

	* builder
	* site
	* site section

We only have to leverage all these open source technologies and add a dimension of "database" (Deva Bhava) management. Git makes it secure adn tamper proof - something the new web3 is reaching towards.

</section><section>

## Going Database-less

Since that is one of Amadeus' USPs, we are going even further to say:

* Transactions can be stored on "git technology" - likely provided by Google.
* Operations team, during the onboarding, will.
    * setup a local "site" which will run offline on the PoS device.
    * configure the device from the business' git account.
* Git readonly access will be given to the:
    * Franchisor and it's assignees.
    * Us, the platform designers and builders (AmadeusWeb).
    * Microbusiness owner & their admin / accounts / auditor.
    * Stakeholders like the banks which have enabled the business / other MSME support personnel.
    * Observers like IT Ministry and Advisors
* Git write access will be given to all these on the "activity" repository which will keep track of who has done what / visited what page.


</section><section>

## Report by Google

* A google script will be used to download the summaries from "git" into spreadsheets.
* No passwords / who has access to what will be stored on our server.
* Files created by the "end vendor" will be in their "Google Drive Shared Readonly Folder"
* The "link only" will available to people who know their "DossierId" and access code.
	* Access code will only show the link.
	* Google Drive Permissions will actually control who can see it.
* Rather than authorize a new "application" in Google the vendor will be able to "copy a demo drive spreadsheet" and change their "git endpoint" and "app apssword".

We will have versioned excel templates that look for configuration in a private only file.
