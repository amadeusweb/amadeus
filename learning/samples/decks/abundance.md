<input type="hidden" value="introduction" />

## Intimate Gatherings

With New-Made Friends and Loved Ones
(Yielding More Neurolinguistically and Phonetically)
### with Imran Ali Namazi
[originally made in 2022 at sabd](https://bitbucket.org/yieldmore/ideas/src/master/content/programs/sessions/decks/intimate-gatherings.md)

----

<input type="hidden" value="picture-of-facilitators" >

<img src="https://ideas.yieldmore.org/assets/mixed/church-the-baptized-speaketh.jpg" style="width: 60%;">

Imran the Annually Rebaptized Speaketh! <3

----

<input type="hidden" value="purpose" >

## MAIN PURPOSE
<h1>Enjoy | Explore | Express</h1>
### Wisdom With Words

----

<input type="hidden" value="objectives" />

Tickling the Emotional Body and Trusting the Inner Expression through words, sounds and art.

----

<input type="hidden" value="sessions" />

SESSIONS FORESEEN

* (1) Inspiring Stories by Others (Curation)
* (2) Abundance and Mindset
* (3) Share-and-Heal by Speaking your Heart
* (4) Poems by YOU on Inner and Outer Nature

----

<input type="hidden" value="picture-of-healees" >

<a href="https://legacy.yieldmore.org/works/leaf-by-niggle/">The Importance of ART</a> and it's<br> INTEGRALITY TO DAILY LIVING
<img src="https://ideas.yieldmore.org/assets/mixed/intimate-gatherings-invocation-niggles-mountains-by-maria.jpg" style="width: 60%;">

----

<input type="hidden" value="invocation" />

<!--
    INSTRUCTIONS:
    * Light a Lamp and let the feelings and emotions flow
    * Journals for OUR Free Writing and reflections and imaginative thoughts to help our Myriad Communities
    * The Courage to LIVE those DREAMS and HEAL SWIFT AND STRONGLY
    * With a Barbaric YAWP and a Sankalpa / VOW for SELF, let us begin
-->
### (0) Invocation and Bonding

* <a href="https://archives.yieldmore.org/our-gajananam/">Prayer to the Old Indic Lord Ganesha</a>
* JRR Tolkien says <a href="https://ideas.yieldmore.org/serve/earth/">Save the Earth</a>
* Replenish your SOULS in Niggle's Parish <3
* Dedicated to ALL MOTHERs EVERYWHERE
* <a href="https://imran.yieldmore.org/niggle/">imran.yieldmore.org/niggle/</a>
* <a href="https://legacy.yieldmore.org/books/the-silmarillion/two-trees/">books/the-silmarillion/two-trees/</a>


----

<input type="hidden" value="session-on-curation" />

## (1) Some Inspired Music and Books

* Short and Sweet - Our Anthem
* Curious Lives - Richard Bach
* Tolkien's Creation Myth and Case for Art

----

<input type="hidden" value="session-on-abundance" />

## (2) Programs and Discussion on Abundance

* 11 Minute Vacation
* Serenity Chapter
* Sacred Body Prayer
* Your OWN Affirmation
* Creative-Abundance Read & 21 Day Program

----

<input type="hidden" value="session-on-share-and-heal" />

## (3) Sharing Openly and Healing

* Understanding our Needs and Feelings
* Releasing Past Pains
* Exploring Similarities
* Feeling Understood and Loved

----

<input type="hidden" value="session-on-nature" />

## (4) Nature themed Poetry Writing

That <a href="https://imran.yieldmore.org/niggle/">HUMAN TURNED GOD / PARASISE BUILDER - Niggle</a> by JRR Tolkien

* Our Own Nature
* The Nature of People
* Mother Nature

----

<input type="hidden" value="program-summary" />

Likely in Egmore, Chennai in Jan 2022

* Rs 10,000 for a 8 hour workshop over 2 days. Discounting Possible
* Invitations for participants to become facilitators
* Read this with your children - <a href="https://imran.yieldmore.org/keep-smiling/">Keep Smiling</a>

----

<input type="hidden" value="song-pink-floyd-time" />

### NO ONE TOLD YOU when to run - NOW DON'T MISS THE STARTING GUN
<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/T2LUl9C_Yfk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

----

<input type="hidden" value="imran-yieldmore-purpose-2017" />

### YM is a link between People, Organizations and Movements
<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/PTIqjpkF5Ss" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
