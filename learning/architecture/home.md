## What is Amadeus?

Written in PHP, Amadeus is a Microframework or Content Management System used for hand crafting powerful websites.

It's a no-nonsense proprietary (source available) system, unique for its NO-DATABASE and customizable menu structure (taxonomy / content architecture).

It has been used in [over 30 websites](https://code.amadeusweb.com/) - best of all, it uses a single copy of its libraries per server and can run on your server using the [AMADEUSURL variable](https://bitbucket.org/amadeusweb/amadeus/src/master/framework/entry.php), explained [in this commented section here](https://bitbucket.org/amadeusweb/amadeus/src/master/index.php).

<!--more-->

The themes and framework folders contained in its core are accessed via [an entry point](https://bitbucket.org/amadeusweb/amadeus/src/master/framework/entry.php) by the [cms file](https://bitbucket.org/amadeusweb/amadeus/src/master/code/cms.php) of the site's [code folder](https://bitbucket.org/amadeusweb/amadeus/src/master/code/).<!--TODO - reword-->

It boasts a [90kb footprint](https://bitbucket.org/amadeusweb/amadeus/src/master/framework/), has a few [modules](https://bitbucket.org/amadeusweb/amadeus/src/master/modules/) and loads the html [in a blazing < .05 seconds, using on average < 1MB](https://bitbucket.org/amadeusweb/amadeus/src/master/framework/stats.php) as [seen here]([url]assets/docs/amadeusweb-statistics.jpg). It extensively uses the scandir and file_get_contents which [calls the disk](https://bitbucket.org/amadeusweb/amadeus/src/master/framework/files.php) as [seen here]([url]assets/docs/amadeusweb-statistics-added-disk-calls.jpg).

It uses [git technology](https://en.wikipedia.org/wiki/Git) to effectively manage content and code in a single online repository. Databases, though frowned upon, [GCP](https://en.wikipedia.org/wiki/Google_Cloud_Platform) may become the de facto Headless CMS for systems like <u>Sunlight</u>. A PWA was [proven in November 2021](https://bitbucket.org/yieldmore/archives/src/master/mobile-app/) to bring a mobile app feel.

</section><section>

## Next Steps

* [Don't PANIC - for the newbies](%url%for-newbies/)
* Look [under the hood](%url%architecture/under-the-hood/).
* [Get started](%url%how-to/setup-on-your-machine/) crafting on your machine.
* Build your [first website](%url%how-to/make-a-new-website/).
* See [the how tos](%url%how-to/).
* Look at the various [Features](%url%features/) available.
