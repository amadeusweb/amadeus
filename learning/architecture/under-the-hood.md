# Introduction

Amadeus uses a server(Apache or PHP standalone) and when localhost/amadues is visited in the browser, this server executes index.php which loads the framework(entry.php) and configures the website(cms.php). By default the framework looks for files in the configurable content folder. If the website requires to organize by sub-folder, functions.php will have to define did_render_page. 

## Basic Flow

1. sitepath/index.php is loaded(if Apache, .htaccess is used).
2. framework/entry.php loads all framework files which defines the various methods used by framework, site and content PHP files. 
3. code/cms.php bootstraps the framework with an array of site configurations. It then calls render() which includes the theme files and loads the content of the current page(node).
4. code/functions.php defines further variables such as sections. It also may 
   - define sections(sub-folder for content).
   - parse the current url(node) and search for a match, setting filename found.
   - call render_text_or_md if url is in sub-folder. Additionally, as in the case of YieldMore.org it supports multiple files of same name but with different extension for example pdfs, mp3s, jpgs etc.
   - reads the sitemap sheet to build the menu and set SEO information.
   - insert HTML above & below the page when home.php is different from inner pages. 
5. [theme]/header.php of the theme will be included before the page content and it begins the HTML output and is the first part of the theme.
   - title, SEO, icon, scripts & styles, head_hooks and analytics are all included here as part of the head tag.
   - logo & menu are also rendered here.
   - code/menu.php is optional and will be loaded if it exists. 
6. content/index.php may call [theme]/home.php for rich single page website home page. Amadeus prefer to drive content using data/home.tsv, a spreadsheet having rows for each section of the page.
7. [theme]/footer.php called after content.
   - ends the wrapper divs used by the theme.
   - code/footer-content.php is optional and may include footer menus, prominent logo and contact details.
   - copyright, theme credits and Amadeus credits are added.
   - non essesntial CSS and themes are included here.
   - ends the body tag.
8. render calls print_stats if not disabled. This however appears after the body tag.
