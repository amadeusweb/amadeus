## Backward Compatibility

Amadeus 3.x does not disrupt the entry point of v2 and will simply pass execution onto the v2 folder provided it has been downloaded on your machine.

In this way, it was possible to start a fresh rewrite of 3.0 with **zipped theme distribution** architecture making the bitbucet repository superlite.


## Steps to Migrate Site

1. Create a code folder and move (rename in git), the _xyz.php without the underscore. If using legacy theme, dont move _menu and _footer_content
2. Add a index.php file to the root with following code

```
<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once '../amadeus/framework/entry.php';
define('SITEPATH', __DIR__);
include_once 'code/cms.php';
?>
```

3. Change .htaccess from _cms.php to index.php (lines 6 and 8)
4. Now run the website, you will get "cannot redeclare" and other errors ->
> Remove include to framework from cms.php
> Change entry point from core.php to entry.php
> Change path to 'path' => SITEPATH,
> add 	'folder' => 'content/',
5. Replace cs_var with am_var in cms.php and functions.php
6. Remove _ga.php and add 'google-analytics' => '[ga4id]'

