# Introduction

It is expected for you to amend this document as you have doubts / clarify them with Imran.
This is an in-progress document which we expect each new developer to expand on.

We assume, you to have Amadeus running locally as per [the setup](../getting-started/#setup).

</section><section>

# Stage 1 - Hello World!

1. Clone amadeusweb/www using [this "site repositories" link](%builder-url%site-repositories/). We assume you know how to clone using tortoise git and what a relative path is.
2. In Bitbucket, create a repo for your new "site" and clone it to your relative path (businesses, symphony, people).
3. Henceforth, "www" will refer to the amadeusweb.com website (amadeusweb/www/).
4. Copy .htaccess from www to your site folder. This will instruct apache to intercept urls and redirect them to index.php in the site's root folder. The relative url will be sent as a querystring parameter called "node". 
5. Copy index.php from www to site folder. You may need to adjust the path to framework entry file. Remove the stand-alone related commented code lines.
6. Copy also site-cms.php to the site folder. In the copied file, change all the variables to suit your need.
> An alternative to site-cms.php is data/site.tsv which runFwk(site) will initiate. Example to be seen in site repositories(eg:wise-owls).
7. Ensure to change the last parameter namely url.
8. Visit the website in localhost (after making sure that setup is complete and apache is running).
9. Empty the sections array and the social array in the site config (site.tsv or site-cms.php).
10. Create a content folder with an index.md file which says "hello world!".


</section><section>

# Starting a Website From Scratch( pre v4.2 of 2023 - deprecated in favour of code flavours).

**NOTE: Do this only if the design is radically different from yieldmore.org**

* Add a folder matching the name of your domain (yoursite for instance) to localhost and prepare to copy over multiple files from the amadeus folder.
* Copy index.php file in the root folder and adjust the relative path of the entry.php (add a "../amadeus/" to it).
* Copy .htaccess also from the root
* Amadeus has the concept of "safeName", configured in cms.php which usually matches the site's folder name and is used for the logo / icon which files are expected. Therefore, if your safeName is "yoursite" then pls add icon-yoursite.png | logo-yoursite@2x.png | logo-yoursite.png.
* Now create a "code" folder and copy the cms.php from it and make an empty functions.php
* Open cms.php and change the **name** | **byline** | **safeName**, reset the **version** to 1 with the current date, include **contact** information, comment out **image-in-logo** for now and **styles** and **scripts** also update the **social** items
* It is good to remember the fact that the urls inside the app are either relative (in content) or absolute (in the menu). It is mixed because the home page is one level up from the inner urls (we always end with a trailing slash). Hence change the url variable to match the site folder name for local and your domain name for the other. Do note that ternary conditions are used and it's a double ternary when maintaining preview subdomains separately.
* Please also remove the section on "standalone.php". With this, the work on the code folder is done.
* Add a "content" folder to the site root with and index.md file and use the markdown syntax for it's content.
* Add other file names in kebab casing and keep watching as they get added to the menu automatically on refresh. In the root content folder, Amadeus will recognize (in it's renderFile method) .html, .md, .php and .txt, using wordpress' wpautop (auto paragraph) function for txt files unless # or <!--markdown--> are detected.
