## An Introduction

If this is too technical / you are not a developer, feel free to reach out to Imran for support / install over remote control.

### Why?

Just like for personal files, we have a "My Documents" folder created, we ask you to create an AmadeusWeb folder for:

* Setups (the installers we are about to download)
* Programs (like FTPSync which just run in-situ)
* xampp (the bundled local server mentioned below)
* "all" for all the websites we will "clone" from "bitbucket"

### Why File Based?

Being a "file based" cms / web builder means that we make use of "version control" technology like git / github.com / bitbucket.org to synchronize and backup YOUR website and the "builder" - builder.amadeusweb.com.

For now, just keep these words at the back of your mind until we have created a glossary.

### Unified folder structures

Once we have git installed and apache running locally (pointing to the **all** folder as it's document root), localhost will run the sites in all. This means that

D:\AmadeusWeb\all\amadeus will be served when https://localhost/amadeus/ is visited in the browser. The [Code Explorer](https://code.amadeusweb.com/domain-checker/) will show the "local" paths. For eg:
```
http://localhost/amadeusweb/www/	preview.amadeusweb.com
http://localhost/our-sites/imran/	imran.yieldmore.org
https://wiseowls.life/			wiseowls.life
```
When logged in to hostdime, those domains / subdomains will be pointed to say "all/our-sites/imran"

### Convention over Configuration

By matching the same folder structure on everyone's machine AND the common amadeusweb server - at hostdime.com) - we use the power of "convention" not configuring different folders on different developer machines differently.

</section><section>

# On A Laptop / Desktop

As tech evolved, we switched from 32 bit processors to 64. Check your machine and install the appropriate ones from the links provided.

We use several open source software for this, a trial bound BeyondCompare and a site upload tool for windows Imran wrote in 2007.

</section><section>

## Main Working Folder

> If there is no "D" Drive, you may have to go to [Disk Management and Shrink Volume](https://learn.microsoft.com/en-us/windows-server/storage/disk-management/shrink-a-basic-volume) to create a D:\ drive if it doesnt exist, or any drive other than C (which data will get lost in the event of an OS / hardware failure).

Create folder **D:\AmadeusWeb\all** (for reasons stated above).

</section><section>

## Site and Builder Repositories

[Git](https://en.wikipedia.org/wiki/Git) is a system for creating, backing up, and merging changes made to the files of a software application. Since we use files (no database in AmadeusWeb) everywhere - <abbr>content</abbr> (md html php txt), <abbr>assets</abbr> (jpg, png, pdf), and <abbr>sheets</abbr> (tsv) - the "<abbr>site</abbr>" <abbr>code</abbr> is a separate <abbr>repository</abbr> like the [ones seen here](https://code.amadeusweb.com/).

Git is a command line utility, more suited for developers who work in linux. In windows (and mac / linux) we use the graphic interface of "[TortoiseGit](https://en.wikipedia.org/wiki/TortoiseGit)". With these two, [website, code and framework can be managed](https://www.atlassian.com/git/tutorials/what-is-version-control).

**NOTE**: These are abstract concepts more easily understod if you "explored" this www "site" on [bitbucket](https://bitbucket.org/amadeusweb/www/src) - Each website, including the builder and the main amadeusweb.com are "<abbr>repositories</abbr>" <abbr>hosted</abbr> in <abbr>bitbucket</abbr>. History of changes can be seen in "<abbr>commits</abbr>" like [here](https://bitbucket.org/amadeusweb/www/commits/branch/main). Individual files - like this very one - [setup on yur machine](https://bitbucket.org/amadeusweb/www/history-node/main/learning/how-to/setup-on-your-machine.md?at=main) can be seen too.

#### Install **[git](https://git-scm.com/download/win) 

> Git requires to check-in and check-out **line endings as is**, so make sure to select this correctly in the Wizard. If you miss this, in git bash, you can run [git config -global core.autocrlf "false"](https://linuxhint.com/change-line-ending-settings-in-git/).

#### Install [tortoisegit](https://tortoisegit.org/download/)**

> TortoiseGit First Run Wizard **requires a email** - give team@yieldmore.org and your name.

Tortoise will have the Clone option in folder right click. Give it the clone path of [bitbucket/amadeusweb/amadeus/](https://bitbucket.org/amadeusweb/amadeus/) into "all" folder which will be the localhost root (setup in Apache in the next step).

</section><section>

## Installing a Web Server

Typically we buy hosting "space" - a server running linux, but in AmadeusWeb, for a [number of reasons](%url%reasons/), we develop locally and only push changes via Secure FTP. While a php standalone installed with Chocolatey is possible, [this method](#php-standalone) has now been deprecated.

The local server is [Apache](https://en.wikipedia.org/wiki/Apache_HTTP_Server).

* [Download xampp](https://www.apachefriends.org/) which is a bundle of Apache (server) and the [php programming language](https://en.wikipedia.org/wiki/PHP).
* Install to D:\AmadeusWeb\xampp\
* Open it's xampp-control.exe as an "administrator" (elevated permissions)
* Click Apache > Config and look for "DocumentRoot" and the "<Directory " line after it and change it from "D:/AmadeusWeb/xampp/htdocs" to "D:/AmadeusWeb/all"  - after this a start/restart of Apache will be needed in xampp control panel - then it will start running localhost from the "all" folder.
* Try running apache (it should show in green if ports 80 / 433 are available and its able to start running)
* Tick the checkbox at the beginning of the Apache row to install as a service. If a service, it will run automatically on each boot, else you will have to run xampp-control everytime and hit run.

</section><section>

## Downloading and running websites

See the code explorer and download whichever site you need. Use the domain checker tool / [data file](https://bitbucket.org/amadeusweb/code/src/master/data/all-domains.tsv) to know the paths it needs to be extacted to.

Remember all this is "Source Available" and PROPRIETARY - we we will not allow server deployments (to your own server) without joining through one of our programs / as a client / partner.

### Downloading via git

This is the only way. Else you will not be able to get updates.

* Go to the bitbucket repository - say https://bitbucket.org/amadeusweb/amadeus - whose clone path is the 'url' parameter relative to localhost/ - so "all" click the clone button and copy the url.
* Right click on the <abbr>relative path</abbr> (creating it if necessary) and click Git Clone (tortoise git button).
* In the window which comes up, just say OK. (very rarely does the git folder differ from the clone path. Follow the localhost "url variable" in case of any doubt.

NB: Remember to always do a git pull when starting working / you know a change has been made. This applies to your "site" repository AND the "builder" repository.

</section><section>

## Theme and Asset Download

Extract the **theme assets** (see below) that your site is using - for Amadeus, this is biz-land. To know what theme is being used, look in

* ./code/cms.php - pre code flavour way
* ./site-cms.php - called by the code flavour so if not set here, it will default to what is being set [in the flavour](https://bitbucket.org/amadeusweb/amadeus/src/master/flavours/yieldmore/cms.php#lines-20).
* in case using the ['site' framework](https://bitbucket.org/amadeusweb/amadeus/src/master/framework/site.php) file (reads config from ./data/site.tsv), else if not set, it defaults to 'biz-land'.
	> //this line means it cannot be set as an am_var, unless we introduce another ternary on this line 73.
	> 'theme' => isset($siteVars['theme']) ? $siteVars['theme'] : 'biz-land'

----

This assumes a theme has been created and zipped as [per this page](%url%how-to/create-a-theme/).

> 1. Save the [zip from the theme downloads page](https://builder.amadeusweb.com/themes/downloads/) into the theme folder. For instance, for Amadeus the theme is biz-land.
> 2. Extract "here" and get all the assets - css, js and default images.
> 3. Load / refresh the website and see that there are no missing assets.
> 4. Additionally, if planning to use decks, the [revealjs](https://builder.amadeusweb.com/assets/revealjs.zip) will need to be downloaded and extracted to the assets folder.

**NOTE:** If this step is not done properly for the theme the current website is using / trying to use you will get a lot of 404 not found errors and the website will appear broken / plain white.

**NOTE:** If it asks to replace files that already exist, say skip, else you can revert them in tortoisegit's check for modifications window.

</section><section>

## Making Your Own Website

We **recommend using the [YM code Flavour to jump start your development](%url%how-to/make-a-new-website/#code-flavours)**.

Create a bitbucket repository for your website / ask Imran to set it up for you - using his shared account so you dont need to create your own. If you are approaching us through a locality, group or institution, we recommend setting up a single shared account for yourselves.

Note that the **bitbucket app key** can be had from Imran for account **amadeuswebdev** / the new **amadeuswebwork** one.

Install a Code Editor like **[VSCode](https://code.visualstudio.com/download) on any platform** or [PSPad](https://pspad.com/en/) on Windows.

Our "learning" section should be able to give more details on [how to do things](%url%how-to/) / you need to reach out to someone in our team.

</section><section>

## Publishing Changes (over sFTP)

Before uploading to [a server](https://amadeusweb.com/cpanel/), you will need an **FTP Client like [FTPSync](https://archives.yieldmore.org/ftp-sync/)** or [Beyond Compare](https://www.scootersoftware.com/download.php).

</section><section>

# Setting Up on Mobile

The [following Apps](%url%assets/docs/mobile-development-environment.jpg) need to be installed:

* Git Client - **PocketGit** on [Android](#)
* Server in Mobile - **AWebServer** - [Android](#) - [screenshot](%url%assets/docs/mobile-web-server.jpg)
* Code Editor - **APhPEditor** - [Android](#)

In a folder called Magique in the Mobile (configured as the server's wwwroot), the Amadeus repository needs to be cloned.

Server should run on port 8080 (or any) - see port substitution code in cms.php

> 'url' => $local ? replace_vars('http://localhost%port%/amadeus/', 'port') : 'https://amadeusweb.com/',

**NOTE: for Mobile & Standalone Development**

* Php is detected when Magique is the containing folder.
* Url rewriting is only configued when running in apache (not the standalone php server we use in env setup). Hence, inner pages will have an index.php/ just after the url.
* A new method has been made available and can be seen in action as:
> &lt;a href="&lt;?php echo am_page_url('creatively-yours/');?&gt;"&gt;a course&lt;/a&gt;
* The menu and recursive menu already use this.
* In renderFile (md/txt) its &#37;page-url&#37;

</section><section>

# Old Command Line Setup
_Deprecated in 2024 in favour of xampp as too difficult_

_(Do Panic)_

</section><section>

## Install Chocolatey

[Chocolatey](https://chocolatey.org/), dubbed "The Package Manager for Windows" is a system for installing (with dependancies) packages like PHP which have different prerequisites depending on the version, service pack and hotfixes installed in windows. The PowerShell and choco install commands below to be run in [CMD Prompt with Administrator privileges](https://www.intowindows.com/command-prompt-as-administrator-in-windows-10/). REM indicates a batch file remark and is the source where said command is from.

</section><section>

## Install PHP

With administrator command prompt using the **[chocolatey](https://community.chocolatey.org/packages/php) instructions below** on windows or [on mac](https://www.php.net/manual/en/install.macosx.php).

```bat
rem https://docs.chocolatey.org/en-us/choco/setup

@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

rem https://community.chocolatey.org/packages/php

choco install php --package-parameters='"/ThreadSafe"'
```

</section><section>

## Running PHP Standalone Server

* Make a batch file called **run-web.bat** in the web folder with **"php -S localhost:80"**.
* Always make sure this is running while doing development, else localhost will not open.
* Make sure Skype or anything else is not using port 80 - or if unable to change that, you can do the [replace_vars in local url](https://bitbucket.org/yieldmore/yieldmore/src/master/data/site.tsv#lines-9), running it from a different port.
* Run the server (bat file) and open [localhost/amadeus/](http://localhost/amadeus/).
