# Zip file per Theme
1. Copy theme folder.
2. Add everything except home.php, header.php, footer.php to the zip file. 
3. move zip file to amadeus/themes/downloads & upload it to server using ftp sync.
4. copy install-theme.bat (from cv-modern-blog) and change file name. 
5. delete extrected contents (from zip file) and test batch file. 

# Converting Legacy Theme
 - replace cs_var with am_var
 - use new runCode('menu') which returns false to be used to call menu() or render the default footer content. 
 
# 1. Finding a Theme

Start with a HTML Template from the internet. Some good places to start:

 * https://bootstrapmade.com/
 * http://themeforest.com/

# 2. Save to themes folder
 - **Download the zip file** to the "amadeus/themes/" folder / move it there
 - **Extract the folder** to themes so that the subfolder contains "index.html"
   - Rename the folder to add hyphens before new words and remove capitals.
   - For eg: BizLand should become biz-land (we prefer lowercase for urls so all folder/file names should follow that rule.

# 3. header.php
 - Save the index.html as header.php
 - Use a code editor like PSPad to edit the php, html, css, js files

## a. Title and Meta
**Search For**
```
<title>
```
and replace the line with:
```
<title><?php title(); ?></title>
```

**Search for**
```
<meta content="" name="description">
<meta content="" name="keywords">
```
and replace with (a single)
```
<?php seo_tags(); ?>
```
## b. Icon (for browser tab)
and add this line just after that
```
	<link href="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-icon.png" rel="icon" />
```

## c. $theme
Search for

**"assets**

and replace with

**"<?php echo $theme; ?>assets**

* some themes use directly css/ and js/ folders and this may need to be treated separately

## d. site assets
Search for all the css/js files in the file. This is either in the head or just before </body>.
After it, add the line:
```
<?php styles_and_scripts(); ?>
<?php foot_hooks(); ?>
```
## e. head hooks
 Before the </head> tag ends, add the line
```
<?php head_hooks(); ?>
```

## f. Menu
In bootstrapmade.com, its usually within this tag
```
<nav class="nav-menu d-none d-lg-block">
```

Move it to menu.html and replace it with
	<?php if (!runCode('menu')) menu(); ?>

# 4. Specific Features

## head.php
Some themes have a right first page header or a splash image. This code can be controlled pagewise using
```
<?php runCode('head'); ?>
```

## LOGO / link to home
This should look something like this:
```
<a href="<?php echo am_var('url');?>"><img src="<?php echo am_var('url'); ?><?php echo am_var('safeName'); ?>-logo.png" alt="<?php echo am_var('name'); ?>" class="img-fluid"></a>
```

# 5. Chop into home.php and footer.php

## Detect home.php
Detect begin of content of the page
This is sometimes where 
```
</header>
```
ends

On bootstrapmade, this is the beginning of the hero section rest of content should go to home.php or content/index.php

## Detect Footer
This is sometimes where the </main> tag ends

Rest of file should go into footer.php

# 6. Footer

## Footer Content
this is usually a footer-top sort of div above which this line should be placed
```
<?php if (!runCode('footer-content')) { ?>
//[default theme content]
<?php } ?>
```
this will tell php to use the [optional] _footer-content.php from the website

## Copyright
```
<?php _copyright(); ?>
//this is same as 
&copy; Copyright <strong><span><?php echo am_var('name'); ?></span></strong>. <?php echo (am_var('start_year') ? am_var('start_year') . ' - ' : '') . date('Y'); ?> All Rights Reserved
```

## Credits
Add
```
<br /><?php _credits(); ?>
```
in the footer (usually below the theme credits

Or, the 2 can be clubbed as copyright_and_credits with either '<br />' or 'row' passed as separator

## Post Credits
if necessary, add
```
<?php if (am_var('footer_post_credits')) { echo renderFile(am_var('footer_post_credits')); } ?>
```
