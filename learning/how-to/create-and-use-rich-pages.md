# Prerequisites

Once the website is setup locally, do the following

* Look at [the new theme](%builder-url%themes/cv-yoga/) and understand its various elements.
* Compare it with [BizLand](%builder-url%themes/biz-land/) to know that rich page sections can vary.
* See ./amadeus/themes/cv-yoga/home.php / view source of the output to understand the html.
* See Rich Pages built in Amadeus that are [sheet driven](https://docs.google.com/spreadsheets/d/1q-SnfWPw0qyndp6WOULvTVTXbMxjBKyltW6l67taK4k/edit#gid=820206942).
* Start correlating the content with the sheet and take a note on what section to be done.

</section><section>

# Understanding the System

## Basic Syntax

### Load Sheet

This command will load the 'home' sheet from the data folder of the website
```
<?php $sheet = get_sheet('home', 'homeSection'); ?>
```

### Looping

This will let you loop through rows matching a given section. Once you start a flower brackets { and switch to html by typing ?>  then you need to make sure there is a closing bracket.

```
<?php foreach ($home->sections['hero'] as $item) {?>
//html that may use item_r
<?php } ?>
```

### Load Column

An item_r function is defined in functions.php and it handles any needed substitutions and strategies for the website. It will include | to newline character replacement as sheets do not support multiline content. For Example, to render the Heading, you may say

```
<h1><?php item_r('title', $item); ?></h1>
```

### Full Sample

```
<ul>
  <?php foreach ($home->sections['abouts'] as $item) {?>
  <li>
    <i class="<?php item_r_or('css', $item, 'bx bx-store-alt'); ?>"></i>
    <div>
      <h5><?php item_r('title', $item); ?></h5>
      <p><?php item_r('content', $item); ?></p>
    </div>
  </li>
  <?php } ?>
</ul>
```

</section><section>

# Wrapping Up

* Make a new sheet copying the column names and rows of the section column alone
* Correlate the html you are converting with what you are defining. Try to use the same names as the sections in html.
* Remember html will copy the same seeming block multiple times but in our case, we use a template and content will come from the sheet as multiple rows.
* Even a single item will need to use the foreach syntax and it is understood that whoever is loading the content will have a single row only. Example is the about section heading text / the hero section
