## Using Google Photos

1. Log into Google Photos on your computer.
2 .Click on the Album you want to embed. ...
3. Click on Get Link and then Copy.
4. Go to [publicalbum.org](https://www.publicalbum.org/blog/embedding-google-photos-albums).
5. Paste your Google Albums link into the box and press Create Embed Code.
6. Here you'll be able to change the way your album is displayed if you wish.
