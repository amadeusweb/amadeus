## Customizing Icons Supplied by a theme

Years ago we would use individual images or css image sprites for icons for buttons and in boxes. Today there are libraries with many many usable icons made from svgs and bundled into a single font.

They may provide cheatsheets like boxicons below or have an accompanying html file or require a css class explorer (a tool Imran has made in the past and will make accessible again).

* [boxicons cheatsheet](https://boxicons.com/cheatsheet)
* [icofont cheatshet](https://icofont.com/examples)

----

We recently wrote our own icon-explorer as a tech exercise.

If  these customizations are needed for a rich home page, will discuss how it can be done.
