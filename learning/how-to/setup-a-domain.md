## Pointing to Our Servers

If the Domain Name Server is to be ours, whereby the website is hosted on our server, then you will need to login to your Domain Admin and set the nameservers as

 - ns10565.dizinc.com
 - ns10566.dizinc.com

This is our hostdime.com where we have been customers of their shared hosting plan since May 2005.

## Setup Subdomain preview.[yourwebsite.com]
__(if our's is not your DNS server)__

1. go to your domain host
2. create an A record for the subdomain
3. give the IP as 198.136.61.225
