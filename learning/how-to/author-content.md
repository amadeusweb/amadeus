# Authoring with Amadeus

We use a simple text notation system called [markdown](https://www.markdownguide.org/getting-started/) which is easier to maintain that typical html. If you already have a docx file, you can [convert it here](https://gdoc2md.com/).

The fallback is to html which is supported in md/txt files. The amadeus / site typography pages (to be developed) will explain things with examples.

## Ideal Setup

Ideally you [setup the environment](./environment-setup/) on your laptop and upload changes from there and check them in to [bitbucket](https://code.amadeusweb.com/) like how all the other websites are built.

Bitbucket is a storage service (originally intended for code) that can backup our website content (which is managed as trackable files not a database) It helps synchronize across devices each of which has a full backup with history of changes. git technology also lets us merge and resolve conflicts in case 2 authors change the same page.

## Wysiwyg Offline

An admin / edit feature can be developed for changing on a machine but we would rather demonstrate how to create markdown files by understanding it's notation. Wysiwyg will never be done online as that would pose security risks - just the thing we wanted to avoid when we setup the "develop offline" strategy.

## Through a content coordinator

Imran / one of the developers can load in content for you but this makes them a bottleneck.

## Alternate Authoring Online

Bitbucket gives us an interface for adding/editing online. If you are mobile bound, you could author content directly here and give someone who has a laptop a heads up to do a git pull (updates repository changes to local) and then an ftp sync.

</section><section>

# Using PHP

More complex content can use all the php and amadeus builtin funcions in php, like an [authors site](https://imran.yieldmore.org/) or a [home page that is driven by a sheet](https://bitbucket.org/amadeusweb/amadeus/src/master/data/home.tsv) or a [complex, cached, delta providing sitemap](https://bitbucket.org/yieldmore/archives/src/master/_functions.php#lines-208) or a [simple one](https://bitbucket.org/amadeusweb/amadeus/src/master/features/code/sitemap.php) that uses the [recursive_menu](https://bitbucket.org/amadeusweb/amadeus/src/master/framework/entry.php#lines-103).

All 4 files types viz .md, .txt .html and .php are supported in advanced taxonomies by the [renderFile method](https://bitbucket.org/amadeusweb/amadeus/src/master/framework/render.php).
