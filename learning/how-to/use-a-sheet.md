# Learning by Example

1. **Sheet** - look at ./amadeus/content/showcase.php to see how amadeus.com/showcase/ or localhost/amadeus/showcase/ works and note the following
    * Sheet data comes from ./amadeus/data/showcase.tsv (copy pase this into excel / google to see it)
    * sheet rows are grouped by featured column value is 'Y' or '' - lines 22 and 23
    * a foreach is done on the items in showcase function defined on line 25
    * item_r is called wherever $item column needs to be extracted. this function is defined in ./amadeus/code/functions.php
2. **Menu** - 
