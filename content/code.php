<?php if (hasPageParameter('scanner')) { disk_include_once(SITEPATH . '/content/' . am_var('node') . '/scanner.php'); return; } ?>
<section>
<h2>Amadeus' Code Explorer</h2>

These repo details are loaded from bitbucket api <?php echo makeLink('<b>scanned</b>', am_var('url') . am_var('node') . '/scanner/', false, !am_var('local')); ?>  into a single repos.json.<br />
Now moved to the amadeus backend as
 a <a href="https://bitbucket.org/amadeusweb/amadeus/src/master/features/tables.php" target="_blank">tables module</a>
 that uses <a href="https://bitbucket.org/amadeusweb/amadeus/src/master/features/tables/tables-loader.js" target="_blank">datatables and jQuery</a>.

<hr />

<?php
table('code',
	SITEPATH . '/content/code/code-filters.md',
	SITEPATH . '/data/repos-sorted.json',
	['Order, Name, Description, Project, Account', 'website, link, icon, updated'],
'		<tr>
			<td>%order%</td>
			<td><a href="%link%" target="_blank"><img src="%icon%" height="60" /> %name%</a><hr style="margin: 2px;" />%updated%</td>
			<td>%description%<hr style="margin: 2px;" /><a href="%website%" target="_blank"><b>%website%</b></a></td>
			<td>%project%</td>
			<td>%account%</td>
		</tr>
');
?>
</section>
