<?php
$base = dirname(SITEPATH) . '/';
$scriptName = 'amadeus-workspace.php';
$gitExists = !am_var('local') ? 'skip' : disk_file_exists($base . $scriptName);
$notice = $gitExists !== false ? '' : '<span class="notice">To update sites from this tool,'
	. 'run "all/amadeus/scripts/setup-amadeus-workspace.bat"</span>' . am_var('2nl'); //TODO: better explanation?

$thisUrl = getFeatureUrl('panels/');

$siteGroup = isset($_GET['group']) ? $_GET['group'] : 'home';
$wantsMobile = (isset($_GET['mobile']) || isMobile()) && !isset($_GET['desktop']);

renderGrid([
	[	'id' => 'menu', 'class' => 'overflow-auto linkable', 'startAt' => 'md-9',
		'heading' => togglingH2('Website Explorer'),
		'notice' => $notice,
		'groups' => _groups($siteGroup),
 		'html overflow-auto' => _topHtml($siteGroup, $gitExists, $scriptName),
	],
	[	'id' => 'sitemap', 'class' => 'overflow-at-starting-height', 'startAt' => 'md-3',
		'heading' => togglingH2('Sitemap Browser'),
		'content' => '<p>Click on sitemap for one of the sites to the left and see the magic!</p>',
	],
	[	'id' => 'content', 'class' => 'auto-adjust-height', 'startAt' => 'md-12',
		'heading' => togglingH2('Welcome'),
		'content' => '<p>Explore one of the <a>websites</a> from the list above!</p>',
	],
]);

function _groups($siteGroup) {
	$groups = [];
	$baseUrl = am_var('url') . am_var('node') . '/?group=';
	foreach(skipExcludedFiles(disk_scandir(SITEPATH . '/data/websites/'), [], true) as $item) {
		$class = $item == $siteGroup ? ' class="selected"' : '';
		$groups[] = getLink(humanize($item), $baseUrl . $item, $class);
	}

	return '<div id="groups">Groups: ' . implode(am_var('nl'), $groups) . '</div>' . am_var('nl');
}

function _topHtml($siteGroup, $gitExists, $scriptName) {
	$base = dirname(SITEPATH) . '/';

	$sheet = get_sheet(AMADEUSPATH . '/data/websites/' . $siteGroup . '.tsv', false);
	$cols = $sheet->columns;

	$r = '<table id="' . am_var('node') . '-panels" class="amadeus-table" border="1"><thead><tr><th>Group</th><th>Server</th><th>Local</th><th>Here</th>'
		. ($gitExists ? '<th>Git Actions</th>' : '') . '<th>Comments</th></tr></thead>' . am_var('nl') . '<tbody>';

	$embedSitemap = '/sitemap/embed/?open-in=content';

	$group = '';
	$events = ' class="open-in" data-panel="%where%"';
	$rightEvents = replaceItems($events, ['where' => 'sitemap'], '%');
	$bottomEvents = replaceItems($events, ['where' => 'content'], '%');

	foreach ($sheet->rows as $item) {
		$cellOne = $item[$cols['SafeName']];

		if ($cellOne == '') { $r .= '<hr />' . am_var('2nl'); continue; }
		if (startsWith($cellOne, '~ ')) { $group = substr($cellOne, 1); continue; }

		$domain = $item[$cols['Domain']];
		$folder = $item[$cols['DocumentRoot']];

		$site = getLink($domain, 'https://' . $domain . '/', $bottomEvents);
		$site .= getLink('sitemap', 'https://' . $domain . $embedSitemap, $rightEvents);

		$local = getLink('localhost', 'http://localhost/' . $folder . '/', $bottomEvents);
		$local .= getLink('sitemap', 'http://localhost/' . $folder . $embedSitemap, $rightEvents);

		$exists = disk_is_dir($fol = $base . $folder) ? 'YES' : '<b style="color: red;" title="expecting: ' . $fol . '">NOPE</b>';
		$actions = '';
		if ($gitExists === true && $exists == 'YES') {
			$folderSafe = _makeSlashesConsistent($folder); //should work on linux/mac? 
			$actions .= getLink('pull', '/' . $scriptName . '?git-action=pull&site=' . $folderSafe, $bottomEvents);
			$actions .= getLink('recent', '/' . $scriptName . '?git-action=log&site=' . $folderSafe, $bottomEvents);
		}

		$row = [$group, $site, $local, $exists]; if ($gitExists) $row[] = $actions; $row[] = $item[$cols['Comments']];
		$cells = implode('</td>' . am_var('nl') . '		<td>', $row);
		$r .= am_var('nl') . '	<tr>' . am_var('nl') . '		<td>' . $cells . '</td>' . am_var('nl') . '	</tr>' . am_var('nl');
	}

	$r .= '</tbody></table>';

	return $r;
}
