## Import codebase lists from Bitbucket

We've evolved a unique [code explorer](%url%code/) that lists all 40+ Amadeus Web Builder repositories on top of another 30 websites built by Imran since 2013.

This "scanner" can only be run locally and involves the following bitbucket export as json links being saved.

----

[AmadeusWeb](https://api.bitbucket.org/2.0/repositories/amadeusweb/?pagelen=100)
/ [YieldMore](https://api.bitbucket.org/2.0/repositories/yieldmore/?pagelen=100)
/ [AmadeusWeb Team](https://api.bitbucket.org/2.0/repositories/amadeuswebwork/?pagelen=100)
/ [Aurrra Team](https://api.bitbucket.org/2.0/repositories/aurrrah/?pagelen=100)

And these past ones:

[awakentolife](https://api.bitbucket.org/2.0/repositories/awakentolife/?pagelen=100)
/ [IANamazi](https://api.bitbucket.org/2.0/repositories/ianamazi/?pagelen=100)
/ [cselian](https://api.bitbucket.org/2.0/repositories/cselian/?pagelen=100)
/ [panishq](https://api.bitbucket.org/2.0/repositories/panishq/?pagelen=100)
/ [buildindiagroup](https://api.bitbucket.org/2.0/repositories/buildindiagroup/?pagelen=100)

----

After each one is saved to **./data/repos/** and formatted in VSCode you may run the scan below.
