<?php
section();
renderMarkdown(SITEPATH . '/content/' . am_var('node') . '/scanner.md');
section('end');

$scanning = am_var('page_parameter2') == 'scan-now';
sectionId('scanner', 'text-center');
h2('Code Scanner', 'amadeus-icon');

if (!am_var('local')) {
	echo '<div class="flash flash-yellow">This can only be run on a developer machine</div>';
	section('end'); return;
} else {
	if (!$scanning) {
		echo getLink('Scan Now', am_var('url') . am_var('node') . '/scanner/scan-now/', ' class="btn btn-primary"');
		section('end'); return;
	}
}

$sources = disk_scandir($fol = SITEPATH . '/data/repos/');
$all = [];

echo '<h1>Scanning: ' . $fol . '</h1>';
echo getLink('Back to Code Explorer', am_var('url') . am_var('node') . '/', ' class="btn btn-primary"');
$verbose = isset($_GET['verbose']);

foreach ($sources as $name) {
	if ($name[0] == '.') continue;
	$file = $fol . $name;
	$name = str_replace('.json', '', $name);

	echo '<hr />';
	echo '<h2>' . $name . '</h2>' . am_var('nl');
	$json = json_to_array($file);

	if ($verbose) {
		echo '	<h4>Source</h4>' . am_var('nl');
		echo '	<textarea style="width: 100%; height: 150px;">' . print_r($json, 1) . '</textarea>';
	}

	$op = [];
	foreach ($json['values'] as $ix => $item) {
		$op[] = [
			'order' => 'auto',
			'account' => $name,
			'name' => $item['name'],
			'project' => $item['project']['name'],
			'link' => $item['links']['html']['href'],
			'icon' => $item['links']['avatar']['href'],
			'description' => $item['description'],
			'updated' => $item['updated_on'],
			'website' => $item['website'],
		];
	}
	echo '	<b>' . count($op) . ' items found!</b>' . am_var('brnl');
	if ($verbose) {
		echo '	<b>Output</b>' . am_var('brnl');
		echo '	<textarea style="width: 100%; height: 150px;">' . print_r($op, 1) . '</textarea>';
	}
	$all = array_merge($all, $op);
}

$sortedInput = $all;
$all = json_encode($all, JSON_PRETTY_PRINT);
file_put_contents($fol . '.scans/repos-' . time() . '.json', $all);
file_put_contents(SITEPATH . '/data/repos.json', $all);

$items = json_to_array('repos');
$sorted = [];
foreach ($sortedInput as $row) {
	$date = $row['updated'];
	$sorted[$date] = $row;
}
krsort($sorted);
$ix = 0;
foreach ($sorted as $key => $row) {
	$sorted['order'] = $ix++;
	$sorted[$key]['order'] = $ix;
	$sorted[$key]['updated'] = substr($row['updated'], 2, 8);
}
sort($sorted);
$sortedJson = json_encode($sorted, JSON_PRETTY_PRINT);
file_put_contents(SITEPATH . '/data/repos-sorted.json', $sortedJson);
