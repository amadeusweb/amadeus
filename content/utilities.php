<?php
$sections = [
	'smart-site-uploader' => ['suffix' => ' (Begun in 2007 as FTPSync)'],
];

$fol = 'content/' . am_var('node') . '/';
foreach ($sections as $name => $vars) {
	section();
	$suffix = '';

	if (is_string($vars)) {
		$name = $vars;
	} else {
		if (isset($vars['suffix'])) $suffix = $vars['suffix'];
	}

	h2(humanize($name) . $suffix);

	echo replaceItems('<img class="img-fluid"'
		. ' src="%url%%fol%%name%.jpg" alt="%alt%" />' . am_var('nl'),
			['url' => am_var('url'), 'fol' => $fol, 'name' => $name, 'alt' => humanize($name)], '%');

	renderAny(SITEPATH . '/' . $fol . $name . '.md');
	section('end');
}
