<section>
<h2>Icon Explorer</h2>
Here, we show all icons available from font libraries available in the theme.<br /><br />

Start by clicking the theme you are developing with!<br /><br />
<style type="text/css">
#icons div { text-align: center; padding-bottom: 8px; margin-bottom: 15px; }
#icons div span { text-wrap: pretty; overflow-wrap: break-word; }
label { width: 70px; margin-bottom: 6px; }
</style>
</section><section>
<h2>Pick Theme => Library</h2>
<?php
$libs = [
	'icofont' => ['theme' => 'biz-land', 'css' => '/assets/vendor/icofont/icofont.min.css', 'startsWith' => 'icofont-', 'magnify' => 'icofont-2x'], //https://icofont.com/
	'boxicons' => ['theme' => 'biz-land', 'css' => '/assets/vendor/boxicons/css/boxicons.min.css', 'startsWith' => 'bxl-', 'magnify' => 'bx icon-box'],
	'font-icons' => ['theme' => 'cv-blog', 'css' => 'css/font-icons.css', 'startsWith' => 'icon-', 'magnify' => 'icon-lg'],
];

$key = isset($_GET['lib']) ? $_GET['lib'] : array_keys($libs)[0];
$lib = $libs[$key];

foreach ($libs as $k => $v) {
	$text = humanize($k . ' &mdash; ' . $v['theme']);
	$url = './?lib=' . $k;
	$sel = $k == $key;
	echo '<a href="' . $url . '">' . $text . '</a><br />';
}

section('end'); section();
echo '<h2>Icon Search By Name</h2>' . am_var('2nl');

echo '<link id="current" rel="stylesheet" href="' . concatSlugs([am_var('app'), 'themes/', $lib['theme'], '/', $lib['css']]) .'" type="text/css" />';
echo '<label for="magnify">Magnify:</label> <input id="magnify" value="' . $lib['magnify'] . '" /><br />';
echo '<label for="prefix">Prefix:</label> <input id="prefix" value="' . $lib['startsWith'] . '" /><br />';
echo '<label for="search">Search:</label> <input id="search" /><br />';
echo '<label for="counts">Counts:</label> <input id="counts" /><br />';

section('end'); section();
echo '<div id="icons" class="row">';
echo '</div>';
?>

<script type="text/javascript">
window.onload = function() {
	initializeExplorer();
	//$('#prefix').on('change', initializeExplorer);
	$('#search').on('change', filterItems);
}

window.pageVars = {
	'itemCount': 0,
	'itemsShown': 0,
}

function updateCounts() {
	$('#counts').val(window.pageVars.itemsShown + ' of ' + window.pageVars.itemCount);
}

function initializeExplorer() {
	const div = $('#icons');
	const prefix = $('#prefix').val();
	const magnify = $('#magnify').val();
	const items = getAllSelectors('.' + prefix);
	
	window.pageVars.itemsShown = window.pageVars.itemCount = items.length;
	updateCounts();
	
	items.forEach(function (item) {
		$('<div class="col-md-3 col-sm-6" />')
			.append('<span class="' + magnify + ' '
				+ item + '"><br />'
				+ item.replaceAll(prefix, '').replaceAll('-', ' ')
				+ '</span>').appendTo(div);
	});
};

function filterItems() {
	const searchVal = $('#search').val();
	let shown = 0;

	$('#icons span').each(function() {
		const span = $(this);
		const div = span.closest('div');

		if (searchVal == '' || span.text().includes(searchVal)) {
			shown += 1;
			div.show();
		} else {
			div.hide();
		} 
	});

	window.pageVars.itemsShown = shown;
	updateCounts();
}

function getAllSelectors(prefix) {
	const ret = [];

	for(var i = 0; i < document.styleSheets.length; i++) {
		const sheet = document.styleSheets[i];
		if (sheet.ownerNode && sheet.ownerNode.id != 'current') continue;

		const rules = sheet.rules || sheet.cssRules;

		for(var x in rules) {
			const txt = rules[x].selectorText;
			if(typeof txt == 'string'){
				if (txt.startsWith(prefix))
					ret.push(txt.replaceAll('.', '').replaceAll('::before', '').replaceAll('::after', ''));
			} 
		}
	}

	return ret;
}

</script>

