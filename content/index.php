<section>
<h2>Brief Intro</h2>

<p>This is a powerful framework with which <a href="<?php echo am_var('url');?>code/">these sites</a> have been built.</p>

<p>Please refer to <a href="<?php echo am_var('main');?>" class="btn btn-primary">amadeusweb.com</a> for more details.</p>

<p>Or see it's <a href="https://bitbucket.org/amadeusweb/amadeus" class="btn btn-primary" target="_blank">proprietary code here</a>(yearlt licensed / source available).</p>
</section>

<?php includeFeature('sitemap'); ?>
