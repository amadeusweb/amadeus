<hr />

## Features

* Watches a folder and queues files added / changed for upload to FTP.
* It has become a MUST for any AmadeusWeb Developer, with its "diff only" upload.
* Features: drag-drop from windows explorer / tortoise git, add files by path.
* Queue automatically on pull (if the other developer doesnt use this / needs review).
* Pause watching, have a list of projects and encrypt passwords.
* Roadmap: exclusion list, open containing folder
* Effort: Init - 2007 - 1 day, 2012-13 - 2 days, 2025 - 1 day.
* Source code: [amadeusweb/utilities/](https://bitbucket.org/amadeusweb/utilities/)
* Formerly available at [YM Archives](https://archives.yieldmore.org/ftp-sync/)
