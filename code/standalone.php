<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Amadeus Web - Standalone Version</title>
	<link href="./amadeus-icon.png" rel="icon" />
<style type="text/css">
body { text-align: center; background-color: #28A6DB; }
#main { max-width: 600px; text-align: left; display: inline-block; background-color: #99CC99; padding: 60px; text-align: center;
 border-radius: 40px; line-height: 36px; font-size: x-large; margin-top: 50px; }
</style>
  </head>
  <body>
  <div id="main">
    This is a standalone of <a href="https://amadeusweb.com/" target="_blank">Amadeus Web</a>.<br><br>
    <img src="./amadeus-logo.png?fver=2" style="width: 50%; max-width: 80%;" /><br><br>
    You may <a href="mailto:imran@amadeusweb.com?subject=interest in amadeus">contact us</a> to know more.
  </div>
  </body>
</html>
