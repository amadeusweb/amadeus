<?php
am_var('local', $local = startsWith($_SERVER['HTTP_HOST'], 'localhost'));

bootstrap([
	'name' => 'Amadeus Web Builder',
	'byline' => 'Powering the new web',
	'footer-message' => 'Supporting Your Life\'s Missions With Enriching Technology',
	'safeName' => 'amadeus',
	'siteMenuName' => 'Amadeus\' Tools',

	'version' => [ 'id' => '6.5.2.4', 'date' => '11 Jan 2025' ],

	//TODO: let's scaffold links & add tests/registry here
	'sections' => [ ],

	'folder' => 'content/',
	'sections' => ['design', 'learning'],
	'sections-have-files' => true,

	'siteHumanizeReplaces' => [
		'awe' => 'AWE++',
		'3p' => '3rd Parties'
	],

	'use-menu-files' => true,

	'start_year' => '2019',
	'theme' => 'biz-land',
	'image-in-logo' => '-logo@2x.png',

	'ChatraID' => '--use-amadeusweb',
	'google-analytics' => '--use-amadeusweb',

	'uses' => 'custom-image-background, always-topbar',
	'banner' => 'featured-services',
	'no-assistant' => true,

	'email' => 'imran@amadeusweb.com',
	'phone' => '+919841223313',
	'address' => 'Chennai, India',

	'social' => [
		[ 'type' => 'linkedin', 'link' => 'https://www.linkedin.com/company/amadeusweb/', 'name' => 'LI' ],
		[ 'type' => 'youtube', 'link' => 'https://www.youtube.com/@amadeuswebbuilder', 'name' => 'YT' ],
		[ 'type' => 'github', 'link' => 'https://bitbucket.org/amadeusweb/amadeus', 'name' => 'Git' ],
	],

	'url' => $local ? replace_vars('http://localhost%port%/amadeus/', 'port') : 'https://builder.amadeusweb.com/',
	'path' => SITEPATH,
]);

if (hasPageParameter('embed')) am_var('embed', true); //needed for website-tests (frameset loader)
am_var('logoUrl', am_var('main')); //TODO: @<team> logo - later!

/***
 * Self hosted site? (but the platform is proprietary!)
 * TODO: tweak UI? run from default theme? how to offer / enforce?
 * DEFINE('AMADEUSURL', am_var('url'));

if (DEFINED('AMADEUSURL')) {
	include SITEPATH . '/code/standalone.php';
	die();
}
*/
